#!/bin/bash

#Create textual graph of module import dependencies.
#	I found this helpful to inspect the set
#	of package imports for trouble spots.

echo "digraph BeamSpotUtils {"
echo "//X -> Y means X imports Y"
echo "\"Beware Upward Going Arrows!\" [fontcolor=red]"

find ../ -iname "*.py" | xargs grep "^import" | \
    sed -r "s/.*\/(\w+)\..*import\s+(BeamSpotUtils\.)?(\w+).*/\1 -> \3/g" | \
    sed -r "/\W(sys|os|re|time|csv|logging|copy|getopt|traceback|ROOT|subprocess)($|\W)/d" | \
    sed -r "/\W(signal|stat|shutil|math|numpy|urllib|shlex|HTMLParser|cPickle|pickle)($|\W)/d" | \
    sed -r "s/^/    /g"

echo "}"

