#!/bin/bash
#
# $Id$
#

#Try moving a beamspot snapshot through different

# You can change this, but it makes shit really really verbose 
BST_DEBUG_LEVEL=0

## Which tests to run...
doFile=false
doCOOL=false

#JUST FOR PRETTY PRINTING
#stolen from http://techknack.net/bash-rpad-lpad-and-center-pad/
function rpad {
    if [ "$1" ]; then
        word="$1";
    else
        word="";
    fi;

    if [ "$2" ]; then
        len=$2;
    else
        len=${#word};
    fi;

    if [ "$3" ]; then
        padding="$3";
    else
        padding=" ";
    fi;

    while [ ${#word} -lt $len ]; do
        word="$word$padding";
    done;
    echo "$word";
}

app=../scripts/beamSpotConverterApp.py
name=testFile

if $doFile ;
then
    
    #Files must exist before translation
    # its a bad feature =\
    for ext in csv pickle sql ;
    do
        rm -rf $name.$ext
        touch  $name.$ext
    done
    rm -rf $name.new.csv
    touch  $name.new.csv

    echo
    echo "onmon->csv"
    echo
    python $app -v2 --repInput onmon --repOutput $name.csv --run 191514
    
    echo
    echo "csv->sql"
    echo
    python $app -v2 --repInput $name.csv --repOutput $name.sql --run 191514
    
    echo
    echo "sql->pickle"
    echo
    python $app -v2 --repInput $name.sql --repOutput $name.pickle --run 191514
    
    echo
    echo "pickle->csv"
    echo
    python $app -v2 --repInput $name.pickle --repOutput $name.new.csv --run 191514
    
    diff $name.csv $name.new.csv > theDiff.txt
    echo '~~~~~~~~~~~~~~~~~~~~~~~~~~'
    echo There are $(wc -l theDiff.txt | cut -d " " -f 1) lines in diff between the old and new csv
    echo '~~~~~~~~~~~~~~~~~~~~~~~~~~'
    rm -f theDiff.txt $name.new.csv
    touch $name.new.csv

# same thing but in reverse now..

    echo
    echo "csv->pickle"
    echo
    python $app -v2 --repInput $name.csv --repOutput $name.pickle --run 191514

    echo
    echo "pickle->sql"
    echo
    python $app -v2 --repInput $name.pickle --repOutput $name.sql --run 191514

    echo
    echo "sql->csv"
    echo
    python $app -v2 --repInput $name.sql --repOutput $name.new.csv --run 191514

    diff $name.csv $name.new.csv > theDiff.txt
    echo '~~~~~~~~~~~~~~~~~~~~~~~~~~'
    echo There are $(wc -l theDiff.txt | cut -d " " -f 1) lines in diff between the old and new csv
    echo '~~~~~~~~~~~~~~~~~~~~~~~~~~'
    rm theDiff.txt $name.new.csv
fi # end of filetranslator tests


if $doCOOL ;
then
    echo
    echo "Checking ability to read/write various COOL formats"
    echo "    Reading from onbunch is slow (lots of data)"
    echo
    
    for where in onbunch onmon onhlt ;
    do
        file=$name.$where.csv
        rm -rf $file 
        touch  $file

        echo "Reading from $where and dump to $file"
        python $app -v4 --repInput $where --repOutput $file --run 182787

    done

    echo "Writing to onbunch"
    python $app -v4 --repInput thelast.bcid.csv --repOutput onbunch
    echo "Writing to onmon"
    python $app -v4 --repInput thelast.csv --repOutput onmon

fi # end of cool tests

#### SLOW only needs to be run once for file thelast.csv
#####python $app -v4 --repInput onhlt --repOutput thelast.csv  -n 1
