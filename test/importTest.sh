#!/bin/bash
#
# $Id$
#

#Try to import all our packages and resolve namespaces
#  Some of "Rainer"'s scripts don't use an if __name__ protection
#  I'll solve this after we merge back into the trunk.

#For The "mains" in scripts/
#   goto scripts and try "import filename"
#For the library package
#   goto / and try "import BeamSpotUtils.filename"


# You can change this, but it makes shit really really verbose 
BST_DEBUG_LEVEL=0


#JUST FOR PRETTY PRINTING
#stolen from http://techknack.net/bash-rpad-lpad-and-center-pad/
function rpad {
    if [ "$1" ]; then
        word="$1";
    else
        word="";
    fi;

    if [ "$2" ]; then
        len=$2;
    else
        len=${#word};
    fi;

    if [ "$3" ]; then
        padding="$3";
    else
        padding=" ";
    fi;

    while [ ${#word} -lt $len ]; do
        word="$word$padding";
    done;
    echo "$word";
}


for d in ../python/BeamSpotUtils ../scripts ;
do
    importList=`ls $d/{Hist,beamSpot}*.py 2> /dev/null | \
                sed -r "s:^.*\/(.*)\.py:\1:g"` 
    if [ "$d" = "../scripts" ];
    then
        pushd $d  > /dev/null
    else
        pushd ../ > /dev/null
        importList=`echo "$importList" | sed -r "s:(\w+):BeamSpotUtils.\1:g"`
    fi
    
    echo "Working from `pwd`"
    echo "Will try to import these packages:"
    echo $importList | sed -r "s:(^|\s+):\n    :g"
    echo

    for f in $importList;
    do
        python -c "import $f" 2> /dev/null 
        ret=$?
        
        if [ "$ret" -eq 0 ]
        then
            echo `rpad "Imported $f" 50 .` successfully
        else
            echo `rpad "Imported $f" 50 .` incorrectly!!!!!!
        fi
    done
    
    popd > /dev/null
done

