#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.cmdline module
"""

from __future__ import annotations

import logging
import unittest
from unittest.mock import patch

from BeamSpotUtils.r4p import Ready4Physics
import ispy


class Ready4PhysicsMock:

    def __init__(self, value: bool, do_raise: bool = False):
        self.ready4physics = value
        self.do_raise = do_raise

    def checkout(self) -> None:
        if self.do_raise:
            # I do not know exact exception type raised by ispy
            # but r4p catches all of them
            raise ValueError("mock")

    def __call__(self, part: str, var: str) -> Ready4PhysicsMock:
        return self


class TestR4P(unittest.TestCase):
    """Test case for BeamSpotUtils.r4p module.
    """

    def setUp(self) -> None:
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger("rootil")

    def tearDown(self) -> None:
        pass

    def test_no_r4p(self) -> None:

        r4p = Ready4Physics(False, "", "")
        self.assertTrue(r4p())

    def test_missing(self) -> None:

        with patch.object(ispy, "ISObject", Ready4PhysicsMock(False, True)):
            r4p = Ready4Physics(True, "", "")
            r4p()

    def test_true(self) -> None:

        with patch.object(ispy, "ISObject", Ready4PhysicsMock(True)):
            r4p = Ready4Physics(True, "", "")
            self.assertTrue(r4p())

    def test_false(self) -> None:

        with patch.object(ispy, "ISObject", Ready4PhysicsMock(False)):
            r4p = Ready4Physics(True, "", "")
            self.assertFalse(r4p())


if __name__ == "__main__":
    unittest.main()
