#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.differ module
"""

from __future__ import annotations

import logging
import math
import unittest

from BeamSpotUtils.freaking_ROOT import ROOT

from BeamSpotUtils.bscalc_vtx import BSCalcVertex, BSCalcVertexConfig
from BeamSpotUtils.lbn_merge import LBNMerge


class TestBSCalcVertex(unittest.TestCase):
    """Test case for BeamSpotUtils.bscalc_vtx module.
    """

    def setUp(self) -> None:
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger("bscalc_vtx")

    def tearDown(self) -> None:
        pass

    def test_calcCorr(self) -> None:

        print("test _calcCorr on preset values")
        benchmark = [0.29955, 0.0137166, 0]
        config = BSCalcVertexConfig(savePlots=False)
        lbn_merge = LBNMerge(0, 1000, -1)
        bscalc = BSCalcVertex(config=config, output_dir="", lbn_merge=lbn_merge)
        r = bscalc._calcCorr(5.25051e-01, 6.39499e-03, 4.31217e-01, 5.49184e-03)

        self.assertFalse(math.fabs(r[0] - benchmark[0]) > 0.0001 or
                         math.fabs(r[1] - benchmark[1]) > 0.0001 or
                         math.fabs(r[2] - benchmark[2]) > 0.0001,
                         "_calcCorr test, expected: {} got: {}".format(benchmark, r))

    def test_slices(self) -> None:

        p = ROOT.TH2F("SomeName", "SomeTitle", 10, 0.5, 10.5, 1000, -5, 5)
        r = ROOT.TRandom2()

        for bin in range(1, p.GetNbinsX() + 1):
            sigma = bin / 10.0
            for i in range(bin * 10000):
                p.Fill(bin, r.Gaus(0, sigma))

        config = BSCalcVertexConfig(savePlots=False)
        lbn_merge = LBNMerge(0, 1000, -1)
        bscalc = BSCalcVertex(config=config, output_dir="", lbn_merge=lbn_merge)
        h = bscalc._fitSlices(p, code="ntrks", axis="X", projAxis=0, firstBin=0)[1]
        assert h is not None

        delta = 0
        rms = 0
        for bin in range(1, p.GetNbinsX() + 1):
            val = h.GetBinContent(h.FindBin(bin))
            sigma = bin / 10.0
            delta += val - sigma
            rms += delta * delta

        delta = delta / p.GetNbinsX()
        rms = math.sqrt(rms) / p.GetNbinsX()

        self.assertFalse(math.fabs(delta) > 0.01 or rms > 0.01,
                         "Found a large deviation: average delta = {} RMS = {}".format(delta, rms))

#     def test_Hist(self):
#
#         print("test _plotCorrVTrks on dummy histograms. josh broke this by calling _fitslices outside of "
#               "plotCorrVTrks. He says its okay because now all testing of cvt is done at the bst.py "
#               "level anyway")
#
#         bs_width = 0.3
#
#         p = TH2F("p", "p", 10, 0, 10, 1000, -5, 5)
#         s = TH2F("s", "s", 20, 0, 10, 500, -1.25, 1.25)
#         r = TRandom2()
#
#         for bin in range(1, p.GetNbinsX()):
#
#             s_width = 0.2 - (0.02 * bin)
#             p_width = math.sqrt (math.pow(s_width, 2) + math.pow(bs_width, 2))
#             for i in range(1, 10000):
#                 p.Fill(bin, r.Gaus(0, p_width))
#                 s.Fill(bin, r.Gaus(0, s_width))
#
#         c = BSCalcVertex("")._plotCorrVTrks(p, s)
#
#         c.Fit("pol0", "Q0")
#
#         if (math.fabs(c.GetFunction("pol0").GetParameter(0) - bs_width) / 0.3 > 0.02):
#             print("Failed _testHist")
#             return False
#
#         print("Passed _testHist")
#         return True

    def test_getSliceTitle(self) -> None:

        names = [
            ("ntrks", "X", 10., "Vertex X for nTrks = 10.0"),
            ("split_ntrks", "Y", 12., "Split Vertex Y for nTrks = 12.0"),
            ("bcid", "Z", 555., "BCID Vertex Z = 555.0"),
        ]

        for code, axis, sliceValue, title in names:
            self.assertEqual(BSCalcVertex._getSliceTitle(code, axis, sliceValue), title)


if __name__ == "__main__":
    unittest.main()
