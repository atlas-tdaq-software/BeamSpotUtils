#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.rootil module
"""

from __future__ import annotations

import logging
import unittest
from collections.abc import Iterable

from BeamSpotUtils.freaking_ROOT import ROOT

from BeamSpotUtils import rootil


class TestRootil(unittest.TestCase):
    """Test case for BeamSpotUtils.rootil module.
    """

    def setUp(self) -> None:
        logging.basicConfig(level=logging.DEBUG)

    def tearDown(self) -> None:
        pass

    def test_fillGauss(self) -> None:
        """Test for fillGauss() method"""

        hist = ROOT.TH1D("hist", "hist", 10, 0.5, 10.5)
        hist1 = rootil.fillGauss(hist, 5., 2., 12.)
        self.assertIs(hist, hist1, "Different object returned from fillGauss")

        for bin in range(1, 11):
            binval = hist.GetBinContent(bin)
            expected = ROOT.TMath.Gaus(bin, 5., 2.) * 12.
            self.assertAlmostEqual(binval, expected, 5, "bin value is different")

    def test_projectNTrkRange(self) -> None:
        """test for projectNTrkRange() method"""

        def _binWeight(binx: int, biny: int) -> int:
            return binx + biny * 2

        def _testBinContents(proj: ROOT.TH1, axis: str, bins: Iterable[int]) -> None:
            for bin in range(1, proj.GetNbinsX() + 1):
                if axis == 'Y':
                    expect = sum(_binWeight(bin, biny) for biny in bins)
                else:
                    expect = sum(_binWeight(binx, bin) for binx in bins)
                self.assertEqual(proj.GetBinContent(bin), expect)

        # make 2-d histo
        h2 = ROOT.TH2I("h2", "h2", 10, 0.5, 10.5, 20, 0.5, 20.5)
        for binx in range(1, h2.GetNbinsX() + 1):
            for biny in range(1, h2.GetNbinsY() + 1):
                h2.Fill(binx, biny, _binWeight(binx, biny))

        # Project on X, use bins 1-20 from y
        axis, minBin, maxBin = 'Y', 1, 20
        proj = rootil.projectNTrkRange(h2, axis, minBin, maxBin, "title")
        self.assertEqual((proj.GetDimension(), proj.GetNbinsX()), (1, 10))
        _testBinContents(proj, axis, range(1, 21))

        # Project on X, use bins 1-10 from y
        axis, minBin, maxBin = 'Y', 1, 10
        proj = rootil.projectNTrkRange(h2, axis, minBin, maxBin, "title")
        _testBinContents(proj, axis, range(1, 11))

        # Project on X, use bins 1-9 from y
        axis, minBin, maxBin = 'Y', 1, 9
        proj = rootil.projectNTrkRange(h2, axis, minBin, maxBin, "title")
        _testBinContents(proj, axis, range(1, 10))

        # Project on X, use bins 2-11 from y
        axis, minBin, maxBin = 'Y', 2, 11
        proj = rootil.projectNTrkRange(h2, axis, minBin, maxBin, "title")
        _testBinContents(proj, axis, range(2, 12))

        # Project on X, use bins 13-14 from y
        axis, minBin, maxBin = 'Y', 13, 14
        proj = rootil.projectNTrkRange(h2, axis, minBin, maxBin, "title")
        _testBinContents(proj, axis, (13, 14))

        # Project on Y, use bins 1-10 from x
        axis, minBin, maxBin = 'X', 0, 20
        proj = rootil.projectNTrkRange(h2, axis, minBin, maxBin, "title")
        self.assertEqual((proj.GetDimension(), proj.GetNbinsX()), (1, 20))
        _testBinContents(proj, axis, range(1, 11))

        # Project on Y, use bins 1-10 from x
        axis, minBin, maxBin = 'X', 1, 10
        proj = rootil.projectNTrkRange(h2, axis, minBin, maxBin, "title")
        _testBinContents(proj, axis, range(1, 11))

        # Project on Y, use bins 1-9 from x
        axis, minBin, maxBin = 'X', 1, 9
        proj = rootil.projectNTrkRange(h2, axis, minBin, maxBin, "title")
        _testBinContents(proj, axis, range(1, 10))

        # Project on Y, use bins 4-10 from x
        axis, minBin, maxBin = 'X', 4, 11
        proj = rootil.projectNTrkRange(h2, axis, minBin, maxBin, "title")
        _testBinContents(proj, axis, range(4, 11))


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRootil)
    unittest.TextTestRunner(verbosity=1).run(suite)
