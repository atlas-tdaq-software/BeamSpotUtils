#!/bin/env tdaq_python

"""
Unit test for beamSpotLogging module
"""

from __future__ import annotations

import sys
import tempfile
import unittest

from BeamSpotUtils import bslog

separate = False


class TestLogging(unittest.TestCase):

    def setUp(self) -> None:
        self.stdout = sys.stdout
        self.stderr = sys.stderr
        sys.stdout = tempfile.TemporaryFile("w+")  # type: ignore
        if separate:
            sys.stderr = tempfile.TemporaryFile("w+")  # type: ignore
        else:
            sys.stderr = sys.stdout

    def tearDown(self) -> None:
        sys.stdout = self.stdout
        sys.stderr = self.stderr

    def runTest(self) -> None:
        logger = bslog.initLogger(4)
        logger.info("info message")
        logger.warning("warning message")
        logger.error("error message")

        sys.stdout.seek(0)
        lines = sys.stdout.readlines()
        self.assertEqual(len(lines), 3)

        sys.stderr.seek(0)
        lines = sys.stderr.readlines()
        if separate:
            self.assertEqual(len(lines), 2)
        else:
            self.assertEqual(len(lines), 3)


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'separate':
        separate = True
    # need special test runner here which does not write to stdout/stderr
    suite = unittest.TestLoader().loadTestsFromTestCase(TestLogging)
    result = unittest.TextTestRunner(verbosity=1).run(suite)
    if result.wasSuccessful():
        exit(0)
    else:
        exit(1)
