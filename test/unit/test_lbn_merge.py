#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.lbn_merge module
"""

from __future__ import annotations

import logging
import unittest

from BeamSpotUtils import htree, hist_def
from BeamSpotUtils.lbn_merge import LBNMerge, _buckets
from BeamSpotUtils.test import _mocks


def _get_stat(hist_tree: htree.HTree, lbn: int, hdefs: list[hist_def.HistoDef]) -> float:
    """Return max value of GetEntries()
    """
    allNames = set(hdef.name for hdef in hdefs)
    hists = [hist_tree.get_histogram(name, lbn) for name in allNames]
    return max(hist.GetEntries() for hist in hists if hist is not None)


class TestLBNMerge(unittest.TestCase):
    """Test case for BeamSpotUtils.lbn_merge module.
    """

    def setUp(self) -> None:
        logging.basicConfig(level=logging.DEBUG)

    def tearDown(self) -> None:
        pass

    def test_buckets(self) -> None:
        """Test for _buckets() method"""

        buckets = list(_buckets(range(10), 3))
        self.assertEqual(buckets, [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [9],
        ])

        buckets = list(_buckets(range(10), -1))
        self.assertEqual(buckets, [
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        ])

    def test_wanted_lbns(self) -> None:
        """Test for _wanted_lbns() method"""

        lbns = list(range(100))

        # 0 is stripped
        merger = LBNMerge(0, 5, 0)
        lbs = merger._wanted_lbns(lbns)
        self.assertEqual(lbs, [1, 2, 3, 4, 5])

        # inclusivity
        merger = LBNMerge(90, 95, 0)
        lbs = merger._wanted_lbns(lbns)
        self.assertEqual(lbs, [90, 91, 92, 93, 94, 95])

        # inclusivity
        merger = LBNMerge(98, 200, 0)
        lbs = merger._wanted_lbns(lbns)
        self.assertEqual(lbs, [98, 99])

        # negative
        merger = LBNMerge(-1, -1, 0)
        lbs = merger._wanted_lbns(lbns)
        self.assertEqual(lbs, [99])

        merger = LBNMerge(-3, -1, 0)
        lbs = merger._wanted_lbns(lbns)
        self.assertEqual(lbs, [97, 98, 99])

    def test_lbn_generator(self) -> None:
        """Test for _lbn_generator() method
        """
        server, provider = "Histogramming", "TopMIG-OH_HLT"
        detail, chain = "EXPERT", "T2VertexBeamSpot_FTF"
        run = 123
        lbns = [1, 2, 3, 4, 7, 10]

        for variant in htree.HTreeVariant:
            with self.subTest(variant=variant):
                tfile = _mocks.make_mda_tree(server=server, provider=provider, detail=detail,
                                             chain=chain, run=run, lbs=lbns, variant=variant)
                atree = htree.make_htree(tfile, server, provider, detail, chain)
                self.assertIsNotNone(atree)
                assert atree is not None  # for mypy

                minLBN, maxLBN, split = 0, -1, -1
                merger = LBNMerge(minLBN, maxLBN, split)
                lbs = list(merger._lbn_generator([atree]))
                self.assertEqual(lbs, [
                    (1, atree),
                    (2, atree),
                    (3, atree),
                    (4, atree),
                    (7, atree),
                    (10, atree),
                ])
                lbs = list(merger._lbn_generator([atree], reverse=True))
                self.assertEqual(lbs, [
                    (10, atree),
                    (7, atree),
                    (4, atree),
                    (3, atree),
                    (2, atree),
                    (1, atree),
                ])

                minLBN, maxLBN, split = -4, -1, -1
                merger = LBNMerge(minLBN, maxLBN, split)
                lbs = list(merger._lbn_generator([atree]))
                self.assertEqual(lbs, [
                    (7, atree),
                    (10, atree),
                ])
                lbs = list(merger._lbn_generator([atree], reverse=True))
                self.assertEqual(lbs, [
                    (10, atree),
                    (7, atree),
                ])

    def test_group_by_split(self) -> None:
        """Test for group_by_split() method"""

        server, provider = "Histogramming", "TopMIG-OH_HLT"
        detail, chain = "EXPERT", "T2VertexBeamSpot_FTF"
        run = 123
        lbns = [1, 2, 3, 4, 7, 10]

        for variant in htree.HTreeVariant:
            with self.subTest(variant=variant):
                tfile = _mocks.make_mda_tree(server=server, provider=provider, detail=detail,
                                             chain=chain, run=run, lbs=lbns, variant=variant)
                atree = htree.make_htree(tfile, server, provider, detail, chain)
                self.assertIsNotNone(atree)
                assert atree is not None  # for mypy

                # negative means no splitting
                minLBN, maxLBN, split = 0, -1, -1
                merger = LBNMerge(minLBN, maxLBN, split)
                lbs = merger.group_by_split([atree])
                self.assertEqual(lbs, [{
                    1: atree,
                    2: atree,
                    3: atree,
                    4: atree,
                    7: atree,
                    10: atree,
                }])

                minLBN, maxLBN, split = -4, -1, -1
                merger = LBNMerge(minLBN, maxLBN, split)
                lbs = merger.group_by_split([atree])
                self.assertEqual(lbs, [{
                    7: atree,
                    10: atree,
                }])

                minLBN, maxLBN, split = 0, 3, -1
                merger = LBNMerge(minLBN, maxLBN, split)
                lbs = merger.group_by_split([atree])
                self.assertEqual(lbs, [{
                    1: atree,
                    2: atree,
                    3: atree,
                }])

                # single LB as a step
                minLBN, maxLBN, split = 0, 3, 1
                merger = LBNMerge(minLBN, maxLBN, split)
                lbs = merger.group_by_split([atree])
                self.assertEqual(lbs, [
                    {1: atree},
                    {2: atree},
                    {3: atree},
                ])

                # each group has same size even if some LBs are not there
                minLBN, maxLBN, split = 0, -1, 3
                merger = LBNMerge(minLBN, maxLBN, split)
                lbs = merger.group_by_split([atree])
                self.assertEqual(lbs, [
                    {
                        1: atree,
                        2: atree,
                        3: atree,
                    },
                    {
                        4: atree,
                        7: atree,
                        10: atree,
                    }
                ])

                # multiple files
                trees: list[htree.HTree] = []
                for lbn in lbns:
                    tfile = _mocks.make_offline_lbn_tree(detail=detail, chain=chain, lbn=lbn)
                    atree = htree.make_htree(tfile, server, provider, detail, chain)
                    assert atree is not None  # for mypy
                    trees.append(atree)

                minLBN, maxLBN, split = 0, -1, -1
                merger = LBNMerge(minLBN, maxLBN, split)
                lbs = merger.group_by_split(trees)
                self.assertEqual(lbs, [{
                    1: trees[0],
                    2: trees[1],
                    3: trees[2],
                    4: trees[3],
                    7: trees[4],
                    10: trees[5],
                }])

                minLBN, maxLBN, split = 0, -1, 4
                merger = LBNMerge(minLBN, maxLBN, split)
                lbs = merger.group_by_split(trees)
                self.assertEqual(lbs, [
                    {
                        1: trees[0],
                        2: trees[1],
                    },
                    {
                        3: trees[2],
                        4: trees[3],
                        7: trees[4],
                        10: trees[5],
                    }
                ])

    def test_merge_vtx(self) -> None:
        """Test for merge() method with vxt histos"""

        hdefs = hist_def.make_hdefs_vtx(do_bcid=True)
        lbns = list(range(1, 11))

        trees = {}
        tree = _mocks.HTreeMock(hdefs)
        for lbn in lbns:
            tree.add_histograms(lbn)
            trees[lbn] = tree

        merger = LBNMerge(-1, -1, -1)
        htree, merged_lbns = merger.merge(trees, hdefs)

        self.assertEqual(merged_lbns, [10, 9, 8, 7, 6, 5, 4, 3, 2, 1])
        self.assertEqual(htree.get_lbns(), [])
        histo = htree.get_histogram("posX")
        self.assertIsNotNone(histo)
        self.assertEqual(histo.GetEntries(), 10 * 1000)  # type: ignore

        # now test case with some histos missing
        tree = _mocks.HTreeMock(hdefs)
        tree.add_histograms(lbn=1, n_missing=0)
        tree.add_histograms(lbn=2, n_missing=1)
        tree.add_histograms(lbn=3, n_missing=0)
        tree.add_histograms(lbn=4, n_missing=3)
        tree.add_histograms(lbn=5, n_missing=0)
        trees = {i: tree for i in range(1, 6)}

        htree, merged_lbns = merger.merge(trees, hdefs)

        self.assertEqual(merged_lbns, [5, 3, 1])
        self.assertEqual(htree.get_lbns(), [])
        histo = htree.get_histogram("posX")
        self.assertIsNotNone(histo)
        self.assertEqual(histo.GetEntries(), 3 * 1000)  # type: ignore

        # now test case with histos are empty
        tree = _mocks.HTreeMock(hdefs)
        tree.add_histograms(lbn=1, all_empty=True)
        tree.add_histograms(lbn=2, all_empty=False)
        tree.add_histograms(lbn=3, all_empty=False)
        tree.add_histograms(lbn=4, all_empty=False)
        tree.add_histograms(lbn=5, all_empty=True)
        trees = {i: tree for i in range(1, 6)}

        htree, merged_lbns = merger.merge(trees, hdefs)

        self.assertEqual(merged_lbns, [4, 3, 2])
        self.assertEqual(htree.get_lbns(), [])
        histo = htree.get_histogram("posX")
        self.assertIsNotNone(histo)
        self.assertEqual(histo.GetEntries(), 3 * 1000)  # type: ignore

        # combination of empty and missing
        tree = _mocks.HTreeMock(hdefs)
        tree.add_histograms(lbn=1, n_missing=0, all_empty=True)
        tree.add_histograms(lbn=2, n_missing=1, all_empty=False)
        tree.add_histograms(lbn=3, n_missing=0, all_empty=False)
        tree.add_histograms(lbn=4, n_missing=2, all_empty=False)
        tree.add_histograms(lbn=5, n_missing=0, all_empty=True)
        trees = {i: tree for i in range(1, 6)}

        htree, merged_lbns = merger.merge(trees, hdefs)

        self.assertEqual(merged_lbns, [3])
        self.assertEqual(htree.get_lbns(), [])
        histo = htree.get_histogram("posX")
        self.assertIsNotNone(histo)
        self.assertEqual(histo.GetEntries(), 1000)  # type: ignore

        # result is empty if everything is missing
        tree = _mocks.HTreeMock(hdefs)
        tree.add_histograms(lbn=1, n_missing=3)
        tree.add_histograms(lbn=2, n_missing=1)
        tree.add_histograms(lbn=3, n_missing=4)
        tree.add_histograms(lbn=4, n_missing=3)
        tree.add_histograms(lbn=5, n_missing=1)
        trees = {i: tree for i in range(1, 6)}

        htree, merged_lbns = merger.merge(trees, hdefs)

        self.assertEqual(merged_lbns, [])
        self.assertEqual(htree.get_lbns(), [])
        histo = htree.get_histogram("posX")
        self.assertIsNone(histo)

    def test_merge_vtx_runsummary(self) -> None:
        """Test for merge() method with vxt histos and runsummary"""

        hdefs = hist_def.make_hdefs_vtx(do_bcid=True)
        lbns = list(range(1, 11))

        trees = {}
        tree = _mocks.HTreeMock(hdefs)
        for lbn in lbns:
            tree.add_histograms(lbn)
            trees[lbn] = tree

        merger = LBNMerge(-1, -1, -1, use_rs_res=True)
        htree, merged_lbns = merger.merge(trees, hdefs)
        self.assertEqual(merged_lbns, [10, 9, 8, 7, 6, 5, 4, 3, 2, 1])
        for axis in "XYZ":
            histo = htree.get_histogram(f"split{axis}_vs_ntrks")
            self.assertIsNotNone(histo)
            self.assertEqual(histo.GetEntries(), 10 * 1000)  # type: ignore

        # add runsummary histos
        tree.add_run_summary()
        htree, merged_lbns = merger.merge(trees, hdefs)
        self.assertEqual(merged_lbns, [10, 9, 8, 7, 6, 5, 4, 3, 2, 1])
        for axis in "XYZ":
            histo = htree.get_histogram(f"split{axis}_vs_ntrks")
            self.assertIsNotNone(histo)
            # Entries match runsummary (30k)
            self.assertEqual(histo.GetEntries(), 30000)  # type: ignore

    def test_merge_trk(self) -> None:
        """Test for merge() method with trk histos"""

        # difference from vtx is that we want to specify limit on LBN and stat

        hdefs = hist_def.make_hdefs_trk(do_bcid=True)
        lbns = list(range(1, 11))

        trees = {}
        tree = _mocks.HTreeMock(hdefs)
        for lbn in lbns:
            tree.add_histograms(lbn)
            trees[lbn] = tree

        merger = LBNMerge(-1, -1, -1)

        # specify minimum stats
        htree, merged_lbns = merger.merge(trees, hdefs, min_stat=4500, get_stat=_get_stat)
        self.assertEqual(merged_lbns, [10, 9, 8, 7, 6])
        self.assertEqual(htree.get_lbns(), [])
        histo = htree.get_histogram("llpoly")
        self.assertIsNotNone(histo)
        self.assertEqual(histo.GetEntries(), 5 * 1000)  # type: ignore

        # one LBN should be OK
        htree, merged_lbns = merger.merge(trees, hdefs, min_stat=500, get_stat=_get_stat)
        self.assertEqual(merged_lbns, [10])
        self.assertEqual(htree.get_lbns(), [])
        histo = htree.get_histogram("llpoly")
        self.assertIsNotNone(histo)
        self.assertEqual(histo.GetEntries(), 1000)  # type: ignore

        # also give a lower limit on LBN
        htree, merged_lbns = merger.merge(trees, hdefs, min_stat=500, min_lbn_count=3, get_stat=_get_stat)
        self.assertEqual(merged_lbns, [10, 9, 8])
        self.assertEqual(htree.get_lbns(), [])
        histo = htree.get_histogram("llpoly")
        self.assertIsNotNone(histo)
        self.assertEqual(histo.GetEntries(), 3 * 1000)  # type: ignore

        # limit on LBN exceeded
        htree, merged_lbns = merger.merge(trees, hdefs, min_stat=500, min_lbn_count=30, get_stat=_get_stat)
        self.assertEqual(merged_lbns, [10, 9, 8, 7, 6, 5, 4, 3, 2, 1])
        self.assertEqual(htree.get_lbns(), [])
        histo = htree.get_histogram("llpoly")
        self.assertIsNotNone(histo)
        self.assertEqual(histo.GetEntries(), 10 * 1000)  # type: ignore

        # limit on stat exceeded
        htree, merged_lbns = merger.merge(trees, hdefs, min_stat=500000, min_lbn_count=2, get_stat=_get_stat)
        self.assertEqual(merged_lbns, [10, 9, 8, 7, 6, 5, 4, 3, 2, 1])
        self.assertEqual(htree.get_lbns(), [])
        histo = htree.get_histogram("llpoly")
        self.assertIsNotNone(histo)
        self.assertEqual(histo.GetEntries(), 10 * 1000)  # type: ignore


if __name__ == "__main__":
    unittest.main()
