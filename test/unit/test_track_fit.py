#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.track_fit module
"""

from __future__ import annotations

import logging
import unittest

from BeamSpotUtils import track_fit


def all_fitters() -> list[track_fit.TrackFitter]:
    fitters: list[track_fit.TrackFitter] = [track_fit.MinuitFitter()]
    try:
        # ScipyFitter only works if numpy can be imported
        import numpy  # noqa: F401
        fitters += [track_fit.ScipyFitter()]
    except ImportError:
        pass
    return fitters


class TestTrackFit(unittest.TestCase):
    """Test case for BeamSpotUtils.track_fit module.
    """

    def setUp(self) -> None:
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger(__name__)

    def test_make_ll_powers(self) -> None:
        """Test for _make_ll_powers() method"""

        powers = list(track_fit._make_ll_powers())
        self.assertEqual(len(powers), 90)

        self.assertEqual(powers, [
            (0, 0, 0, 0, 0, 0),
            (0, 0, 0, 0, 0, 1),
            (0, 0, 0, 0, 0, 2),
            (0, 0, 0, 0, 1, 0),
            (0, 0, 0, 0, 1, 1),
            (0, 0, 0, 0, 2, 0),
            (0, 0, 0, 1, 0, 0),
            (0, 0, 0, 1, 0, 1),
            (0, 0, 0, 1, 0, 2),
            (0, 0, 0, 1, 1, 0),
            (0, 0, 0, 1, 1, 1),
            (0, 0, 0, 1, 2, 0),
            (0, 0, 0, 2, 0, 0),
            (0, 0, 0, 2, 0, 1),
            (0, 0, 0, 2, 0, 2),
            (0, 0, 0, 2, 1, 0),
            (0, 0, 0, 2, 1, 1),
            (0, 0, 0, 2, 2, 0),
            (0, 0, 1, 0, 0, 0),
            (0, 0, 1, 0, 0, 1),
            (0, 0, 1, 0, 0, 2),
            (0, 0, 1, 0, 1, 0),
            (0, 0, 1, 0, 1, 1),
            (0, 0, 1, 0, 2, 0),
            (0, 0, 1, 1, 0, 0),
            (0, 0, 1, 1, 0, 1),
            (0, 0, 1, 1, 0, 2),
            (0, 0, 1, 1, 1, 0),
            (0, 0, 1, 1, 1, 1),
            (0, 0, 1, 1, 2, 0),
            (0, 0, 2, 0, 0, 0),
            (0, 0, 2, 0, 0, 1),
            (0, 0, 2, 0, 0, 2),
            (0, 0, 2, 0, 1, 0),
            (0, 0, 2, 0, 1, 1),
            (0, 0, 2, 0, 2, 0),
            (0, 1, 0, 0, 0, 0),
            (0, 1, 0, 0, 0, 1),
            (0, 1, 0, 0, 0, 2),
            (0, 1, 0, 0, 1, 0),
            (0, 1, 0, 0, 1, 1),
            (0, 1, 0, 0, 2, 0),
            (0, 1, 0, 1, 0, 0),
            (0, 1, 0, 1, 0, 1),
            (0, 1, 0, 1, 0, 2),
            (0, 1, 0, 1, 1, 0),
            (0, 1, 0, 1, 1, 1),
            (0, 1, 0, 1, 2, 0),
            (0, 1, 1, 0, 0, 0),
            (0, 1, 1, 0, 0, 1),
            (0, 1, 1, 0, 0, 2),
            (0, 1, 1, 0, 1, 0),
            (0, 1, 1, 0, 1, 1),
            (0, 1, 1, 0, 2, 0),
            (0, 2, 0, 0, 0, 0),
            (0, 2, 0, 0, 0, 1),
            (0, 2, 0, 0, 0, 2),
            (0, 2, 0, 0, 1, 0),
            (0, 2, 0, 0, 1, 1),
            (0, 2, 0, 0, 2, 0),
            (1, 0, 0, 0, 0, 0),
            (1, 0, 0, 0, 0, 1),
            (1, 0, 0, 0, 0, 2),
            (1, 0, 0, 0, 1, 0),
            (1, 0, 0, 0, 1, 1),
            (1, 0, 0, 0, 2, 0),
            (1, 0, 0, 1, 0, 0),
            (1, 0, 0, 1, 0, 1),
            (1, 0, 0, 1, 0, 2),
            (1, 0, 0, 1, 1, 0),
            (1, 0, 0, 1, 1, 1),
            (1, 0, 0, 1, 2, 0),
            (1, 0, 1, 0, 0, 0),
            (1, 0, 1, 0, 0, 1),
            (1, 0, 1, 0, 0, 2),
            (1, 0, 1, 0, 1, 0),
            (1, 0, 1, 0, 1, 1),
            (1, 0, 1, 0, 2, 0),
            (1, 1, 0, 0, 0, 0),
            (1, 1, 0, 0, 0, 1),
            (1, 1, 0, 0, 0, 2),
            (1, 1, 0, 0, 1, 0),
            (1, 1, 0, 0, 1, 1),
            (1, 1, 0, 0, 2, 0),
            (2, 0, 0, 0, 0, 0),
            (2, 0, 0, 0, 0, 1),
            (2, 0, 0, 0, 0, 2),
            (2, 0, 0, 0, 1, 0),
            (2, 0, 0, 0, 1, 1),
            (2, 0, 0, 0, 2, 0),
        ])

    def test_gaussian_estimate(self) -> None:
        """Test for _gaussian_estimate() method"""

        # exceptional case, no stats
        n, sum, sum2 = 0, 0., 0.
        with self.assertRaises(ZeroDivisionError):
            track_fit._gaussian_estimate(n, sum, sum2)

        # exceptional case, RMS is zero
        n, sum, sum2 = 1000, 0, 0
        with self.assertRaises(ZeroDivisionError):
            track_fit._gaussian_estimate(n, sum, sum2)

        # exceptional case, RMS is zero
        n, sum, sum2 = 1000, 1000., 1000.
        with self.assertRaises(ZeroDivisionError):
            track_fit._gaussian_estimate(n, sum, sum2)

        # exceptional case, imaginary RMS
        n, sum, sum2 = 1000, 1000., 990.
        with self.assertRaises(ValueError):
            track_fit._gaussian_estimate(n, sum, sum2)

        # Use sample [1, 2, 2, 3]
        n, sum, sum2 = 4, 8., 18.
        mean, mean_err, rms, rms_err = track_fit._gaussian_estimate(n, sum, sum2)
        var = sum2 / (n - 1) - sum**2 / n / (n - 1)
        self.assertAlmostEqual(mean, sum / n)
        self.assertAlmostEqual(rms**2, var)
        self.assertAlmostEqual(mean_err**2, var / n)
        self.assertAlmostEqual(rms_err**2, 2 * var / (n - 1) / 4)

        # Sample of 100 numbers with mu=-1 sigma=2
        n, sum, sum2 = 100, -102.25567421184664, 467.17078868536385
        mean, mean_err, rms, rms_err = track_fit._gaussian_estimate(n, sum, sum2)
        var = sum2 / (n - 1) - sum**2 / n / (n - 1)
        self.assertAlmostEqual(mean, sum / n)
        self.assertAlmostEqual(rms**2, var)
        self.assertAlmostEqual(mean_err**2, var / n)
        self.assertAlmostEqual(rms_err**2, 2 * var / (n - 1) / 4)

    def test_fitter_ls_total(self) -> None:
        """Test for fit_total_data() method
        """

        # data comes from one random histogram produced by HLT
        lsm = track_fit.LSMatrix.from_array([
            9.333533695451506e+007,
            3.599760221437737e+005,
            9.448055469797011e+007,
            -8.180991586590694e+008,
            1.410184578577153e+007,
            8.938321281305653e+010,
            1.410184578577153e+007,
            -8.444010151939176e+008,
            -8.189668496445019e+008,
            9.139938593793092e+010,
            -3.649826095180542e+007,
            -8.591944417588462e+007,
            3.017213692991881e+008,
            7.583297301640664e+008,
            9.267576533093539e+007,
            -4.444086359598336e+006,
            4.537560362554367e+008,
            490828.
        ])

        for fitter in all_fitters():
            with self.subTest(fitter.__class__.__name__):
                snap = fitter.fit_total_data(lsm)
                snap.prettyLog(self.logger)
                self.assertAlmostEqual(snap.posX, -0.38780751)
                self.assertAlmostEqual(snap.posXErr, 0.00010793)
                self.assertAlmostEqual(snap.posY, -0.90821056)
                self.assertAlmostEqual(snap.posYErr, 0.00010741)
                self.assertAlmostEqual(snap.posZ, -9.05426414)
                self.assertAlmostEqual(snap.posZErr, 0.04143033)
                self.assertEqual(snap.sigmaX, 0.)
                self.assertEqual(snap.sigmaXErr, 0.)
                self.assertEqual(snap.sigmaY, 0.)
                self.assertEqual(snap.sigmaYErr, 0.)
                self.assertAlmostEqual(snap.sigmaZ, 29.02572269)
                self.assertAlmostEqual(snap.sigmaZErr, 0.0292957)
                self.assertAlmostEqual(snap.tiltX, -3.092468944e-05)
                self.assertAlmostEqual(snap.tiltY, -3.414371746e-05)
                self.assertEqual(snap.sigmaXY, 0.)
                self.assertEqual(snap.sigmaXYErr, 0.)

    def test_fitter_ls_bcid(self) -> None:
        """Test for fit_bcid_data() method
        """

        # data comes from one random histogram produced by HLT
        lsm = track_fit.LSMatrixBCID.from_array([
            1.035284364987938e+005,
            -1.197124699138750e+004,
            7.963940088002088e+004,
            -3.018779586757259e+004,
            -6.724276598828439e+004,
            -3.443469889757357e+003,
            1.952876990774413e+005,
            477.,
        ])

        for fitter in all_fitters():
            with self.subTest(fitter.__class__.__name__):
                snap = fitter.fit_bcid_data(lsm, 123)
                snap.prettyLog(self.logger)
                self.assertEqual(snap.bcid, 123)
                self.assertAlmostEqual(snap.posX, -0.3961075713534186)
                self.assertAlmostEqual(snap.posXErr, 0.0031352901535098253)
                self.assertAlmostEqual(snap.posY, -0.9038825853117836)
                self.assertAlmostEqual(snap.posYErr, 0.0035747327084119313)
                self.assertAlmostEqual(snap.posZ, -7.219014443935759)
                self.assertAlmostEqual(snap.posZErr, 0.8663819347001471)
                self.assertEqual(snap.sigmaX, 0.)
                self.assertEqual(snap.sigmaXErr, 0.)
                self.assertEqual(snap.sigmaY, 0.)
                self.assertEqual(snap.sigmaYErr, 0.)
                self.assertAlmostEqual(snap.sigmaZ, 18.922067072113588)
                self.assertAlmostEqual(snap.sigmaZErr, 0.6132677166723914)
                self.assertEqual(snap.tiltX, 0.)
                self.assertEqual(snap.tiltY, 0.)
                self.assertEqual(snap.sigmaXY, 0.)
                self.assertEqual(snap.sigmaXYErr, 0.)

    def test_fitter_ll(self) -> None:
        """Test for ll_fit_data() method
        """

        # data comes from one random histogram produced by HLT
        llpoly = track_fit.PolyLL.from_array([
            -5.282276928324173e+007,
            5.637561619670757e+010,
            -1.402579195870132e+014,
            2.690718372113154e+010,
            -6.348439250192299e+013,
            -4.967498239486381e+013,
            8.601612546785070e+008,
            -9.855475464483594e+011,
            2.366197723398073e+015,
            -3.086267893319418e+011,
            9.417501746710013e+014,
            4.319111135529252e+014,
            -5.237964186487318e+010,
            6.478795916918710e+013,
            -1.666707566101163e+017,
            2.127840336663254e+013,
            -6.637669156988640e+016,
            -3.226032339076898e+016,
            3.395578286422774e+008,
            -1.147224979441934e+011,
            1.832477033596467e+014,
            -3.639480029974532e+011,
            3.016064211004254e+014,
            8.814435225047588e+014,
            1.059841581929358e+009,
            -1.635169150280620e+012,
            2.440491602050442e+015,
            -1.649224454865065e+012,
            5.086538105105339e+015,
            4.857417667007494e+015,
            -5.112310364213480e+010,
            2.127840336663254e+013,
            -3.318834578494320e+016,
            6.145803801596888e+013,
            -6.452064678153795e+016,
            -1.559463884101015e+017,
            -9.799166446702935e+007,
            1.166366460150060e+011,
            -2.964697463162411e+014,
            3.844772603081089e+010,
            -1.185427407630637e+014,
            -5.742648255003445e+013,
            9.590776709986206e+008,
            -1.099782777778409e+012,
            2.625274872017970e+015,
            -3.601037721303848e+011,
            1.091084786328602e+015,
            5.335389672651361e+014,
            -1.929775522280614e+007,
            2.655238739733876e+010,
            -2.936981853010446e+013,
            4.546495720344235e+010,
            -1.210701464700223e+014,
            -1.326465408857357e+014,
            -5.386938403883566e+007,
            6.401262807537330e+010,
            -1.624425341088405e+014,
            2.115617401006075e+010,
            -6.526935160148366e+013,
            -3.192642703182515e+013,
            -4.155412945495328e+007,
            1.665256476554870e+010,
            -2.660556271758917e+013,
            4.817034137347797e+010,
            -4.858193250830034e+013,
            -1.207766001731095e+014,
            -1.929775522280614e+007,
            2.655238739733876e+010,
            -2.936981853010446e+013,
            4.546495720344235e+010,
            -1.210701464700223e+014,
            -1.326465408857357e+014,
            9.286437940911397e+008,
            -3.601037721303848e+011,
            5.455423931643011e+014,
            -1.053271611492311e+012,
            1.067077934530272e+015,
            2.606056501109147e+015,
            -3.755931517653883e+005,
            2.732185647449746e+008,
            -1.436820487502669e+012,
            -1.912267358874587e+008,
            1.036676418570135e+012,
            1.935204263933101e+012,
            -5.316840112803882e+007,
            2.115617401006075e+010,
            -3.263467580074183e+013,
            6.232488262858090e+010,
            -6.385285406365031e+013,
            -1.579127028807053e+014,
            1.335213816193791e+006,
            -5.386938403883566e+007,
            3.200631403768665e+010,
            -5.316840112803882e+007,
            2.115617401006075e+010,
            3.116244131429045e+010,
            -4.444086359598336e+006,
            4.537560362554367e+008,
            4.908280000000000e+005,
            0.000000000000000e+000,
        ])

        for fitter in all_fitters():
            with self.subTest(fitter.__class__.__name__):
                snap = fitter.ll_fit_data(llpoly)
                snap.prettyLog(self.logger)
                self.assertAlmostEqual(snap.posX, -0.38781313108927495)
                self.assertAlmostEqual(snap.posXErr, 0.00010621692997499186)
                self.assertAlmostEqual(snap.posY, -0.9082841428766564)
                self.assertAlmostEqual(snap.posYErr, 0.00010578029963909483)
                self.assertAlmostEqual(snap.posZ, -9.05426414)
                self.assertAlmostEqual(snap.posZErr, 0.04143033)
                self.assertAlmostEqual(snap.sigmaX, 0.008852330484776303)
                self.assertAlmostEqual(snap.sigmaXErr, 0.00018546774976767942)
                self.assertAlmostEqual(snap.sigmaY, 0.00893526829168299)
                self.assertAlmostEqual(snap.sigmaYErr, 0.0001787821155081021)
                self.assertAlmostEqual(snap.sigmaZ, 29.02572269)
                self.assertAlmostEqual(snap.sigmaZErr, 0.0292957)
                self.assertAlmostEqual(snap.tiltX, -3.0803208421674054e-05)
                self.assertAlmostEqual(snap.tiltY, -3.44571345295791e-05)
                self.assertEqual(snap.sigmaXY, 0.)
                self.assertEqual(snap.sigmaXYErr, 0.)


if __name__ == "__main__":
    unittest.main()
