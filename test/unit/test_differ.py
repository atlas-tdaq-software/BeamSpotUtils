#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.differ module
"""

from __future__ import annotations

import copy
import logging
import unittest

from BeamSpotUtils.model import BSSnapshot, BeamSpotRepresentation
from BeamSpotUtils.differ import boxCut, Differ


class TestDiffer(unittest.TestCase):
    """Test case for BeamSpotUtils.differ module.
    """

    def setUp(self) -> None:
        logging.basicConfig(level=logging.DEBUG)

    def tearDown(self) -> None:
        pass

    def test_boxCut(self) -> None:
        """Simple tests for boxCut() method
        """
        # if ref.pos#Err == 0 or ref.sigma#Err == 0 should always return True
        cur = BSSnapshot(status=7)
        for var in ('pos', 'sigma'):
            for vec in 'XYZ':
                ref: dict[str, int | float] = dict(
                    status=7, posXErr=1, posYErr=1, posZErr=1,
                    sigmaXErr=1, sigmaYErr=1, sigmaZErr=1)
                attrib = var + vec + 'Err'
                ref[attrib] = 0.0
                self.assertTrue(boxCut(BSSnapshot(ref), cur), attrib + " == 0")

        # if ref.sigma# == 0 should always return True
        cur = BSSnapshot(status=7)
        for vec in 'XYZ':
            ref = dict(status=7, posXErr=1, posYErr=1, posZErr=1,
                       sigmaXErr=1, sigmaYErr=1, sigmaZErr=1,
                       sigmaX=1, sigmaY=1, sigmaZ=1)
            attrib = 'sigma' + vec
            ref[attrib] = 0.0
            self.assertTrue(boxCut(BSSnapshot(ref), cur), attrib + " == 0")

    def test_boxCut_pos(self) -> None:
        """Simple tests for boxCut() method
        """
        # Returns true if position changed by more than 10% of sigma and significance > 2
        for vec in 'XYZ':
            ref_dict: dict[str, int | float] = dict(
                status=7,
                posX=0, posY=0, posZ=0,
                posXErr=.01, posYErr=.01, posZErr=.01,
                sigmaX=1, sigmaY=1, sigmaZ=1,
                sigmaXErr=.01, sigmaYErr=.01, sigmaZErr=.01)
            ref = BSSnapshot(ref_dict)
            cur = BSSnapshot(ref)
            attrib = 'pos' + vec
            setattr(cur, attrib, 0.11)
            self.assertTrue(boxCut(ref, cur), attrib + " changed by 11%")
            setattr(cur, attrib, -0.11)
            self.assertTrue(boxCut(ref, cur), attrib + " changed by 11%")
            # reduce significance below 2
            setattr(ref, 'pos' + vec + 'Err', 0.04)
            setattr(cur, 'pos' + vec + 'Err', 0.04)
            self.assertFalse(boxCut(ref, cur), attrib + " changed by 11%")

    def test_boxCut_sigma(self) -> None:
        """Simple tests for boxCut() method
        """
        # Returns true if width changed by more than 10% and significance > 2
        for vec in 'XYZ':
            ref_dict: dict[str, int | float] = dict(
                status=7,
                posX=0, posY=0, posZ=0,
                posXErr=.01, posYErr=.01, posZErr=.01,
                sigmaX=1, sigmaY=1, sigmaZ=1,
                sigmaXErr=.01, sigmaYErr=.01, sigmaZErr=.01)
            ref = BSSnapshot(ref_dict)
            cur = BSSnapshot(ref)
            attrib = 'sigma' + vec
            setattr(cur, attrib, 1.11)
            self.assertTrue(boxCut(ref, cur), attrib + " changed by 11%")
            setattr(cur, attrib, 0.89)
            self.assertTrue(boxCut(ref, cur), attrib + " changed by 11%")
            # reduce significance below 2
            setattr(ref, 'sigma' + vec + 'Err', 0.042)
            setattr(cur, 'sigma' + vec + 'Err', 0.042)
            self.assertFalse(boxCut(ref, cur), attrib + " changed by 11%")

    def test_boxCut_err(self) -> None:
        """Simple tests for boxCut() method
        """
        # Returns true if error reduced by more than 50%
        for var in ('pos', 'sigma'):
            for vec in 'XYZ':
                ref_dict: dict[str, int | float] = dict(
                    status=7,
                    posX=0, posY=0, posZ=0,
                    posXErr=.01, posYErr=.01, posZErr=.01,
                    sigmaX=1, sigmaY=1, sigmaZ=1,
                    sigmaXErr=.01, sigmaYErr=.01, sigmaZErr=.01)
                ref = BSSnapshot(ref_dict)
                cur = BSSnapshot(ref)
                attrib = var + vec + 'Err'
                setattr(cur, attrib, 0.00499)
                self.assertTrue(boxCut(ref, cur), attrib + " changed by 50%")
                setattr(cur, attrib, 0.00501)
                self.assertFalse(boxCut(ref, cur), attrib + " changed by 50%")
                # error increased, no update
                setattr(cur, attrib, 0.05)
                self.assertFalse(boxCut(ref, cur), attrib + " changed by 50%")

    def test_differ_sanityCheck(self) -> None:
        """Test for Differ._sanityCheck() method
        """
        differ = Differ(BSSnapshot())

        snap_dict: dict[str, int | float] = {}
        for var in ('posX', 'posY', 'posZ', 'sigmaX', 'sigmaY', 'sigmaZ', 'tiltX', 'tiltY'):
            for key in (var, var + 'Err'):
                snap_dict[key] = 1.
        snap = BSSnapshot(snap_dict)

        self.assertTrue(differ._sanityCheck(snap), "complete obj")

        for var in ('posX', 'posY', 'posZ', 'sigmaX', 'sigmaY', 'sigmaZ', 'tiltX', 'tiltY'):
            for key in (var, var + 'Err'):
                snap1 = copy.copy(snap)
                setattr(snap1, key, 1)
                self.assertFalse(differ._sanityCheck(snap1), key + " is non-float")
                setattr(snap1, key, "")
                self.assertFalse(differ._sanityCheck(snap1), key + " is non-float")
                setattr(snap1, key, float("nan"))
                self.assertFalse(differ._sanityCheck(snap1), key + " is nan")

    def test_differ_storeRepr(self) -> None:
        """Test for Differ.storeRepr() method
        """

        ref = BSSnapshot(status=7, run=1, lbn=1,
                         posX=0., posY=0., posZ=0.,
                         posXErr=.01, posYErr=.01, posZErr=.01,
                         sigmaX=1., sigmaY=1., sigmaZ=1.,
                         sigmaXErr=.01, sigmaYErr=.01, sigmaZErr=.01,
                         tiltX=0., tiltY=0.,
                         tiltXErr=0., tiltYErr=0.)
        differ = Differ(ref)

        cur = BSSnapshot(ref, lbn=100)
        self.assertIsNone(differ.storeRepr(BeamSpotRepresentation([cur]), r4p=True))

        cur.posX = 0.11
        srepr = differ.storeRepr(BeamSpotRepresentation([cur]), r4p=True)
        self.assertIsNotNone(srepr)
        assert srepr is not None
        last = srepr.last()
        assert last is not None
        self.assertEqual(last.lbn, 100)

        cur.status = 4
        srepr = differ.storeRepr(BeamSpotRepresentation([cur]), r4p=True)
        self.assertIsNotNone(srepr)
        assert srepr is not None
        last = srepr.last()
        assert last is not None
        self.assertEqual(last.status, 4)
        self.assertEqual(last.lbn, 100)

        cur.status = 7
        srepr = differ.storeRepr(BeamSpotRepresentation([cur]), r4p=True)
        self.assertIsNotNone(srepr)
        assert srepr is not None
        last = srepr.last()
        assert last is not None
        self.assertEqual(last.status, 7)
        self.assertEqual(last.lbn, 100)

        # replace methods
        srepr = differ.storeRepr(BeamSpotRepresentation([cur]), lambda r, c: True, r4p=True)
        self.assertIsNotNone(srepr)
        srepr = differ.storeRepr(BeamSpotRepresentation([cur]), lambda r, c: False, r4p=True)
        self.assertTrue(srepr is None)

        # no current snapshot and R4P is false - expect status 4 and reference
        srepr = differ.storeRepr(BeamSpotRepresentation(), r4p=False)
        self.assertIsNotNone(srepr)
        assert srepr is not None
        last = srepr.last()
        assert last is not None
        self.assertEqual(last.lbn, 1)
        self.assertEqual(last.status, 4)

        # no current snapshot and R4P is true - expect None
        srepr = differ.storeRepr(BeamSpotRepresentation(), r4p=True)
        self.assertTrue(srepr is None)

        # no current snapshot and R4P is None - expect None
        srepr = differ.storeRepr(BeamSpotRepresentation(), r4p=None)
        self.assertTrue(srepr is None)

        # R4P is false - expect status 4
        srepr = differ.storeRepr(BeamSpotRepresentation([cur]), r4p=False)
        self.assertIsNotNone(srepr)
        assert srepr is not None
        last = srepr.last()
        assert last is not None
        self.assertEqual(last.lbn, 100)
        self.assertEqual(last.status, 4)

        # no current snapshot and R4P is true - expect None
        srepr = differ.storeRepr(BeamSpotRepresentation([cur]), r4p=True)
        self.assertIsNotNone(srepr)
        assert srepr is not None
        last = srepr.last()
        assert last is not None
        self.assertEqual(last.lbn, 100)
        self.assertEqual(last.status, 7)

        # no current snapshot and R4P is None - expect None
        cur.status = 0
        srepr = differ.storeRepr(BeamSpotRepresentation([cur]), r4p=None)
        self.assertIsNotNone(srepr)
        assert srepr is not None
        last = srepr.last()
        assert last is not None
        self.assertEqual(last.lbn, 100)
        self.assertEqual(last.status, 7)


if __name__ == "__main__":
    unittest.main()
