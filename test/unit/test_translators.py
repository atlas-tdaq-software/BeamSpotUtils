#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.translators module
"""

from __future__ import annotations

import os
import tempfile
import unittest
from unittest.mock import MagicMock

import ers
from BeamSpotUtils import model, translators


class _WriteError(Exception):
    pass


class _ReadError(Exception):
    pass


class _TestTran(translators.Translator):
    """Simple implementation of translator for testing"""

    def __init__(self) -> None:
        self.data: model.BeamSpotRepresentation | None = None

    def read(self) -> model.BeamSpotRepresentation | None:
        if self.data is None:
            return model.BeamSpotRepresentation()
        return self.data

    def write(self, rep: model.BeamSpotRepresentation) -> None:
        self.data = rep


class _TestWriteOnlyTran(translators.Translator):
    """Simple implementation of translator for testing"""

    def __init__(self, doException: bool = False):
        self.doException = doException
        self.data: model.BeamSpotRepresentation | None = None

    def read(self) -> model.BeamSpotRepresentation | None:
        if self.doException:
            raise _ReadError("read error")
        else:
            return None

    def write(self, rep: model.BeamSpotRepresentation) -> None:
        self.data = rep


class _TestReadOnlyTran(translators.Translator):
    """Simple implementation of translator for testing"""

    def __init__(self, data: model.BeamSpotRepresentation = model.BeamSpotRepresentation()):
        self.data = data

    def read(self) -> model.BeamSpotRepresentation | None:
        return self.data

    def write(self, rep: model.BeamSpotRepresentation) -> None:
        raise _WriteError("write is not supported")


class _ISMock:

    def __init__(self) -> None:
        self.BeamSpotParameters = MagicMock()
        self.BeamSpotParametersByBunch = MagicMock()


class TestTranslators(unittest.TestCase):
    """Test case for BeamSpotUtils.rootil module.
    """

    def setUp(self) -> None:
        os.environ['TDAQ_PARTITION'] = ""

    def tearDown(self) -> None:
        pass

    def test_multi(self) -> None:
        """Test for MultiTranslator class"""

        tr1 = _TestTran()
        tr_ro = _TestReadOnlyTran()
        tr_wo = _TestWriteOnlyTran()
        tr_wo_ex = _TestWriteOnlyTran(True)

        # instantiate empty translator and fill it
        tr = translators.MultiTranslator()
        self.assertEqual(len(tr._translators), 0)
        tr.append(tr1)
        tr.append(tr_ro)
        tr.append(tr_wo)
        self.assertEqual(len(tr._translators), 3)

        # instantiate with a list
        tr = translators.MultiTranslator([tr1, tr_ro, tr_wo])
        self.assertEqual(len(tr._translators), 3)

        # test write() method
        tr = translators.MultiTranslator([tr1, tr_wo])
        rep = model.BeamSpotRepresentation()
        tr.write(rep)
        self.assertIs(tr1.data, rep)
        self.assertIs(tr_wo.data, rep)

        # test write() method with one guy raising exception
        tr = translators.MultiTranslator([tr1, tr_ro, tr_wo])
        rep = model.BeamSpotRepresentation()
        with self.assertRaises(_WriteError):
            tr.write(rep)
        self.assertIs(tr1.data, rep)
        self.assertIsNot(tr_wo.data, rep)

        # test write() method
        rep = model.BeamSpotRepresentation()
        tr1.write(rep)
        tr = translators.MultiTranslator([tr1])
        self.assertIs(tr.read(), tr1.data)
        tr = translators.MultiTranslator([tr_ro, tr1])
        self.assertIs(tr.read(), tr_ro.data)
        self.assertIsNot(tr.read(), tr1.data)

        # test read() with one guy returning None
        tr = translators.MultiTranslator([tr_wo, tr1])
        self.assertIs(tr.read(), tr1.data)

        # test read() with one guy raising exception
        tr = translators.MultiTranslator([tr_wo_ex, tr1])
        self.assertIs(tr.read(), tr1.data)

    def test_factory(self) -> None:
        """Test for make_translator method"""

        # file name *.csv should return CSVTranslator
        tran = translators.make_translator("test.csv")
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.CSVTranslator)

        # file name *.db,*.sql should return SQLiteTranslator
        tran = translators.make_translator("test.db")
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.SQLiteTranslator)

        # Any other file name (with dot in it) should give Pickle
        tran = translators.make_translator("test.anything")
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.PickleTranslator)

        # file name with tag
        tran = translators.make_translator("test.csv:sometag")
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.CSVTranslator)

        # file name with keyword tag
        tran = translators.make_translator("test.csv", tag="gat")
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.CSVTranslator)

        # test few known names that do not try to access database immediately
        tran = translators.make_translator('isctp')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.ISCTPTranslator)

        tran = translators.make_translator('ctp')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.CTPTranslator)

        tran = translators.make_translator('dummyctp')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.DummyCTPTranslator)

        tran = translators.make_translator('conctp')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.ConservativeCTPTranslator)

        tran = translators.make_translator('stdout')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.StdOutTranslator)

        tran = translators.make_translator('massi')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.MassiTranslator)

        # sqlite needs a file name
        tran = translators.make_translator('sqlite:test.xyz')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.SQLiteTranslator)
        with self.assertRaises(TypeError):
            tran = translators.make_translator('sqlite')

        tran = translators.make_translator('csv:test.xyz')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.CSVTranslator)
        with self.assertRaises(TypeError):
            tran = translators.make_translator('csv')

        tran = translators.make_translator('pickle:test.xyz')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.PickleTranslator)
        with self.assertRaises(TypeError):
            tran = translators.make_translator('pickle')

        tran = translators.make_translator('file : test.csv')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.CSVTranslator)
        with self.assertRaises(TypeError):
            tran = translators.make_translator('file')

        # these should not try to open database connection immediately
        # so it should be safe to test them
        for desc in ("off", "onhlt", "onbunch", "onmon"):
            tran = translators.make_translator(desc)
            self.assertIsNotNone(tran)
            self.assertIsInstance(tran, translators.DBTranslator)

        # unknown
        with self.assertRaises(TypeError):
            tran = translators.make_translator('not-a-thing')

        # description with a tag
        tran = translators.make_translator('stdout:tag-123')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.StdOutTranslator)

        # few of them together
        tran = translators.make_translator('stdout,csv:test.dat, sqlite:test.sqlite , onbunch ')
        self.assertIsNotNone(tran)
        self.assertIsInstance(tran, translators.MultiTranslator)
        assert isinstance(tran, translators.MultiTranslator)  # mypy
        self.assertEqual(len(tran._translators), 4)
        self.assertIsInstance(tran._translators[0], translators.StdOutTranslator)
        self.assertIsInstance(tran._translators[1], translators.CSVTranslator)
        self.assertIsInstance(tran._translators[2], translators.SQLiteTranslator)
        self.assertIsInstance(tran._translators[3], translators.DBTranslator)

        # unexpected keyword
        with self.assertRaises(TypeError):
            translators.make_translator('stdout', debug=1)

    def test_is_tran(self) -> None:
        """Test for IS translator."""

        snap0 = model.BSSnapshot(run=358333, lbn=95)
        snap1 = model.BSSnapshot(run=358333, lbn=95, bcid=1)
        snap2 = model.BSSnapshot(run=358333, lbn=95, bcid=2)
        bsrepr = model.BeamSpotRepresentation([snap0, snap1, snap2])

        # Non-BCID translator
        tran = translators.make_translator('ismon')
        self.assertIsInstance(tran, translators.ISTranslator)
        assert isinstance(tran, translators.ISTranslator)
        self.assertFalse(tran.do_bcid)
        tran._IS = _ISMock()
        tran.write(bsrepr)
        tran._IS.BeamSpotParameters.assert_called_once_with("", "BeamSpot.LiveMon")
        tran._IS.BeamSpotParametersByBunch.assert_not_called()

        tran = translators.make_translator('ismon', do_bcid=True)
        self.assertIsInstance(tran, translators.ISTranslator)
        assert isinstance(tran, translators.ISTranslator)
        self.assertTrue(tran.do_bcid)
        tran._IS = _ISMock()
        tran.write(bsrepr)
        tran._IS.BeamSpotParameters.assert_not_called()
        tran._IS.BeamSpotParametersByBunch.assert_called_once_with("", "BeamSpot.ByBunchMon")

    def test_db_tran(self) -> None:
        """Test for DBTranslator class"""
        run = 358333
        lbn = 95

        # make an object to store in a database
        snap = model.BSSnapshot(run=run, lbn=lbn)
        bsrepr = model.BeamSpotRepresentation([snap])

        with tempfile.NamedTemporaryFile() as file:
            tran = translators.make_translator('sqlite:' + file.name)
            tran.write(bsrepr)

            tran = translators.make_translator('sqlite:' + file.name, run=358333, lbn=95)
            repr = tran.read()
            self.assertIsNotNone(repr)
            assert repr is not None
            self.assertEqual(len(repr), 1)

        # With multiple BCIDs
        bsrepr = model.BeamSpotRepresentation(
            [
                model.BSSnapshot(run=run, lbn=lbn, bcid=10),
                model.BSSnapshot(run=run, lbn=lbn, bcid=20),
                model.BSSnapshot(run=run, lbn=lbn, bcid=30),
            ]
        )

        with tempfile.NamedTemporaryFile() as file:
            tran = translators.make_translator('sqlite:' + file.name)
            tran.write(bsrepr)

            tran = translators.make_translator('sqlite:' + file.name, run=run, lbn=lbn)
            repr = tran.read()
            self.assertIsNotNone(repr)
            assert repr is not None
            self.assertEqual(len(repr), 3)
            self.assertEqual({snap.bcid for snap in repr}, {10, 20, 30})

    def test_ctp_tran(self) -> None:
        """Test for CTP translator."""

        snap0 = model.BSSnapshot(run=358333, lbn=-1, status=7)
        bsrepr = model.BeamSpotRepresentation([snap0])

        tran = translators.make_translator("ctp")
        self.assertIsInstance(tran, translators.CTPTranslator)
        assert isinstance(tran, translators.CTPTranslator)
        tran.is_translator._IS = _ISMock()
        tran._ctp_commander = MagicMock()
        tran.write(bsrepr)
        tran.is_translator._IS.BeamSpotParameters.assert_called_once_with("", "BeamSpot.HLTParameters")
        tran.is_translator._IS.BeamSpotParametersByBunch.assert_not_called()
        tran._ctp_commander.update.assert_called_once_with(0)

        # Check exceptions generated in CTPUpdateConditions.
        tran._ctp_commander.update = MagicMock(side_effect=ers.Issue("unexpected error", {}, None))
        with self.assertRaisesRegex(ers.Issue, "unexpected error"):
            tran.write(bsrepr)

        # Exception with this message should be masked.
        ignored_msg = (
            "An error occurred during command: set conditions update. Reason: CommandExecutionFailed"
        )
        tran._ctp_commander.update = MagicMock(side_effect=ers.Issue(ignored_msg, {}, None))
        tran.write(bsrepr)


if __name__ == "__main__":
    unittest.main()
