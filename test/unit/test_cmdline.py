#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.cmdline module
"""

from __future__ import annotations

import logging
import unittest

from BeamSpotUtils import cmdline


class TestCmdline(unittest.TestCase):
    """Test case for BeamSpotUtils.rootil module.
    """

    def setUp(self) -> None:
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger("rootil")

    def tearDown(self) -> None:
        pass

    def test_source(self) -> None:

        opts = cmdline.getCLIOptions([])
        self.assertEqual(opts.source, ["OHCP", "COCA"])

        opts = cmdline.getCLIOptions("--source OHCP".split())
        self.assertEqual(opts.source, ["OHCP"])

        opts = cmdline.getCLIOptions("--source OHCP,COCA,MDA".split())
        self.assertEqual(opts.source, ["OHCP", "COCA", "MDA"])

    def test_files(self) -> None:

        opts = cmdline.getCLIOptions([])
        self.assertEqual(opts.fileLocation, [])

        opts = cmdline.getCLIOptions("file1".split())
        self.assertEqual(opts.fileLocation, ["file1"])

        opts = cmdline.getCLIOptions("file1 file2.root".split())
        self.assertEqual(opts.fileLocation, ["file1", "file2.root"])

    def test_noEOR(self) -> None:

        opts = cmdline.getCLIOptions([])
        self.assertFalse(opts.noEOR)

        opts = cmdline.getCLIOptions("--noEOR".split())
        self.assertTrue(opts.noEOR)

        opts = cmdline.getCLIOptions("--minLBN -5 --maxLBN -1".split())
        self.assertTrue(opts.noEOR)

        opts = cmdline.getCLIOptions("--split 1 file1".split())
        self.assertTrue(opts.noEOR)

    def test_split(self) -> None:

        opts = cmdline.getCLIOptions([])
        self.assertEqual(opts.split, -1)

        opts = cmdline.getCLIOptions("--split 1 file".split())
        self.assertEqual(opts.split, 1)

    def test_runs(self) -> None:

        opts = cmdline.getCLIOptions([])
        self.assertIsNone(opts.runNumber)

        opts = cmdline.getCLIOptions("--runNumber 1".split())
        self.assertEqual(opts.runNumber, 1)


if __name__ == "__main__":
    unittest.main()
