#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.hist_def module
"""

from __future__ import annotations

import logging
import unittest

from BeamSpotUtils.hist_def import make_hdefs_trk, make_hdefs_vtx


class TestHistDef(unittest.TestCase):
    """Test case for BeamSpotUtils.hist_def module.
    """

    def setUp(self) -> None:
        logging.basicConfig(level=logging.DEBUG)

    def tearDown(self) -> None:
        pass

    def test_make_hdefs_vtx(self) -> None:
        """Simple tests for make_hdefs_vtx() method
        """

        expectedNames = {
            "posX", "posY", "posZ",
            "deltaX", "deltaY", "deltaZ",
            "tiltX", "tiltY",
            "posX_vs_ntrks", "posY_vs_ntrks", "posZ_vs_ntrks",
            "splitX_vs_ntrks", "splitY_vs_ntrks", "splitZ_vs_ntrks",
        }

        expectedBCIDNames = {
            "deltaX_bcid", "deltaY_bcid", "deltaZ_bcid",
            "posX_bcid", "posY_bcid", "posZ_bcid",
        }

        for doBCID in (False, True):
            with self.subTest(f"doBCID={doBCID}"):
                hdefs = make_hdefs_vtx(do_bcid=doBCID)
                self.assertEqual(len(hdefs), 20 if doBCID else 14)
                self.assertEqual(any(hdef.is_bcid for hdef in hdefs), doBCID)
                allNames = set(hdef.name for hdef in hdefs)
                expect = expectedNames
                if doBCID:
                    expect |= expectedBCIDNames
                self.assertEqual(allNames, expect)
                self.assertEqual(set(hdef.name for hdef in hdefs if hdef.is_rs_res),
                                 {"splitX_vs_ntrks", "splitY_vs_ntrks", "splitZ_vs_ntrks"})

    def test_make_hdefs_trk(self) -> None:
        """Simple tests for make_hdefs_trk() method
        """

        expectedNames = {"llpoly", "beam_ls"}
        expectedBCIDNames = {"beam_ls_bcid"}

        for doBCID in (False, True):
            with self.subTest(f"doBCID={doBCID}"):
                hdefs = make_hdefs_trk(do_bcid=doBCID)
                self.assertEqual(len(hdefs), 3 if doBCID else 2)
                self.assertEqual(any(hdef.is_bcid for hdef in hdefs), doBCID)
                allNames = set(hdef.name for hdef in hdefs)
                expect = expectedNames
                if doBCID:
                    expect |= expectedBCIDNames
                self.assertEqual(allNames, expect)


if __name__ == "__main__":
    unittest.main()
