#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.htree module
"""

from __future__ import annotations

import logging
import unittest

from BeamSpotUtils import htree
from BeamSpotUtils.test import _mocks


class TestHtree(unittest.TestCase):
    """Test case for BeamSpotUtils.htree module.
    """

    def setUp(self) -> None:
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger("htree_test")

    def tearDown(self) -> None:
        pass

    def test_variant(self) -> None:
        """testing HTreeVariant class"""
        runs = [
            (0, "A"),
            (188800, "A"),
            (188801, "B"),
            (222222, "B"),
            (333333, "B"),
            (374999, "B"),
            (375000, "R3"),
            (1000000, "R3"),
            (1000000000, "R3"),
        ]
        for run, variant in runs:
            self.assertEqual(htree.HTreeVariant.from_run(run), htree.HTreeVariant[variant])

    def test_guess(self) -> None:
        """testing guess() method"""

        server, provider = "Histogramming", "TopMIG-OH_HLT"
        detail, chain = "EXPERT", "T2VertexBeamSpot_FTF"

        for variant in htree.HTreeVariant:
            with self.subTest(variant=variant):

                tfile = _mocks.make_mda_tree(
                    server=server, provider=provider, detail=detail, chain=chain, run=1000, variant=variant
                )
                self.assertEqual(htree._guess_htree_type(tfile, server, chain), htree.HTreeType.MDA)

                tfile = _mocks.make_mda_tree(
                    server=server, provider=provider, detail=detail, chain=chain, run=1000, variant=variant,
                    meta=True
                )
                self.assertEqual(htree._guess_htree_type(tfile, server, chain), htree.HTreeType.MDA)

                tfile = _mocks.make_ohcp_tree(
                    server=server, provider=provider, detail=detail, chain=chain, run=1000, variant=variant
                )
                self.assertEqual(htree._guess_htree_type(tfile, detail, chain), htree.HTreeType.OHCP_OFF)

                tfile = _mocks.make_offline_lbn_tree(detail=detail, chain=chain, lbn=101, variant=variant)
                tree = htree._guess_htree_type(tfile, server, chain)
                if variant is not htree.HTreeVariant.R3:
                    # lbn-monitoring is empty in R3
                    self.assertEqual(tree, htree.HTreeType.OFFLINE_LBN)
                else:
                    self.assertIsNone(tree)

                tfile = _mocks.make_offline_tree(chain=chain, variant=variant)
                self.assertEqual(htree._guess_htree_type(tfile, server, chain), htree.HTreeType.OFFLINE)

        tfile = _mocks.make_empty_tree()
        self.assertIsNone(htree._guess_htree_type(tfile, server, chain))

    def test_factory(self) -> None:
        """testing factory method"""

        server, provider = "Histogramming", "TopMIG-OH_HLT"
        detail, chain = "EXPERT", "T2VertexBeamSpot_FTF"

        for variant in htree.HTreeVariant:
            with self.subTest(variant=variant):

                tfile = _mocks.make_mda_tree(
                    server=server, provider=provider, detail=detail, chain=chain, run=1000, variant=variant
                )
                self.assertIsInstance(htree.make_htree(tfile, server, provider, detail, chain),
                                      htree.MDAHTree)

                tfile = _mocks.make_mda_tree(
                    server=server, provider=provider, detail=detail, chain=chain, run=1000, variant=variant,
                    meta=True
                )
                self.assertIsInstance(htree.make_htree(tfile, server, provider, detail, chain),
                                      htree.MDAHTree)

                tfile = _mocks.make_ohcp_tree(
                    server=server, provider=provider, detail=detail, chain=chain, run=1000, variant=variant
                )
                self.assertIsInstance(htree.make_htree(tfile, server, provider, detail, chain),
                                      htree.OHCPHTree)

                tfile = _mocks.make_offline_lbn_tree(detail=detail, chain=chain, lbn=101, variant=variant)
                tree = htree.make_htree(tfile, server, provider, detail, chain)
                if variant is not htree.HTreeVariant.R3:
                    # lbn-monitoring is empty in R3
                    self.assertIsInstance(tree, htree.OfflineLbnHTree)
                else:
                    self.assertIsNone(tree)

                tfile = _mocks.make_offline_tree(chain=chain, variant=variant)
                self.assertIsInstance(htree.make_htree(tfile, server, provider, detail, chain),
                                      htree.OfflineHTree)

        tfile = _mocks.make_empty_tree()
        self.assertIsNone(htree.make_htree(tfile, server, provider, detail, chain))

    def test_mda_htree(self) -> None:
        """test for MDAHTree class"""

        server, provider = "Histogramming", "TopMIG-OH_HLT"
        detail, chain = "EXPERT", "T2VertexBeamSpot_FTF"

        lbs = [10, 15, 30]

        for variant in htree.HTreeVariant:
            with self.subTest(variant=variant):

                tfile = _mocks.make_mda_tree(
                    server=server, provider=provider, detail=detail, chain=chain, run=1000,
                    variant=variant, lbs=lbs)

                has_runsummary = variant is not htree.HTreeVariant.R3

                # test how it works with explicitly-defined server/provider
                hist_tree = htree.make_htree(tfile, server, provider, detail, chain)
                assert hist_tree is not None  # mypy
                self.assertIsInstance(hist_tree, htree.MDAHTree)
                self._correct_htree_structure(hist_tree, lbs, variant=variant, has_runsummary=has_runsummary)

                # use non-existing provider, should be able to find LBs but histogram
                # retrieval should fail
                hist_tree = htree.make_htree(tfile, server, "not-a-provider", detail, chain)
                assert hist_tree is not None  # mypy
                self.assertEqual(sorted(hist_tree.get_lbns()), lbs)

                obj = hist_tree.get_histogram("posX", lbn=None)
                self.assertIsNone(obj)

                obj = hist_tree.get_histogram("posX", lbn=10)
                self.assertIsNone(obj)

                # use non-existing server, this should fail with MDA
                hist_tree = htree.make_htree(tfile, "not-a-server", provider, detail, chain)
                self.assertIsNone(hist_tree)

    def test_ohcp_htree(self) -> None:
        """test for OHCPHTree class"""

        server, provider = "Histogramming", "TopMIG-OH_HLT"
        detail, chain = "EXPERT", "T2VertexBeamSpot_FTF"

        lbs = [10, 15, 30]

        for variant in htree.HTreeVariant:
            with self.subTest(variant=variant):

                tfile = _mocks.make_ohcp_tree(
                    server=server, provider=provider, detail=detail, chain=chain, run=1000,
                    lbs=lbs, variant=variant
                )

                has_runsummary = variant is not htree.HTreeVariant.R3

                # test how it works with explicitely-defined server/provider
                hist_tree = htree.make_htree(tfile, server, provider, detail, chain)
                assert hist_tree is not None  # mypy
                self.assertIsInstance(hist_tree, htree.OHCPHTree)
                self._correct_htree_structure(hist_tree, lbs, variant=variant, has_runsummary=has_runsummary)

                # use non-existing provider
                hist_tree = htree.make_htree(tfile, server, "not-a-provider", detail, chain)
                assert hist_tree is not None  # mypy
                self.assertEqual(sorted(hist_tree.get_lbns()), lbs)

                obj = hist_tree.get_histogram("posX", lbn=None)
                self.assertIsNone(obj)

                obj = hist_tree.get_histogram("VertexXPass", lbn=10)
                self.assertIsNone(obj)

                # use non-existing server
                hist_tree = htree.make_htree(tfile, "not-a-server", provider, detail, chain)
                assert hist_tree is not None  # mypy
                self.assertEqual(sorted(hist_tree.get_lbns()), lbs)

                obj = hist_tree.get_histogram("VertexXPass", lbn=None)
                self.assertIsNone(obj)

                obj = hist_tree.get_histogram("VertexXPass", lbn=10)
                self.assertIsNone(obj)

    def test_hltlbn_htree(self) -> None:
        """test for OfflineLbnHTree class"""

        server, provider = "Histogramming", "TopMIG-OH_HLT"  # not really used but needed by factory
        detail, chain = "EXPERT", "T2VertexBeamSpot_FTF"

        lbn = 30

        for variant in htree.HTreeVariant:
            with self.subTest(variant=variant):
                tfile = _mocks.make_offline_lbn_tree(detail=detail, chain=chain, lbn=lbn, variant=variant)

                hist_tree = htree.make_htree(tfile, server, provider, detail, chain)
                if variant is htree.HTreeVariant.R3:
                    self.assertIsNone(hist_tree)
                else:
                    assert hist_tree is not None  # mypy
                    self.assertIsInstance(hist_tree, htree.OfflineLbnHTree)
                    self._correct_htree_structure(hist_tree, [lbn], has_runsummary=False, variant=variant)

    def test_offline_htree(self) -> None:
        """test for OfflineTree class"""

        server, provider = "Histogramming", "TopMIG-OH_HLT"
        detail, chain = "EXPERT", "T2VertexBeamSpot_FTF"

        for variant in htree.HTreeVariant:

            # for run3 LBN is used to store histograms
            lbs = []
            if variant is htree.HTreeVariant.R3:
                lbs = [95]

            with self.subTest(variant=variant):
                tfile = _mocks.make_offline_tree(chain=chain, variant=variant)

                # test how it works with explicitly-defined server/provider
                hist_tree = htree.make_htree(tfile, server, provider, detail, chain)
                assert hist_tree is not None  # mypy
                self.assertIsInstance(hist_tree, htree.OfflineHTree)
                has_runsummary = variant is not htree.HTreeVariant.R3
                self._correct_htree_structure(
                    hist_tree, lbs, has_runsummary=has_runsummary, variant=variant)

                # use non-existing provider, does not matter for offline
                hist_tree = htree.make_htree(tfile, server, "not-a-provider", detail, chain)
                self.assertIsNotNone(hist_tree)
                assert hist_tree is not None  # mypy
                self.assertEqual(sorted(hist_tree.get_lbns()), lbs)

                obj = hist_tree.get_histogram("posX", lbn=None)
                if variant is htree.HTreeVariant.R3:
                    self.assertIsNone(obj)
                else:
                    self.assertIsNotNone(obj)

                obj = hist_tree.get_histogram("posX", lbn=10)
                self.assertIsNone(obj)

                # use non-existing server, does not matter for offline
                hist_tree = htree.make_htree(tfile, "not-a-server", provider, detail, chain)
                self.assertIsNotNone(hist_tree)

    def test_mem_htree(self) -> None:
        """test for MemoryHTree class"""

        hist_tree = htree.MemoryHTree[_mocks.tobject]()
        histo = _mocks.tobject("name")

        self.assertEqual(hist_tree.get_lbns(), [])

        for lbn in (1, 2, 3):
            hist_tree.add_histogram("posX", histo, lbn)

        self.assertCountEqual(hist_tree.get_lbns(), [1, 2, 3])
        self.assertIs(hist_tree.get_histogram("posX", 1), histo)
        self.assertIs(hist_tree.get_histogram("posX", 2), histo)
        self.assertIs(hist_tree.get_histogram("posX", 3), histo)
        self.assertIsNone(hist_tree.get_histogram("posX", 4))
        self.assertIsNone(hist_tree.get_histogram("posX"))

        hist_tree.add_histogram("posX", histo)
        self.assertCountEqual(hist_tree.get_lbns(), [1, 2, 3])
        self.assertIs(hist_tree.get_histogram("posX"), histo)

        # all added histos are run-summary
        hist_tree = htree.MemoryHTree(no_lbns=True)
        self.assertEqual(hist_tree.get_lbns(), [])

        for lbn in (1, 2, 3):
            hist_tree.add_histogram("posX", histo, lbn)

        self.assertEqual(hist_tree.get_lbns(), [])
        self.assertIs(hist_tree.get_histogram("posX", 1), histo)
        self.assertIs(hist_tree.get_histogram("posX", 100), histo)
        self.assertIs(hist_tree.get_histogram("posX"), histo)

    def _correct_htree_structure(self, hist_tree: htree.HTree, lbs: list[int], *,
                                 variant: htree.HTreeVariant, has_runsummary: bool = True) -> None:
        """Check that tree structure looks reasonable.
        """

        self.assertEqual(sorted(hist_tree.get_lbns()), lbs)

        obj = hist_tree.get_histogram("posX", lbn=None)
        if has_runsummary:
            self.assertIsNotNone(obj)
            self.assertEqual(obj.GetName(), "VertexXPass_runsummary")  # type: ignore
        else:
            # run3 folders miss LB subfolder and per-LBN histograms can be
            # found as runsummary.
            if variant is not htree.HTreeVariant.R3:
                self.assertIsNone(obj)

        # one special histogram which has both per-LBN and non-LBN copies with
        # reversed names (A_vs_B and B_vs_A)
        obj = hist_tree.get_histogram("posZ_vs_ntrks", lbn=None)
        if has_runsummary:
            self.assertIsNotNone(obj)
            if variant is htree.HTreeVariant.R3:
                name = "VertexZPass_vs_VertexNTrksPass"
                if has_runsummary:
                    name += "_runsummary"
                self.assertEqual(obj.GetName(), name)  # type: ignore
            elif variant in (htree.HTreeVariant.A, htree.HTreeVariant.B):
                name = "VertexNTrksPass_vs_VertexZPass"
                if has_runsummary:
                    name += "_runsummary"
                self.assertEqual(obj.GetName(), name)  # type: ignore
        else:
            if variant is not htree.HTreeVariant.R3:
                self.assertIsNone(obj)

        if 10 in lbs:
            obj = hist_tree.get_histogram("posX", lbn=10)
            self.assertIsNotNone(obj)
            self.assertIn(obj.GetName(), ["VertexXPass", "VertexXPass_LB10"])  # type: ignore

        if 15 in lbs:
            obj = hist_tree.get_histogram("posX", lbn=15)
            self.assertIsNotNone(obj)
            self.assertIn(obj.GetName(), ["VertexXPass", "VertexXPass_LB15"])  # type: ignore

        if 1 not in lbs:
            obj = hist_tree.get_histogram("posX", lbn=1)
            self.assertIsNone(obj)

        obj = hist_tree.get_histogram("posX_extra", lbn=10)
        self.assertIsNone(obj)


if __name__ == "__main__":
    unittest.main()
