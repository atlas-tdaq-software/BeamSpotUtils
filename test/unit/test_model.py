#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.model module
"""

from __future__ import annotations

import logging
import unittest

from BeamSpotUtils import model


class TestModel(unittest.TestCase):
    """Test case for BeamSpotUtils.model module.
    """

    def setUp(self) -> None:
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger(__name__)

    def tearDown(self) -> None:
        pass

    def test_snap_ctor(self) -> None:
        """Test for BSSnapshot constructor
        """

        snap = model.BSSnapshot()

        self.assertEqual(snap.status, model.STATUS_NOT_CONVERGED)
        self.assertEqual(snap.run, 0)
        self.assertEqual(snap.lbn, 0)
        self.assertEqual(snap.bcid, -1)

        keys = [
            "posX", "posY", "posZ", "posXErr", "posYErr", "posZErr",
            "sigmaX", "sigmaY", "sigmaZ", "sigmaXErr", "sigmaYErr", "sigmaZErr",
            "tiltX", "tiltY", "tiltXErr", "tiltYErr",
            "sigmaXY", "sigmaXYErr",
        ]
        for key in keys:
            self.assertEqual(getattr(snap, key), 0.0)
        keys = [
            "unCorrX", "unCorrY", "unCorrZ", "unCorrXErr", "unCorrYErr", "unCorrZErr",
            "deltaX", "deltaY", "deltaZ", "deltaXErr", "deltaYErr", "deltaZErr",
            "resX", "resY", "resZ", "resXErr", "resYErr", "resZErr",
        ]
        for key in keys:
            self.assertIsNone(getattr(snap, key))

        data = dict(run=10, posX=10.)
        snap = model.BSSnapshot(data)
        self.assertEqual(snap.run, 10)
        self.assertEqual(snap.posX, 10.)

        snap = model.BSSnapshot(run=10, posX=10.)
        self.assertEqual(snap.run, 10)
        self.assertEqual(snap.posX, 10.)

        snap = model.BSSnapshot(data, run=20, posY=20.)
        self.assertEqual(snap.run, 20)
        self.assertEqual(snap.posX, 10.)
        self.assertEqual(snap.posY, 20.)

    def test_snap_ctor_fail(self) -> None:
        """Testing constructor exceptions for BSSnapshot
        """
        data = dict(nvtx=10, otherX=10.)
        with self.assertRaises(ValueError):
            model.BSSnapshot(data, data)
        with self.assertRaises(KeyError):
            model.BSSnapshot(data)
        with self.assertRaises(KeyError):
            model.BSSnapshot(**data)

    def test_snap_emptyPhysicsField(self) -> None:
        """Testing BSSnapshot.emptyPhysicsField() method
        """
        snap = model.BSSnapshot()
        self.assertTrue(snap.emptyPhysicsField('posX'))
        snap = model.BSSnapshot(posX=0.01)
        self.assertFalse(snap.emptyPhysicsField('posX'))
        snap = model.BSSnapshot(posXErr=0.01)
        self.assertFalse(snap.emptyPhysicsField('posX'))
        snap = model.BSSnapshot(posX=0.01, posXErr=0.01)
        self.assertFalse(snap.emptyPhysicsField('posX'))

    def test_snap_metrics(self) -> None:
        """Testing BSSnapshot.metric() method
        """
        snap1 = model.BSSnapshot(run=100, lbn=1)
        snap2 = model.BSSnapshot(run=100, lbn=2)
        self.assertLess(snap1.metric(), snap2.metric())

        snap1 = model.BSSnapshot(run=100, lbn=1)
        snap2 = model.BSSnapshot(run=101, lbn=1)
        self.assertLess(snap1.metric(), snap2.metric())

    def test_snap_attributes(self) -> None:
        """Test for attribute assignment, reject unexpected names.
        """
        snap = model.BSSnapshot()

        snap.posX = 1.
        self.assertEqual(snap.posX, 1.)

        with self.assertRaises(AttributeError):
            snap.newKeyX = 2.

    def test_slice(self) -> None:
        """Testing SliceView class
        """
        snap = model.BSSnapshot()
        snapx = snap.slice('X')

        self.assertTrue(hasattr(snapx, 'pos'))
        self.assertFalse(hasattr(snapx, 'pospos'))

        self.assertEqual(snap.posX, 0.0)
        self.assertEqual(snapx.pos, 0.0)
        snap.posX = 100.
        self.assertEqual(snapx.pos, 100.)
        snapx.pos = 200.
        self.assertEqual(snap.posX, 200.)

        self.assertEqual(snap.posXErr, 0.0)
        self.assertEqual(snapx.posErr, 0.0)
        snap.posXErr = 1.
        self.assertEqual(snapx.posErr, 1.)
        snapx.posErr = 2.
        self.assertEqual(snap.posXErr, 2.)

        # get non-exisint aggtibute
        with self.assertRaises(AttributeError):
            _ = snapx.nonExistent

        # set non-exisint aggtibute
        with self.assertRaises(AttributeError):
            snapx.newKey = 10

    def test_bsrep_ctor(self) -> None:
        """Test for BeamSpotRepresentation constructor
        """
        bsrep = model.BeamSpotRepresentation()
        self.assertEqual(len(bsrep), 0)

        # flat list of several snaps
        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(run=1, lbn=10),
            model.BSSnapshot(run=1, lbn=20),
            model.BSSnapshot(run=1, lbn=30),
        ])
        self.assertEqual(len(bsrep), 3)
        self.assertEqual(bsrep[0].lbn, 10)
        self.assertEqual(bsrep[1].lbn, 20)
        self.assertEqual(bsrep[2].lbn, 30)

        # snap with embedded BCID-snaps
        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(run=1, lbn=10, bcids=[
                model.BSSnapshot(run=2, lbn=20, bcid=1),
                model.BSSnapshot(run=2, lbn=30, bcid=5),
            ]),
        ])
        self.assertEqual(len(bsrep), 3)
        for i in range(3):
            self.assertEqual(bsrep[i].run, 1)
            self.assertEqual(bsrep[i].lbn, 10)
        self.assertEqual(bsrep[0].bcid, -1)
        self.assertEqual(bsrep[1].bcid, 1)
        self.assertEqual(bsrep[2].bcid, 5)

    def test_bsrep_iter(self) -> None:
        """Test for BeamSpotRepresentation iteration
        """
        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(run=1, lbn=10),
            model.BSSnapshot(run=1, lbn=20),
            model.BSSnapshot(run=1, lbn=30),
        ])
        self.assertEqual(len(bsrep), 3)
        itr = iter(bsrep)

        snap = next(itr)
        self.assertEqual(snap.lbn, 10)
        snap = next(itr)
        self.assertEqual(snap.lbn, 20)
        snap = next(itr)
        self.assertEqual(snap.lbn, 30)
        with self.assertRaises(StopIteration):
            next(itr)

    def test_bsrep_groupBy(self) -> None:
        """Test for BeamSpotRepresentation groupBy method
        """
        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(run=1, lbn=10, bcid=1),
            model.BSSnapshot(run=1, lbn=10, bcid=10),
            model.BSSnapshot(run=1, lbn=10),
            model.BSSnapshot(run=1, lbn=20),
            model.BSSnapshot(run=1, lbn=20, bcid=20),
            model.BSSnapshot(run=1, lbn=30),
        ])
        self.assertEqual(len(bsrep), 6)

        groups = bsrep.groupBy("lbn")
        self.assertEqual(set(groups.keys()), set([10, 20, 30]))

        self.assertEqual(len(groups[10]), 3)
        self.assertEqual(groups[10][0].bcid, 1)
        self.assertEqual(groups[10][1].bcid, 10)
        self.assertEqual(groups[10][2].bcid, -1)

        self.assertEqual(len(groups[20]), 2)
        self.assertEqual(groups[20][0].bcid, -1)
        self.assertEqual(groups[20][1].bcid, 20)

        self.assertEqual(len(groups[30]), 1)
        self.assertEqual(groups[30][0].bcid, -1)

        for key, rep in groups.items():
            for snap in rep:
                self.assertEqual(snap.lbn, key)

    def test_bsrep_last(self) -> None:
        """Test for BeamSpotRepresentation last() method
        """
        bsrep = model.BeamSpotRepresentation()
        snap = bsrep.last()
        self.assertIsNone(snap)

        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(run=1, lbn=30),
            model.BSSnapshot(run=1, lbn=10),
            model.BSSnapshot(run=1, lbn=20),
        ])
        snap = bsrep.last()
        assert snap is not None
        self.assertEqual(snap.lbn, 30)

        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(run=1, lbn=30),
            model.BSSnapshot(run=2, lbn=10),
            model.BSSnapshot(run=1, lbn=20),
        ])
        snap = bsrep.last()
        assert snap is not None
        self.assertEqual(snap.run, 2)
        self.assertEqual(snap.lbn, 10)

    def test_bsrep_needsDeltaToPosition(self) -> None:
        """Test for BeamSpotRepresentation needsDeltaToPosition() method
        """
        bsrep = model.BeamSpotRepresentation()
        self.assertFalse(bsrep.needsDeltaToPosition())

        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(),
        ])
        self.assertFalse(bsrep.needsDeltaToPosition())

        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(deltaX=0.01),
        ])
        self.assertTrue(bsrep.needsDeltaToPosition())

        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(deltaXErr=0.01),
        ])
        self.assertTrue(bsrep.needsDeltaToPosition())

        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(deltaX=0.01, deltaXErr=0.01),
        ])
        self.assertTrue(bsrep.needsDeltaToPosition())

        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(deltaX=0.01, deltaXErr=0.01),
            model.BSSnapshot(posX=0.01, posXErr=0.01),
        ])
        self.assertTrue(bsrep.needsDeltaToPosition())

        bsrep = model.BeamSpotRepresentation([
            model.BSSnapshot(posX=0.01, posXErr=0.01),
        ])
        self.assertFalse(bsrep.needsDeltaToPosition())


if __name__ == "__main__":
    unittest.main()
