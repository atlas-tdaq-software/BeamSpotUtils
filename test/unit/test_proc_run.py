#!/usr/bin/env tdaq_python

"""Unit test for BeamSpotUtils.proc_run module
"""

from __future__ import annotations

import logging
import unittest
from collections.abc import Mapping

from BeamSpotUtils.bscalc import BSCalc
from BeamSpotUtils.htree import HTree
from BeamSpotUtils.proc_run import SingleRun, SingleRunConfig
from BeamSpotUtils.model import BeamSpotRepresentation, BSSnapshot, STATUS_OK


class BSCalcMock(BSCalc):
    """Simple mock for BSCalc"""
    def calc(self, hist_trees: Mapping[int, HTree],
             run: int | None = None,
             lbn: int | None = None) -> BSSnapshot | None:
        return None

    def finish(self) -> None:
        pass


class TestProcRun(unittest.TestCase):
    """Test case for BeamSpotUtils.proc_run module.
    """

    def setUp(self) -> None:
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger("proc_run")

    def tearDown(self) -> None:
        pass

    def _makeProcRun(self, config: SingleRunConfig,
                     run_number: int = 0, output_dir: str = "") -> SingleRun:

        def ready4physics() -> bool:
            return True

        def emscan() -> bool:
            return True

        return SingleRun(
            config=config,
            bscalc_vertex=BSCalcMock(),
            bscalc_track=BSCalcMock(),
            run_number=run_number,
            emscan=emscan,
            ready4physics=ready4physics,
            output_dir=output_dir,
        )

    def test_choseBS(self) -> None:
        """Test for _choseBS method"""

        bsVtxEmpty = BeamSpotRepresentation()
        bsVtx = BeamSpotRepresentation([BSSnapshot(status=STATUS_OK)])
        bsTrkEmpty = BeamSpotRepresentation()
        bsTrk = BeamSpotRepresentation([BSSnapshot(status=STATUS_OK)])

        config = SingleRunConfig()
        procRun = self._makeProcRun(config=config)

        # For now it prefers vertex-based unless it's None or empty and
        # track-based one is non-empty
        bs = procRun._choseBS(bsVtxEmpty, bsTrkEmpty)
        self.assertIs(bs, bsVtxEmpty)
        bs = procRun._choseBS(None, bsTrkEmpty)
        self.assertIsNone(bs)
        bs = procRun._choseBS(bsVtxEmpty, None)
        self.assertIs(bs, bsVtxEmpty)
        bs = procRun._choseBS(bsVtx, None)
        self.assertIs(bs, bsVtx)
        bs = procRun._choseBS(None, None)
        self.assertIsNone(bs)

        # if track-based one is non-empty then it is always returned
        bs = procRun._choseBS(bsVtx, None)
        self.assertIs(bs, bsVtx)
        bs = procRun._choseBS(bsVtx, bsTrkEmpty)
        self.assertIs(bs, bsVtx)
        bs = procRun._choseBS(bsVtx, bsTrk)
        self.assertIs(bs, bsVtx)

        # second is returned when it's not empty but first is
        bs = procRun._choseBS(bsVtxEmpty, bsTrk)
        self.assertIs(bs, bsTrk)
        bs = procRun._choseBS(None, bsTrk)
        self.assertIs(bs, bsTrk)


if __name__ == "__main__":
    unittest.main()
