#!/bin/bash
#
# $Id$
#

run=${1:-200000}
lbn=${2:-100}

export TDAQ_PARTITION=initial

dbfile="BeamSpotUtils_test.db"
connection="sqlite://;schema=BeamSpotUtils_test.db;dbname=CONDBR2"
folder='/Indet/Onl/Beampos'
tag='IndetBeamposOnl-HLT-UPD1-000-00'

trap "rm -f $dbfile" 0 1 2 3 15

echo "Starting IS server in $TDAQ_PARTITION partition..."
is_server -p $TDAQ_PARTITION -n BeamSpot &
sleep 2 # horrific race condition

echo "Writing beam spot parameters to IS..."
beamSpotISWriterApp 7 0.070 1.100 -5. 0.050 0.050 60. 0.00040 -7.e-05 0. 0.001 0.001 0.001 0.01 0.01 0.01 0.000001 0.000001 ||
{ echo $?; exit $?; }

echo "Transferring beam spot parameters from IS to COOL..."
beamSpotIS2CoolApp $run $lbn $connection $folder $tag ||
{ echo $?; exit $?; }

echo "Reading beam spot parameters from file..."
# beamSpotCoolReaderApp $run $lbn $connection $tag ||
testCoolReaderApp $run $lbn $connection $folder $tag ||
{ echo $?; exit $?; }


echo "Killing IS server..."
kill %%
sleep 2

echo
exit 0
