#!/usr/bin/env tdaq_python
"""
PartitionMaker script to generate standalone partition which includes BeamSpot
segment few test applications to simulate running at P1. One of the applications
is LumiBlock generator which updates LumiBlock IS object. Another application is
OH provider which reads HLT-produced BeamSpot histograms from MDA file and sends
them to OH server (to be read by BST).
"""

import argparse
from configparser import ConfigParser
import os
import sys

import pm
import pm.project
from config.dal import module as dal_module
from BeamSpotUtils.test import pm_bs


# default values for some parameters

# host name where to run partition
_host_name = os.environ.get('HOSTNAME', '')

# Tag name
_tag_name = os.environ['CMTCONFIG']


def main() -> int:
    """run PM to create partition for BeamSpot
    """

    parser = argparse.ArgumentParser(description='Generate segment or partition for BeamSpot')
    parser.add_argument('-c', '--config', metavar='PATH', type=argparse.FileType(),
                        required=True, help='Configuration file')
    parser.add_argument('output_file', help='The name of the output file')
    args = parser.parse_args()

    output_file = args.output_file

    config = ConfigParser(dict(Tag=_tag_name,
                               Host=_host_name))
    config.read_file(args.config)

    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    includes = ['daq/schema/core.schema.xml']

    tag = config.get("DEFAULT", "Tag")
    try:
        tags = pm.project.Project("daq/sw/tags.data.xml")
        tag_dal = tags.getObject("Tag", tag)
        includes += ["daq/sw/tags.data.xml"]
    except RuntimeError:
        tag_dal = dal.Tag(tag)

    # Computer
    host = config.get("DEFAULT", "Host")
    if host:
        try:
            hosts = pm.project.Project("daq/hw/hosts.data.xml")
            host = hosts.getObject("Computer", host)
            comp = host
            includes += ["daq/hw/hosts.data.xml"]
        except RuntimeError:
            hw_tag = '-'.join(tag.split('-')[:2])
            comp = dal.Computer(host, HW_Tag=hw_tag)

    scripts = pm_bs.get_scripts(config, tag_dal)
    includes += scripts.get('includes', [])
    bs_sw_repo = scripts['sw_repo']

    configs, cinc = pm_bs.make_configs(config)
    includes += cinc

    bs_seg, sinc = pm_bs.make_segment(config, scripts, output_file, tag_dal, comp)
    includes += sinc

    hi_seg, sinc = pm_bs.make_histo_segment(config, comp, bs_sw_repo)
    includes += sinc

    part, pinc = pm_bs.make_partition([bs_seg, hi_seg], config, tag_dal, comp)
    includes += pinc

    includes.sort()
    save_db = pm.project.Project(output_file, includes)
    save_db.addObjects([part] + configs)

    return 0


if __name__ == '__main__':
    sys.exit(main())
