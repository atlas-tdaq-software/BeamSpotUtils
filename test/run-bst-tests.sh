#!/bin/bash

#
# Run all tests in /eos/atlas/atlascerngroupdisk/trig-daq/online-beamspot/bst-tests
#

test_dir=/eos/atlas/atlascerngroupdisk/trig-daq/online-beamspot/bst-tests
log=run-bst-tests.log

failed=""
for test in $test_dir/[0-9]*.sh; do
    name=$(basename $test)
    echo Running test $name...
    if $test >>$log 2>&1; then
        echo Success
    else
        echo failed
        failed="$failed $name"
    fi
done

if [ -n "$failed" ]; then
    echo "Failed tests:$failed"
    echo "Check log file $log for details"
    exit 1
fi
