#ifndef DQM_ALGORITHMS_CORRWIDTH_H
#define DQM_ALGORITHMS_CORRWIDTH_H

#include <string>
#include <TH1D.h>
#include <TH2F.h>

#include "dqm_core/Algorithm.h"


namespace dqm_algorithms{

	class CorrWidth : public dqm_core::Algorithm {
	public:

		CorrWidth();
		CorrWidth( const CorrWidth& other );
  
		virtual ~CorrWidth();
		virtual dqm_core::Algorithm*  clone();
		virtual dqm_core::Result*     execute( const std::string& name, const TObject& data,
																					 const dqm_core::AlgorithmConfig& config );
		virtual void                  printDescription();

	protected:
		
		int m_nRMS;
		int m_axis;
		float m_factor;
		int m_binMin;
		int m_binMax;
		std::string  name;
		std::string  limitName;
		
	private:
		
		TH1D * fitSlices(TH2 * h2);
		void calcCorr(float p, float dp, float s, float ds, float &corr, float &dcorr);
		TH1D * calcCorrVTrks(TH1D * p, TH1D * s);
		
		double GetFirstFromMap( const std::string &paramName, const std::map<std::string, double > &params, double defaultValue);
		
	};
} //namespace dqm_algorithms

#endif // DQM_ALGORITHMS_CORRWIDTH_H
