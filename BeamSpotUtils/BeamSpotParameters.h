//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_BEAMSPOTPARAMETERS_H
#define DAQ_BEAMSPOTPARAMETERS_H

#include <iostream>
#include <vector>
#include <string>

namespace daq
{

  class BeamSpotParameters
  {
  public:

    // Constructor
  BeamSpotParameters()
      :  m_status   (0 )
      ,    m_posX   (0.)
      ,    m_posY   (0.)
      ,    m_posZ   (0.)
      ,  m_sigmaX   (0.)
      ,  m_sigmaY   (0.)
      ,  m_sigmaZ   (0.)
      ,   m_tiltX   (0.)
      ,   m_tiltY   (0.)
      , m_sigmaXY   (0.)
      ,    m_posXErr(0.)
      ,    m_posYErr(0.)
      ,    m_posZErr(0.)
      ,  m_sigmaXErr(0.)
      ,  m_sigmaYErr(0.)
      ,  m_sigmaZErr(0.)
      ,   m_tiltXErr(0.)
      ,   m_tiltYErr(0.)
      , m_sigmaXYErr(0.)
      {}

    // Constructor
  BeamSpotParameters( int   status    ,
                      float posX      ,
                      float posY      ,
                      float posZ      ,
                      float sigmaX    ,
                      float sigmaY    ,
                      float sigmaZ    ,
                      float tiltX     ,
                      float tiltY     ,
                      float sigmaXY   ,
                      float posXErr   ,
                      float posYErr   ,
                      float posZErr   ,
                      float sigmaXErr ,
                      float sigmaYErr ,
                      float sigmaZErr ,
                      float tiltXErr  ,
                      float tiltYErr  ,
                      float sigmaXYErr )
      :  m_status   ( status     )
      ,    m_posX   ( posX       )
      ,    m_posY   ( posY       )
      ,    m_posZ   ( posZ       )
      ,  m_sigmaX   ( sigmaX     )
      ,  m_sigmaY   ( sigmaY     )
      ,  m_sigmaZ   ( sigmaZ     )
      ,   m_tiltX   ( tiltX      )
      ,   m_tiltY   ( tiltY      )
      , m_sigmaXY   ( sigmaXY    )
      ,    m_posXErr( posXErr    )
      ,    m_posYErr( posYErr    )
      ,    m_posZErr( posZErr    )
      ,  m_sigmaXErr( sigmaXErr  )
      ,  m_sigmaYErr( sigmaYErr  )
      ,  m_sigmaZErr( sigmaZErr  )
      ,   m_tiltXErr( tiltXErr   )
      ,   m_tiltYErr( tiltYErr   )
      , m_sigmaXYErr( sigmaXYErr )
      {}


    // Accessors
    int    status   () const { return   m_status   ; }
    float    posX   () const { return     m_posX   ; }
    float    posY   () const { return     m_posY   ; }
    float    posZ   () const { return     m_posZ   ; }
    float  sigmaX   () const { return   m_sigmaX   ; }
    float  sigmaY   () const { return   m_sigmaY   ; }
    float  sigmaZ   () const { return   m_sigmaZ   ; }
    float   tiltX   () const { return    m_tiltX   ; }
    float   tiltY   () const { return    m_tiltY   ; }
    float sigmaXY   () const { return  m_sigmaXY   ; }
    float    posXErr() const { return     m_posXErr; }
    float    posYErr() const { return     m_posYErr; }
    float    posZErr() const { return     m_posZErr; }
    float  sigmaXErr() const { return   m_sigmaXErr; }
    float  sigmaYErr() const { return   m_sigmaYErr; }
    float  sigmaZErr() const { return   m_sigmaZErr; }
    float   tiltXErr() const { return    m_tiltXErr; }
    float   tiltYErr() const { return    m_tiltYErr; }
    float sigmaXYErr() const { return  m_sigmaXYErr; }
   
    // Methods
    void copyFrom( int argc, char* argv[] );
    void copyFrom( const std::vector< std::string >& args );
    bool isValid();

  protected:

    // Data members
    int    m_status   ;
    float    m_posX   ;
    float    m_posY   ;
    float    m_posZ   ;
    float  m_sigmaX   ;
    float  m_sigmaY   ;
    float  m_sigmaZ   ;
    float   m_tiltX   ;
    float   m_tiltY   ;
    float m_sigmaXY   ;
    float    m_posXErr;
    float    m_posYErr;
    float    m_posZErr;
    float  m_sigmaXErr;
    float  m_sigmaYErr;
    float  m_sigmaZErr;
    float   m_tiltXErr;
    float   m_tiltYErr;
    float m_sigmaXYErr;
  };

}

std::ostream& operator<<( std::ostream& os, const daq::BeamSpotParameters& parameters );

#endif // DAQ_BEAMSPOTPARAMETERS_H
