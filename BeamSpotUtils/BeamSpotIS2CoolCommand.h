//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_BEAMSPOT_BEAMSPOTIS2COOLCOMMAND_H
#define DAQ_BEAMSPOT_BEAMSPOTIS2COOLCOMMAND_H

#include "CoolUtils/IS2CoolCommand.h"
using daq::coolutils::IS2CoolCommand;

#include <string>


namespace daq
{
  namespace beamspot
  {
    // This is the IS object name used for the in-run conditions updates of the HLT.
    const std::string defaultObjectName     = "BeamSpot.HLTParameters";

    // This is the DB connection used for the in-run conditions updates of the HLT.
    const std::string defaultConnectionName = "COOLONL_INDET/CONDBR2";
    const std::string defaultFolderName     = "/Indet/Onl/Beampos";
    const std::string defaultTagName        = "IndetBeamposOnl-HLT-UPD1-001-00";

    class BeamSpotIS2CoolCommand : public IS2CoolCommand
    {
    public:
      // Constructors
      BeamSpotIS2CoolCommand( const std::string& dbConnectionName = defaultConnectionName,
                              const std::string& dbFolderName     = defaultFolderName,
                              const std::string& dbTagName        = defaultTagName,
                              const std::string& isObjectName     = defaultObjectName );

      // Destructor
      virtual ~BeamSpotIS2CoolCommand();

      // Methods
      bool execute( unsigned int runNumber, unsigned int lumiBlockNumber );

    private:
      // Data members
    };

  }
}

#endif // DAQ_BEAMSPOT_BEAMSPOTIS2COOLCOMMAND_H
