#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ namespace daq::beamspot;

#pragma link C++ class daq::beamspot::CorrWidth-!;

#endif
