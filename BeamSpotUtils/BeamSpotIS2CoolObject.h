//
// @Author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_BEAMSPOT_BEAMSPOTIS2COOLOBJECT_H
#define DAQ_BEAMSPOT_BEAMSPOTIS2COOLOBJECT_H

#include <string>

#include "CoolUtils/IS2CoolTransferrable.h"
using daq::coolutils::IS2CoolTransferrable;

class IPCPartition;
class ISNamedInfo;


namespace daq
{
  namespace beamspot
  {
    class BeamSpotIS2CoolObject : public IS2CoolTransferrable
    {
    public:
      // Constructor

      // Destructor
      virtual ~BeamSpotIS2CoolObject() {}

    protected:
      // This method is implemented for being ISReadable
      virtual ISNamedInfo* create( const IPCPartition& partition, const std::string& objectName ) const;

      // These methods are implemented for being CoolWritable
      virtual void fillRecord( cool::Record& record ) const;
      virtual void setRecordSpec( cool::RecordSpecification& recordSpec ) const;

    private:
      // Data members
    };

  }
}

#endif // DAQ_BEAMSPOT_BEAMSPOTIS2COOLOBJECT_H
