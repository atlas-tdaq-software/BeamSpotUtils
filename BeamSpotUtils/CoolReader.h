//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_BEAMSPOT_COOLREADER_H
#define DAQ_BEAMSPOT_COOLREADER_H

#include <string>

#include "BeamSpotUtils/BeamSpotParametersCool.h"

namespace daq
{
  namespace beamspot
  {

    class CoolReader
    {
    public:
      // Constructors
      CoolReader( const std::string& connectionName, const std::string& tagName );

      // Destructor
      ~CoolReader();

      // Accessors
      const BeamSpotParametersCool& parameters() const;

      // Methods
      bool read( unsigned int runNumber,
                 unsigned int lumiBlockNumber );

    private:
      // Data members
      std::string m_connectionName;
      std::string m_tagName;

      BeamSpotParametersCool m_parameters;
    };

  }
}

#endif // DAQ_BEAMSPOT_COOLREADER_H
