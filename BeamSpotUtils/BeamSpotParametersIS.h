//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_BEAMSPOTPARAMETERS_IS_H
#define DAQ_BEAMSPOTPARAMETERS_IS_H

// Base class
#include "BeamSpotUtils/BeamSpotParameters.h"

// Forward declare IS info object that is auto-generated
namespace beamspot
{
  class BeamSpotParametersNamed;
  class BeamSpotParameters;
}

namespace daq
{

  class BeamSpotParametersIS : public BeamSpotParameters
  {
  public:

    // Constructors
    BeamSpotParametersIS( const BeamSpotParameters& other )
      : BeamSpotParameters( other )
    {}

    BeamSpotParametersIS()
      : BeamSpotParameters()
    {}

    // Methods
    void copyFrom( const ::beamspot::BeamSpotParametersNamed& namedInfo );
    void copyTo( ::beamspot::BeamSpotParametersNamed& namedInfo ) const;
    void copyTo( ::beamspot::BeamSpotParameters& isInfo ) const;
  };

}

#endif // DAQ_BEAMSPOTPARAMETERS_IS_H
