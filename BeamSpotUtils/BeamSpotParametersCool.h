//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_BEAMSPOTPARAMETERS_COOL_H
#define DAQ_BEAMSPOTPARAMETERS_COOL_H

// Base class
#include "BeamSpotUtils/BeamSpotParameters.h"

namespace cool
{
  class IRecord;
  class Record;
}

namespace coral
{
  class AttributeList;
}

namespace daq
{

  class BeamSpotParametersCool : public BeamSpotParameters
  {
  public:

    // Constructors
    BeamSpotParametersCool( const BeamSpotParameters& other )
      : BeamSpotParameters( other )
    {}

    BeamSpotParametersCool()
      : BeamSpotParameters()
    {}

    // Methods
    void copyFrom( const cool::IRecord& object );
    void copyTo( cool::Record& record ) const;
  };

}

#endif // DAQ_BEAMSPOTPARAMETERS_COOL_H
