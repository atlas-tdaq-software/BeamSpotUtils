//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

// Forwarding header for backwards comapibility with the old CTP interface
#include "BeamSpotUtils/BeamSpotIS2CoolCommand.h"

namespace daq
{
  namespace beamspot
  {
    typedef BeamSpotIS2CoolCommand IS2CoolCommand;
  }
}
