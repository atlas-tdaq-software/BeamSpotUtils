//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_BEAMSPOT_ISWRITER_H
#define DAQ_BEAMSPOT_ISWRITER_H

#include <string>
#include <vector>
#include <owl/time.h>

#include "BeamSpotUtils/BeamSpotParameters.h"

namespace daq
{
  namespace beamspot
  {

    class ISWriter
    {
    public:
      // Constructors
      ISWriter( const std::string& objectName,
                const std::string& byBunchObjectName );

      // Destructor
      ~ISWriter();

      // Methods
      bool write( const std::vector<BeamSpotParameters>& bsParsVec );

    private:
      // Data members
      std::string m_objectName;
      std::string m_byBunch_objectName;
      std::string m_partitionName;
    };

  }
}

#endif // DAQ_BEAMSPOT_ISWRITER_H
