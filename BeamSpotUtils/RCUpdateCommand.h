//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_BEAMSPOT_RCUPDATECOMMAND_H
#define DAQ_BEAMSPOT_RCUPDATECOMMAND_H

#include <string>

namespace daq
{
  namespace beamspot
  {
    // Where to send the user command
    const std::string defaultController = "L1Ctp_RCD";
    const std::string defaultCommand    = "USER";
    const std::string defaultArgument   = "UPDATEBEAMSPOT";

    // Where to find the current lumi block number
    const std::string defaultISObject   = "RunParams.LumiBlock";

    class RCUpdateCommand
    {
    public:
      // Constructors
      RCUpdateCommand( const std::string& controller = defaultController,
                       const std::string& command    = defaultCommand,
                       const std::string& argument   = defaultArgument,
                       const std::string& isObject   = defaultISObject );

      // Destructor
      ~RCUpdateCommand();

      // Methods

      // If doSend==false, will prepare and display the command, but not send it
      bool send( bool doSend = true, bool useMasterTrigger = true );

    private:
      // Data members
      std::string m_controller;
      std::string m_command;
      std::string m_argument;
      std::string m_isObject;
    };

  }
}

#endif // DAQ_BEAMSPOT_RCUPDATECOMMAND_H
