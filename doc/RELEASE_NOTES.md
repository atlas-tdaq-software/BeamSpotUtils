# BeamSpotUtils

- Updates to support new HLT histogram naming convention for Run3.
- Add support for new track-based method.
- Improvements and refactoring to support easier testing.

## tdaq-09-02-01

- tag: BeamSpotUtils-03-02-06
