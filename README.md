BeamSpotUtils package
=====================

This package is responsible for the calculation of the beam spot, and 
communication in the ATLAS online environment.  It includes several
applications (something with a main() and command line interface)
as well as many library/module files.

More documentation can be found inside the modules in the standard PyDoc form.

Applications
------------

### beamSpotTool.py

This is the package workhorse. Lovingly known as _BST_, this application
fetches the histogram files aggregates lumiblocks worth of beam spot
histograms, fits gaussians, produces physics numbers, and publishes them to its
eagerly awaiting friends. BST itself is only a thin main sitting atop massive
libraries that will be describe below. This tool is called (with different
command line args) by the `ISController` to calculate the beam spots used for
beam- average monitoring, per bunch monitoring, and HLT updates. Its CLI is
extensive so check out --help.


### beamSpotArchiver.py

(Probably obsolete, not tested.) This application dumps per bunch measurements
into the _Massi_ format. At the end of each run, this application fetches the
run number and fill number, then dumps all of the per bunch measurements into
an unusually formatted set of `csv` files. This set of `csv` files (n.b. Massi
after Massimilian Ferro-Luzzi) contain one bunch per file and list the
parameters in LHC coordinates. The archiver then tarballs these 1000+ csv files
and registers them with CoCa. The tool is also checks if there was already a
tarball registered with CoCa for this run number (and lumi block). Thus if for
some reason we registered file at LB = 30 and tried again at LB = 300, we would
not repeat the first 30 lb's. Explore with --help and --debug.

### beamSpotConverterApp.py

The general beamspot viewing tool. The tool converts a beam spot from one
representation to another. For instance COOL might have a list of beam spots
(corresponding to different lumiblocks) for some run. Perhaps you want to dump
these to a screen, or send them to a csv file. Simply tell the tool, where to
look, where to write and voila! COOL access is also much faster if you tell it
a specific run (at the CLI). Explore --help and try it without any command line
args.

### beamSpotHistFileGetterApp.py

Fetches those histograms for you. ATLAS provides several interfaces to get the
root files that contain the beam spot monitoring histograms. NB these are the
histograms from L2 that we will process to measure a beam spot. The tool makes
a best effort and tries each method it knows until it gets you a file, and
dumps it in a /tmp space. These files get big, like ~1 GB! Honestly this tool
has trouble working in the online environment where DQM and MDA getters don't
work so well... Explore --help and try $>prog 187815.

### beamSpotISController.py

Interfaces _BST_ with Run Control. The Run Control tree executes this
application 3 times, each with a different sent or arguments. Each
`ISController` instance registers a callback to 'LumiBlock' on IS, and decides
when and how to execute its respective instance of BST. Furthermore the command
line args are specified in OKS. This piece of code is experts only.

### beamSpotInvalidator.py

Pushes the last HLT beam spot onto COOL as invalid. Under certain conditions,
we want to invalidate the HLT beam spot, to announce our uncertainty with the
actual value. Imagine the following: we successfully measure a beam spot (for
the HLT) at the end of some run and push it to COOL. Well it has an open ended
IOV so it stays there forever. Now if there is a long technical stop, maybe the
next luminous region will be 10 microns away. Well at the beginning of the run,
we haven't published a beam spot yet, *but* the b-jet algorithms will fire like
crazy because they're using the *old* beam spot. To alleviate this we
invalidate our own beam spot (setting its status to 4) at the end of a run.
This tool is not really for interactive use but you can explore it with --help
and --debug.

### beamSpot_set.py

Write beamspot snapshot into *offline* COOL folder `/Indet/Beampos` and tag
name `IndetBeampos-ES1-UPD2`. Snapshot parameters and interval (run/LBN) come
from command line, use `-h` option to see the arguments and their defaults.
COOL database connection string used by this script is `COOLOFL_INDET/CONDBR2`,
you will need a properly configured CORAL `dblookup.xml` and
`authentication.xml` to use it.

### beamSpotOnl_set.py

Write beamspot snapshot into *online* COOL folder `/Indet/Onl/Beampos` and tag
name `IndetBeamposOnl-HLT-UPD1-001-00`. Snapshot parameters and interval
(run/LBN) come from command line, use `-h` option to see the arguments and
their defaults. COOL database connection string used by this script is
`COOLONL_INDET/CONDBR2`, you will need a properly configured CORAL
`dblookup.xml` and `authentication.xml` to use it.

Libraries/Modules
-----------------

### file_getter.py

Three classes designed to fetch histogram files. This library also contains
some file management helpers and a few OO constructs to simplify the interface.

* `GenericFileGetter`:
Abstract base class for other getter classes.

* `OHCPGetter`:
This getter "copies" the histograms from the
Online Histogram Provider (OHP) via the shell tool `oh_cp`.
Unlike the other getters, there is no actual root file until `oh_cp`
is called.  This means that when we specify the range of lumiblocks
we are interested in, we only get those back. This method does not work
natively on lxplus unless you are hosting an OH server.

* `COCAGetter`:
Getter class which implements copy from CoCa archive. It executes
`coca_get_files` application to copy files from CoCa to a local folder.

* `SmartFileGetter`:
This "getter" is not really a getter at all.  It exposes the same
`GenericFileGetter` interface, but hides which one actually
succeeded in fetching.  SmartFileGetter allows the casual user to
use one class (this one!) and nto worry about where the file actually
is.  Note the file getting application uses this class since it
doesn't try to be fast, just robust.

* `SmartFileGetterConfig`:
Configuration class used by `SmartFileGetter`. Instance of this class is
populated by `COnfig` class based on command line parameters and passed to
`SmartFileGetter` constructor.

Also see `beamSpotHistFileGetterApp.py` for the `main()` wrapper.

### os_utils.py

This modules define a few useful methods that I used over and over again.
Specifically, and usefully, we have a single function to submit OS calls
and quit the application.

* `quickOSCall`:
    This function handles all the subprocess I/O for spawning a new process.
    It can be handed a logger--or else it spawns its own--to dump to stdout
    it's progress (of spawning the process).  Here's a gotcha: if you pass
    it a userEnv, which is a dictionary of environment variables and values,
    it will override the *entire* env.   For instance:

    if userEnv = {'stage_host': 'castor'} but the process needs some
    other variable as well, like PYTHONPATH, it will fail.  You told
    it, by accident, there was no PYTHONPATH
    Try merging your dicts using `userEnv = dict(oldEnv, \*\*newEnv)`

    Finally, for mission critical OS calls, the method can be told via
    `mustReturnZero` to quit the whole program if the subprocess yields
    a non zero return code.  The program quits through exit() in this
    module.

* `rmdir(folder)`:
    Wraps python's `shutil.rmtree`, but captures the "Folder didn't exist"
    exception and ignores them.

* `deleteFileOrFolder(str_path)`:
    Attempts to emulate the shell's rm -r.

* `makeTempDir()`:
    Creates temporary directory to store files.

### pipe.py

This module simplifies the conversion from one beam spot representation to
another. Its called a pipe because stuff goes in one end and comes out the
other, but perhaps transfigured. If you like functional programming the class
also suppose the common map and reduce semantics. For more information on
representations themselves see model.py. To use this from the command line see
`beamSpotConverterApp.py`. The module implements a single class called
`BeamSpotPipe`.

`BeamSpotPipe`:
This class simplifies converting between two representations.
The constructor has 3 required arguments: repInput, repOutput, and lastN:
* `repInput` tells the pipe where to go look for its representations,
  and is described in `Input/output translators`.
* `repOutput` tells the pipe where to go write its representations,
  and is described in `Input/output translators`.
* If `lastN` is a positive integer the pipe will only write out that
  many representations.  If there are less than N available it pushes
  out that many then.

A pipe object supports one more method and it is powerful.  The
push method!  Calling this method triggers the pipe to begin the
conversion which depending on the I/O could be a long a slow operation.
Push can be called with the following three options...

* `snapMap` is a function which when passed a dictionary (of beam spot
    parameters) returns a possibly modified dictionary.  This is the
    map part.  If the function returns None, then that snap shot
    (a single beam spot at a given time) is filtered out.
* `readArgs` is a map of keyword args (aka \*\*kwargs) passed to the input
    translator during its read() call
* `writeArgs` is a map of keyword args (aka \*\*kwargs) passed to the output
    translator during its write() call
* `skipEmpty` (False by default) If `True` then empty snapshot is not saved.


### model.py

Module defining in-memory data model. Defines `BSSnapshot` and
`BeamSpotRepresentation` classes.

### differ.py

Compares two representations and determines if they look different
enough to warrant update of HLT conditions.

### proc_run.py

Handles processing of a single "run" or rather a single execution of
BST either on whole-run data or a subset of LBs from one run.

### bscalc_vtx.py

Processing of histograms and calculation of the resulting snapshot(s)
using our standard vertex-based method.

### run_query.py

Interface to ATLAS Run Query.

### cmdline.py

Parser for command line arguments.


Input/output translators
------------------------

BST defines an abstraction which represent a source or a destination for
a beamspot data (representation) called _Translator_. At Python code level
_translator_ is usually represented as a class with methods to read or
write representation data. At the user interface level this abstraction
is represented as a string specifying translator name (e.g. "ohcp", "onmon").
Output destination can also be specified with multiple translator names
separated by commas (e.g. "ismon,onmon") which allows saving representation
to a multiple destinations at the same time.

Some translators can take additional parameters when they follow translator
name after separating colon, e.g. "sqlite:/tmp/beamspot.db".

Presently we have a predefined set of translators, here the list with the
names and descriptions:

* `off` - read-only, no parameters

    Reads representation from offline COOL folder
    (dbname="COOLOFL_INDET/CONDBR2", foder="/Indet/Beampos",
    tag="IndetBeampos-ES1-UPD2").

* `onhlt` - read-only, no parameters

    Reads representation from online/HLT COOL folder
    (dbname="COOLONL_INDET/CONDBR2", foder="/Indet/Onl/Beampos",
    tag="IndetBeamposOnl-HLT-UPD1-001-00"). This folder is updated by CTP,
    it is read by BST as a reference for HLT update decisions.

* `onhlt_update` - read-write, no parameters

    Reads or writes representation to/from online/HLT COOL folder
    (dbname="COOLONL_INDET/CONDBR2", foder="/Indet/Onl/Beampos",
    tag="IndetBeamposOnl-HLT-UPD1-001-00"). This should not be used in
    production (where CTP is responsible for COOL updates), but it is
    useful in testing.

* `onmon` - read-write, no parameters

    Reads or writes representation to/from online COOL folder, used for
    "live monitoring" data (dbname="COOLONL_INDET/CONDBR2",
    foder="/Indet/Onl/Beampos", tag="IndetBeamposOnl-LiveMon-001-00").
    Same folder as HLT, different tag.

* `onbunch` - read-write, no parameters

    Reads or writes representation to/from online COOL folder with per-bunch
    info (dbname="COOLONL_INDET/CONDBR2", foder="/Indet/Onl/BeamposPerBunch",
    tag="IndetBeamposOnlPerBunch-LiveMon-001-00").

* `ismon` - write-only, no parameters

    Write representation to IS server (server="BeamSpot", object="LiveMon"),
    per-bunch data are sent to IS object with name "ByBunchMon".

* `isctp` - write-only, no parameters

    Write representation to IS server (server="BeamSpot", object="HLTParameters"),
    per-bunch data are sent to IS object with name "ByBunchMon". IS object
    is the same as used by CTP translator.

* `is` - write-only, requires parameter which is IS object name

    Write representation to IS, parameter contains optional partition name
    followed by `!`, optional IS server name followed by dot, and required
    object name. Default partition name is empty (meaning local partition),
    default server name is `BeamSpot`. Examples of parameter strings:
    "is:LiveMon", "is:BeamSpot.LiveMon", "is:ATLAS!BeamSpot.LiveMon".

* `ctp` - write-only, no parameters

    Translator used for updating HLT conditions via special CTP mechanism.
    First it writes representation to IS (server="BeamSpot",
    object="HLTParameters") and then notifies CTP that update has to be
    performed. CTP code (C++ code in this package) reads data from IS server
    and stores it in COOL database (same folder/tag as `onhlt` above).

* `dummyctp`  - write-only, no parameters

    Same as `tcp` but does not do actual updates, only prints information
    to a log, useful for debugging.

* `conctp` - write-only, no parameters

    "Conservative" CTP updater, sets status in COOL record equal to 5
    (means that data is problematic)

* `stdout` - write-only, no parameters

    Formats representation to standard output, useful for debugging.

* `massi` - write-only, no parameters

    Writes CSV file in "Massi" format and sends it to CoCa archive.

* `sqlite` - read-write, requires parameter which is a file name

    Reads or writes to COOL database in SQLite database. Folder and
    tag name are the same as for `onhlt` translator.

* `csv` - write-only, requires parameter which is a file name

    Writes representation to a file in a comma-separated format.

* `pickle` - read-write, requires parameter which is a file name

    Reads or writes representation as a Python object in Pickle format.

* `file` - write-only, requires parameter which is a file name

    This is a meta-translator which guesses actual translator type based
    on the extension of the file name given as a parameter. If file name
    ends with ".db", ".sql", ".sqlite", or ".sqlite3" then `sqlite` translator
    is used, if file name ends with ".csv" then `csv` translator is used,
    otherwise `pickle` translator is used. This translator is considered
    deprecated, please use one of the specific translators.
