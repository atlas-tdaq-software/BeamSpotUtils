//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "BeamSpotUtils/BeamSpotParametersIS.h"

#include "beamspot/BeamSpotParametersNamed.h"
#include "beamspot/BeamSpotParameters.h"

using namespace daq;

// Methods

void 
BeamSpotParametersIS::copyFrom( const ::beamspot::BeamSpotParametersNamed& namedInfo )
{
   m_status    = namedInfo.status    ;
     m_posX    = namedInfo.   posX   ;
     m_posY    = namedInfo.   posY   ;
     m_posZ    = namedInfo.   posZ   ;
   m_sigmaX    = namedInfo. sigmaX   ;
   m_sigmaY    = namedInfo. sigmaY   ;
   m_sigmaZ    = namedInfo. sigmaZ   ;
    m_tiltX    = namedInfo.  tiltX   ;
    m_tiltY    = namedInfo.  tiltY   ;
  m_sigmaXY    = namedInfo.sigmaXY   ;
     m_posXErr = namedInfo.   posXErr;
     m_posYErr = namedInfo.   posYErr;
     m_posZErr = namedInfo.   posZErr;
   m_sigmaXErr = namedInfo. sigmaXErr;
   m_sigmaYErr = namedInfo. sigmaYErr;
   m_sigmaZErr = namedInfo. sigmaZErr;
    m_tiltXErr = namedInfo.  tiltXErr;
    m_tiltYErr = namedInfo.  tiltYErr;
  m_sigmaXYErr = namedInfo.sigmaXYErr;

}


void
BeamSpotParametersIS::copyTo( ::beamspot::BeamSpotParametersNamed& namedInfo ) const
{
  namedInfo.status     =  m_status   ;
  namedInfo.   posX    =    m_posX   ;
  namedInfo.   posY    =    m_posY   ;
  namedInfo.   posZ    =    m_posZ   ;
  namedInfo. sigmaX    =  m_sigmaX   ;
  namedInfo. sigmaY    =  m_sigmaY   ;
  namedInfo. sigmaZ    =  m_sigmaZ   ;
  namedInfo.  tiltX    =   m_tiltX   ;
  namedInfo.  tiltY    =   m_tiltY   ;
  namedInfo.sigmaXY    = m_sigmaXY   ;
  namedInfo.   posXErr =    m_posXErr;
  namedInfo.   posYErr =    m_posYErr;
  namedInfo.   posZErr =    m_posZErr;
  namedInfo. sigmaXErr =  m_sigmaXErr;
  namedInfo. sigmaYErr =  m_sigmaYErr;
  namedInfo. sigmaZErr =  m_sigmaZErr;
  namedInfo.  tiltXErr =   m_tiltXErr;
  namedInfo.  tiltYErr =   m_tiltYErr;
  namedInfo.sigmaXYErr = m_sigmaXYErr;

}

void
BeamSpotParametersIS::copyTo( ::beamspot::BeamSpotParameters& isInfo ) const
{
  isInfo.status     =  m_status   ;
  isInfo.   posX    =    m_posX   ;
  isInfo.   posY    =    m_posY   ;
  isInfo.   posZ    =    m_posZ   ;
  isInfo. sigmaX    =  m_sigmaX   ;
  isInfo. sigmaY    =  m_sigmaY   ;
  isInfo. sigmaZ    =  m_sigmaZ   ;
  isInfo.  tiltX    =   m_tiltX   ;
  isInfo.  tiltY    =   m_tiltY   ;
  isInfo.sigmaXY    = m_sigmaXY   ;
  isInfo.   posXErr =    m_posXErr;
  isInfo.   posYErr =    m_posYErr;
  isInfo.   posZErr =    m_posZErr;
  isInfo. sigmaXErr =  m_sigmaXErr;
  isInfo. sigmaYErr =  m_sigmaYErr;
  isInfo. sigmaZErr =  m_sigmaZErr;
  isInfo.  tiltXErr =   m_tiltXErr;
  isInfo.  tiltYErr =   m_tiltYErr;
  isInfo.sigmaXYErr = m_sigmaXYErr;

}

