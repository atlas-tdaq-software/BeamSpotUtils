//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include <cstdlib>
#include <string>
#include <sstream>

using namespace std;

#include "BeamSpotUtils/RCUpdateCommand.h"

#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/partition.h"
#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/UserExceptions.h"
#include "TriggerCommander/TriggerCommander.h"
#include "TTCInfo/LumiBlockNamed.h"


namespace daq 
{
  ERS_DECLARE_ISSUE(	beamspot,
			InvalidPartition,
			"Partition is not valid: " << explanation,
                        ( (const char*) explanation )
                        )

  ERS_DECLARE_ISSUE(	beamspot,
			EnvironmentMissing,
			"Environment variable is not defined: " << explanation,
                        ( (const char*) explanation )
                        )
}


// Constructors

daq::beamspot::RCUpdateCommand::RCUpdateCommand( const std::string& controller,
                                                 const std::string& command,
                                                 const std::string& argument,
                                                 const std::string& isObject )
  : m_controller( controller )
  , m_command   ( command )
  , m_argument  ( argument )
  , m_isObject  ( isObject )
{
}

// Destructor

daq::beamspot::RCUpdateCommand::~RCUpdateCommand()
{
}

// Methods

bool
daq::beamspot::RCUpdateCommand::send( bool doSend, bool useMasterTrigger )
{
  // Initialize IPC
  try
    {
      int argc=1;
      const char* argv[] = { "RCUpdateCommand" };

      // FIXME: Need to cast away const-ness because IPCCore::init()
      // signature is not const-clean
      IPCCore::init( argc, (char**) argv );
    }
  catch ( daq::ipc::CannotInitialize& e )
    {
      ers::error(e);
      return false;
    }
  catch( daq::ipc::AlreadyInitialized& e)
    {
      ers::warning(e);
    }
           
  // Retrieve name of partition from the environment
  const char* partitionName = getenv( "TDAQ_PARTITION" );
  if ( ! partitionName )
    {
      daq::beamspot::EnvironmentMissing i( ERS_HERE, "TDAQ_PARTITION" );
      ers::error(i);
      return false;
    }
   
  ERS_LOG( "Trying to connect to partition \"" << partitionName << "\"" );

  // Connect to partition
  IPCPartition partition( partitionName );

  if ( ! partition.isValid() )
    {
      daq::beamspot::InvalidPartition i( ERS_HERE, partitionName );
      ers::error(i);
      return false;
    }

  try
    {
      // Read the current luminosity block number from IS
      LumiBlockNamed lumiBlock( partition, m_isObject );
      lumiBlock.checkout();

      ERS_LOG( "Read the following luminosity info from IS: " << lumiBlock );

      // Choose the next lumi block (there is a race condition here that is harmless)
      const int nextLBN = lumiBlock.LumiBlockNumber + 1;

      if ( doSend )
        {
          if ( ! useMasterTrigger )
            {
              // Set up the command
              stringstream arguments;
              arguments << m_argument << " " << nextLBN;
              std::vector<std::string> parameters;
              parameters.push_back(arguments.str());

              // Send the command
              ERS_LOG( "Sending \"" << m_command << "\" command to controller \""
                       << m_controller << "\" with arguments: " << arguments.str() );

              daq::rc::CommandSender commander( partition, "RCUpdateCommand" );
              commander.userCommand( m_controller, daq::rc::UserCmd(m_command, parameters) );
            }
          else
            {
              const int folderIndex = 0; // FIXME: this needs to be fetched from
              // the TRIGGER/COOLUPDATE folder as the
              // index of folder "/Indet/Onl/Beampos"

              // Send the command
              ERS_LOG( "Sending \"" << m_command << "\" command to controller \""
                       << m_controller << "\" with folder index " << folderIndex << " and LBN " << nextLBN );

              daq::trigger::TriggerCommander commander( partition, m_controller );
              commander.setConditionsUpdate( folderIndex, nextLBN );
            }
        }
      else
        ERS_LOG( "Command not sent per user request" );
    }
  catch ( ers::Issue& e )
    {
      ers::error(e);
      return false;
    }
   
  return true;
}
