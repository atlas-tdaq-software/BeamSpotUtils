//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include <cstdlib>
#include <string>
#include <sstream>

using namespace std;

//#include <boost/program_options/cmdline.hpp>
//#include <boost/program_options/environment_iterator.hpp>
//#include <boost/program_options/eof_iterator.hpp>
//#include <boost/program_options/errors.hpp>
//#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
//#include <boost/program_options/positional_options.hpp>
//#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
//#include <boost/program_options/version.hpp>

namespace po = boost::program_options;

#include "ers/ers.h"
#include "BeamSpotUtils/RCUpdateCommand.h"


int main( int argc, char* argv[] )
{
  enum { SUCCESS=0, BAD_USAGE=1, BAD_COMMAND=2 };

  // Parse the command line
  po::options_description desc("Options");
  desc.add_options()
    ( "help,h", "this help message" )
    ( "do-not-send,n", "prepare the command, but do not send it" )
    ( "use-master-trigger,m", "use the MasterTrigger interface to send the command (now default)" )
    //    ( "lumiblock,l", po::value<int>( &lbn )->default_value(0), "luminosity block to be used for update" )
    ;

  po::variables_map vm;
  po::store( po::parse_command_line( argc, argv, desc ), vm );
  po::notify( vm );

  if ( vm.count("help") )
    {
      cout << desc << endl;
      return BAD_USAGE;
    }

  // Prepare the command and send it
  const bool doSend           = ! vm.count("do-not-send");
  //  const bool useMasterTrigger =   vm.count("use-master-trigger");
  const bool useMasterTrigger = true;

  try
    {
      daq::beamspot::RCUpdateCommand rcUpdateCommand;
      rcUpdateCommand.send( doSend, useMasterTrigger );
    }
  catch ( ers::Issue& e )
    {
      ers::error(e);
      return BAD_COMMAND;
    }

  return SUCCESS;
}
