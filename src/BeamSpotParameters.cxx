//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "BeamSpotUtils/BeamSpotParameters.h"

#include <boost/lexical_cast.hpp>
using boost::lexical_cast;
using boost::bad_lexical_cast;

#include <vector>
#include <string>
using std::vector;
using std::string;

#include <iostream>
using std::cerr;
using std::endl;

using namespace daq;

void 
BeamSpotParameters::copyFrom( int argc, char* argv[] )
{
  if ( argc > 1 )
    {
      const vector< string > args( argv + 1, argv + argc );
      copyFrom( args );
    }
}


// replaced by ISWriterApp::copyArgsToBSPars
void 
BeamSpotParameters::copyFrom( const vector<string>& pars )
{
  const int n = pars.size();
  int i = -1;

  try
    {
      // first 19 arguments are the all-bunch-averaged values
      if ( ++i < n )  m_status    = lexical_cast<int>   ( pars[i] ); 
      if ( ++i < n )    m_posX    = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )    m_posY    = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )    m_posZ    = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )  m_sigmaX    = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )  m_sigmaY    = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )  m_sigmaZ    = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )   m_tiltX    = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )   m_tiltY    = lexical_cast<double>( pars[i] ); 
      if ( ++i < n ) m_sigmaXY    = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )    m_posXErr = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )    m_posYErr = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )    m_posZErr = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )  m_sigmaXErr = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )  m_sigmaYErr = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )  m_sigmaZErr = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )   m_tiltXErr = lexical_cast<double>( pars[i] ); 
      if ( ++i < n )   m_tiltYErr = lexical_cast<double>( pars[i] ); 
      if ( ++i < n ) m_sigmaXYErr = lexical_cast<double>( pars[i] ); 
    }
  catch ( const bad_lexical_cast& )
    {
    //  std::cout << std::endl << i << " " << pars[i-1] << " " << pars[i] << std::endl;
            std::cerr << "failed to parse the argument list" << std::endl;
      throw;
    }
}


std::ostream& operator<<( std::ostream& os, const daq::BeamSpotParameters& p )
{
  os
    << "  status="    << p. status   ()
    <<    " posX="    << p.   posX   ()
    <<    " posY="    << p.   posY   ()
    <<    " posZ="    << p.   posZ   ()
    <<  " sigmaX="    << p. sigmaX   ()
    <<  " sigmaY="    << p. sigmaY   ()
    <<  " sigmaZ="    << p. sigmaZ   ()
    <<   " tiltX="    << p.  tiltX   ()
    <<   " tiltY="    << p.  tiltY   ()
    << " sigmaXY="    << p.sigmaXY   ()
    <<    " posXErr=" << p.   posXErr()
    <<    " posYErr=" << p.   posYErr()
    <<    " posZErr=" << p.   posZErr()
    <<  " sigmaXErr=" << p. sigmaXErr()
    <<  " sigmaYErr=" << p. sigmaYErr()
    <<  " sigmaZErr=" << p. sigmaZErr()
    <<   " tiltXErr=" << p.  tiltXErr()
    <<   " tiltYErr=" << p.  tiltYErr()
    << " sigmaXYErr=" << p.sigmaXYErr()
    ;
  return os;
}

bool BeamSpotParameters::isValid()
{
    bool validParams =    m_posX > -10 // 10x the largest deviations 
                       && m_posX <  10 // Josh has seen
                       && m_posY > -10
                       && m_posY <  10
                       && m_posZ > -200
                       && m_posZ <  200

                       && m_sigmaX > 0
                       && m_sigmaX < 200  // 4x the fattest beamspot Josh has seen
                       && m_sigmaY > 0
                       && m_sigmaY < 200
                       && m_sigmaZ > 0
                       && m_sigmaZ < 200  // 4x the fattest beamspot (Units change!)

                       && m_tiltX < -1000 // Josh has no idea.
                       && m_tiltX >  1000
                       && m_tiltY < -1000 // Josh has no idea.
                       && m_tiltY >  1000;

    return validParams;
//    posXErr > 0
//    posXErr < 10
//    posYErr > 0
//    posYErr < 10
//    posZErr > 0
//    posZErr < 200
//
//    m_sigmaXErr > 0
//    m_sigmaXErr < 200  // 4x the fattest beamspot Josh has seen
//    m_sigmaYErr > 0
//    m_sigmaYErr < 200
//    m_sigmaZErr > 0
//    m_sigmaZErr < 200  // 4x the fattest beamspot Josh has seen
//
//    m_tiltXErr > 0
//    m_tiltXErr < 1000   //Josh has no idea

}
