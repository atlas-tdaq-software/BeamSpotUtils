//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

//#include "BeamSpotUtils/BeamSpotIS2CoolCommand.h"
#include "BeamSpotUtils/IS2CoolCommand.h"
// Both includes are possible, for compatibility with the existing CTP code (see below)

#include "ers/ers.h"

#include <cstdlib>
using std::atoi;

#include <string>
using std::string;

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
namespace po = boost::program_options;

// Default IS and DB connection parameters

const std::string defaultISServer     = "BeamSpot";
const std::string defaultISObject     = "HLTParameters";
const std::string defaultDbConnection = "sqlite://;schema=beamSpotIS2CoolApp-test.db;dbname=CONDBR2";
const std::string defaultDbFolder     = "/Indet/Beampos/Onl";
const std::string defaultDbTag        = "IndetBeamposOnl-HLT-UPD1-001-00";


int main( int argc, char* argv[] )
{
  enum { SUCCESS=0, BAD_USAGE=1, BAD_RETURN=2, BAD_EXCEPTION=3 };

  // Parse the command line
  po::options_description desc( "Options" );
  desc.add_options()
    ( "help,h", "this help message" )
    ( "server,n"     , po::value<string>()->default_value( defaultISServer     ), "name of the IS server to read from" )
    ( "object,o"     , po::value<string>()->default_value( defaultISObject     ), "name of the IS object to read" )
    ( "connection,c" , po::value<string>()->default_value( defaultDbConnection ), "COOL database connection string" )
    ( "folder,f"     , po::value<string>()->default_value( defaultDbFolder     ), "COOL folder " )
    ( "tag,t"        , po::value<string>()->default_value( defaultDbTag        ), "COOL tag" )

    ( "run,r"        , po::value<unsigned>()->default_value( 0 ), "run number at start of IOV" )
    ( "lb,l"         , po::value<unsigned>()->default_value( 0 ), "lumi block number at start of IOV" )
    ;

  // FIXME: allowing freestyle run numbers is too dangerous

  bool success = false;
  po::variables_map vm;
  po::store( po::parse_command_line( argc, argv, desc ), vm );
  po::notify( vm );

  if ( vm.count( "help" ) )
    {
      ERS_LOG( desc );
      return BAD_USAGE;
    }

  try
    {
      // daq::beamspot::BeamSpotIS2CoolCommand command( dbConnection, dbFolder, dbTag, isObject );
      daq::beamspot::IS2CoolCommand command( vm [ "connection" ].as<string>(),
                                             vm [ "folder"     ].as<string>(),
                                             vm [ "tag"        ].as<string>(),
                                             vm [ "server"     ].as<string>() + "." +
                                             vm [ "object"     ].as<string>()
                                             );

        success = command.execute( vm[ "run" ].as<unsigned>(), vm[ "lb" ].as<unsigned>() );
    }
  catch ( std::exception& e )
    {
      return BAD_EXCEPTION;
    }

  if ( ! success )
    return BAD_RETURN;

  return SUCCESS;
}
