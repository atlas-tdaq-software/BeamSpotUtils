//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "BeamSpotUtils/BeamSpotIS2CoolCommand.h"

#include "CoolUtils/IS2CoolCommandHandler.h"
#include "BeamSpotUtils/BeamSpotIS2CoolObject.h"
#include "ers/ers.h"

// Constructors

daq::beamspot::BeamSpotIS2CoolCommand::BeamSpotIS2CoolCommand( const std::string& dbConnectionName,
                                                               const std::string& dbFolderName,
                                                               const std::string& dbTagName,
                                                               const std::string& isObjectName )
  : IS2CoolCommand( dbConnectionName,
                    dbFolderName,
                    dbTagName,
                    isObjectName )
{
}

// Destructor

daq::beamspot::BeamSpotIS2CoolCommand::~BeamSpotIS2CoolCommand()
{
}

// Methods

bool
daq::beamspot::BeamSpotIS2CoolCommand::execute( unsigned int runNumber,
                                                unsigned int lumiBlockNumber )
{
  ERS_LOG( "Executing NEW generic command handler for BeamSpot IS2Cool transfer"
           " for run " << runNumber << " and lumi block " << lumiBlockNumber << " ..." );

  // Create a Beam Spot object and transfer it via the handler
  BeamSpotIS2CoolObject beamSpotObject;

  const bool success = handler()->transfer( runNumber, lumiBlockNumber, beamSpotObject );
  return success;
}
