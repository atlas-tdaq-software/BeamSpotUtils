//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include <string>

#include "BeamSpotUtils/CoolReader.h"
#include "ers/ers.h"

#include <CoralBase/AttributeList.h>
#include <CoralBase/Attribute.h>
#include <CoolKernel/RecordSpecification.h>
#include <CoolKernel/Record.h>
#include <CoolKernel/StorageType.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/FolderVersioning.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/pointers.h>
#include <CoolApplication/DatabaseSvcFactory.h>


// Folder specification

const std::string folderName = "/Indet/Onl/Beampos";

// Custom ERS exceptions

namespace daq 
{
  ERS_DECLARE_ISSUE(	beamspot,
			BadCoolRead,
			"Reading from COOL failed because: " << explanation,
                        ( (const char*) explanation )
                        )
}

// Constructors

daq::beamspot::CoolReader::CoolReader( const std::string& connectionName,
                                       const std::string& tagName )
  : m_connectionName( connectionName )
  , m_tagName( tagName )
{
  ERS_LOG( "Instantiated a CoolReader for connection \"" << m_connectionName
           << "\" and tag \"" << m_tagName << "\"" );
}

// Destructor

daq::beamspot::CoolReader::~CoolReader()
{
}

// Accessors

const daq::BeamSpotParametersCool&
daq::beamspot::CoolReader::parameters() const
{
  return m_parameters;
}

// Methods

bool
daq::beamspot::CoolReader::read( unsigned int runNumber,
                                 unsigned int lumiBlockNumber )
{
  ERS_LOG( "Reading beam spot parameters from COOL"
           << " folder \"" << folderName
           << "\" in database \"" << m_connectionName
           << "\" with tag \"" << m_tagName
           << "\"" );

  ERS_LOG( "Start of IOV is run number: " << runNumber
           << ", lumi block number: " << lumiBlockNumber );

  try
    {
      // Open or create the database
      cool::IDatabasePtr database;

      try 
        {
          ERS_LOG( "Trying to open database \"" << m_connectionName << "\"" );

          const bool read_only = true;
          database = cool::DatabaseSvcFactory::databaseService().openDatabase( m_connectionName, read_only );
        }
      catch ( cool::DatabaseDoesNotExist& e )
        {
          daq::beamspot::BadCoolRead i( ERS_HERE, "Database does not exist" );
          ers::error(i);
          return false;
        }

      if ( ! database )
        {
          daq::beamspot::BadCoolRead i( ERS_HERE, "Failed to open database" );
          ers::error(i);
          return false;
        }

      ERS_DEBUG(0, "Successfully opened database" );

      // Find the folder
      ERS_LOG( "Looking for folder \"" << folderName << "\"" );

      if ( ! database->existsFolder( folderName ) )
        {
          daq::beamspot::BadCoolRead i( ERS_HERE, "Folder does not exist" );
          ers::error(i);
          return false;
        }

      // Fetch the folder
      cool::IFolderPtr folder = database->getFolder( folderName );

      if ( ! folder )
        {
          daq::beamspot::BadCoolRead i( ERS_HERE, "Failed to get folder" );
          ers::error(i);
          return false;
        }

      ERS_DEBUG(0, "Retrieved folder \"" << folderName << "\"" );

      const cool::ChannelId channelId = 0;

      // Index IOV by run number and lumi block
      const uint64_t a = runNumber;
      const cool::ValidityKey since = ((a<<32) | lumiBlockNumber);
      const cool::ValidityKey until = cool::ValidityKeyMax;

      ERS_LOG( "Computed validity keys: since=" << since << ", until=" << until );

      try
        {
          // Fetch the object for the given time
          ERS_LOG( "Looking for object with tag \"" << m_tagName << "\"" );

          cool::IObjectPtr object = folder->findObject( since, channelId, m_tagName );

          if ( ! object )
            {
              daq::beamspot::BadCoolRead i( ERS_HERE, "Failed to find object" );
              ers::error(i);
              return false;
            }

          // Extract the parameters
          m_parameters.copyFrom( object->payload() );

          ERS_LOG( "BeamSpotParameters:" << m_parameters );
        }
      catch ( cool::TagNotFound& e )
        {
          daq::beamspot::BadCoolRead i( ERS_HERE, "Failed to find tag" );
          ers::error(i);
          return false;
        }
      catch ( cool::ObjectNotFound& e )
        {
          daq::beamspot::BadCoolRead i( ERS_HERE, "Failed to find object" );
          ers::error(i);
          return false;
        }

    }
  catch ( std::exception& e )
    {
      // COOL, CORAL and POOL exceptions inherit from std exceptions,
      // so catching std::exception will catch all errors from COOL,
      // CORAL and POOL.
      daq::beamspot::BadCoolRead i( ERS_HERE, e.what() );
      ers::error(i);
      // FIXME: close database?
      return false;
    }

  return true;
}
