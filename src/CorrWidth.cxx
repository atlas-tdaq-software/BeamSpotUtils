// **********************************************************************
// $Id$
// **********************************************************************

#include "BeamSpotUtils/CorrWidth.h"

#include <cmath>
#include <iostream>

#include <TH1.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TObjArray.h>
#include <TClass.h>

#include <iostream>

#include "dqm_core/exceptions.h"
#include "dqm_core/AlgorithmManager.h"
#include "dqm_core/Result.h"

#include <string>
#include <sstream>

static dqm_algorithms::CorrWidth staticInstance;

namespace dqm_algorithms {
  
  // *********************************************************************
  // Public Methods
  // *********************************************************************

  CorrWidth::CorrWidth()
    : m_nRMS(3), m_axis(0), m_factor(1.), m_binMin(6), m_binMax(999), name("AlgCorrWidth")	{
    dqm_core::AlgorithmManager::instance().registerAlgorithm( name, this );
		
		ERS_LOG("Initialize CorrWidth with name" << name);
  }


  CorrWidth::CorrWidth( const CorrWidth& other )
    : dqm_core::Algorithm(other),	m_nRMS(other.m_nRMS), m_axis(other.m_axis), m_factor(other.m_factor),
      m_binMin(other.m_binMin), m_binMax(other.m_binMax), name(other.name)
  {
		ERS_LOG("Copy contructor CorrWidth with name" << name);
  }


  CorrWidth::~CorrWidth()	{
  }


  dqm_core::Algorithm * CorrWidth::clone(){
		ERS_LOG("Clone CorrWidth with name" << name);
    return new CorrWidth(*this);
  }


  dqm_core::Result * CorrWidth::execute( const std::string& name, const TObject& data, const dqm_core::AlgorithmConfig& config ) {

    ERS_LOG("Inside execute");

    //-----------------------------------------------------------------------------------
    //Read in parameters if they are available
    //-----------------------------------------------------------------------------------
    double holder;
    holder = GetFirstFromMap( "nRMS", config.getParameters(), 1 );
    //ERS_LOG( "EAS: nRMS = " << holder );
    if (holder >= 0) m_nRMS = (int) holder;
    holder = GetFirstFromMap( "binMin", config.getParameters(), 1 );
    //ERS_LOG( "EAS: binMin = " << holder );
    if (holder > 0) m_binMin = (int) holder;
    holder = GetFirstFromMap( "binMax", config.getParameters(), 1 );
    //ERS_LOG( "EAS: binMax = " << holder );
    if (holder > 0) m_binMax = (int) holder;
    holder = GetFirstFromMap( "axis", config.getParameters(), 0 );
    //ERS_LOG( "EAS: axis before input = " << m_axis );
    //ERS_LOG( "EAS: axis input = " << holder );
    if (holder >= 0 && holder <= 1) {
      m_axis = (int) holder;
    } else {
      m_axis = 0;
    }
    //ERS_LOG( "EAS: axis has been set to " << m_axis );
    holder = GetFirstFromMap( "factor", config.getParameters(), 1 );
    if (holder > 0) {
      m_factor = holder;
    } else {
      m_factor = 1.;
    }
    //ERS_LOG( "EAS: factor = " << holder );

		ERS_LOG("nRMS: " << m_nRMS << ", binMin: " << m_binMin << ", binMax: " << m_binMax << ", axis: " << m_axis << ", factor: " << m_factor);
		
		
    //-----------------------------------------------------------------------------------
    //Read in the raw and the resolution histograms
    //-----------------------------------------------------------------------------------
    if (!data.IsA()->InheritsFrom("TObjArray")){
      throw dqm_core::BadConfig( ERS_HERE, name, "Cannot cast data to type TObjArray" );
    }
		
    TObjArray * collection = (TObjArray *) &data;
    const int nObjects = collection->GetEntries();
    if (nObjects < 2) {
      char buffer[255];
      sprintf(buffer, "Input had %d entries. Expected at least two.", nObjects);
      throw dqm_core::BadConfig( ERS_HERE, name, buffer);
    }

    ERS_LOG("TObjArray size is " << nObjects);
    TObjArrayIter next(collection);
    TObject * object;
    int counter = 0;
    while ( (object = next()) ) {
      if (object == NULL) continue;
      ERS_LOG("data[" << counter << "] = " << object->GetName() << " : " << object->GetTitle());
      counter++;
    }

    TH2 * h2_raw = NULL, * h2_res = NULL;

    TObjArrayIter inputs(collection);
    counter = 0;
    while ( (object = inputs()) ) {
      TH2 * h2_temp = static_cast<TH2 *>(object);
      if (h2_temp == NULL) {
	ERS_LOG("Object at " << counter << " does not cast to a TH2");
	counter++;
	continue;
      }
      std::string name = h2_temp->GetTitle();
      if (name.find("Acc. Vertex") != std::string::npos) {
	h2_raw = h2_temp;
      } else if (name.find("Split Vertex") != std::string::npos) {
	h2_res = h2_temp;
      } else {
	ERS_LOG("Received unexpected histogram: " << name.c_str());
      }
      counter++;
    }

    if (h2_raw == NULL) {
      ERS_LOG("raw 2D histogram was empty or could not be cast");
      dqm_core::Result* result = new dqm_core::Result();
      result->status_ = dqm_core::Result::Red;
      return  result;
    }
    
    if (h2_res == NULL) {
      ERS_LOG( "res 2D histogram was empty or could not be cast");
      dqm_core::Result* result = new dqm_core::Result();
      result->status_ = dqm_core::Result::Red;
      return  result;
    }
		
    //ERS_LOG("Input histograms have " << h2_raw->Integral() << " and " << h2_res->Integral() << " entries.");
    ERS_LOG("Raw title: " << h2_raw->GetName() << " : " << h2_raw->GetTitle() );
    ERS_LOG("Res title: " << h2_res->GetName() << " : " << h2_res->GetTitle() );
		

    //-----------------------------------------------------------------------------------
    //Extract the width, as a function of the selected axis for each,
    //and calculate the corrected width
    //-----------------------------------------------------------------------------------
      
    TH1D * h1_raw = fitSlices(h2_raw);
    
    if (h1_raw == 0) {
      ERS_LOG( "Cannot fit raw slices with " << h2_raw->GetEntries() << " entries");
      dqm_core::Result* result = new dqm_core::Result();
      result->status_ = dqm_core::Result::Red;
      return 	result;			
    }

    float sum = 0;
    for (int i = 0, end = h1_raw->GetNbinsX(); i < end; i++) {
      sum += fabs(h1_raw->GetBinContent(i));
    }
		
    if (sum == 0) {
      ERS_LOG("All raw entries are 0");
      dqm_core::Result* result = new dqm_core::Result();
      result->status_ = dqm_core::Result::Red;
      return 	result;			
    }

    //ERS_LOG("raw abs sum = " << sum);

    TH1D * h1_res = fitSlices(h2_res);
    if (h1_res == 0) {
      ERS_LOG( "Cannot fit res slices with " << h2_res->GetEntries() << " events.");
      dqm_core::Result* result = new dqm_core::Result();
      result->status_ = dqm_core::Result::Red;
      return 	result;
    }

    sum = 0;
    for (int i = 0, end = h1_res->GetNbinsX(); i < end; i++) {
      sum += fabs(h1_res->GetBinContent(i));
    }
    if (sum == 0) {
      ERS_LOG("All res entries are 0");
      dqm_core::Result* result = new dqm_core::Result();
      result->status_ = dqm_core::Result::Red;
      return 	result;			
    }

    //ERS_LOG("res abs sum = " << sum);

    h1_res->Scale(m_factor / sqrt(2));

    TH1D * h1_corr = calcCorrVTrks(h1_raw, h1_res);

    if (h1_corr == 0) {
      ERS_LOG( "Cannot calculate corrected with per bin");
      dqm_core::Result* result = new dqm_core::Result();
      result->status_ = dqm_core::Result::Red;
      return 	result;	
    }

    //-----------------------------------------------------------------------------------
    //Report the average corrected width
    //-----------------------------------------------------------------------------------
    dqm_core::Result* result = new dqm_core::Result();
    result->status_ = dqm_core::Result::Green;

    h1_corr->Fit("pol0", "Q0R", "", m_binMin, m_binMax);
		
    TF1 * f_corr = h1_corr->GetFunction("pol0");
		
    if (f_corr == 0) {
      ERS_LOG( "Could not fit average corrected width");
      result->status_ = dqm_core::Result::Red;
    } else {
			result->tags_["CorrWidth"] = f_corr->GetParameter(0);
			result->tags_["CorrWidthError"] = f_corr->GetParError(0);
		}

    h1_raw->Fit("pol0", "Q0R", "", m_binMin, m_binMax);

    TF1 * f_raw = h1_raw->GetFunction("pol0");

    if (f_raw == 0) {
      ERS_LOG( "Could not fit average raw width");
      result->status_ = dqm_core::Result::Red;
    } else {
			result->tags_["AvgRawWidth"] = f_raw->GetParameter(0);
			result->tags_["AvgRawWidthError"] = f_raw->GetParError(0);
		}

    h1_res->Fit("pol0", "Q0R", "", m_binMin, m_binMax);

    TF1 * f_res = h1_res->GetFunction("pol0");

    if (f_res == 0) {
      ERS_LOG( "Could not fit average resolution");
      result->status_ = dqm_core::Result::Red;
    } else {
			result->tags_["AvgRes"] = f_res->GetParameter(0);
			result->tags_["AvgResError"] = f_res->GetParError(0);
		}
  
    //-----------------------------------------------------------------------------------
    //Clean up and return
    //-----------------------------------------------------------------------------------
    delete(f_corr);
    delete(h1_corr);
    delete(h1_raw);
    delete(h1_res);

    // Return the result
    ERS_LOG( "Return a happy man");
    return 	result;
  }


  void CorrWidth::printDescription() {
    std::string message;
    message += "\n";
    message += "Algorithm: \"" + name + "\"\n";
    message += "Description: Extract a gaussian width given A) a TH2 of the convolved distribution and B) a TH2 of one of the two component distributions\n";
    message += "Parameters: nRMS   - the number of RMS over which to fit the Gaussian (default = 3)\n";
    message += "            binMin - the lowest over which to average the corrected value (default = 6)\n";
    message += "            binMax - the highest bin over which to average the corrected value (default = 999)\n";
    message += "            axis   - the axis along which to project the distribution, 0 for Y and 1 for  X (default = 0)\n";
    message += "            factor - a constant factor to apply to the component distribution before the quadratic subtraction (default = 1 / sqrt(2))\n";
    message += "Limits: None\n";
    //		message += " \"" + limitName + "\" : generate warnings on the chi2 value\n";
    message += "\n";
  
    std::cout << message;
  }


  //-----------------------------------------------------------------------------------
  //Private Methods
  //-----------------------------------------------------------------------------------

  TH1D * CorrWidth::fitSlices(TH2 * h2) {
    if (m_axis != 0 && m_axis != 1) {
      return NULL;
    }

    //ERS_LOG("fitSlices for " << h2->GetTitle());

    TAxis * x = NULL;
//    TAxis * y = NULL;
    if (m_axis == 0) {
      x = h2->GetXaxis();
//      y = h2->GetYaxis();
    }
    else if (m_axis == 1) {
      x = h2->GetYaxis();
//      y = h2->GetXaxis();
    }
		
    const int nX = x->GetNbins();
    //const int nY = y->GetNbins();

//     std::stringstream ss;
//     ss.str("");
//     for (int i = 0; i < nX; i++) {
//       ss <<  " " << x->GetBinLowEdge(i);
//     }
//     ERS_LOG("x axis bins: " << ss.str().c_str());

//     ss.str("");
//     for (int i = 0; i < nY; i++) {
//       ss <<  " " << y->GetBinLowEdge(i);
//     }
//     ERS_LOG("y axis bins: " << ss.str().c_str());

		
    //Work around ROOT memory issues
    char buffer[255];
    sprintf(buffer, "%s_sigma", h2->GetName());

    TH1D * gr = NULL;
    if (m_axis == 0)
      gr = h2->ProjectionX(buffer);
    else if (m_axis == 1)
      gr = h2->ProjectionY(buffer);

    gr->Reset();
		
    TH1D * h = NULL;
    TF1 * f = NULL;
    sprintf(buffer, "%s_proj", h2->GetName());
    for (int i = 1; i <= nX; i++) {
      if (m_axis == 0)
	h = h2->ProjectionY(buffer, i, i);
      else if (m_axis == 1)
	h = h2->ProjectionY(buffer, i, i);

      float mean = h->GetMean();
      float rms = h->GetRMS();
      if (rms == 0)
	rms = 0.2;

      if (h->GetEntries() > 10)
	h->Fit("gaus", "Q0R", "", mean - (m_nRMS * rms), mean + (m_nRMS * rms));

      f = h->GetFunction("gaus");
      if (f) {
	float par = f->GetParameter(2), parE = f->GetParError(2);
	gr->SetBinContent(i, par);
	gr->SetBinError(i, parE);
	//ERS_LOG(i << " (" << gr->GetXaxis()->GetBinLowEdge(i) << ") " << par);
	//ERS_LOG("xaxis low edge: " << x->GetBinLowEdge(i) );
      }
      delete(h);
    }
		
    return gr;
  }

  //-----------------------------------------------------------------------------------
  //Do the quadratic subtraction and calculate the error
  //-----------------------------------------------------------------------------------
  void CorrWidth::calcCorr(float p, float dp, float s, float ds, float &corr, float &dcorr) {
    if (s > p) return;
    corr = sqrt( pow(p, 2) - pow(s, 2) );
    float a = pow(p * dp, 2);
    float b = pow(s * ds, 2);
    dcorr = sqrt( (a + b) )/ corr;
  }

  //-----------------------------------------------------------------------------------
  //Compute the correction for each bin
  //-----------------------------------------------------------------------------------
  TH1D * CorrWidth::calcCorrVTrks(TH1D * h_raw, TH1D * h_res) {

    //Prepare the output                                                                                                                                                                                                                       
    TH1D * output = (TH1D *) h_res->Clone();
    output->Reset();

    //For each bin, calculate the correction                                                                                                                                                                                                   
    for (int i = 2, nBins = h_raw->GetXaxis()->GetBinLowEdge(h_raw->GetNbinsX()); i < nBins; i++) {
      int raw_bin = h_raw->FindBin(i);
      float raw = h_raw->GetBinContent(raw_bin);
      float rawE = h_raw->GetBinError(raw_bin);

      int res_bin = h_res->FindBin(i);
      float res = h_res->GetBinContent(res_bin);
      float resE = h_res->GetBinError(res_bin);

			
      //We are not fans of imaginary numbers in our histograms                                                                                                                                                                                 
      //ERS_LOG("calcCorr " << i << " : (" << raw_bin << "): " << raw << " +- " << rawE << ", (" << res_bin << "): " << res << " +- " << resE)
			
	if (res > raw || res == 0) continue;
			
      float corr, corrE;
      calcCorr(raw, rawE, res, resE, corr, corrE);
			
      output->SetBinContent(output->FindBin(i), corr);
      output->SetBinError(output->FindBin(i), corrE);
    }

    return output;

  }

  double CorrWidth::GetFirstFromMap(const std::string &paramName, const std::map<std::string, double > &params, double defaultValue) {
    std::map<std::string, double >::const_iterator it = params.find(paramName);
    if (it == params.end() ) {
      return defaultValue; // this is the only difference between the two overloaded versions
    } else {
      return it->second;
    }
  }

        
} // namespace dqm_algorithms

