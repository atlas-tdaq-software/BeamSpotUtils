//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include <string>
#include <vector>

// This class
#include "BeamSpotUtils/ISWriter.h"

#include "BeamSpotUtils/BeamSpotParametersIS.h"

#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/partition.h"

#include "beamspot/BeamSpotParametersNamed.h"
#include "beamspot/BeamSpotParametersByBunchNamed.h"

// Custom ERS declarations

namespace daq
{
  ERS_DECLARE_ISSUE(	beamspot,
			BadISWrite,
			"Writing to IS failed because: " << explanation,
                        ( (const char*) explanation )
                        )

  ERS_DECLARE_ISSUE(	beamspot,
			InvalidPartition,
			"Partition is not valid: " << explanation,
                        ( (const char*) explanation )
                        )

  ERS_DECLARE_ISSUE(	beamspot,
			EnvironmentMissing,
			"Environment variable is not defined: " << explanation,
                        ( (const char*) explanation )
                        )
}

// Constructors

daq::beamspot::ISWriter::ISWriter( const std::string& objectName,
                                   const std::string& byBunchObjectName )
  : m_objectName( objectName )
  , m_byBunch_objectName( byBunchObjectName )
{
  try
    {
      int argc=1;
      const char* argv[] = { "ISWriter" };

      // FIXME: Need to cast away const-ness because IPCCore::init()
      // signature is not const-clean
      IPCCore::init( argc, (char**) argv );
    }
  catch ( daq::ipc::CannotInitialize& e )
    {
      ers::error(e);
      return;
    }
  catch ( daq::ipc::AlreadyInitialized& e )
    {
      // This should be harmless
      ers::log(e);
    }

  // Retrieve TDAQ_PARTITION from the environment
  const char* partitionName = getenv( "TDAQ_PARTITION" );
  if ( partitionName )
    {
      m_partitionName = partitionName;
    }
  else
    {
      daq::beamspot::EnvironmentMissing i( ERS_HERE, "TDAQ_PARTITION" );
      ers::error(i);
      return;
    }

}

// Destructor

daq::beamspot::ISWriter::~ISWriter()
{
}

// Methods

bool
daq::beamspot::ISWriter::write( const std::vector<BeamSpotParameters>& bsParsVec )
{
  ERS_LOG( "Trying to connect to partition \"" << m_partitionName << "\"" );

  IPCPartition partition( m_partitionName );

  if ( ! partition.isValid() )
    {
      daq::beamspot::InvalidPartition i( ERS_HERE, m_partitionName.c_str() );
      ers::error(i);
      return false;
    }

  ERS_DEBUG( 0, "Successfully connected to partition \"" << partition.name() << "\"" );

  if ( bsParsVec.size() == 1) { // only 1 snapshot means bunch-averaged
    try
      {
        ERS_LOG( "Trying to write beam spot parameters to IS object \"" << m_objectName << "\"" );
        ERS_LOG( "BeamSpotParameters:" << bsParsVec[0] );

        ::beamspot::BeamSpotParametersNamed beamSpotInfo( partition, m_objectName );

        const BeamSpotParametersIS parametersIS( bsParsVec[0] );
        parametersIS.copyTo( beamSpotInfo );

        beamSpotInfo.checkin();

        ERS_LOG( "Wrote the following beam spot parameters to IS: " << beamSpotInfo );
      }
    catch ( ers::Issue& i )
      {
        ers::error(i);
        return false;
      }
  }
  else { // multiple snapshots mean per-bunch values
    try
      {
        ERS_LOG( "Trying to write per-bunch parameters to IS object \"" << m_byBunch_objectName << "\"" );

        ::beamspot::BeamSpotParametersByBunchNamed beamSpotInfoByBunch 
                                         ( partition, m_byBunch_objectName );

        for (size_t bcid = 0; bcid < bsParsVec.size(); ++bcid) {
          const BeamSpotParametersIS bsParsThisBunch( bsParsVec[bcid] );
          ::beamspot::BeamSpotParameters bsParsISThisBunch;
          bsParsThisBunch.copyTo( bsParsISThisBunch );
          beamSpotInfoByBunch.paramsByBunch.push_back(bsParsISThisBunch);
        }

        beamSpotInfoByBunch.checkin();
       }
     catch ( ers::Issue& i )
       {
         ers::error(i);
         return false;
       }
  }

  return true;
}
