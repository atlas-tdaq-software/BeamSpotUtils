//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include <string>

// This class
#include "BeamSpotUtils/BeamSpotIS2CoolObject.h"

#include "ipc/partition.h"
//#include "daq/beamspot/BeamSpotParametersNamed.h"
#include "beamspot/BeamSpotParametersNamed.h"
using beamspot::BeamSpotParametersNamed;
// FIXME: We may move this to namespace to daq::beamspot for consistency

#include <CoolKernel/RecordSpecification.h>
#include <CoolKernel/Record.h>


namespace daq
{
  namespace beamspot
  {

    ISNamedInfo*
    BeamSpotIS2CoolObject::create( const IPCPartition& partition, const std::string& objectName ) const
    {
      //      return new daq::beamspot::BeamspotParametersNamed( partition, objectName );
      return new BeamSpotParametersNamed( partition, objectName );
    }


    void
    BeamSpotIS2CoolObject::fillRecord( cool::Record& record ) const
    {
      // Is there a better way to do the down cast?
      const BeamSpotParametersNamed& isObject = static_cast< const BeamSpotParametersNamed& >( *isNamedInfoPtr() );
      record[ "status"     ].setValue<cool::Int32>( isObject. status    );
      record[    "posX"    ].setValue<cool::Float>( isObject.   posX    );
      record[    "posY"    ].setValue<cool::Float>( isObject.   posY    );
      record[    "posZ"    ].setValue<cool::Float>( isObject.   posZ    );
      record[  "sigmaX"    ].setValue<cool::Float>( isObject. sigmaX    );
      record[  "sigmaY"    ].setValue<cool::Float>( isObject. sigmaY    );
      record[  "sigmaZ"    ].setValue<cool::Float>( isObject. sigmaZ    );
      record[   "tiltX"    ].setValue<cool::Float>( isObject.  tiltX    );
      record[   "tiltY"    ].setValue<cool::Float>( isObject.  tiltY    );
      record[ "sigmaXYErr" ].setValue<cool::Float>( isObject.sigmaXY    );
      record[    "posXErr" ].setValue<cool::Float>( isObject.   posXErr );
      record[    "posYErr" ].setValue<cool::Float>( isObject.   posYErr );
      record[    "posZErr" ].setValue<cool::Float>( isObject.   posZErr );
      record[  "sigmaXErr" ].setValue<cool::Float>( isObject. sigmaXErr );
      record[  "sigmaYErr" ].setValue<cool::Float>( isObject. sigmaYErr );
      record[  "sigmaZErr" ].setValue<cool::Float>( isObject. sigmaZErr );
      record[   "tiltXErr" ].setValue<cool::Float>( isObject.  tiltXErr );
      record[   "tiltYErr" ].setValue<cool::Float>( isObject.  tiltYErr );
      record[ "sigmaXYErr" ].setValue<cool::Float>( isObject.sigmaXYErr );
    }


    void
    BeamSpotIS2CoolObject::setRecordSpec( cool::RecordSpecification& recordSpec ) const
    {
      recordSpec.extend( "status"    , cool::StorageType::Int32 );
      recordSpec.extend(    "posX"   , cool::StorageType::Float );
      recordSpec.extend(    "posY"   , cool::StorageType::Float );
      recordSpec.extend(    "posZ"   , cool::StorageType::Float );
      recordSpec.extend(  "sigmaX"   , cool::StorageType::Float );
      recordSpec.extend(  "sigmaY"   , cool::StorageType::Float );
      recordSpec.extend(  "sigmaZ"   , cool::StorageType::Float );
      recordSpec.extend(   "tiltX"   , cool::StorageType::Float );
      recordSpec.extend(   "tiltY"   , cool::StorageType::Float );
      recordSpec.extend( "sigmaXY"   , cool::StorageType::Float );
      recordSpec.extend(    "posXErr", cool::StorageType::Float );
      recordSpec.extend(    "posYErr", cool::StorageType::Float );
      recordSpec.extend(    "posZErr", cool::StorageType::Float );
      recordSpec.extend(  "sigmaXErr", cool::StorageType::Float );
      recordSpec.extend(  "sigmaYErr", cool::StorageType::Float );
      recordSpec.extend(  "sigmaZErr", cool::StorageType::Float );
      recordSpec.extend(   "tiltXErr", cool::StorageType::Float );
      recordSpec.extend(   "tiltYErr", cool::StorageType::Float );
      recordSpec.extend( "sigmaXYErr", cool::StorageType::Float );
    }

  }
}
