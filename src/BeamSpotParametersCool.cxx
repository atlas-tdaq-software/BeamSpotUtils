//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "BeamSpotUtils/BeamSpotParametersCool.h"

#include <CoolKernel/Record.h>
#include <CoralBase/AttributeList.h>
#include <CoralBase/Attribute.h>

using namespace daq;

// Methods

void 
BeamSpotParametersCool::copyFrom( const cool::IRecord& record )
{
  m_status     =  record[ "status"     ].data<cool::Int32>();
     m_posX    =  record[    "posX"    ].data<cool::Float>();
     m_posY    =  record[    "posY"    ].data<cool::Float>();
     m_posZ    =  record[    "posZ"    ].data<cool::Float>();
   m_sigmaX    =  record[  "sigmaX"    ].data<cool::Float>();
   m_sigmaY    =  record[  "sigmaY"    ].data<cool::Float>();
   m_sigmaZ    =  record[  "sigmaZ"    ].data<cool::Float>();
    m_tiltX    =  record[   "tiltX"    ].data<cool::Float>();
    m_tiltY    =  record[   "tiltY"    ].data<cool::Float>();
  m_sigmaXY    =  record[ "sigmaXYErr" ].data<cool::Float>();
     m_posXErr =  record[    "posXErr" ].data<cool::Float>();
     m_posYErr =  record[    "posYErr" ].data<cool::Float>();
     m_posZErr =  record[    "posZErr" ].data<cool::Float>();
   m_sigmaXErr =  record[  "sigmaXErr" ].data<cool::Float>();
   m_sigmaYErr =  record[  "sigmaYErr" ].data<cool::Float>();
   m_sigmaZErr =  record[  "sigmaZErr" ].data<cool::Float>();
    m_tiltXErr =  record[   "tiltXErr" ].data<cool::Float>();
    m_tiltYErr =  record[   "tiltYErr" ].data<cool::Float>();
  m_sigmaXYErr =  record[ "sigmaXYErr" ].data<cool::Float>();
}


void
BeamSpotParametersCool::copyTo( cool::Record& record ) const
{
  record[ "status"     ].setValue<cool::Int32>(  m_status   );
  record[    "posX"    ].setValue<cool::Float>(    m_posX   );
  record[    "posY"    ].setValue<cool::Float>(    m_posY   );
  record[    "posZ"    ].setValue<cool::Float>(    m_posZ   );
  record[  "sigmaX"    ].setValue<cool::Float>(  m_sigmaX   );
  record[  "sigmaY"    ].setValue<cool::Float>(  m_sigmaY   );
  record[  "sigmaZ"    ].setValue<cool::Float>(  m_sigmaZ   );
  record[   "tiltX"    ].setValue<cool::Float>(   m_tiltX   );
  record[   "tiltY"    ].setValue<cool::Float>(   m_tiltY   );
  record[ "sigmaXYErr" ].setValue<cool::Float>( m_sigmaXY   );
  record[    "posXErr" ].setValue<cool::Float>(    m_posXErr);
  record[    "posYErr" ].setValue<cool::Float>(    m_posYErr);
  record[    "posZErr" ].setValue<cool::Float>(    m_posZErr);
  record[  "sigmaXErr" ].setValue<cool::Float>(  m_sigmaXErr);
  record[  "sigmaYErr" ].setValue<cool::Float>(  m_sigmaYErr);
  record[  "sigmaZErr" ].setValue<cool::Float>(  m_sigmaZErr);
  record[   "tiltXErr" ].setValue<cool::Float>(   m_tiltXErr);
  record[   "tiltYErr" ].setValue<cool::Float>(   m_tiltYErr);
  record[ "sigmaXYErr" ].setValue<cool::Float>( m_sigmaXYErr);
}
