//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include <cstdlib>
using std::atoi;

//#include "BeamSpotUtils/BeamSpotParameters.h"
#include "BeamSpotUtils/CoolReader.h"

// DB and IS connection parameters

std::string dbConnection = "sqlite://;schema=beamSpotIS2CoolApp_test.db;dbname=CONDBR2";
std::string dbTag        = "IndetBeamposOnl-HLT-UPD1-000-00";


int main( int argc, char* argv[] )
{
  enum { SUCCESS=0, ERROR_RETURN=1, ERROR_EXCEPTION=2 };

  unsigned int run=0, lbn=0;

  int i=0;
  if (argc > ++i) run    = atoi( argv[i] );
  if (argc > ++i) lbn    = atoi( argv[i] );
  if (argc > ++i) dbConnection = argv[i];
  if (argc > ++i) dbTag        = argv[i];

  bool success = false;

  try
    {
      daq::beamspot::CoolReader reader( dbConnection, dbTag );
      success = reader.read( run, lbn );
    }
  catch ( std::exception& e )
    {
      return ERROR_EXCEPTION;
    }

  return success ? SUCCESS : ERROR_RETURN;
}
