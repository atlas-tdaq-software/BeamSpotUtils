//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "BeamSpotUtils/BeamSpotParameters.h"
#include "BeamSpotUtils/ISWriter.h"

#include "ers/ers.h"

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/lexical_cast.hpp>
using boost::lexical_cast;
using boost::bad_lexical_cast;

#include <vector>
#include <string>

#include <iostream>
using std::endl;
using std::cerr;

using std::vector;
using std::string;

namespace po = boost::program_options;

const std::string defaultServerName = "BeamSpot";
const std::string defaultObjectName = "HLTParameters";
const std::string defaultByBunchObjectName = "ByBunchMon";

// copy CLI args to a vector of BeamSpotParameters objects
// replaces BeamSpotParameters.copyFrom()
void copyArgsToBSPars( const vector<string>&,
                             vector<daq::BeamSpotParameters>&);

int main( int argc, char* argv[] )
{

  enum { SUCCESS=0, BAD_USAGE=1, BAD_COMMAND=2 };

  // Parse the command line
  po::options_description desc( "Options" );
  desc.add_options()
    ( "help", "this help message" )
    ( "server-name", po::value<string>()->default_value( defaultServerName ), "name of the IS server to publish to" )
    ( "name"       , po::value<string>()->default_value( defaultObjectName ), "name under which to publish the bunch-averaged IS object" )
    ( "by-bunch-name", po::value<string>()->default_value( defaultByBunchObjectName ), "name under which to publish the per-bunch IS object" )
    ( "beamspot-parameters" , po::value< vector<string> >(), "list of beam spot parameters" )
    ;

  po::positional_options_description pod;
  pod.add( "beamspot-parameters", -1 );
  po::variables_map vm;
  // We must disable short options in order to be able to parse negative numbers without quoting
  const int s =  po::command_line_style::default_style ^ po::command_line_style::allow_short;
  po::store( po::command_line_parser( argc, argv ).style( s ).options( desc ).positional( pod ).run(), vm );
  po::notify( vm );

  if ( vm.count( "help" ) )
    {
      ERS_LOG( desc );
      return BAD_USAGE;
    }

  const vector<string> positionalPars = vm[ "beamspot-parameters" ].as< vector<string> >();

  vector<daq::BeamSpotParameters> bsParsVec;
  // bsPars.copyFrom( positionalPars ); // -> moved copy fcn to ISWriterApp
  copyArgsToBSPars( positionalPars, bsParsVec );

  const string isName = vm[ "server-name" ].as<string>() + "." + vm[ "name" ].as<string>();
  const string isByBunchName = vm[ "server-name" ].as<string>() + "." + vm[ "by-bunch-name" ].as<string>();
  daq::beamspot::ISWriter writer( isName, isByBunchName );
  const bool success = writer.write( bsParsVec );
  if ( ! success )
    return BAD_COMMAND;

  return SUCCESS;
}

void copyArgsToBSPars( const vector<string>& pars, 
                             vector<daq::BeamSpotParameters>& bsParsVec )
{
  const int n = pars.size();
  int i = -1;
  unsigned temp_status = 4;
  float temp_posX = 0.     , temp_posY = 0.     , temp_posZ = 0.      ,
        temp_sigmaX = 0.   , temp_sigmaY = 0.   , temp_sigmaZ = 0.    ,
        temp_tiltX = 0.    , temp_tiltY = 0.    , temp_sigmaXY = 0.   ,
        temp_posXErr = 0.  , temp_posYErr = 0.  , temp_posZErr = 0.   ,
        temp_sigmaXErr = 0., temp_sigmaYErr = 0., temp_sigmaZErr = 0. ,
        temp_tiltXErr = 0. , temp_tiltYErr = 0. , temp_sigmaXYErr = 0.;

  try {
    do {
      if ( ++i < n )  temp_status    = lexical_cast<unsigned>( pars[i] ); else break;
      if ( ++i < n )    temp_posX    = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )    temp_posY    = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )    temp_posZ    = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )  temp_sigmaX    = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )  temp_sigmaY    = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )  temp_sigmaZ    = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )   temp_tiltX    = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )   temp_tiltY    = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n ) temp_sigmaXY    = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )    temp_posXErr = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )    temp_posYErr = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )    temp_posZErr = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )  temp_sigmaXErr = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )  temp_sigmaYErr = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )  temp_sigmaZErr = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )   temp_tiltXErr = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n )   temp_tiltYErr = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n ) temp_sigmaXYErr = lexical_cast<double>( pars[i] ); else break;
      daq::BeamSpotParameters bsp ( temp_status,
                                    temp_posX,
                                    temp_posY,
                                    temp_posZ,
                                    temp_sigmaX,
                                    temp_sigmaY,
                                    temp_sigmaZ,
                                    temp_tiltX,
                                    temp_tiltY,
                                    temp_sigmaXY,
                                    temp_posXErr,
                                    temp_posYErr,
                                    temp_posZErr,
                                    temp_sigmaXErr,
                                    temp_sigmaYErr,
                                    temp_sigmaZErr,
                                    temp_tiltXErr,
                                    temp_tiltYErr,
                                    temp_sigmaXYErr ) ;
     bsParsVec.push_back(bsp);
    } while (true);
  } catch ( const bad_lexical_cast& )
    {
      std::cerr << "failed to parse argument list" << std::endl;
      throw;
    }
}
