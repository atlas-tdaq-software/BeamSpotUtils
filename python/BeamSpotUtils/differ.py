''' Library: Statis methods to decide if an HLT should occur
    Contains class differ and static method boxCut
'''

from __future__ import annotations

import logging
import math
from math import hypot
from collections.abc import Callable

from . import model, translators


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


# ### Different kind of cuts!  ####
def boxCut(ref: model.BSSnapshot, cur: model.BSSnapshot) -> bool:
    """Returns True if HLT should be updated bc of boxCut logic.

    This method is used by default by Differ class below.

    Parameters
    ----------
    ref : model.BSSnapshot
        Reference BeamSpot snapshot
    cur : model.BSSnapshot
        Newly calculated BeamSpot snapshot
    """

    # Invalid reference parameters
    for var in ('pos', 'sigma'):
        for axis in 'XYZ':
            varErr = var + axis + 'Err'
            if getattr(ref, varErr) == 0:
                _LOG.info('Choose to update because %s is 0', varErr)
                return True

    for axis in 'XYZ':
        sigma = 'sigma' + axis
        if getattr(ref, sigma) == 0:
            _LOG.info('Choose to update because %s is 0', sigma)
            return True

    # Significant change in parameters compared to reference
    minRelChangePos = 0.10  # position change as a fraction of the width
    minRelChangeSigma = 0.10  # fractional width change
    minRelChangeErr = 0.50  # fractional error change
    minSignificance = 2  # minimal number of sigmas

    # Change in position
    for axis in 'XYZ':
        pos = 'pos' + axis
        posErr = 'pos' + axis + 'Err'
        sigma = 'sigma' + axis
        relChange = (getattr(cur, pos) - getattr(ref, pos)) / getattr(ref, sigma)
        delChange = hypot(getattr(cur, posErr), getattr(ref, posErr)) / getattr(ref, sigma)
        significance = abs(relChange) / delChange

        if abs(relChange) > minRelChangePos and significance > minSignificance:
            _LOG.info('Choose to update because %s moved by %s%% of %s with significance %s',
                      pos, 100 * relChange, sigma, significance)
            return True

    # Change in width
    for axis in 'XYZ':
        sigma = 'sigma' + axis
        sigmaErr = 'sigma' + axis + 'Err'
        relChange = getattr(cur, sigma) / getattr(ref, sigma) - 1.
        delChange = getattr(cur, sigma) / getattr(ref, sigma) * \
            hypot(getattr(cur, sigmaErr) / getattr(cur, sigma),
                  getattr(ref, sigmaErr) / getattr(ref, sigma))
        significance = abs(relChange) / delChange

        if abs(relChange) > minRelChangeSigma and significance > minSignificance:
            _LOG.info('Choose to update because %s changed by %s%% with significance %s',
                      sigma, 100 * relChange, significance)
            return True

    # Decrease in error
    for var in ('pos', 'sigma'):
        for axis in 'XYZ':
            varErr = var + axis + 'Err'
            relChange = getattr(cur, varErr) / getattr(ref, varErr) - 1.

            if relChange < -minRelChangeErr:
                _LOG.info('Choose to update because %s decreased by %s%%', varErr, 100 * relChange)
                return True

    return False


class Differ:
    '''Class which compares new representation with the reference
    and decides what representation has to be saved.

    Parameters
    ----------
    reference : object
        Can be either a string, a `BeamSpotRepresentation` instance
        or a single `BSSnapshot`.
    '''

    def __init__(self, reference: model.BSSnapshot | str = ""):
        self.reference = reference

    def _getLastSnapshot(self, desc: model.BeamSpotRepresentation | model.BSSnapshot | str,
                         run: int | None = None, lbn: int | None = None) -> model.BSSnapshot | None:
        """Returns latest snapshot from representation.

        Parameters
        ----------
        desc : object
            See _getRep() documentation.
        run : int, optional
            Run number
        lbn : int, optional
            LB number

        Returns
        -------
        BeamSpot snapshot (dict-like object)
        """
        rep = self._getRep(desc, run, lbn)
        if rep is None:
            # should not really happen at all
            _LOG.error("Failed to find a beamspot rep for <%s>", desc)
            return None

        # returns None if rep is empty
        return rep.last()

    def _getRep(self, desc: model.BeamSpotRepresentation | model.BSSnapshot | str,
                run: int | None = None, lbn: int | None = None
                ) -> model.BeamSpotRepresentation | None:
        '''Retrieve representation given description

        Parameters
        ----------
        desc : object
            Can be either a string, a BeamSpotRepresentation instance or a snapshot.
            If string then translator for this description will be used to retrieve
            representation. If snapshot then representation with a single snapshot
            will be returned. If BeamSpotRepresentation the it will be returned
            without change. In all other cases None is returned.
        run : int, optional
        lbn : int, optional
            `run` and `lb` are only used by COOL database translators.

        Returns
        -------
        `BeamSpotRepresentation` instance

        Raises
        ------
        Exception is raised if `desc` cannot be recognized.
        '''
        if isinstance(desc, model.BeamSpotRepresentation):
            _LOG.debug("Was a beamspot representation anyway")
            return desc
        elif isinstance(desc, model.BSSnapshot):
            _LOG.debug("Desc was a full snapshot")
            return model.BeamSpotRepresentation([model.BSSnapshot(desc)])
        # elif isinstance(desc, dict):
        #     _LOG.debug("Desc was a dictionary, hopefully its a full snapshot")
        #     return model.BeamSpotRepresentation([model.BSSnapshot(desc)])
        else:
            # must be a string
            _LOG.debug("I received this desc: %s", desc)
            trans = translators.make_translator(desc, run=run, lbn=lbn)
            # this can return None or throw on failure
            return trans.read()

    def storeRepr(self, current_rep: model.BeamSpotRepresentation, method: Callable = boxCut,
                  r4p: bool | None = True) -> model.BeamSpotRepresentation | None:
        '''Compare new snapshot to reference.

        This method is supposed to be used with bunch-average parameters,
        so representations (current and reference) are supposed to have
        only single snapshot in them. If they contain more than one snapshot
        then latest snapshot is used.

        Current and reference snapshots are compared for difference in
        parameters using separate caller-provided method which should return
        True if there is a significant difference between snapshots.
        Current snapshot is also checked for validity to filter out
        unphysical parameter values.

        If current snapshot does not exist (fit failed) or is not valid then
        this method can return reference snapshot instead. In this case and
        other conditions it can update status fields to invalidate output
        representation based on the value of `r4p` flag.

        Here are all possible cases for what we want to be in output based
        on inputs:
          - `reference` is missing or invalid:
             - `current` is missing or invalid: return None
             - r4p=False: `current` with status=4
             - r4p=True:  `current` with status unchanged
             - r4p=None:  `current` with status unchanged
          - `current` is missing or invalid:
             - r4p=False: `reference` with status=4
             - r4p=True:  None
             - r4p=None:  None
          - `current` is valid:
             - r4p=False: `current` with status=4
             - r4p=True:  `current` with status unchanged
             - r4p=None:  `current` with status taken from reference
          - in all cases we compare that result with `reference`, difference
            in status is always considered significant.

        Parameters
        ----------
        current_rep : object
            Can be either a string, a BeamSpotRepresentation instance
            or a snapshot.
        method : callable, optional
            Callable to return True if update is necessary. Defaults to
            `boxCut` method defined above, any other method that accepts
            the same arguments is allowed.
        r4p : bool or None
            Ready4Physics flag, if none it means it cannot be determined
            (IS failure).

        Returns
        -------
        Representation that should be saved, or None if there nothing to
        be saved.
        '''
        current = self._getLastSnapshot(current_rep)

        # get run/lb for reference, use very large number if not defined
        if current:
            run = current.run
            lbn = current.lbn
        else:
            run = 2000000000
            lbn = 0

        if current is not None and not self._sanityCheck(current):
            _LOG.info("Current snapshot failed sanity check, discarding")
            current = None

        if current is None and (r4p is None or r4p):
            # stop here, no update is needed
            _LOG.info("Current snapshot missing, r4p=%s, no update", r4p)
            return None

        # get reference snapshot
        reference = self._getLastSnapshot(self.reference, run, lbn)
        if reference is None:
            # could happen on very first update (e.g. no data in database yet)
            # so we want to store it in this case
            if current:
                if r4p is False:
                    current = model.BSSnapshot(current, status=4)
                _LOG.info("No reference beamspot, will force update with status %s",
                          current.status)
                return model.BeamSpotRepresentation([current])
            else:
                _LOG.info("No reference or current beamspot, no updates")
                return None

        _LOG.info("Here is the reference:")
        reference.prettyLog(_LOG)

        if current is None:
            # r4p is always False here, non-False case is handled above
            # want to invalidate what is stored already
            _LOG.info("Current snapshot is missing, replace with with reference [status=4]")
            current = model.BSSnapshot(reference, status=4)
        else:
            if r4p is None:
                _LOG.info("Ready4Physics is unknown, use status from reference [status=%s]",
                          reference.status)
                current = model.BSSnapshot(current, status=reference.status)
            elif not r4p:
                _LOG.info("Ready4Physics is False, invalidate status [status=4]")
                current = model.BSSnapshot(current, status=4)
            _LOG.info("Here is the current:")
            current.prettyLog(_LOG)

        # compare current with reference, check status first
        if current.status != reference.status:
            _LOG.info('Do update because status different: ref=%s cur=%s',
                      reference.status, current.status)
            return model.BeamSpotRepresentation([current])

        # compare parameters
        if method(reference, current):
            return model.BeamSpotRepresentation([current])

        # no diffs
        return None

    def sanityCheck(self, current_rep: model.BeamSpotRepresentation) -> bool:
        """Check that representation passes basic checks.

        Parameters
        ----------
        current : BeamSpotRepresentation

        Returns
        -------
        False if any field is unphysical (e.g. NaN of negative sigmas).
        """
        current = self._getLastSnapshot(current_rep)
        if current is None:
            # this means that rep was empty
            _LOG.info("No current beamspot, nothing to check")
            return False

        if not self._sanityCheck(current):
            _LOG.info("Current snapshot failed sanity check")
            return False

        return True

    @staticmethod
    def _sanityCheck(current: model.BSSnapshot) -> bool:
        '''Checks that none of the parameters is NaN'''

        for var in ('posX', 'posY', 'posZ', 'sigmaX', 'sigmaY', 'sigmaZ', 'tiltX', 'tiltY'):
            for key in (var, var + 'Err'):
                num = getattr(current, key)
                if num is None:
                    _LOG.warning('Entry is None: current.%s', key)
                    return False
                if not isinstance(num, float):
                    _LOG.warning('Entry is not a float: current.%s -- %s (%s)', key, num, type(num))
                    return False
                if math.isnan(num):
                    _LOG.warning('Result for %r is NaN', key)
                    return False
        for key in ('sigmaX', 'sigmaY', 'sigmaZ'):
            if getattr(current, key) < 0:
                _LOG.warning('Result for %r is negative', key)
                return False
        return True
