'''Library of helper functions wrapping python module logging'''

from __future__ import annotations

import logging
import sys
import os
from typing import TextIO

LOG_FORMAT = '%(asctime)s %(levelname)-8s %(name)-24s :: %(message)s'


def _sameFile(file1: TextIO, file2: TextIO) -> bool:
    """ Returns True if two open file objects refer to the same file """
    try:
        stat1 = os.fstat(file1.fileno())
        stat2 = os.fstat(file2.fileno())
        return stat1.st_dev == stat2.st_dev and stat1.st_ino == stat2.st_ino
    except OSError as exc:
        print("Exception while comparing files:", str(exc), file=sys.stderr)
        return False


def initLogger(verbosity: int) -> logging.Logger:
    """Initialize Python logging, return root logger.

    Parameters
    ----------
    verbosity : int
        Integer in the range 1-5, higher values will produce more verbose logging

    Returns
    -------
    logging.Logger instance.
    """

    lvl = logging.DEBUG if verbosity > 4 else logging.INFO

    formatter = logging.Formatter(LOG_FORMAT)
    outHandler = logging.StreamHandler(sys.stdout)
    outHandler.setFormatter(formatter)
    headLogger = logging.getLogger()
    headLogger.addHandler(outHandler)
    headLogger.setLevel(lvl)

    # send errors and warnings to stderr too but only if it's different from stdout
    if not _sameFile(sys.stderr, sys.stdout):
        errHandler = logging.StreamHandler(sys.stderr)
        errHandler.setLevel(logging.WARNING)
        errHandler.setFormatter(formatter)
        headLogger.addHandler(errHandler)

    headLogger.debug('Verbosity level set to ' + logging.getLevelName(lvl))
    return headLogger
