"""Random collection of useful utility methods
"""

from __future__ import annotations

from collections.abc import Iterable


def prettyRange(collWDuplicates: Iterable[int]) -> str:
    """Make printable representation of a collection of numbers.

    Produces comma-separated sequence of ranges, e.g. {1, 5, 7-12}.
    If input contains repeating numbers those are printed too.

    Parameters
    ----------
    collWDuplicates : iterable

    Returns
    -------
    String in the form "{1, 5, 7-12}"
    """
    collList = list(collWDuplicates)
    coll = set(collList)
    if not coll:
        return '{}'

    up = min(coll)
    last = max(coll)
    li = list('{%d' % up)
    prev = up

    for e in sorted(coll):
        if e > up + 1:
            if up > prev:
                li.append('-%d' % up)

            li.append(', %d' % e)
            prev = e
        if e == last:
            if e > prev:
                li.append('-%d' % e)
            li.append('}')
        up = e

    s = ''.join(li)

    theDuplicates = collList[:]
    for e in coll:
        theDuplicates.remove(e)

    duplicates = set(theDuplicates)
    if len(duplicates) > 0:
        s += ' These had duplicates '
        s += prettyRange(duplicates)

    return s
