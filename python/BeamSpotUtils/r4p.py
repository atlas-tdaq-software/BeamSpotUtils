"""Access to Ready4Physics flag.
"""

from __future__ import annotations

import logging

import ispy


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


class Ready4Physics:
    """Class which encapsulates access to Ready4Physics IS object.

    Parameters
    ----------
    use_r4p : `bool`
        If False then always return True.
    r4p_partition : `str`
        Name of partition with IS server.
    r4p_variable : `str`
        IS object name including server name
    """
    def __init__(self, use_r4p: bool, r4p_partition: str, r4p_variable: str):
        self.use_r4p = use_r4p
        self.r4p_partition = r4p_partition
        self.r4p_variable = r4p_variable

    def __call__(self) -> bool | None:
        """Return value of Ready4Physics flag.

        Returns True if `use_r4p` is False, otherwise returns value of
        Ready4Physics from IS or None if it cannot be found in IS.

        Returns
        -------
        ready4physics : `bool`
            True, False, or None.
        """

        if not self.use_r4p:
            return True

        # Try to get R4P flag
        try:
            isobj = ispy.ISObject(self.r4p_partition, self.r4p_variable)
            isobj.checkout()
            r4p = isobj.ready4physics  # pylint: disable=no-member
            _LOG.info('Retrieved Ready4Physics flag: %s', r4p)
        except Exception as exc:
            r4p = None
            _LOG.error('Failed to retrieve Ready4Physics flag: %s', exc)
        return r4p
