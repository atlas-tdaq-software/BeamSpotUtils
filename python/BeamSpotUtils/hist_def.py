"""Module includes classes and methods for histogram definitions.
"""

from __future__ import annotations

import functools
from dataclasses import dataclass


@dataclass
class HistoDef:
    """Definition of a histogram."""

    name: str
    """Histogram name, this is a BST internal short name like "posX"."""

    title: str | None = None
    """Title to use for newly-made histograms, may be None."""

    is_bcid: bool = False
    """True if histogram is per-BCID histogram."""

    is_rs_res: bool = False
    """True if histogram should be taken from runsummary folder when --useRSRes
    option is used."""


def make_hdefs_vtx(do_bcid: bool) -> list[HistoDef]:
    """Create the set of histogram definitions for vertex-based method.

    Parameters
    ----------
    do_bcid : bool
        If True then return BCID histograms also

    Returns
    -------
    hdefs : `list` [ `HistoDef` ]
        List of HistoDef objects.
    """

    hdefs = []
    for axis in "XYZ":
        hdefs += [
            HistoDef(f"pos{axis}", title=f"Acc. Vertex {axis}"),
            HistoDef(f"delta{axis}", title=f"Acc. Vertex {axis} - Nominal"),
            HistoDef(f"pos{axis}_vs_ntrks", title=f"Vertex {axis} vs NTrks"),
            HistoDef(f"split{axis}_vs_ntrks", title=f"Vertex D{axis} vs NTrks", is_rs_res=True),
        ]

    for axis in "XY":
        hdefs += [
            HistoDef(f"tilt{axis}", title=f"Acc. Vertex {axis} vs Vertex Z")
        ]

    if do_bcid:
        for axis in "XYZ":
            hdefs += [
                HistoDef(f"delta{axis}_bcid", title=f"Vertex {axis} - Nominal vs BCID", is_bcid=True),
                HistoDef(f"pos{axis}_bcid", title=f"Vertex {axis} vs BCID", is_bcid=True),
            ]

    return hdefs


def make_hdefs_trk(do_bcid: bool) -> list[HistoDef]:
    """Create the set of histogram definitions for track-based method.

    Parameters
    ----------
    doBCID : bool
        If True then return BCID histograms also

    Returns
    -------
    hdefs : `list` [ `HistoDef` ]
        List of HistoDef objects.
    """
    hdefs = [
        HistoDef("beam_ls", title="Beam LS matrix"),
        HistoDef("llpoly", title="TRack LL Polynomial coefficients"),
    ]
    if do_bcid:
        hdefs += [
            HistoDef("beam_ls_bcid", title="Per-BCID Beam LS matrices", is_bcid=True)
        ]
    return hdefs


@functools.lru_cache(maxsize=3)
def names_vtx() -> set[str]:
    """Complete set of histogram names used by vertex method"""
    return set(hdef.name for hdef in make_hdefs_vtx(do_bcid=True))


@functools.lru_cache(maxsize=3)
def names_trk() -> set[str]:
    """Complete set of histogram names used by track method"""
    return set(hdef.name for hdef in make_hdefs_trk(do_bcid=True))
