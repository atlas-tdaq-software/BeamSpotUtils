"""Module containing HistPlot class and related methods.
"""

from __future__ import annotations

import logging
import math
import os
import re
from typing import Any, TYPE_CHECKING

from .freaking_ROOT import ROOT

if TYPE_CHECKING:
    from .bscalc_vtx import BSCalcVertexConfig


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


class HistPlot:
    """Class responsible for producing files with histogram plots.

    Parameters
    ----------
    outputDir : str
        Location of the output directory for plots
    params : bscalc_vtx.BSCalcVertexConfig
        Configuration used by BSCalcVertex
    """

    def __init__(self, outputDir: str, params: BSCalcVertexConfig):

        self.outputDir = outputDir
        self.maxPlotsPerPage = params.maxPlotsPerPage
        self.outExt = params.outExt

        self.canvFile = os.path.join(self.outputDir, params.outBase + '.ps')
        self.canv = ROOT.TCanvas("canv", "canv")
        # '[' at the end of file name means open file but do not close
        self.canv.Print(self.canvFile + "[")

        self.perBinFile = os.path.join(self.outputDir, 'PerBinFits.pdf')
        self.perBinCanv = ROOT.TCanvas("perBinCanv", "perBinCanv")
        self.perBinCanv.Print(self.perBinFile + "[")

        self._makeTitlePage(params)

    def _makeTitlePage(self, params: BSCalcVertexConfig) -> None:
        """Print title page with all parameters, output goes to self.perBinFile
        """
        allowedTypes = [type(x) for x in (0, 0.0, '')]
        disallowedKeys = re.compile('^(h_|_)')

        self.perBinCanv.cd()
        tp = ROOT.TPaveText(0, 0, 1, 1)
        tp.AddText("Product of the Online Beamspot Group")

        # params can be namedtuple
        attributes = getattr(params, "_fields", None)
        if attributes is None:
            # regular class with __dict__
            attributes = set(vars(params).keys()) | set(vars(params.__class__).keys())
        for attr in sorted(list(attributes)):
            if disallowedKeys.match(attr) is not None:
                continue
            val = getattr(params, attr)
            if type(val) in allowedTypes:
                tp.AddText("%-20s #rightarrow %-20s" % (attr, val))
        tp.Draw()
        self.perBinCanv.Print(self.perBinFile)

    def finish(self) -> None:
        """Close output files
        """
        self.canv.Print(self.canvFile + "]")
        self.perBinCanv.Print(self.perBinFile + "]")

    @staticmethod
    def _resetCanv(canv: ROOT.TCanvas, nPlotsX: int | None = None, nPlotsY: int | None = None) -> None:
        """Clears canvas and optionally divides into pads.
        """
        # note cd(0) is the watermark not the upper left =)
        canv.cd(0).Clear()
        if None not in (nPlotsX, nPlotsY):
            canv.Divide(nPlotsX, nPlotsY)

    def saveFitSlicePlots(self, histList: list[list[Any]]) -> None:
        """Plot and save fit results.
        """
        maxPlotsPerPage = self.maxPlotsPerPage
        nBins = len(histList)
        nPlotsX = int(min(math.sqrt(maxPlotsPerPage) - 1, math.floor(math.sqrt(nBins))))
        nPlotsY = int(min(math.sqrt(maxPlotsPerPage), math.ceil(math.sqrt(nBins))))
        if nPlotsX * nPlotsY < nBins:
            nPlotsX += 1

        self._resetCanv(self.perBinCanv, nPlotsX, nPlotsY)
        for logy in (False, True):
            for i, hlist in enumerate(histList):
                whichCanv = (i % maxPlotsPerPage) + 1
                self.perBinCanv.cd(whichCanv).SetLogy(logy)
                for j, hist in enumerate(hlist):
                    hist.Draw('' if j == 0 else "same")

                if whichCanv == maxPlotsPerPage:
                    self.perBinCanv.Print(self.perBinFile)
                    self._resetCanv(self.perBinCanv, nPlotsX, nPlotsY)

            if nBins % maxPlotsPerPage != 0:
                self.perBinCanv.Print(self.perBinFile)
                self._resetCanv(self.perBinCanv, nPlotsX, nPlotsY)

        self._resetCanv(self.perBinCanv)

    def plotHisto(self, histo: ROOT.TH1, perBin: bool = False,
                  saveAs: list[str] | None = None, logy: bool = False, options: str = '') -> None:
        """Plot a histogram and save it.

        Parameters
        ----------
        histo : TH1
            Any ROOT histogram (2-d is OK too).
        perBin : bool
            If True then save to per-bin plots file, otherwise to a regular file.
        saveAs : list of strings, optional
            If given then save plot to each of the given file names, this could be
            PDF, ROOT, .C, or anything supported by TCanvas.SaveAs() method.
        options : str
            Options for Draw() method
        """
        canvas = self.perBinCanv if perBin else self.canv
        canvas.cd()
        canvas.SetLogy(logy)
        histo.Draw(options)
        canvas.Print(self.perBinFile if perBin else self.canvFile)

        for fname in saveAs or []:
            canvas.SaveAs(os.path.join(self.outputDir, fname))

        canvas.SetLogy(False)

    def plotHistos(self, histos: list[ROOT.TH1], perBin: bool = False,
                   saveAs: list[str] | None = None, logy: bool = False, options: str = '') -> None:
        """Plot a histogram and save it.

        Parameters
        ----------
        histos : list of TH1
            List of ROOT histograms (2-d is OK too).
        perBin : bool
            If True then save to per-bin plots file, otherwise to a regular file.
        saveAs : list of strings, optional
            If given then save plot to each of the given file names, this could be
            PDF, ROOT, .C, or anything supported by TCanvas.SaveAs() method.
        options : str
            Options for Draw() method
        """
        canvas = self.perBinCanv if perBin else self.canv
        canvas.cd()
        canvas.SetLogy(logy)

        stats_pos = 0.97
        for i, histo in enumerate(histos):
            if i == 0:
                histo.Draw(options)
            else:
                histo.Draw(options + ',sames')

            # Shift stat box position left on each new stat box
            ROOT.gPad.Update()  # have to do this before "stat"
            stats = histo.FindObject("stats")
            if stats:
                stats.SetX1NDC(stats_pos - 0.25)
                stats.SetX2NDC(stats_pos)
                stats_pos -= 0.26
                stats.SetFillColorAlpha(histo.GetLineColor(), 0.07)

        canvas.Print(self.perBinFile if perBin else self.canvFile)

        for fname in saveAs or []:
            canvas.SaveAs(os.path.join(self.outputDir, fname))

        canvas.SetLogy(False)

    def plotRedGreenBlack(self, graphs: list[tuple[ROOT.TH1, int, bool]],
                          suffix: str = "_redgreenblack", options: str = '') -> None:
        """Make red-green-black plot from few histograms/fits

        Parameters
        ----------
        graphs : list of tuples
            each item in list is a 3-tuple (graph, color, showStatFlag),
            graph is histogram/graph, color is integer color number,
            is boolean flag to show stat box.
        suffix : str
            suffix for saved file name
        """
        high = max(gr[0].GetMaximum() for gr in graphs) * 1.25
        graphs[0][0].SetMaximum(high)

        optStat = ROOT.gStyle.GetOptStat()
        ROOT.gStyle.SetOptStat("n")  # only histogram name is printed in stat box (and fit)

        for gr, color, showStat in graphs:
            gr.SetLineColor(color)
            gr.SetStats(showStat)
            if high < 0.3:
                # for X and Y directions
                gr.GetYaxis().SetRangeUser(0, 0.1)

# Show extrapolation below minFitRes as dashed line
            resFit = gr.GetFunction("sqrtN")
            if resFit:
                extrap = resFit.Clone("extrap")
                extrap.SetRange(0., 100.)
                extrap.SetLineStyle(2)
                gr.GetListOfFunctions().Add(extrap)

        name = graphs[0][0].GetName() + suffix
        saveAs = [name + '.pdf', name + '.C']
        self.plotHistos([g[0] for g in graphs], perBin=True, saveAs=saveAs, options=options)

        ROOT.gStyle.SetOptStat(optStat)

    def oneDimDraw(self, histSlice: dict[str, Any], direction: str, doBCIDs: bool) -> None:
        """Plot histograms for single dimension.

        Parameters
        ----------
        histSlice : dict
            Dictionary where key is the histogram short name and value
            is a histogram object
        direction : str
            Dimension axis name (e.g. 'X')
        doBCIDs : bool
        """
        namesAndLimits: list[tuple[str, str, float, float]] = [
            ('pos', 'XYZ', 0, 0),
            ('delta', 'XYZ', 0, 0),
            ('ntrks', 'XYZ', 0, .08),
            ('resBCID', 'XYZ', 0, 0),
            ('sigma', 'XY', 0, .1),
            ('sigma', 'Z', 40, 100),
            ('res', 'XYZ', 0, .2),
            ('unCorr', 'XY', 0, .5),
            ('unCorr', 'Z', 40, 100)]
        if doBCIDs:
            namesAndLimits.append(('bcidDelta', 'XYZ', 0, 0))

        for plotParams in namesAndLimits:
            name: str = plotParams[0]
            if direction not in plotParams[1]:
                continue

            hist = histSlice.get(name)
            if hist:
                hist.SetStats(name in ('pos', 'bcidDelta', 'resBCID', 'delta', 'ntrks'))
                if plotParams[3] > plotParams[2]:
                    hist.SetMinimum(plotParams[2])
                    hist.SetMaximum(plotParams[3])

                options = 'colz' if name == 'ntrks' else ''
                saveAs = ["Online_" + name + '_' + direction + self.outExt]
                self.plotHisto(hist, options=options, saveAs=saveAs, logy=True)
