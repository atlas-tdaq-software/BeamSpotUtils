"""Workaround for ROOT import to suppress its handling of command line.

Instead of doing

    import ROOT

all modules should be doing

    from .freaking_ROOT import ROOT
"""

import sys

tmpVars = sys.argv
sys.argv = []
import ROOT  # noqa: E402
ROOT.gROOT.SetBatch(True)

# -- need to import something from ROOT to trigger command line parsing
from ROOT import TFile  # noqa: F401,E402
sys.argv = tmpVars
ROOT.gErrorIgnoreLevel = 5000

del TFile
del tmpVars
del sys
