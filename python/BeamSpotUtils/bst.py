"""main() method of beamSpotTool"""

from __future__ import annotations

import logging
from collections.abc import Mapping

from . import bslog, cmdline, file_getter, htree
from .freaking_ROOT import ROOT
from .config import Config


_LOG = logging.getLogger("bst")


def main() -> int:
    """Main entry.
    """
    config = Config(cmdline.getCLIOptions())
    bslog.initLogger(config.verbosity)

    config.dump(_LOG, logging.INFO)

    # if --use-emscan is given and we are in emittance scan then stop right here
    emscan = config.make_emscan()
    if emscan():
        _LOG.info("Emittance scan is in progress, skipping all operations.")
        return 0

    with config.tmpdir_context():
        return _main(config)


def _get_file(config: file_getter.SmartFileGetterConfig) -> htree.HTree | None:
    """Retrieve ROOT file from remote location for a given run.

    Parameters
    ----------
    config : `file_getter.SmartFileGetterConfig`
        Configuration.

    Returns
    -------
    htree :  `htree.HTree`
        Histogram tree instance.
    """
    fgetter = file_getter.SmartFileGetter(config=config)
    finfo = fgetter.goFetch()
    if finfo is None:
        _LOG.critical("SmartFileGetter failed to get the file for run %s", config.run_number)
        return None

    _LOG.info('Using ROOT file %s', finfo.path)
    return finfo.htree


def _make_trees(config: Config) -> list[htree.HTree] | None:
    """Retrieve all histogram files/trees

    Parameters
    ----------
    config : `config.Config`
        Configuration.

    Returns
    -------
    trees :  list of `htree.HTree`
        List of histogram tree instances or None.
    """
    # retrieve file from somewhere if not on command line
    trees = []
    if not config.files:
        hist_tree = _get_file(config.fileGetterConfig)
        if hist_tree is None:
            return None
        trees = [hist_tree]
    else:
        for fileLoc in config.files:
            tfile = ROOT.TFile.Open(fileLoc)
            if not tfile:
                _LOG.error('Failed to open ROOT file %s', fileLoc)
                return None
            tree = htree.make_htree(
                tfile=tfile,
                server=config.htree_config.server,
                provider=config.htree_config.provider,
                detail=config.htree_config.detail,
                chain_vtx=config.htree_config.algorithm_vtx,
                chain_trk=config.htree_config.algorithm_trk
            )
            if tree is None:
                _LOG.error('Failed to determine file type or origin (%s)', fileLoc)
                return None
            trees.append(tree)

    return trees


def _find_run_number(config: Config, trees: list[htree.HTree]) -> int | None:
    """Determine run number from trees and config.

    Parameters
    ----------
    config : `config.Config`
        Configuration.
    trees :  list of `htree.HTree`
        List of histogram tree instances.

    Returns
    -------
    run :  int
        Run number or None.
    """
    runs = set(htree.get_run() for htree in trees)
    if len(runs) > 1:
        _LOG.error('Files belong to different run numbers %s', runs)
        return None
    run_number = runs.pop()

    if run_number is None:
        # if cannot find run from file then need -r option
        run_number = config.run_number
        if run_number is None:
            _LOG.error('Cannot determine run number from ROOT file, please use -r option')
            return None
    else:
        # if file has a run number in it and command line has one then they should match
        if config.run_number is not None and config.run_number != run_number:
            _LOG.error('Conflicting run numbers, ROOT file has run=%s and options give run=%s',
                       run_number, config.run_number)
            return None
    _LOG.info('Using run number %d', run_number)
    return run_number


def _main(config: Config) -> int:
    """Main entry point, called with a correct context by `main()`

    Parameters
    ----------
    config : `config.Config`
        Configuration.
    """
    trees = _make_trees(config)
    if trees is None:
        return 2

    run_number = _find_run_number(config, trees)
    if run_number is None:
        return 2

    # update run number in config so that other classes can depend on it
    config.run_number = run_number

    retCode = 0
    try:

        lbGroups: list[Mapping[int, htree.HTree]] = []
        if config.procRunConfig.use_run_summary:
            # runsummary case, we can only have a single file
            if len(trees) > 1:
                _LOG.error("only one input file can be used without --noEOR option")
                return 2
            # input file may not even have LB number in it use -1 as
            # indication of runsummary
            lbGroups = [{-1: trees[0]}]
        else:
            # per-LB calculations
            lbn_merge = config.make_lbn_merge()
            try:
                lbGroups = lbn_merge.group_by_split(trees)
            except LookupError:
                _LOG.info("No LBs to analyze")
            _LOG.debug("lbGroups are %s", lbGroups)

        if lbGroups:
            proc_run = config.make_proc_run()
            for hist_trees in lbGroups:
                proc_run.process(hist_trees=hist_trees)

    except KeyboardInterrupt:
        _LOG.critical("Okay I got a KeyboardInterrupt.")
        retCode = -6
    except Exception as inst:
        _LOG.critical('Top level received this exception: %s', inst, exc_info=True)
        retCode = -1

    return retCode
