'''Module for handling command line parsing and related methods.
'''

from __future__ import annotations

import os
from argparse import ArgumentParser, Namespace

from .htree import HTreeVariant
from .translators import factory


TDAQ_PARTITION = os.environ.get("TDAQ_PARTITION", "")


def _typeStrList(strValue: str) -> list[str]:
    """Special "type" for argparse.

    Takes a string and splits it into list on commas.
    """
    return [item.strip() for item in strValue.split(',')]


def _typeRunList(strValue: str) -> list[int]:
    """Special "type" for argparse.

    Takes a string and splits it into list on commas as run ranges
    """
    runsWithHyphens = strValue.split(',')

    runs = []
    for run in runsWithHyphens:
        first, dash, last = run.partition('-')
        if dash == '':
            runs.append(int(first.strip()))
        else:
            ifirst = int(first.strip())
            ilast = int(last.strip())
            runs += range(ifirst, ilast + 1)
    return runs


def defineInputOpts(parser: ArgumentParser) -> None:
    """Define command line options describing input

    Parameters
    ----------
    parser : argparse.ArgumentParser
    """

    inputs = parser.add_argument_group(
        'Input Options',
        'These options control how the application attempts to get data.'
    )

    inputs.add_argument(
        '--tempDir', default=None, metavar='PATH',
        help='Which folder to temporarily store the root files in (~300 MB).'
        ' By default new temporary directory is created inside system'
        ' standard temporary folder. If specified directory does not exist'
        ' it will be created and removed (unless --keepTemp is given).'
    )

    inputs.add_argument(
        '--keepTemp', action='store_true', default=False,
        help='If the application unexpectedly quits'
        ' it normally cleans up tempDir.  However this flag'
        ' keeps tempDir around allowing inspection.'
    )

    inputs.add_argument(
        '--repInput', action='store', default=None,
        help='Specify where the representation for diffing comes from.  If repOutput is specified it'
             ' only executes if diff passes. See below for representation specification.'
    )

    inputs.add_argument(
        '--rep-delta-corr', action='store', default="onhlt",
        help='Specify where the representation for delta-to-position correction comes from. '
             'Correction is only performed in specific cases. Default: %(default)s.'
    )


def defineSourceOpts(parser: ArgumentParser) -> None:
    """Define command line options describing source of data.

    Parameters
    ----------
    parser : argparse.ArgumentParser
    """

    source = parser.add_argument_group(
        'Source Options',
        'Specify where the data for plots and calculations come from.'
    )

    source.add_argument(
        '--partition', default=TDAQ_PARTITION, metavar='STRING',
        help='Which partition to use for ohcp.'
        ' Not expressed in the root file. [default: %(default)s]'
    )

    source.add_argument(
        '--server', default='Histogramming', metavar='STRING',
        help='OH server name, first folder inside the root file [default: %(default)s]'
    )

    source.add_argument(
        '--provider', default='TopMIG-OH:HLT', metavar='STRING',
        help='OH provider name, second folder inside the root file [default: %(default)s]'
    )

    source.add_argument(
        '--detail', default='EXPERT', metavar='STRING',
        help='Level of detail, third folder inside the root file [default: %(default)s]'
    )

    source.add_argument(
        '--chain', default='vertex_FS/T2VertexBeamSpotTool', metavar='STRING',
        help='Chain or algorithm name, fourth folder inside the root file [default: %(default)s]'
    )

    source.add_argument(
        '--trk-chain', default='vertex_FS/T2TrackBeamSpotTool', metavar='STRING',
        help='Chain/algorithm name producing track histograms [default: %(default)s]'
    )

    source.add_argument(
        '--source', type=_typeStrList, default=['OHCP', 'COCA'], metavar='SRC[,SRC...]',
        help='Comma-separated list of the file sources. If one fails, try the next one'
        ' without dying.  If all fail. then die.  [default: "OHCP,COCA"]'
    )

    source.add_argument(
        '--useRSRes', action='store_true', default=False,
        help='This flag causes BSCalcVertex to always examine the run summary folder for the'
        ' perRunHistograms such (ie SplitVertex for resolution). The default behavior'
        ' reconstructs these histograms from the integration window specified by min and maxLBN.'
    )


def defineCalcsOpts(parser: ArgumentParser) -> None:
    """Define command line options controlling calculations

    Parameters
    ----------
    parser : argparse.ArgumentParser
    """

    calcs = parser.add_argument_group(
        'Numerical Options',
        'Options to control the LumiBlock averaging and ranges'
    )

    calcs.add_argument(
        '--algo-name', default='auto', metavar='STRING',
        choices=['vertex_only', 'track_only', 'vertex', 'track', 'auto'],
        help="Chose algorithm name for publishing the result; possible values: %(choices)s."
        " [default: %(default)s]"
    )

    calcs.add_argument(
        '--noEOR', action='store_true', default=False,
        help='This causes the calculation to be run over each lumi block and summed. This tag is'
        ' automatically inserted if min, max or split is specified.'
    )

    calcs.add_argument(
        '--minLBN', type=int, default=0, metavar='NUMBER',
        help='Minimum luminosity block to calculate beamspot parameters over'
    )

    calcs.add_argument(
        '--maxLBN', type=int, default=-1, metavar='NUMBER',
        help='Maximum luminosity block to calculate beamspot parameters over'
    )

    calcs.add_argument(
        '--split', type=int, default=-1, metavar='NUMBER',
        help='This takes N lumi blocks and treats them as separate entities with their BSR updates'
        ' and diffs as necessary.  This can only be performed on a local file.'
    )

    calcs.add_argument(
        '--minTrk', type=int, default=6, metavar='NUMBER',
        help='The lowest trk multiplicity bin to use for per bin fits. [default: %(default)s]'
    )

    calcs.add_argument(
        '--histogramPeriod', default='auto', metavar='STRING',
        choices=["auto"] + list(HTreeVariant.__members__.keys()),
        help='set the data-period for the histogram names, "auto" will use the run number to intuit the'
        ' period; possible values: %(choices)s. [default: %(default)s]'
    )

    calcs.add_argument(
        '--resFit', action='store_true', default=False,
        help='Use a 1/sqrt(N) fit to describe the resolution'
    )

    calcs.add_argument(
        '--minFitRes', type=int, default=20, metavar='NUMBER',
        help='Sets the lower bound of the resolution fit range. [default: %(default)s]'
    )

    calcs.add_argument(
        '--minFitWidth', type=int, default=30, metavar='NUMBER',
        help='Sets the lower bound of the fit on the pol0 corrected width. [default: %(default)s]'
    )

    calcs.add_argument(
        '--bcidMinTrk', type=int, default=10, metavar='NUMBER',
        help='The lowest trk multiplicity to use for BCID fits, it has to be the same as'
        ' VertexBCIDMinNTrk parameter in HLT algorithm. [default: %(default)s]'
    )

    calcs.add_argument(
        '--trk-min-count', type=int, default=10000, metavar='NUMBER',
        help="Lowest number of tracks needed for track-based algorithm [default: %(default)s]"
    )

    calcs.add_argument(
        '--trk-fit-scipy', action="store_true", default=False,
        help="Use scipy for minimization, by default Minuit is used."
    )

    calcs.add_argument(
        '--trk-min-lbn', type=int, default=1, metavar='NUMBER',
        help="Minimum number of LBs to merge for track-based algorithm [default: %(default)s]."
    )


def defineOutputOpts(parser: ArgumentParser) -> None:
    """Define command line options describing output

    Parameters
    ----------
    parser : argparse.ArgumentParser
    """

    output = parser.add_argument_group(
        'Output Options',
        'These options control what to produce and where to put it'
    )

    output.add_argument(
        '--outputDir', default=None, metavar='PATH',
        help='Folder to write final plots to, default is "run_{runNumber}" in current folder'
    )

    output.add_argument(
        '--cvtPlots', action='store_true', default=False,
        help='CorrVTrks goes verbose and saves its plots.'
    )

    output.add_argument(
        '--repOutput', default=None, metavar='STRING',
        help=('Specify where the representation goes if a repInput is specified then only output'
              ' if diff passes. See below for representation specification.')
    )

    output.add_argument(
        '--repNFOutput', default=None, metavar='STRING',
        help='Specify destination for no-filter, this always produces output even if diff fails.'
    )

    output.add_argument(
        '--vtx-mon-output', default=None, metavar='STRING',
        help='Specify destination for vertex-based method, typically to IS for monitoring.'
    )

    output.add_argument(
        '--trk-mon-output', default=None, metavar='STRING',
        help='Specify destination for track-based method, typically to IS for monitoring.'
    )

    output.add_argument(
        '--doBCIDs', action='store_true', default=False,
        help='BSCalcVertex always produces bcid information but this flag causes a csv file to be written.'
    )

    output.add_argument(
        '--noBCIDCorrection', action='store_false', dest='doBCIDCorrection', default=True,
        help='Do not correct BCID widths using the global width'
    )

    output.add_argument(
        '--use-r4p', action='store_true', default=False,
        help='Set result status based on Ready4Physics value'
    )

    output.add_argument(
        '--r4p-partition', action='store', default=TDAQ_PARTITION,
        help='Partition for IS server containing Ready4Physics variable. [default: %(default)s]'
    )

    output.add_argument(
        '--r4p', action='store', default="RunParams.Ready4Physics",
        help='Name of IS Ready4Physics variable. [default: %(default)s]'
    )

    output.add_argument(
        '--emscan-partition', action='store', default="initial",
        help="Partition for IS server containing emittance scan flag. [default: %(default)s]"
    )

    output.add_argument(
        '--emscan-variable', action='store', default="LHC.EmittanceScanOngoing",
        help="Name of IS emittance scan flag/variable. [default: %(default)s]"
    )

    output.add_argument(
        '--use-emscan', action='store_true', default=False,
        help="If this option is specified then no updates are performed when emittance scan is in progress"
    )


def defineOHPublishOpts(parser: ArgumentParser) -> None:
    """Define command line options describing OH publishing

    Parameters
    ----------
    parser : argparse.ArgumentParser
    """
    group = parser.add_argument_group(
        'OH Publishing Options',
        'These options control how OH publishing works'
    )

    group.add_argument(
        '--oh-publish-partition', default=None, metavar='STRING',
        help='Partition with OH server, use $TDAQ_PARTITION by default. '
        'If $TDAQ_PARTITION is empty and this option is not specified then no publishing happens.'
    )
    group.add_argument(
        '--oh-publish-server', default="Histogramming", metavar='STRING',
        help='OH server name. [default: %(default)s]'
    )
    group.add_argument(
        '--oh-publish-provider', default="BeamSpotTool", metavar='STRING',
        help='OH provider name. [default: %(default)s]'
    )
    group.add_argument(
        '--oh-publish-folder', default=None, metavar='STRING',
        help='Folder for histogram name, if empty then no publishing happens'
    )


def getCLIOptions(args: list[str] | None = None) -> Namespace:
    """Parse command line.

    If `args` is None then `sys.argv` is used instead.
    """

    epilog = (
        "Some options that specify input or output beamspot accept a description string. " +
        factory.userIODesc
    )

    parser = ArgumentParser(
        usage='%(prog)s [options] [@optionsFile] [fileLocation ...]\n'
        'or: %(prog)s -r 155678',
        epilog=epilog, fromfile_prefix_chars='@'
    )

    parser.add_argument(
        '-v', '--verbosity', nargs='?', default=4, const=5, type=int, metavar='LEVEL',
        help="Logging level, without this option script will log messages at INFO level, if -v is"
        " given (without LEVEL) logging will happen at DEBUG level. For backward compatibility we"
        " also allow `-v LEVEL' format, if LEVEL<5 then  INFO level is used, otherwise DEBUG."
    )

    parser.add_argument(
        '-r', '--runNumber', type=int, default=None, metavar='RUN',
        help='The integer run number. If ROOT file name is not provided then this number is used when'
        ' retrieving histogram file from COCA. If histograms come from OHCP source then this number can'
        ' be used in case OHCP cannot determine run number. If ROOT file name is given on command line'
        ' then this number should match run number stored in file (if any).'
    )

    parser.add_argument(
        'fileLocation', nargs='*', default=[],
        help='ROOT file locations. If given then tool will run on these files only.'
    )

    # option groups
    defineInputOpts(parser)
    defineOutputOpts(parser)
    defineOHPublishOpts(parser)
    defineCalcsOpts(parser)
    defineSourceOpts(parser)

    opts = parser.parse_args(args)

    # set --noEOR if some other options are set
    if opts.minLBN != 0 or opts.split > 0:
        opts.noEOR = True

    return opts
