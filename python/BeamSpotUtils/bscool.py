'''Library for COOL (network/local file) reading and writing.

There is a known "feature" with BCID numbers being written to and from COOL.
We use the channel_id field of the cool structure to store the bcid number,
which works very well for onbunch (which stores bcids individually).  For
the global measurement (which is stored in onmon) the channel_id is still
0, but some part of BSUtils would prefer that the bcid number is -1 here
so that they know its a global measuremen.
'''

from __future__ import annotations

__all__ = ["getDB", "openOrMakeFolder", "readDB", "writeToFolder", "runLBTimes"]

import logging
from typing import Any, cast

from coldpie import cool

from . import model


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


def _parseCOOLTime(value: int) -> tuple[int, int]:
    """Converts the output of COOL's until() or since() into run,lbn pair
    """
    value = int(value)
    return int(value >> 32), int(value & 0xFFFFFFFF)


def _makeCOOLTime(run: int, lbn: int) -> int:
    """Make COOL IOV "time" out of run number and LBN
    """
    return (int(run) << 32) + int(lbn)


def getDB(conn: str, readOnly: bool = True, create: bool = False) -> cool.Database:
    """Make instance of COOL database object from connection string.

    If any error happens this method will terminate application.

    Parameters
    ----------
    conn : str
        Connection string in COOL syntax.
    readOnly : bool, optional
        If true (default) then database will be open in read-only mode.
    create : bool, optional
        False by default, if set to True then attempt to create database
        if it does not exist (for sqlite testing only).

    Returns
    -------
    COOL database instance (cool.IDatabase)

    Raises
    ------
    Exception is raised if database cannot be open and `create` is False or
    if database cannot be created when `create` is true.
    """
    dbSvc = cool.DatabaseSvcFactory.databaseService()
    _LOG.info('Trying to connect to: %s. readOnly=%s, create=%s',
              conn, readOnly, create)

    try:
        db = dbSvc.openDatabase(conn, readOnly)
    except Exception as exc:
        # _LOG.warning('Could not connect to database %s', conn)
        # _LOG.warning('This was the exception %s', exc)
        if not create:
            _LOG.error('Failed to connect to database %s: %s', conn, exc)
            raise

        _LOG.debug('Trying to create new db %s', conn)
        try:
            db = dbSvc.createDatabase(conn)
        except Exception as exc:
            _LOG.error('Failed to create new database %s: %s', conn, exc)
            raise exc from None

    return db


def openOrMakeFolder(db: cool.Database, folder: str, spec: cool.RecordSpecification) -> cool.Folder:
    """Open or create COOL folder.

    Parameters
    ----------
    db : cool.IDatabase
        COOL database instance
    folder : str
        COOL folder name.
    spec : cool.RecordSpecification
        Record specification object.

    Returns
    -------
    COOL folder instance (cool.IFolder).

    Raises
    ------
    Exception is raised if folder does not exist and cannot be created.
    """

    if db.existsFolder(folder):
        # this could still throw
        cFolder = db.getFolder(folder)
    else:
        try:
            # hardcoded folder description, but this is never going to change
            stype = 71
            clid = 40774348
            tname = 'AthenaAttributeList'
            desc = '<timeStamp>run-lumi</timeStamp>' + \
                '<addrHeader><address_header service_type=\"' + str(stype) + \
                '\" clid=\"' + str(clid) + '\" /></addrHeader>' + \
                '<typeName>' + tname + '</typeName>'

            _LOG.info('Attempting to create %s with description %s', folder, desc)
            folderSpec = cool.FolderSpecification(cool.FolderVersioning.MULTI_VERSION, spec)
            cFolder = db.createFolder(folder, folderSpec, desc, True)

        except Exception as exc:
            _LOG.error('Could not create folder %s: %s', folder, exc)
            raise

    return cFolder


def writeToFolder(cFolder: cool.Folder, tag: str, rep: model.BeamSpotRepresentation,
                  spec: cool.RecordSpecification, openIOV: bool) -> None:
    """Save records to a COOL folder

    Parameters
    ----------
    cFolder : cool.IFolder
        COOL folder instance
    tag : str
        COOL tag name
    rep : BeamSpotRepresentation
        Beamspot representation instance
    spec : cool.RecordSpecification
        Record specification object.
    openIOV : bool
        If True then make open interval to +Infinity starting from (run, lbn),
        otherwise make interval from (run,firstLBN) to (run, lastLBN)

    Raises
    ------
    COOL exception is raised on error.
    """
    do_bulk = len(rep) > 1
    if do_bulk:
        _LOG.debug('enable bulk insert for folder %s', cFolder.fullPath())
        cFolder.setupStorageBuffer()

    for snapshot in rep:
        payload = cool.Record(spec)
        missing = set()
        for k in model.coolPayloadFields:
            val = getattr(snapshot, k)
            if val is not None:
                payload[k] = val if k != 'status' else int(val)
            else:
                missing.add(k)

        if len(missing) > 0:
            _LOG.warning('This snapshot <%s> was missing these keys %s',
                         snapshot, missing)
        if openIOV:
            since = _makeCOOLTime(snapshot.run, snapshot.lbn)
            until = cool.ValidityKeyMax
        else:
            assert snapshot.firstLBN is not None
            assert snapshot.lastLBN is not None
            since = _makeCOOLTime(snapshot.run, snapshot.firstLBN)
            until = _makeCOOLTime(snapshot.run, snapshot.lastLBN + 1)

        _LOG.debug('Writing with IOV [%s,%s]',
                   _parseCOOLTime(since), _parseCOOLTime(until))

        # BCID-average snapshot has bcid -1 which maps to channel 0
        channelID = max(0, snapshot.bcid)
        try:
            cFolder.storeObject(since, until, payload, channelID, tag)
        except Exception as exc:
            _LOG.error('Unable to write to the db folder, exception: %s', exc)
            raise

    if do_bulk:
        try:
            # actual database insertion happens at flush so it can generate all
            # sorts of errors including overlapping intervals errors
            cFolder.flushStorageBuffer()
        except RuntimeError as exc:
            _LOG.error('Failure in flushing data to folder %s: %s', cFolder.fullPath(), exc)


def _getObjs(db: cool.Database, folder: str, tag: str, run: int, lbn: int | None) -> list[cool.Object]:
    """Retrieve a set of COOL intervals.

    Parameters
    ----------
    db : cool.IDatabase
        COOL database instance
    folder : str
        COOL folder name
    tag : str
        COOL tag name
    run : int
        Run number for which to retrieve COOL data
    lbn : int or None
        LBN for which to retrieve COOL data, if None then intervals for whole
        run are returned

    Returns
    -------
    Sequence of COOL intervals (cool.IObject), possibly empty.

    Raises
    ------
    COOL exception is raised on error.
    """
    try:
        myfolder = db.getFolder(folder)
    except Exception as exc:
        _LOG.error("Failed to find COOL  folder %s: %s", folder, exc)
        raise

    msg = "Getting parameters for tag %s, " % tag
    if lbn is not None:
        msg += "run %d, lbn %d" % (run, lbn)
        begIOV = _makeCOOLTime(run, lbn)
        endIOV = _makeCOOLTime(run, lbn)
    else:
        msg += "just run %d" % run
        begIOV = _makeCOOLTime(run, 0)
        endIOV = _makeCOOLTime(run + 1, 0)

    _LOG.debug(msg)
    _LOG.debug('Reading with IOV [%s,%s]',
               _parseCOOLTime(begIOV), _parseCOOLTime(endIOV))
    try:
        objs = myfolder.browseObjects(begIOV, endIOV, cool.ChannelSelection.all(), tag)
    except Exception as exc:
        _LOG.error('Failure browsing folder - folder=%s begIOV=%s endIOV=%s tag=%s: %s',
                   folder, begIOV, endIOV, tag, exc)
        raise

    return list(objs)


def readDB(db: cool.Database, folder: str, tag: str, run: int, lbn: int | None,
           zeroBCID: int = 0) -> model.BeamSpotRepresentation:
    """Read beamspot representation object from database.

    Parameters
    ----------
    db : cool.IDatabase
        COOL database instance
    folder : str
        COOL folder name
    tag : str
        COOL tag name
    run : int
        Run number for which to retrieve COOL data
    lbn : int or None
        LBN for which to retrieve COOL data, if None then intervals for whole
        run are returned
    zeroBCID : int, optional
        If not zero then its value replaces BCID=0 read from database,
        typically used to when reading from BCID-average folders (in-memory
        representation should have bcid=-1 for those).

    Returns
    -------
    BeamSpotRepresentation instance or None if error happens.

    Raises
    ------
    COOL exception is raised on database error. KeyError is raised if database
    record is missing required fields.
    """
    objs = _getObjs(db, folder, tag, run, lbn)

    payloadFields = model.possibleFields - set(model.coolMetaFields)

    _LOG.debug('Processing objects')

    results = model.BeamSpotRepresentation()
    for obj in objs:
        runSince, lbnSince = _parseCOOLTime(obj.since())
        bcid = obj.channelId()
        # map channel 0 back to BCID=-1 (if zeroBCID == -1)
        if zeroBCID != 0 and bcid == 0:
            bcid = zeroBCID
        di: dict[str, Any] = dict(run=runSince, lbn=lbnSince, bcid=bcid)
        _LOG.debug('Processing object: %s', di)

        payload = obj.payload()
        for k in payloadFields:
            try:
                di[k] = payload[k]
                # print 'payload[%s] = %s' % (k, payload[k])
            except KeyError:
                # print 'payload[%s] = missing' % k
                if k in model.requiredFields:
                    _LOG.error('%s, a required field was missing!', k)
                    raise
        snap = model.BSSnapshot(di)

        results.append(snap)

    return results


# cache for return values of runLBTimes
_ANS_CACHE: dict[tuple[int, int | None], tuple[int, int]] = {}
_RUNLB_CACHE: dict[tuple[int, int | None], tuple[int, int]] = {}


def runLBTimes(run: int, lbn: int | None) -> tuple[int, int]:
    """Return strart/end time of the run/LBN.

    Parameters
    ----------
    run : int
        Run number
    lbn : int or None
        If number is specified then time for that specific LBN is returned,
        if None or 0 is given then begin/end time of the whole run is returned.

    Returns
    -------
    start : int
        Start time of corresponding interval
    end : int
        End time of corresponding interval

    Raises
    ------
    Exception is raised if database cannot be open or if folder does not exist.
    """

    lbn = None if lbn in (0, None) else lbn
    ansKey = (run, lbn)

    # check caches first
    ans = _ANS_CACHE.get(ansKey)
    if ans is not None:
        return ans

    ans = _RUNLB_CACHE.get(ansKey)
    if ans is not None:
        _ANS_CACHE[ansKey] = ans
        return ans

    db = getDB("COOLONL_TRIGGER/CONDBR2", readOnly=True, create=False)
    coolFolder = db.getFolder('/TRIGGER/LUMI/LBLB')

    if lbn is None:
        begIOV = _makeCOOLTime(run, 0)
        endIOV = _makeCOOLTime(run + 1, 0)
    else:
        begIOV = _makeCOOLTime(run, lbn)
        endIOV = _makeCOOLTime(run, lbn)

    _LOG.info("IOVs are %s %s", begIOV, endIOV)
    try:
        objs = coolFolder.browseObjects(begIOV, endIOV, cool.ChannelSelection.all(), '')
    except Exception as exc:
        _LOG.error('Object browsing failed.  This was exception: %s', exc)
        return 0, 0

    starts, ends = [], []
    for obj in objs:
        runSince, lbnSince = _parseCOOLTime(obj.since())
        runUntil = _parseCOOLTime(obj.until())[0]
        if runUntil != run or runSince != run:
            continue

        payload = obj.payload()
        starts.append(cast(int, payload['StartTime']))
        ends.append(cast(int, payload['EndTime']))
        # here i'm assuming that each IOV is a single lbn
        _RUNLB_CACHE[(runSince, lbnSince)] = (cast(int, payload['StartTime']), cast(int, payload['EndTime']))

    if not starts or not ends:
        ret = 0, 0
    else:
        ret = min(starts), max(ends)

    _ANS_CACHE[ansKey] = ret
    return ret
