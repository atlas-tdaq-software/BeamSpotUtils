"""Configuration for beamspot classes.
"""

from __future__ import annotations

import argparse
import contextlib
import logging
import types
from collections.abc import Iterator

from . import (
    emscan, htree, file_getter, lbn_merge, oh_publish,
    os_utils, bscalc_trk, bscalc_vtx, proc_run, r4p
)


_LOG = logging.getLogger("config")


class Config:
    """This is a wrapper for all command line parameters organizing them in a
    more structured way.

    This class is also a factory for some other classes/instances that depend
    on configuration only.

    Parameters
    ----------
    args : `argparse.Namespace`
        Parsed command line
    """
    def __init__(self, args: argparse.Namespace):

        self.verbosity = args.verbosity
        self.files = args.fileLocation
        self.run_number = args.runNumber
        self._output_dir = args.outputDir
        self._histo_variant = args.histogramPeriod

        self.procRunConfig = proc_run.SingleRunConfig(
            use_run_summary=not args.noEOR,
            repr_diff=args.repInput,
            repr_output=args.repOutput,
            repr_nofilter_output=args.repNFOutput,
            do_bcid=args.doBCIDs,
            algo_name=args.algo_name,
            delta_pos_repr=args.rep_delta_corr,
        )

        self.bscalcVtxConfig = bscalc_vtx.BSCalcVertexConfig(
            bcidMinTrk=args.bcidMinTrk,
            minTrk=args.minTrk,
            beQuiet=not args.cvtPlots,
            savePlots=args.cvtPlots,
            doBCIDs=args.doBCIDs,
            doBCIDCorrection=args.doBCIDCorrection,
            resFit=args.resFit,
            minFitRes=args.minFitRes,
            minFitWidth=args.minFitWidth,
            mon_output=args.vtx_mon_output,
        )

        self.bscalcTrackConfig = bscalc_trk.BSCalcTrackConfig(
            min_track_count=args.trk_min_count,
            use_scipy=args.trk_fit_scipy,
            do_bcid=args.doBCIDs,
            min_lbn=args.trk_min_lbn,
            mon_output=args.trk_mon_output,
        )

        self.htree_config = types.SimpleNamespace(
            server=args.server,
            provider=args.provider,
            detail=args.detail,
            algorithm_vtx=args.chain,
            algorithm_trk=args.trk_chain,
        )

        self.fileGetterConfig = file_getter.SmartFileGetterConfig(
            sources=args.source,
            oh_partition=args.partition,
            oh_server=args.server,
            oh_provider=args.provider,
            detail=args.detail,
            algorithm_vtx=args.chain,
            algorithm_trk=args.trk_chain,
            run_number=args.runNumber,
            min_lbn=args.minLBN,
            tmpdir=args.tempDir,
        )

        self.emscan = types.SimpleNamespace(
            use_emscan=args.use_emscan,
            partition=args.emscan_partition,
            object=args.emscan_variable,
        )

        self.r4p = types.SimpleNamespace(
            use_r4p=args.use_r4p,
            partition=args.r4p_partition,
            object=args.r4p,
        )

        self.tmpdir = types.SimpleNamespace(
            path=args.tempDir,
            keep=args.keepTemp,
        )

        self.oh_publish = types.SimpleNamespace(
            partition=args.oh_publish_partition,
            server=args.oh_publish_server,
            provider=args.oh_publish_provider,
            folder=args.oh_publish_folder,
        )

        self.lbn_options = types.SimpleNamespace(
            minLBN=args.minLBN,
            maxLBN=args.maxLBN,
            splitLBN=args.split,
            useRSRes=args.useRSRes,
        )

    @property
    def output_dir(self) -> str:
        """Path to folder with output files (str)"""
        return self._output_dir or f"run_{self.run_number}/"

    @property
    def histo_variant(self) -> htree.HTreeVariant:
        """Histogram structure variant (htree.HTreeVariant)
        """
        if self._histo_variant == 'auto':
            histo_variant = htree.HTreeVariant.from_run(self.run_number)
        else:
            histo_variant = htree.HTreeVariant(self._histo_variant)
        _LOG.debug('Using histogram variant %s', histo_variant)
        return histo_variant

    def make_emscan(self) -> emscan.EmittanceScan:
        """Make and return instance of EmittanceScan.

        Returns
        -------
        emscan : `emscan.EmittanceScan`
        """
        if self.emscan.use_emscan:
            return emscan.EmittanceScan(self.emscan.partition, self.emscan.object)
        else:
            # always returns false
            return emscan.EmittanceScan()

    def make_ohpublisher(self) -> oh_publish.OHPublisher:
        """Make and return instance of OHPublisher.

        Returns
        -------
        publisher : `oh_publish.OHPublisher`
        """
        return oh_publish.OHPublisher(
            self.oh_publish.partition,
            self.oh_publish.server,
            self.oh_publish.provider,
            self.oh_publish.folder
        )

    def make_r4p(self) -> r4p.Ready4Physics:
        """Make an instance of Ready4Physics callable.

        Returns
        -------
        ready4physics : `r4p.Ready4Physics`
        """
        return r4p.Ready4Physics(self.r4p.use_r4p, self.r4p.partition, self.r4p.object)

    def make_lbn_merge(self) -> lbn_merge.LBNMerge:
        """Make an instance of LBNMerge callable.

        Returns
        -------
        lbn_merge : `lbn_merge.LBNMerge`
        """
        return lbn_merge.LBNMerge(
            min_lbn=self.lbn_options.minLBN,
            max_lbn=self.lbn_options.maxLBN,
            split_lbn=self.lbn_options.splitLBN,
            use_rs_res=self.lbn_options.useRSRes
        )

    def make_bscalc_vertex(self) -> bscalc_vtx.BSCalcVertex:
        """Make an instance of BSCalcVertex.

        Returns
        -------
        calc_certex : `bscalc_vtx.BSCalcVertex`
        """
        return bscalc_vtx.BSCalcVertex(
            config=self.bscalcVtxConfig,
            output_dir=self.output_dir,
            lbn_merge=self.make_lbn_merge(),
            oh_publisher=self.make_ohpublisher()
        )

    def make_bscalc_track(self) -> bscalc_trk.BSCalcTrack:
        """Make an instance of BSCalcTrack.

        Returns
        -------
        calc_track : `bscalc_trk.BSCalcTrack`
        """
        return bscalc_trk.BSCalcTrack(
            config=self.bscalcTrackConfig,
            output_dir=self.output_dir,
            lbn_merge=self.make_lbn_merge(),
            oh_publisher=self.make_ohpublisher()
        )

    def make_proc_run(self) -> proc_run.SingleRun:
        """Make an instance of proc_run.SingleRun
        """
        return proc_run.SingleRun(
            config=self.procRunConfig,
            bscalc_vertex=self.make_bscalc_vertex(),
            bscalc_track=self.make_bscalc_track(),
            run_number=self.run_number,
            emscan=self.make_emscan(),
            ready4physics=self.make_r4p(),
            output_dir=self.output_dir,
        )

    @contextlib.contextmanager
    def tmpdir_context(self) -> Iterator[str]:
        """Produce context for managing temporary directory.
        """
        path, tmpDirCreated = os_utils.makeTempDir(self.tmpdir.path)
        # update location in a config
        self.tmpdir.path = path
        # need update path in namedtuple too
        self.fileGetterConfig = self.fileGetterConfig._replace(tmpdir=path)

        yield path

        if not self.tmpdir.keep and tmpDirCreated:
            _LOG.debug('Cleaning up tempDir %s', path)
            os_utils.deleteFileOrFolder(path)

    def dump(self, logger: logging.Logger, level: int = logging.DEBUG) -> None:
        """Dump complete configuration to a logging.
        """
        logger.log(level, "Configuration:")
        attributes = [
            "verbosity",
            "files",
            "run_number",
            "_histo_variant",
            "lbn_options",
            "htree_config",
            "fileGetterConfig",
            "bscalcVtxConfig",
            "bscalcTrackConfig",
            "procRunConfig",
            "emscan",
            "oh_publish",
            "r4p",
            "tmpdir",
            "_output_dir",
        ]

        for attr in attributes:
            logger.log(level, "    %-20s = %s", attr, getattr(self, attr))
