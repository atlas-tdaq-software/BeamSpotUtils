"""Abstract base class for BS calculators.
"""

from __future__ import annotations

from abc import ABCMeta, abstractmethod
from collections.abc import Mapping

from . import htree, model


class BSCalc(metaclass=ABCMeta):
    """Interface for classes that can calculate BS snapshot from histograms.
    """

    @abstractmethod
    def calc(self, hist_trees: Mapping[int, htree.HTree],
             run: int | None = None,
             lbn: int | None = None) -> model.BSSnapshot | None:
        """Calculate  beamspot parameters from histograms in trees.

        This method can be called multiple times on the same instance with
        different histogram trees and LB numbers. Method is not required to use
        all LBNs passed in ``hist_trees``, e.g. could use just few most recent
        LBNs if they have enough statistics.

        Parameters
        ----------
        hist_trees : `dict` [`int`, `htree.HTree`]
            Maps LB number to HTree object providing access to histograms.
        run : `int`, optional
            Run number.
        lbn : `int`, optional
            LBN for which the calculations are performend, usually the latest
            LBN in the hist_trees.

        Returns
        -------
        beamspot : `model.BSSnapshot` or `None`
            Calculated beamspot, None returned if calculations could not be
            performed, e.g. some data is missing.
        """
        raise NotImplementedError()

    @abstractmethod
    def finish(self) -> None:
        """Finish/summarize all calculations.

        This method is called after all calls to calc() method.
        """
        raise NotImplementedError()
