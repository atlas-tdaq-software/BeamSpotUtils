'''Library for file fetching the histogram root files from network sources'''

from __future__ import annotations

import logging
import os
import tempfile
from typing import Any, NamedTuple

from .freaking_ROOT import ROOT

from . import htree, os_utils
import ers
import ispy
import mda


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


class OHCopyError(ers.Issue):
    """ERS Issue class for errors in histogram copying"""

    def __init__(self, message: str, cause: ers.Issue | None = None):
        ers.Issue.__init__(self, 'Failed to copy histograms from OH server: ' + message,
                           {},
                           cause if isinstance(cause, ers.Issue) else None)


class FileInfo(NamedTuple):
    """type which is returned from goFetch() method of getters."""

    path: str | None
    """File name, this could be URL or None if data is not in the file"""

    htree: htree.HTree
    """HTree instance that knows some details about histogram paths"""


class _HTreeWithRun(htree.HTree):
    """Wrapper for HTree which specifies run number in case wrapped tree does
    not provide it.

    Parameters
    ----------
    htree : `htree.HTree`
        Tree to be wrapped.
    run : int
        Run number
    """
    def __init__(self, htree: htree.HTree, run: int):
        self._htree = htree
        self._run = run

    def name(self) -> str:
        # docstring inherited from base class
        return self._htree.name()

    def get_run(self) -> int | None:
        # docstring inherited from base class
        run = self._htree.get_run()
        if run is None or run == 0:
            run = self._run
        return run

    def get_lbns(self) -> list[int]:
        # docstring inherited from base class
        return self._htree.get_lbns()

    def get_histogram(self, name: str, lbn: int | None = None) -> Any | None:
        # docstring inherited from base class
        return self._htree.get_histogram(name, lbn)


class GenericFileGetter:
    """Base class for all getters.
    """

    def goFetch(self, can_fail: bool = False) -> FileInfo | None:
        """Retrieve file.

        Retrieves the file from somewhere and returns an object with
        information about this file.

        Parameters
        ----------
        can_fail : bool, optional
            If set to True then failure should not produce diagnostics.

        Returns
        -------
        finfo : `FileInfo`, optional
            `FileInfo` instance. If file cannot be retrieved then None is
            returned.
        """
        raise NotImplementedError()


class OHCPGetter(GenericFileGetter):
    """Implementation of a file getter to retrieve histograms from OH server.

    Parameters
    ----------
    oh_src : tuple
        Tuple containing (partition, server, provider) for OH information.
    detail : str
        Histogramming detail (e.g. EXPERT).
    algorithm_vtx : str
        Trigger algorithm name for vertex histograms.
    algorithm_trk : str
        Trigger algorithm name for track histograms.
    minLBN : int, optional
        Minimun LBN to read from OH server, by default all LBNs are fetched.
    dirPath : str, optional
        Directory path where to store file, by default CWD is used.
    runNumber : int or None
    """
    def __init__(self, oh_src: tuple[str, str, str], detail: str, algorithm_vtx: str,
                 algorithm_trk: str, minLBN: int | None = None,
                 dirPath: str | None = None, runNumber: int | None = None):
        GenericFileGetter.__init__(self)
        _LOG.debug('In %s constructor calling setparams', self.__class__.__name__)

        self._dirPath = dirPath or "."
        self.runNumber = runNumber
        self.server = oh_src[1]
        self.provider = oh_src[2]
        self.detail = detail
        self.algorithm_vtx = algorithm_vtx
        self.algorithm_trk = algorithm_trk

        self.cmdParams: dict[str, str] = {
            'partition': oh_src[0],
            'server': self.server,
            'provider': self.provider,
            'object': f".*/({algorithm_vtx}|{algorithm_trk})/.*"}

        if minLBN is None or minLBN > -1:
            self.cmdParams['history'] = "-1"
        else:
            self.cmdParams['history'] = str(abs(minLBN))
        _LOG.debug('ohcp parameters are now %s', self.cmdParams)

    def goFetch(self, can_fail: bool = False) -> FileInfo | None:
        # docstring inherited from GenericFileGetter

        fd, path = tempfile.mkstemp(dir=self._dirPath, prefix="beamspot-ohcp-", suffix=".root")
        os.close(fd)

        cmd = ['oh_cp', '-O']
        for arg, val in self.cmdParams.items():
            cmd += ['--' + arg, str(val)]
        cmd += ['--file', path]

        _LOG.info('Trying to copy file from OH server')
        out, err, rc = os_utils.quickOSCallReturnCode(cmd, mustReturnZero=False)
        if rc != 0:
            # cannot determine run number, means file is empty and unusable
            if not can_fail:
                _LOG.error("Failure in oh_cp:")
                _LOG.error("oh_cp standard output: \n<<<\n%s\n>>>", out)
                _LOG.error("oh_cp standard error: \n<<<\n%s\n>>>", err)
                _LOG.error("command parameters: %s", self.cmdParams)
                ers.error(OHCopyError(err.strip()))
            else:
                _LOG.info("oh_cp failed to copy data")
            return None

        tfile = ROOT.TFile(path, "READ")
        hist_tree = htree.OHCPHTree[ROOT.TH1](
            tfile, server=self.server, provider=self.provider, detail=self.detail,
            chain_vtx=self.algorithm_vtx, chain_trk=self.algorithm_trk)
        run = hist_tree.get_run()
        if run == 0:
            # means that we have not used --add-run-number option
            run = self.runNumber
            if run == 0:
                run = None
        if run is None:
            # go and fetch from IS service
            isobj = ispy.ISObject(self.cmdParams['partition'], "RunParams.RunParams")
            isobj.checkout()
            run = isobj.run_number  # pylint: disable=no-member
            _LOG.info('Retrieved run number from IS server: %s', run)
        hist_tree_run = _HTreeWithRun(hist_tree, run)

        return FileInfo(path=path, htree=hist_tree_run)


class COCAGetter(GenericFileGetter):
    """Implementation of a file getter to retrieve histograms from CoCa archive.

    Parameters
    ----------
    server : str
        OH server name.
    provider : str
        OH provider name.
    detail : str
        Histogramming detail (e.g. EXPERT).
    algorithm_vtx : str
        Trigger algorithm name for vertex histograms.
    algorithm_trk : str
        Trigger algorithm name for track histograms.
    runNumber : int
        Run number.
    minLBN : int, optional
        Minimun LBN to read from OH server, by default all LBNs are fetched.
    dirPath : str, optional
        Directory path where to store file, by default CWD is used.
    """

    def __init__(self, server: str, provider: str, detail: str, algorithm_vtx: str,
                 algorithm_trk: str, runNumber: int, minLBN: int | None = None,
                 dirPath: str | None = None):
        GenericFileGetter.__init__(self)
        self.server = server
        self.provider = provider
        self.detail = detail
        self.algorithm_vtx = algorithm_vtx
        self.algorithm_trk = algorithm_trk
        self._runNumber = runNumber
        self._dirPath = dirPath or "."

    def goFetch(self, can_fail: bool = False) -> FileInfo | None:
        # docstring inherited from base class
        tfile = self._retrieve(self._runNumber, self._dirPath, can_fail)
        if tfile:
            hist_tree = htree.MDAHTree[ROOT.TH1](
                tfile, server=self.server, provider=self.provider, detail=self.detail,
                chain_vtx=self.algorithm_vtx, chain_trk=self.algorithm_trk)
            hist_tree_run = _HTreeWithRun(hist_tree, self._runNumber)
            return FileInfo(path=tfile.GetName(), htree=hist_tree_run)
        else:
            _LOG.info('COCA doesn\'t have a location for this run.')
            return None

    def _retrieve(self, runNumber: int, dirPath: str, can_fail: bool) -> ROOT.TFile | None:
        """Retrieve file from CoCa archive.

        It looks like this does not handle any errors.

        Parameters
        ----------
        runNumber : int
        dirPath : str
            Location of directory to store file
        can_fail : bool
            If set to True then failure should not provide diagnostics.

        Returns
        -------
        tfile : ROOT.TFile
            File open for reading or None.
        """
        fileName = f"r{runNumber:010d}_lEoR_ATLAS_MDA-HistogrammingHLT_HistogrammingHLT.root"
        dataset = "Histogramming-HLT"
        reader = mda.FileRead()
        _LOG.debug("opening file %s from CoCa", fileName)
        tfile = reader.openFile(fileName, dataset=dataset)
        if not tfile:
            if not can_fail:
                _LOG.critical('Failed to fetch file: %s', fileName)
            return None
        return tfile


class SmartFileGetterConfig(NamedTuple):
    """Configuration for SmartFileGetter, with default values.

    Some of this values are overridden by `Config` class based on command line
    parameters.
    """
    sources: list[str] = ['OHCP', 'COCA']
    """List of source names for input."""

    oh_partition: str = ""
    """Name of the partition for OH server"""

    oh_server: str = ""
    """OH server name"""

    oh_provider: str = ""
    """OH provider name"""

    detail: str = "EXPERT"
    """Monitoring detail level"""

    algorithm_vtx: str = ""
    """Name of HLT algorithm producing vertex-based histograms"""

    algorithm_trk: str = ""
    """Name of HLT algorithm producing track-based histograms"""

    run_number: int | None = None
    """Run number"""

    min_lbn: int | None = None
    """Starting LBN"""

    tmpdir: str | None = None
    """Location for temporary files"""


class SmartFileGetter(GenericFileGetter):
    """File getter class which combines other getters to find a file in
    more than one location.

    Parameters
    ----------
    config : `SmartFileGetterConfig`
        Configuration
    """

    def __init__(self, config: SmartFileGetterConfig):
        GenericFileGetter.__init__(self)

        sources = config.sources or SmartFileGetterConfig.sources
        _LOG.debug('Using input sources: %s', sources)
        sources = [s.lower() for s in sources]

        self._getters: list[GenericFileGetter] = []
        for src in sources:
            if src == 'ohcp':
                oh_getter = OHCPGetter(
                    oh_src=(config.oh_partition, config.oh_server, config.oh_provider),
                    detail=config.detail,
                    algorithm_vtx=config.algorithm_vtx,
                    algorithm_trk=config.algorithm_trk,
                    runNumber=config.run_number,
                    minLBN=config.min_lbn,
                    dirPath=config.tmpdir,
                )
                self._getters.append(oh_getter)
            elif src == 'coca':
                if config.run_number is not None and config.run_number > 0:
                    coca_getter = COCAGetter(
                        server=config.oh_server,
                        provider=config.oh_provider,
                        detail=config.detail,
                        algorithm_vtx=config.algorithm_vtx,
                        algorithm_trk=config.algorithm_trk,
                        runNumber=config.run_number,
                        minLBN=config.min_lbn,
                        dirPath=config.tmpdir,
                    )
                    self._getters.append(coca_getter)
                else:
                    _LOG.warning("COCA source is requested but run number is not known.")
            else:
                _LOG.error("Unexpected source name: %s", src)

    def goFetch(self, can_fail: bool = False) -> FileInfo | None:
        # docstring inherited from base class
        for getter in self._getters:

            label = getter.__class__.__name__
            _LOG.debug("Now trying %s getter", label.upper())

            finfo = getter.goFetch(can_fail=True)
            if finfo is not None:
                _LOG.debug('%s method returned this file %s', label, finfo.path)
                return finfo

            _LOG.debug('%s method did not return a file', label)

        return None
