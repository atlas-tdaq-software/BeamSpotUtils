'''Library for extra OS helper functions'''

from __future__ import annotations

import errno
import logging
import os
import shutil
import stat
import subprocess
import tempfile
from collections.abc import Mapping


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


def quickOSCall(cmd: str | list[str], userEnv: Mapping[str, str] | None = None,
                mustReturnZero: bool = False, mute: bool = False) -> tuple[str, str]:
    """Execute specified command and return its output.

    Parameters
    ----------
    cmd : str or list
        Command to execute, either string in shell syntax or a list of strings.
    userEnv : dict, optional
        Environment to pass to a subprocess instead of current process
        environment.
    mustReturnZero : bool, optional
        If True then return code will be checked and if not zero the error
        message will be logged and application will terminate. Default value
        is False.
    mute : bool, optional
        If True then do not log debugging message with subprocess output.

    Returns
    -------
    out : str
        Byte string (possibly empty) containing subprocess standard output.
    err : str
        Byte string (possibly empty) containing subprocess standard error.
    """
    out, err, _ = quickOSCallReturnCode(cmd, userEnv, mustReturnZero, mute)
    return (out, err)


def quickOSCallReturnCode(cmd: str | list[str], userEnv: Mapping[str, str] | None = None,
                          mustReturnZero: bool = False, mute: bool = False) -> tuple[str, str, int]:
    """Execute specified command and return its output and return code.

    Parameters
    ----------
    cmd : str or list
        Command to execute, either string in shell syntax or a list of strings.
    userEnv : dict, optional
        Environment to pass to a subprocess instead of current process
        environment.
    mustReturnZero : bool, optional
        If True then return code will be checked and if not zero the error
        message will be logged and application will terminate. Default value
        is False.
    mute : bool, optional
        If True then do not log debugging message with subprocess output.

    Returns
    -------
    out : str
        Byte string (possibly empty) containing subprocess standard output.
    err : str
        Byte string (possibly empty) containing subprocess standard error.
    rc : int
        Subprocess return code.
    """
    _LOG.debug('Executing command: %s', cmd)

    # if string is given then use shell
    useShell = isinstance(cmd, (type(""), type(u"")))

    try:
        proc = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            shell=useShell, env=userEnv,
            encoding="utf8", errors="replace", text=True)
    except OSError as exc:
        _LOG.critical('Python caught an OSError.  The shell probably'
                      ' doesn\'t understand the command: %s.  #%d: %s',
                      cmd, exc.errno, exc.strerror)
        exit(-1)

    (out, err) = proc.communicate()

    if mustReturnZero and proc.returncode != 0:
        _LOG.critical('Command terminated with a non-zero '
                      'exit code (%d): %s', proc.returncode, cmd)
        _LOG.warning('Process had stdout: <%s>', out)
        _LOG.warning('Process had stderr: <%s>', err)
        exit(proc.returncode)

    if out is None:
        out = ''
    if err is None:
        err = ''

    if not mute:
        _LOG.debug('Process had stdout: <%s>', out)
        _LOG.debug('Process had stderr: <%s>', err)

    return (out.strip(), err.strip(), proc.returncode)


def rmdir(folder: str) -> None:
    """Remove specified directory and all its contents.

    Does not do anyhting if directory does not exist.

    Parameters
    ----------
    folder : str
        Path to the folder to remove.

    Raises
    ------
    OSError, same as raised by `shutil.rmtree`.
    """
    try:
        shutil.rmtree(folder)
    except OSError as exc:
        if exc.errno == errno.ENOENT:
            _LOG.debug('%s didnt exist. who cares.', folder)
        else:
            raise


def deleteFileOrFolder(str_path: str) -> None:
    """Remove specified file or directory (recursively).

    Parameters
    ----------
    str_path : str
        Path to the file or folder to remove.

    Raises
    ------
    OSError, same as raised by `shutil.rmtree`.
    """
    _LOG.debug('Removing: ' + str_path)
    try:
        os.remove(str_path)
    except OSError as exc:
        if exc.errno != errno.EISDIR:
            _LOG.warning('Removing %s failed: %s', str_path, exc.strerror)
        else:
            shutil.rmtree(str_path)


def makeTempDir(userTempDir: str | None = None) -> tuple[str, bool]:
    """Create temporary directory.

    Create temporary directory to store files, user can provide location.

    Parameters
    ----------
    userTempDir : str or None
        If not None then it is returned unchanged, otherwise new unique name
        is generated inside system temporary directory.

    Returns
    -------
    path : str
        Directory location.
    created : bool
        True if new directory was created
    """
    created = False
    if userTempDir is None:
        userTempDir = tempfile.mkdtemp(prefix="beamSpotTool")
        # mode is too strict, relax it a bit
        os.chmod(userTempDir, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR |
                 stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH)
        created = True
    else:
        # make sure it exists
        try:
            os.makedirs(userTempDir)
            created = True
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
    return userTempDir, created
