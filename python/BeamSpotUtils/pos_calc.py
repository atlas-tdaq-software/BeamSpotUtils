'''Library: Handles COOL access for deltaX->posX conversion and sigmaX->dsigmaX'''

from __future__ import annotations

import logging
import math

from . import model
from .translators import make_translator


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


def deltaToPosition(rep: model.BeamSpotRepresentation, inputDesc: str) -> None:
    """Calculate snapshot position from their deltas.

    If snapshots are missing positions but have position deltas we
    read reference representations and calculate snapshot positions
    as delta to a reference.

    Note: existing snapshots are modified in-place.

    Parameters
    ----------
    rep : BeamSpotRepresentation
    inputDesc : str
        Input description, where to get reference data from.
    """
    if not rep.needsDeltaToPosition():
        _LOG.info("deltaToPosition correction is not needed.")
        return

#   this was used for validation on lxplus, and should not be used at P1
#    for snap in rep[1:]:
#        refSnap = rep[0]
#        for axis in 'XYZ':
#            field    = 'pos%s'      % axis
#            fieldErr = 'pos%sErr'   % axis
#            delta    = 'delta%s'    % axis
#            deltaErr = 'delta%sErr' % axis
#            if snap.emptyPhysicsField(field) and not \
#            snap.emptyPhysicsField(delta):
#                snap[field]    = snap[delta] + refSnap[field]
#                snap[fieldErr] = math.sqrt( pow(snap[deltaErr]   , 2)
#                                          + pow(refSnap[fieldErr], 2))

    _LOG.debug("deltaToPosition")
    pairs = set([(snap.run, snap.lbn) for snap in rep])
    cache = {}
    for run, lbn in pairs:
        trans = make_translator(inputDesc, run=run, lbn=lbn)

        ref = trans.read()
        if ref is None:
            _LOG.error("Failed to retrieve reference For run %d and lbn %d", run, lbn)
            raise RuntimeError("Failed to retrieve delta reference")

        cache[(run, lbn)] = ref

    for key, val in cache.items():
        _LOG.info("For run %d and lbn %d I'm using a snapshot corresponding to %d, %d",
                  key[0], key[1], val[0].run, val[0].lbn)

    for snap in rep:
        refSnap = cache[(snap.run, snap.lbn)][0]
        for axis in 'XYZ':
            if snap.emptyPhysicsField('pos' + axis) and not snap.emptyPhysicsField('delta' + axis):
                snapSlice = snap.slice(axis)
                refSlice = refSnap.slice(axis)
                snapSlice.pos = snapSlice.delta + refSlice.pos
                snapSlice.posErr = math.hypot(snapSlice.deltaErr, refSlice.posErr)


def sigmaToDSigma(rep: model.BeamSpotRepresentation) -> model.BeamSpotRepresentation:
    '''Given a series of snapshots, I go back to the hlt and calculate dsigma
    the amount, in microns the HLT is off from the actual beamspot

    I return a new rep UNLIKE my brother deltaToPosition to adhere
    to functional programming techniques.

    Parameters
    ----------
    rep : BeamSpotRepresentation

    Returns
    -------
    New updated BeamSpotRepresentation.
    '''

    newRep = model.BeamSpotRepresentation(rep)
    pairs = set((snap.run, snap.lbn) for snap in newRep)
    cache = {}
    for run, lbn in pairs:
        trans = make_translator('onhlt', run=run, lbn=lbn)
        readRep = trans.read()
        if readRep is not None:
            cache[(run, lbn)] = readRep

    for key, val in cache.items():
        _LOG.info("For run %d and lbn %d I'm using a snapshot corresponding"
                  " to %d, %d", key[0], key[1], val[0].run, val[0].lbn)

    for snap in newRep:
        hltSnap = cache[(snap.run, snap.lbn)][0]

        # many runs have no deltaZ for some historical reason
        if snap.emptyPhysicsField("deltaZ") and   \
                not snap.emptyPhysicsField("posZ"):
            snap.deltaZ = snap.posZ - hltSnap.posZ
            snap.deltaZErr = snap.posZErr
        else:
            snap.deltaZ = 0.
            snap.deltaZErr = 0.

        # here is the meat, calculate sigmaNow - sigmaHLT
        for axis in 'XYZ':

            slice = snap.slice(axis)
            hltSlice = hltSnap.slice(axis)

            if not snap.emptyPhysicsField("sigma" + axis):
                slice.dsigma = slice.sigma - hltSlice.sigma
                slice.dsigmaErr = math.hypot(slice.sigmaErr, hltSlice.sigmaErr)
            else:
                slice.dsigma = 0.
                slice.dsigmaErr = 0.

    return newRep
