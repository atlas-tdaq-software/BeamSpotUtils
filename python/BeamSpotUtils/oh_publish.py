"""Module includes classes and methods for histogram publishing in OH server.
"""

from __future__ import annotations

import logging
import os
import time
from typing import TYPE_CHECKING

from ipc import IPCPartition
from oh import OHRootProvider

if TYPE_CHECKING:
    from .freaking_ROOT import ROOT


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


class OHPublisher:
    """Helper class for publishing histograms in OH server.

    Parameters
    ----------
    partition : str
        Parition name, if empty $TDAQ_PARTITION is used.
    server : str
        OH server name.
    provider : str
        Provider name.
    folder : str
        Folder name.
    """
    def __init__(self, partition: str, server: str, provider: str, folder: str):
        self._partition = partition
        if not self._partition:
            self._partition = os.environ.get("TDAQ_PARTITION")  # type: ignore
        self._server = server
        self._provider = provider
        self._folder = folder

        self._ohprovider: OHRootProvider | None = None

    @property
    def do_publish(self) -> bool:
        return all([self._partition, self._server, self._provider, self._folder])

    @property
    def provider(self) -> OHRootProvider | None:
        if self.do_publish and self._ohprovider is None:
            _LOG.info("will publish histograms: parition=%r server=%r provider=%r",
                      self._partition, self._server, self._provider)
            part = IPCPartition(self._partition)

            # try to register OH provider, there may be few BSTs running
            # at the same time trying to do the same, if there is a conflict
            # retry for max. of 3 seconds
            try_until = time.time() + 3
            while self._ohprovider is None:
                try:
                    self._ohprovider = OHRootProvider(part, self._server,
                                                      self._provider, None)
                except Exception as exc:
                    # ERS catches C++ exceptions (ers::Issue) and translates
                    # them into Python exceptions but it's not possible to
                    # use Python exception type directly, have to work based
                    # on class name
                    if exc.__class__.__name__ == "ProviderAlreadyExist" and time.time() < try_until:
                        # wait a bit longer
                        _LOG.info("Provider name already in use, will wait until released")
                        time.sleep(0.1)
                    else:
                        raise

        return self._ohprovider

    def finish(self) -> None:
        """Finish publishing, deactivate provider.
        """
        self._ohprovider = None

    def publish(self, histo: ROOT.TH1, name: str, tag: int | None) -> None:
        """Publish one histogram.

        Parameter
        ---------
        histo : TH1
            ROOT histogram instance
        name : `str`
            Histogram name
        tag : `int`
            Tag/LB number
        """

        if not self.provider:
            # no publishing
            return

        path = self._folder.rstrip('/')
        path += "/"
        path += name
        _LOG.debug("publishing histogram with name %r", path)
        self.provider.publish(histo, path, tag)
