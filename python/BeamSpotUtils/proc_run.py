'''Library: Sequentially calls each step in the beam spot process'''

from __future__ import annotations

import errno
import logging
import os
from collections.abc import Callable, Mapping
from typing import NamedTuple, TYPE_CHECKING

from . import differ, model, pos_calc, translators
from .util import prettyRange

if TYPE_CHECKING:
    from . import bscalc, htree

# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


class SingleRunConfig(NamedTuple):
    """Configuration for SingleRun, with default values.

    Some of this values are overridden by `Config` class based on command line
    parameters.
    """
    use_run_summary: bool = True
    """Use run summary histograms"""

    repr_diff: str | None = None
    """Representation for comparison baseline, can be None to suppress diff"""

    repr_output: str | None = None
    """Representation for output of the result"""

    repr_nofilter_output: str | None = None
    """Representation for output of the result (without filtering)"""

    do_bcid: bool = False
    """Produce BCID data"""

    delta_pos_repr: str = "onhlt"
    """Representation used for retrieving positions for delta-to-position
    correction."""

    output_dir: str | None = None
    """Directory to store output files"""

    algo_name: str = "auto"
    """Algorithm name to use, 'vertex_only', 'track_only', 'vertex', 'track', or 'auto'."""


class SingleRun:
    """Class which encapsulates processing of a single run.

    Parameters
    ----------
    config : `SingleRunConfig`
        Configuration
    bscalc_vertex : `bscalc_vtx.BSCalcVertex`
        BSCalcVertex instance
    bscalc_track : `bscalc_trk.BSCalcTrack`
        BSCalcVertex instance
    run_number : int
        Run number
    output_dir : `str`
        Path to folder for output files.
    emscan : `EmittanceScan`
        EmittanceScan instance.
    ready4physics : `r4p.Ready4Physics`
        Callable returning value of Ready4Physics flag.
    """
    def __init__(self, *, config: SingleRunConfig, bscalc_vertex: bscalc.BSCalc,
                 bscalc_track: bscalc.BSCalc, run_number: int, output_dir: str,
                 emscan: Callable[[], bool | None], ready4physics: Callable[[], bool | None]):
        self.config = config
        self.emscan = emscan
        self.run_number = run_number
        self.ready4physics = ready4physics
        self.output_dir = output_dir
        self.bscalc_vertex = bscalc_vertex
        self.bscalc_track = bscalc_track

    def process(self, hist_trees: Mapping[int, htree.HTree]) -> None:
        """Full processing of histograms from one run (several LBs)

        Parameters
        ----------
        hist_trees : `dict` [`int`, `htree.HTree`]
            Maps LB number to corresponding ROOT histogram tree
        """
        self._makeOutputDir()
        bsRepr = self._calcBS(hist_trees)
        if bsRepr is not None:
            self._publish(bsRepr)

    def _makeOutputDir(self) -> None:
        """Create directory for ouput files
        """
        # go ahead and try to make it
        try:
            os.makedirs(self.output_dir)
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise

        _LOG.debug('Using %s for application output', self.output_dir)

    def _calcBS(self, hist_trees: Mapping[int, htree.HTree]) -> model.BeamSpotRepresentation | None:
        """Main method, calculate beamspot.

        Parameters
        ----------
        hist_trees : `dict` [`int`, `htree.HTree`]
            Maps LB number to corresponding ROOT histogram tree

        Returns
        -------
        bsRepr : `model.BeamSpotRepresentation`
            Beamspot.
        """
        if self.config.algo_name == "vertex_only":
            bsRepr = self._bscalc(hist_trees, self.bscalc_vertex)
        elif self.config.algo_name == "track_only":
            bsRepr = self._bscalc(hist_trees, self.bscalc_track)
        elif self.config.algo_name == "vertex":
            bsRepr = self._bscalc(hist_trees, self.bscalc_vertex)
            if not bsRepr:
                bsRepr = self._bscalc(hist_trees, self.bscalc_track)
        elif self.config.algo_name == "track":
            bsRepr = self._bscalc(hist_trees, self.bscalc_track)
            if not bsRepr:
                bsRepr = self._bscalc(hist_trees, self.bscalc_vertex)
        elif self.config.algo_name == "auto":
            bsReprVtx = self._bscalc(hist_trees, self.bscalc_vertex)
            #bsReprTrk = self._bscalc(hist_trees, self.bscalc_track)
            bsReprTrk = None
            bsRepr = self._choseBS(bsReprVtx, bsReprTrk)
        else:
            raise ValueError(f"unknown algo_name: {self.config.algo_name}")

        if bsRepr is None:
            return None

        # correct deltas (for BCID measurements)
        pos_calc.deltaToPosition(bsRepr, self.config.delta_pos_repr)

        path = os.path.join(self.output_dir, 'OnlineBeamSpotResults.csv')
        toCSV = translators.CSVTranslator(path, mode='a')
        toCSV.write(bsRepr)

        # We filtered status=7 snapshots previously, now filtering is done
        # by individual translators.

        return bsRepr

    def _choseBS(self, bsReprVtx: model.BeamSpotRepresentation | None,
                 bsReprTrk: model.BeamSpotRepresentation | None) -> model.BeamSpotRepresentation | None:
        """Select one beamspot out of two (or few).

        Parameters
        ----------
        bsReprVtx : `model.BeamSpotRepresentation`
            Beamspot from vertex-based method.
        bsReprTrk : `model.BeamSpotRepresentation`
            Beamspot from track-based method.
        """
        # TODO: look a both and decide, for now prefer track
        bsRepr = bsReprVtx
        if not bsRepr and bsReprTrk:
            bsRepr = bsReprTrk
        return bsRepr

    def _bscalc(self, hist_trees: Mapping[int, htree.HTree],
                bscalc_algo: bscalc.BSCalc) -> model.BeamSpotRepresentation | None:
        """Calculate beamspot using given algorithm.

        Calls provided algorithm on the data, updates returned beamspot with
        run/LB information.

        Parameters
        ----------
        hist_trees : `dict` [`int`, `htree.HTree`]
            Maps LB number to corresponding ROOT histogram tree
        bscalc_algo : `bscalc.BSCalc`
            Algorithm which does calculations

        Returns
        -------
        bsRepr : `model.BeamSpotRepresentation` or None
            Beamspot.
        """
        if not self.config.use_run_summary:

            _LOG.debug('Trying to use min, max lbn specs.')

            _LOG.info("Processing next group, LBNs: %s", prettyRange(hist_trees.keys()))
            run = self.run_number
            firstLBN = min(hist_trees.keys())
            lbn = max(hist_trees.keys())

        else:
            # only makes sense for a single file
            if len(hist_trees) != 1:
                raise ValueError("Unexpected multiple input files for runsummary corrections")
            _LOG.info('Trying to use run summary.')
            # this measurement is valid starting from next run
            run = self.run_number + 1
            firstLBN = -1
            lbn = -1

        snap = bscalc_algo.calc(hist_trees, run, lbn)
        bscalc_algo.finish()
        if snap is None:
            return None

        _LOG.info("Finished calculations on LB (or its group) %d", lbn)
        snap.firstLBN = firstLBN
        snap.lastLBN = lbn
        snap.lbn = lbn + 1
        snap.run = run

        res = model.BeamSpotRepresentation([snap])
        _LOG.info("CVTResults has %d entries", len(res))
        return res

    def _publish(self, bsRepr: model.BeamSpotRepresentation) -> None:
        """Publish beamspot to all destinations.

        Parameters
        ----------
        bsRepr : `model.BeamSpotRepresentation`
            Beamspot data.
        """
        # always write to no-filter output
        if self.config.repr_nofilter_output is not None:
            diff = differ.Differ()
            if diff.sanityCheck(bsRepr):
                self._publishOne(bsRepr, self.config.repr_nofilter_output)
            else:
                _LOG.info("Sanity check failed for snapshot, will skip publishing to %s",
                          self.config.repr_nofilter_output)

        # do not save regular outputs while in emittance scan
        if self.emscan():
            _LOG.info("Emittance scan is in progress, skipping all updates.")
            return

        # write to regular output if diff passes (if input is given)
        if self.config.repr_output is not None:
            if self.config.repr_diff is not None:
                diff = differ.Differ(self.config.repr_diff)
                storeRepr = diff.storeRepr(bsRepr, r4p=self.ready4physics())
                if storeRepr is not None:
                    _LOG.info('Diff determined to do UPDATE')
                    self._publishOne(storeRepr, self.config.repr_output)
                else:
                    _LOG.info('Diff determined to do NO UPDATE')
            else:
                # output only, just force it but do some sanity checks first
                diff = differ.Differ()
                if diff.sanityCheck(bsRepr):
                    _LOG.debug('No repr_diff, so forcing an update')
                    self._publishOne(bsRepr, self.config.repr_output)
                else:
                    _LOG.info("Sanity check failed for snapshot, will skip publishing to %s",
                              self.config.repr_output)

    def _publishOne(self, bsRepr: model.BeamSpotRepresentation, repr_output: str) -> None:
        """Publish beamspot to a single destination.

        Parameters
        ----------
        bsRepr : `model.BeamSpotRepresentation`
            Beamspot data.
        repr_output : `str`
            Name of translator for publishing.
        """
        trans = translators.make_translator(repr_output, do_bcid=self.config.do_bcid)

        _LOG.info("Writing out a representation to repr_output %s", repr_output)
        try:
            trans.write(bsRepr)
        except Exception as exc:
            _LOG.error("Publication to %s failed: %s", repr_output, exc, exc_info=True)

    def __str__(self) -> str:
        return f"{self.__class__.__name__}(config={self.config})"
