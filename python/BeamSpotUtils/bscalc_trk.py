"""Implementation of beamspot calculation using new track-based
method with HLT-produced histograms.
"""

from __future__ import annotations

import logging
from collections.abc import Mapping
from typing import Any, NamedTuple, TYPE_CHECKING

from .hist_def import make_hdefs_trk
from . import bscalc, hist_def, htree, model, track_fit, translators, util
import ers

if TYPE_CHECKING:
    from . import lbn_merge, oh_publish


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


class HistoPublishError(ers.Issue):
    """ERS Issue class for errors in histogram copying"""

    def __init__(self, cause: Any = None):
        ers.Issue.__init__(self, 'Failed to publish intermediate histograms (not fatal)',
                           {},
                           cause if isinstance(cause, ers.Issue) else None)


def _get_stat(hist_tree: htree.HTree, lbn: int, hdefs: list[hist_def.HistoDef]) -> float:
    """Return track counter from histograms.
    """
    ntrks = [0]
    histo = hist_tree.get_histogram("llpoly", lbn)
    if histo:
        ntrks.append(histo.GetBinContent(98 + 1))
    histo = hist_tree.get_histogram("beam_ls", lbn)
    if histo:
        ntrks.append(histo.GetBinContent(17 + 1))
    return max(ntrks)


class BSCalcTrackConfig(NamedTuple):
    """Configuration for track-based method, with default values.

    Some of this values are overridden by `Config` class based on command line
    parameters.
    """
    min_track_count: int = 10000
    """threshold for merging LBNs"""

    nBunches: int = 3563
    """The number of bins in BCID histograms"""

    do_bcid: bool = False
    """Produce per-BCID snapshots"""

    use_scipy: bool = False
    """If True then use scipy for fitting/minimization"""

    min_lbn: int = -1
    """Number of LBNs that we want to be merged."""

    mon_output: str = ""
    """Name of the output translator to publish results"""


class BSCalcTrack(bscalc.BSCalc):
    """All of the steps necessary for calculating beamspot.

    Parameters
    ----------
    config : `BSCalcTrackConfig`
        Configuration parameters.
    output_dir : `str`
        Path to folder for output files.
    lbn_merge : `lbn_merge.LBNMerge`
        LBNMerge instance.
    oh_publisher : `OHPublisher`, optional
        OHPublisher instance
    """

    def __init__(self, config: BSCalcTrackConfig, output_dir: str, lbn_merge: lbn_merge.LBNMerge,
                 oh_publisher: oh_publish.OHPublisher | None = None):

        self.config = config
        self.hdefs = make_hdefs_trk(self.config.do_bcid)

        histoNames = [hdef.name for hdef in self.hdefs]
        _LOG.info('Histogram names for track-based method: %s', ' '.join(sorted(histoNames)))

        self.output_dir = output_dir
        self.oh_publisher = oh_publisher
        self.lbn_merge = lbn_merge

    def calc(self, hist_trees: Mapping[int, htree.HTree],
             run: int | None = None,
             lbn: int | None = None) -> model.BSSnapshot | None:
        # docstring inherited from base class

        if not hist_trees:
            return None

        _LOG.info("Input LBNs: %s", util.prettyRange(hist_trees.keys()))

        # merge all LBNs
        if len(hist_trees) > 1:
            hist_tree, merged_lbns = self.lbn_merge.merge(
                hist_trees=hist_trees, hdefs=self.hdefs, min_lbn_count=self.config.min_lbn,
                min_stat=self.config.min_track_count, get_stat=_get_stat)
            _LOG.info("Merged these LBNs: %s", util.prettyRange(merged_lbns))
            if not merged_lbns:
                return None
            fetch_lbn = None
        else:
            fetch_lbn, hist_tree = next(iter((hist_trees.items())))
            if fetch_lbn < 0:
                fetch_lbn = None

        hist = hist_tree.get_histogram("beam_ls", fetch_lbn)
        ls_matrix_data = None if hist is None else track_fit.LSMatrix.from_histo(hist)
        hist = hist_tree.get_histogram("llpoly", fetch_lbn)
        llpoly_data = None if hist is None else track_fit.PolyLL.from_histo(hist)

        if llpoly_data is None and ls_matrix_data is None:
            _LOG.warning("Failed to find beam_ls or llpoly histograms")
            return None

        # check track count first
        n_tracks = 0
        if ls_matrix_data:
            _LOG.info("# of tracks found in LS matrix: %d", ls_matrix_data.n_tracks)
            n_tracks = ls_matrix_data.n_tracks
        if llpoly_data:
            _LOG.info("# of tracks found in LL data: %d beam_size: %f",
                      llpoly_data.n_tracks, llpoly_data.beam_size)
            n_tracks = max(n_tracks, llpoly_data.n_tracks)
        if n_tracks < self.config.min_track_count:
            _LOG.info("Not enough tracks for LBNs %d: minimum=%d, found=%d",
                      lbn, self.config.min_track_count, n_tracks)
            return None

        # make fitter instance
        fitter: track_fit.TrackFitter
        if self.config.use_scipy:
            fitter = track_fit.ScipyFitter()
        else:
            fitter = track_fit.MinuitFitter()

        bcidSnaps = []
        if self.config.do_bcid:
            hist = hist_tree.get_histogram("beam_ls_bcid", fetch_lbn)
            if hist is None:
                _LOG.warning("Failed to find beam_ls_bcid histogram")
            else:
                _LOG.info("====== Fitting per-BCID positions ======")
                for bcid in range(self.config.nBunches + 1):
                    matrix_data = track_fit.LSMatrixBCID.from_histo(hist, bcid)
                    _LOG.debug("BCID %d LS data has %d tracks", bcid, matrix_data.n_tracks)
                    if matrix_data.n_tracks > 0:
                        bcidSnap = fitter.fit_bcid_data(matrix_data, bcid)
                        bcidSnaps.append(bcidSnap)

        snapLS = None
        if ls_matrix_data:
            _LOG.info("====== Fitting with LS ======")
            snapLS = fitter.fit_total_data(ls_matrix_data)
            snapLS.bcids = bcidSnaps
            if run is not None:
                snapLS.run = run
            if lbn is not None:
                snapLS.lbn = lbn
            _LOG.info("LS fit result")
            snapLS.prettyLog(_LOG)

        snapLL = None
        if llpoly_data:
            _LOG.info("====== Fitting polynomial LL ======")
            snapLL = fitter.ll_fit_data(llpoly_data)
            snapLL.bcids = bcidSnaps
            if run is not None:
                snapLL.run = run
            if lbn is not None:
                snapLL.lbn = lbn
            _LOG.info("LL fit result")
            snapLL.prettyLog(_LOG)

        snap = snapLL if snapLL is not None else snapLS

        if snap:
            _LOG.info("Fit result")
            snap.prettyLog(_LOG)

            self._publish(snap)

        return snap

    def finish(self) -> None:
        # docstring inherited from base class
        pass

    def _publish(self, snap: model.BSSnapshot) -> None:
        """Publish snapshot(s).

        Parameters
        ----------
        snap : `model.BSSnapshot`
            Measurement snapshot.
        """
        mon_output = self.config.mon_output
        if not mon_output:
            return
        trans = translators.make_translator(mon_output, do_bcid=self.config.do_bcid)

        bsRepr = model.BeamSpotRepresentation([snap])
        _LOG.info("Writing out a representation to %r", mon_output)
        try:
            trans.write(bsRepr)
        except Exception as exc:
            # do not make error message to avoid shifter confusion
            _LOG.info("Publication to %r failed: %s", mon_output, exc, exc_info=True)
