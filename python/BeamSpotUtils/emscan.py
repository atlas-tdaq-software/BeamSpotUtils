"""Module defining EmittanceScan class and related methods.
"""

from __future__ import annotations

import logging

import ispy

_LOG = logging.getLogger(__name__.partition('.')[2])


class EmittanceScan:
    """Functor class for determining when emittance scan is in progress.

    Parameters
    ----------
    partition : str, optional
        Partition for IS server containing EmittanceScanOngoing variable.
        If not provided then this functor always returns False.
    variable : str, optional
        Name of IS EmittanceScanOngoing variable. If not provided then this
        functor always returns False.
    """
    def __init__(self, partition: str | None = None, variable: str | None = None):
        self._part = partition
        self._var = variable

    def __call__(self) -> bool:
        """Return True if scan is in progress.
        """
        if None in (self._part, self._var):
            # always return false
            return False

        emscan: bool
        try:
            isobj = ispy.ISObject(self._part, self._var)
            isobj.checkout()
            emscan = isobj.value  # pylint: disable=no-member
            _LOG.info('Retrieved %s flag: %s', self._var, emscan)
        except Exception as exc:
            _LOG.warning('Failure when retrieving %s: %s', self._var, exc)
            # it is safest to assume that no scan is going on
            emscan = False
            _LOG.info('Cannot retrieve %s value: assume no emittance scan',
                      self._var)

        return emscan
