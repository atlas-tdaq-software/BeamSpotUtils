"""Methods for merging/grouping of per-LBN histograms.
"""

from __future__ import annotations

import logging
from collections.abc import Callable, Iterable, Iterator, Mapping
from typing import Any, TypeVar

from . import hist_def, htree
from .freaking_ROOT import ROOT
from .util import prettyRange


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


def _buckets(seq: Iterable[Any], size: int) -> Iterator[list[Any]]:
    """Split sequence into a number of "buckets" of equal size.

    Last bucket may not be complete.

    Parameters
    ----------
    seq : iterable
        Sequence of items
    size : int
        Bucket size, if size is negative then bucket is bottomless.

    Yields
    ------
    bucket : list
        Lsit of items in a bucket
    """
    bucket = []
    for item in seq:
        bucket.append(item)
        if size > 0 and len(bucket) >= size:
            yield bucket
            bucket = []
    if bucket:
        yield bucket


T = TypeVar("T", bound=htree.HistoProtocol)


class LBNMerge:
    """

    Parameters
    ----------
    min_lbn, max_lbn : int
        Minimum/maximum LBN to include in output, both numbers are inclusive.
        Any of the two can be negative which means counting back from highest
        LB number that exists in the file(s).
    split_lbn : int
        Size of the groups returned from `group_by_split` method. If
        non-positive then single group is returned.
    use_rs_res : bool, optional
        If True then use run summary histograms for resolution distributions.
    """

    def __init__(self, min_lbn: int, max_lbn: int, split_lbn: int, use_rs_res: bool = False):
        self.min_lbn = min_lbn
        self.max_lbn = max_lbn
        self.split_lbn = split_lbn
        self.use_rs_res = use_rs_res

    def _wanted_lbns(self, lbns: Iterable[int]) -> list[int]:
        """Search ROOT file for LBNs in range.

        Parameters
        ----------
        lbns : list of int
            Full set of LBNs.

        Returns
        -------
        lbns : list of int
            List of LB numbers, sorted in increasing order,
            limited to min_lbn-max_lbn.
        """
        lbns = sorted(lbns)
        if 0 in lbns:
            lbns.remove(0)
            _LOG.info("Popping the 0 lb out")
        if len(lbns) == 0:
            _LOG.info('I found no LBN\'s in the file!')
            return []

        _LOG.info("I found these LBN's in the file: %s", prettyRange(lbns))

        topLBN = max(lbns)

        min_lbn, max_lbn = self.min_lbn, self.max_lbn
        # If min_lbn is -1 and we have 454 lb's then it should go to 454.
        if min_lbn < 0:
            min_lbn = max(topLBN + 1 + min_lbn, 0)
        if max_lbn < 0:
            max_lbn += topLBN + 1

        lbns = [x for x in lbns if min_lbn <= x <= max_lbn]

        _LOG.info('The max lbn in hand is %d. This means min_lbn and '
                  'max_lbn wanted are: %d, %d', topLBN, min_lbn, max_lbn)
        return lbns

    def _lbn_generator(self, trees: list[htree.HTree[T]], reverse: bool = False
                       ) -> Iterator[tuple[int, htree.HTree[T]]]:
        """Makes sequence of (LBN, Htree) tuples.

        Parameters
        ----------
        trees : `list` [`HTree`]
            One or more ROOT histogram tree objects
        reverse : `bool`, optional
            If True then LBNs are returned in descending order, otherwise in
            ascending order.

        Yieds
        -----
        lbn : `int`
            LB number
        tree : `htree.HTree`
            Its corresponding histogram tree.

        Raises
        ------
        ValueError
            Raised if more than one HTree has data for the same LBN.
        """
        all_lbns: dict[int, htree.HTree[T]] = {}
        for atree in trees:
            for lbn in atree.get_lbns():
                if lbn in all_lbns:
                    name1 = all_lbns[lbn].name()
                    name2 = atree.name()
                    raise ValueError(f"LBN {lbn} appears in more than one tree: {name1} and {name2}")
                all_lbns[lbn] = atree

        lbns = self._wanted_lbns(all_lbns.keys())
        lbns = sorted(lbns)
        if reverse:
            lbns = list(reversed(lbns))
        for lbn in lbns:
            yield lbn, all_lbns[lbn]

    def group_by_split(self, trees: list[htree.HTree[T]]) -> list[Mapping[int, htree.HTree[T]]]:
        """Split all LBs in a run into groups of equal size.

        Looks at the list of LBs which exist in ROOT files and combines them
        into groups, size of each group is determined by `split_lbn` number.
        Only LBs in range (min_lbn, max_lbn) are returned (inclusive). If
        `split_lbn` is non-positive then single group is returned.

        Parameters
        ----------
        trees : `list` [`HTree`]
            One or more ROOT histogram tree objects

        Returns
        -------
        groups : `list` [`dict`]
            List of groups, each group is a dictionary with LB number as key and
            its corresponding HTree object.

        Raises
        ------
        ValueError
            Raised if more than one HTree has data for the same LBN.
        LookupError
            Raised if there no LBNs found.
        """
        # we want last group to be always as complete as possible,
        # use 'reverse' for that
        lbns = self._lbn_generator(trees, reverse=True)

        groups: list[Mapping[int, htree.HTree[T]]] = []
        for bucket in _buckets(lbns, self.split_lbn):
            group = dict(bucket)
            # insert in reverse order too
            groups.insert(0, group)

        if len(groups) == 0:
            raise LookupError("I found no LBN's to analyze")

        return groups

    def _tree_filter(self, hist_trees: Mapping[int, htree.HTree[T]],
                     hdefs: list[hist_def.HistoDef], reverse: bool = False
                     ) -> Iterator[tuple[int, htree.HTree[T]]]:
        """Makes sequence of (LBN, Htree) tuples.

        This method checks contents of the histograms, if any histogram is
        missing or if all histograms are empty then corresponding LBN is
        skipped.

        Parameters
        ----------
        trees : `list` [`HTree`]
            One or more ROOT histogram tree objects
        hdefs : `list` [ `hist_def.HistoDef` ]
            Definitions for histograms that are read from files.
        reverse : `bool`, optional
            If True then LBNs are returned in descending order, otherwise in
            ascending order.

        Yieds
        -----
        lbn : `int`
            LB number
        tree : `htree.HTree`
            Its corresponding histogram tree.
        """
        did_do = []
        empty_hist = []
        no_hist = []

        lbns = sorted(hist_trees.keys(), reverse=reverse)

        for lbn in lbns:
            _LOG.debug("Now on LB %d", lbn)

            # Load histograms
            hists: dict[str, ROOT.TH1] = dict()
            has_none = False
            for hdef in hdefs:
                # Try to read run summary stuff, but it can be missing
                histo: T | None = None
                if hdef.is_rs_res and self.use_rs_res:
                    histo = hist_trees[lbn].get_histogram(hdef.name)
                if histo is None:
                    histo = hist_trees[lbn].get_histogram(hdef.name, lbn)
                if histo is None:
                    has_none = True
                else:
                    hists[hdef.name] = histo

            if has_none:
                # some histograms are missing
                _LOG.info("Could not find all histograms, skipping LB %d", lbn)
                no_hist.append(lbn)
            elif all(histo.GetEntries() == 0 for histo in hists.values()):
                # all histograms are empty
                _LOG.debug("All histograms were empty, skipping LB %d ", lbn)
                empty_hist.append(lbn)
            else:
                # histos are usable
                _LOG.debug("All histograms are usable in LB %d ", lbn)
                did_do.append(lbn)
                yield lbn, hist_trees[lbn]

        fail_msg = "These LBs were excluded because"
        if no_hist:
            _LOG.info("%s couldn\'t find histograms: %s", fail_msg, prettyRange(no_hist))
        if empty_hist:
            _LOG.info("%s all histograms are empty: %s", fail_msg, prettyRange(empty_hist))
        _LOG.info("Going to merge these lbns: %s", prettyRange(did_do))

    def merge(self,
              hist_trees: Mapping[int, htree.HTree[T]],
              hdefs: list[hist_def.HistoDef],
              min_lbn_count: int = -1,
              min_stat: float | None = None,
              get_stat: Callable[[htree.HTree[T], int, list[hist_def.HistoDef]], float] | None = None
              ) -> tuple[htree.HTree[T], list[int]]:
        """Merge histograms from few LBNs into one tree.

        Parameters
        ----------
        hist_trees : `dict` [`int`, `htree.HTree`]
            Maps LB number to HTree object providing access to histograms.
        hdefs : `list` [ `hist_def.HistoDef` ]
            Definitions for histograms that are merged.
        tfile : `ROOT.TFile`
            ROOT file which will be used to store merged histos, must be
            initially empty.
        min_lbn_count : `int`, optional
            Minimum number of LBNs to merge, even if statistics collected
            passes the threshold before it reaches this count of LBNs. If
            negative (default) then no constaint on LBN count is used.
        min_stat : `float`, optional
            Minimun statistics to collect, abstract number whose meaning is
            determined by ``get_stat`` method. If ``None`` (default) then
            statistics is not checked.
        get_stat : callable, optional
            Method that returns statistics value for a given LBN. If not
            specified then statistics is not checked. If given then it must
            be a callable that takes three parameters:
              - hist_tree: htree.HTree
              - lbn: int
              - hdefs: list[hist_def.HistoDef]
            and returns a floating point number.

        Returns
        -------
        htree : `htree.HTree`
            Histograms tree with resulting merged histograms, histograms are
            stored in `tfile` and `htree` lifetime should end before `tfile`
            is closed.
        lbns : `list` [ `int` ]
            LBNs that were actually merged, in the order they we merged.

        Notes
        -----
        This method merges histogram starting with the highest LBN and adding
        more LBNs until it collects enough statistics. For LBN to be merged
        its corresponding tree has to contain all histograms defined in
        ``hdefs``. Merging stops when all of these conditions are reached:

          - if ``min_lbn_count`` is provided and non-negative then at least
            this number of LBNs is merged
          - if both ``min_stat`` and ``stat_fun`` are provided then sum of
            values returned by ``stat_fun`` for each LBN exceeds ``min_stat``
          - if the above conditions are not satisfied when all input LBNs are
            merged then this method returns what has been merged so far.
        """
        adding_hdefs: list[hist_def.HistoDef] = []
        histos: dict[str, T] = {}
        if self.use_rs_res:
            # Retrieve runsummary histograms from any tree, they should be the
            # same. But try highest LBNs first in case they might have higher
            # statistics.
            for hdef in hdefs:
                if hdef.is_rs_res:
                    for lbn in sorted(hist_trees, reverse=True):
                        histo = hist_trees[lbn].get_histogram(hdef.name)
                        if histo is not None:
                            _LOG.info('Found %s runsummary histogram', hdef.name)
                            histos[hdef.name] = histo
                            break
                    else:
                        adding_hdefs.append(hdef)
                else:
                    adding_hdefs.append(hdef)
        else:
            # Accumulate all of them
            adding_hdefs = hdefs

        del hdefs

        # Filter out LBNs with missing data, use `reverse` to start with
        # highest LBN
        lbns = self._tree_filter(hist_trees, adding_hdefs, reverse=True)

        total_stat = 0.

        # combine all histograms in this group, in sorted order of LBN
        merged_lbns: list[int] = []
        for counter, (lbn, hist_tree) in enumerate(lbns):

            if min_stat is not None and get_stat is not None:
                lbn_stat = get_stat(hist_tree, lbn, adding_hdefs)
                total_stat += lbn_stat

            # Load histograms
            hists: dict[str, T] = {}
            for hdef in adding_hdefs:
                name = hdef.name
                histo = hist_tree.get_histogram(name, lbn)
                if histo is not None:
                    hists[name] = histo

            if counter == 0:
                _LOG.debug("Start addition for the group (LBN=%d)", lbn)
                for hdef in adding_hdefs:
                    name = hdef.name
                    _LOG.debug('Initializing %s add histogram', name)
                    # clone and make sure that cloned histo is not owned
                    hclone: T = hists[name].Clone()  # type: ignore
                    hclone.SetDirectory(0)
                    histos[name] = hclone
                    _LOG.debug("Histo %s, entries=%d", hists[name].GetName(), hists[name].GetEntries())
            else:
                for hdef in adding_hdefs:
                    name = hdef.name
                    histos[name].Add(hists[name])  # type: ignore
                    _LOG.debug("Add per-LB histo %s (%s), entries=%d",
                               hdef.name, hists[name].GetName(), hists[name].GetEntries())

            merged_lbns.append(lbn)

            # check if we want to stop
            if min_lbn_count < 0 or counter >= (min_lbn_count - 1):
                if min_stat is not None and total_stat >= min_stat:
                    break

        # store all combined histos to htree
        out_tree = htree.MemoryHTree[T](no_lbns=True)
        for name, hist in histos.items():
            # the name in run3 can have suffix, make sure that we write simple names
            hist.SetName(name)
            _LOG.debug("Saving merged histo %s, entries=%d", hist.GetName(), hist.GetEntries())
            out_tree.add_histogram(name, hist)

        # make a tree and return, use latest LBN for tree
        return out_tree, merged_lbns
