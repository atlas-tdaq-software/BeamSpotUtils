"""Collection of various ROOT utility methods.
"""

from __future__ import annotations

import logging
from collections.abc import Iterator

from .freaking_ROOT import ROOT


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


def fillGauss(histo: ROOT.TH1, mean: float, sigma: float, amplitude: float, reset: bool = True) -> ROOT.TH1:
    """Fill a histogram based on the mean, width, and amplitude
    of a Gaussian distribution

    Parameters
    ----------
    histo : ROOT.TH1
        Histogram to be filled.
    mean : float
        Center of the Gaussian
    sigma : float
        Sigma of the Gaussian
    amplitude : float
        Height of the Gaussian at its center
    reset : bool, optional
        If True then reset bin contentc, otherwise add to existing contents.

    Returns
    -------
    Updated histogram (update happens in place).
    """
    nbins = histo.GetNbinsX()
    for i in range(1, nbins + 1):
        value = ROOT.TMath.Gaus(histo.GetBinCenter(i), mean, sigma) * amplitude
        if not reset:
            value += histo.GetBinContent(i)
        histo.SetBinContent(i, value)
    return histo


def projectNTrkRange(h2: ROOT.TH2, ntrkAxis: str, minNTrk: int, maxNTrk: int,
                     title: str, reweight: ROOT.TH2 | int | float | None = None) -> ROOT.TH1:
    """Yet another custom projection/slice function.

    Projects a range of slices from 2-dim histogram onto axis
    perpendicular to "NTrks" axis. Only slices that fall into
    specified min/max range are selected. Optionally weights
    each slice with the weight calculated from the slice in another
    2-dim histogram.

    This method is used to produce 1-d histogram representing
    distribution of the distances between split tracks which is
    later used to extract average (over all track multiplicities)
    resolution for BCID resolution correction.

    Parameters
    ----------
    h2 : ROOT.TH2
        2-dimensional histogram to project
    ntrkAxis : 'X' or 'Y'
        Which axis correspond to NTrk values.
    minNTrk, maxNTrk : int
        Minimum/maximum track numbers to project. If minNTrk is 0 or
        negative then it is set to 1. maxNTrk is inclusive, and if it is
        negative then it is set to max number appearing in a histogram.
    title : str
        Title of a projection histogram.
    reweight : TH2 or float, optional
        Histogram or weight used for re-weighting. If a number is given
        then equal weight is assigned to all slices, otherwise if histogram
        igs given weight is calculated from corresponding slice in that
        histogram

    Returns
    -------
    1-D histogram
    """

    _LOG.debug("%s: ntrkAxis=%s minNTrk=%d maxNTrk=%d", h2.GetName(), ntrkAxis, minNTrk, maxNTrk)
    if reweight is not None:
        if hasattr(reweight, "GetName"):
            _LOG.debug("reweight=%s", reweight.GetName())  # type: ignore
        else:
            _LOG.debug("reweight=%s", reweight)

    ntrk_axis, proj_axis = h2.GetXaxis(), h2.GetYaxis()
    if ntrkAxis == 'Y':
        ntrk_axis, proj_axis = proj_axis, ntrk_axis
    elif ntrkAxis != 'X':
        raise ValueError("Expected ntrkAxis = 'X' or 'Y'")

    # make output histo
    nbins = int(proj_axis.GetNbins())
    minX = float(proj_axis.GetBinLowEdge(1))
    maxX = float(proj_axis.GetBinLowEdge(nbins + 1))
    output = ROOT.TH1F(title, title, nbins, minX, maxX)      # pylint: disable=no-member

    # map track numbers into bin numbers, make sure they are in range
    minNTrk = max(minNTrk, 1)
    minBin = ntrk_axis.FindBin(minNTrk)
    if maxNTrk < minNTrk:
        maxBin = ntrk_axis.GetNbins()
    else:
        maxBin = ntrk_axis.FindBin(maxNTrk)
    if maxBin > ntrk_axis.GetNbins():
        maxBin = ntrk_axis.GetNbins()
    _LOG.debug("minBin=%d maxBin=%d", minBin, maxBin)

    for i in range(minBin, maxBin + 1):
        _LOG.debug("bin=%d", i)
        if ntrkAxis == 'X':
            tmpHisto = h2.ProjectionY("_px", i, i)
        else:
            tmpHisto = h2.ProjectionX("_px", i, i)
        integral = tmpHisto.Integral()

        if reweight and integral > 0:
            if isinstance(reweight, (int, float)):
                rwintegral = reweight
            else:
                # find matching bin in weight histo
                ntrks = ntrk_axis.GetBinCenter(i)
                if ntrkAxis == 'X':
                    minY = 1
                    maxY = reweight.GetYaxis().GetNbins()
                    maxX = minX = reweight.GetXaxis().FindBin(ntrks)
                else:
                    minX = 1
                    maxX = reweight.GetXaxis().GetNbins()
                    maxY = minY = reweight.GetYaxis().FindBin(ntrks)

                rwintegral = reweight.Integral(minX, maxX, minY, maxY)

            weight = rwintegral / integral
            _LOG.debug("integral=%s rwintegral=%s weight=%s bins=%s", integral,
                       rwintegral, weight, (minX, maxX, minY, maxY))
            tmpHisto.Scale(weight)

        output.Add(tmpHisto)

    return output


def objectNames(tdir: ROOT.TDirectory) -> Iterator[str]:
    """Recursively iterate over all object names.

    This excludes all directory names.

    Parameters
    ----------
    tdir : `TDirectory`
        ROOT directory

    Yields
    ------
    path : `str`
        Names of objects.
    """

    def _histoNames(tdir: ROOT.TDirectory, dirname: str = "") -> Iterator[str]:
        for key in tdir.GetListOfKeys():
            if key.IsFolder():
                subdirname = dirname + key.GetName() + '/'
                subdir = key.ReadObj()
                for name in _histoNames(subdir, subdirname):
                    yield name
            else:
                name = dirname + key.GetName()
                _LOG.debug("found name %s;%d", name, key.GetCycle())
                yield name

    for name in _histoNames(tdir):
        yield name
