'''Library: MultiTranslator does I1 -> (O1 and O2 and O3 ...) translation '''

from __future__ import annotations

import logging

from . import base
from .. import model


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


class MultiTranslator(base.Translator):
    ''' This class holds multiple other translators and multiplexes
    writes so the user doesn't have to make loops
    when repOutput=a,b,c. For reading it does not really make sense
    to read from more than one source, what this translator do is to
    try to read from each representation and returns as soon as the
    one representation is returned.

    Parameters
    ----------
    translators : list of base.Translator, optional
        Initial set of child translators used to forward all requests.
    '''

    def __init__(self, translators: list[base.Translator] | None = None):
        self._translators = translators or []

    def append(self, trans: base.Translator) -> None:
        """Add one more child translator.

        Parameters
        ----------
        trans : base.Translator
        """
        self._translators.append(trans)

    def write(self, rep: model.BeamSpotRepresentation) -> None:
        """Translate representation to external format.

        Multi-translator tries to write data to each and every child.

        Parameters
        ----------
        rep : BeamSpotRepresentation
            In-memory beam spot representation.

        Raises
        ------
        If any child throws exception in its write() method then this
        exception is propagated back to caller, remaining children are
        not called then.
        """
        for tran in self._translators:
            tran.write(rep)

    def read(self) -> model.BeamSpotRepresentation | None:
        """Translate external format to in-memory representation.

        Returns
        -------
        List of BeamSpotRepresentation instances. Note that this breaks
        base class interface.
        """
        for tran in self._translators:
            try:
                output = tran.read()
                if output is not None:
                    return output
            except Exception:
                pass
        _LOG.warning("None of translators could read its input")
        return None
