''' Library for write only (publish) python wrappers for the CTP.
    CTPTranslator writes in the normal way,
    DummyCTPTranslator prints to stdout instead of publishing
    ConservativeCTPTranslator sets status to 5 then publishes
'''

from __future__ import annotations

import copy
import fcntl
import logging
import os
from collections.abc import Iterator
from contextlib import contextmanager
from typing import Any

import ers
from .. import model
from . import base
from .is_tran import ISCTPTranslator
from cool_utils import CTPUpdateConditions


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])

_LOCKFILE = "/tmp/beamspot_ctp_serialize"

_COOL_FOLDER_INDEX = 0


@contextmanager
def ctp_serialize() -> Iterator:
    try:
        try:
            fd = os.open(_LOCKFILE, os.O_RDONLY | os.O_CREAT)
            _LOG.info("locking file %s", _LOCKFILE)
            fcntl.flock(fd, fcntl.LOCK_EX)
        except OSError as exc:
            # This is an error, but try to continue without locking.
            _LOG.error("Failed to open lock file %s: %s", _LOCKFILE, exc)
        yield
    finally:
        _LOG.info("unlocking file %s", _LOCKFILE)
        fcntl.flock(fd, fcntl.LOCK_UN)
        os.close(fd)


class CTPError(ers.Issue):
    """ERS Issue class for CTP-related errors"""

    def __init__(self, message: str, cause: Any = None, **kw: Any):
        ers.Issue.__init__(self, 'Failure executing command: ' + message,
                           kw,
                           cause if isinstance(cause, ers.Issue) else None)


class CTPTranslator(base.Translator):
    '''Class capable of executing a write to the CTP.
        It first writes a restricted set of fields
        to the beam spot Information Service then
        sends a special command to the CTP.
        Only works online.
    '''

    def __init__(
        self, tag: str = "", ctp_partition: str = "", ctp_controller: str = "CtpController", **kwargs: Any
    ):
        self._invalidate = False
        if tag == "invalidate":
            # It means that we can accept status=4 updates.
            self._invalidate = True
        elif tag:
            _LOG.error("unexpected tag for CTP translator: %s", tag)
        self.ctp_partition = ctp_partition or os.environ["TDAQ_PARTITION"]
        self.ctp_controller = ctp_controller
        self.is_translator = ISCTPTranslator()
        self._ctp_commander: CTPUpdateConditions | None = None

    def read(self) -> model.BeamSpotRepresentation | None:
        # dosctring is inherited from base class
        raise NotImplementedError("CTPTranslator cannot read, it is write only!")

    def write(self, rep: model.BeamSpotRepresentation) -> None:
        # dosctring is inherited from base class
        if len(rep) != 1:
            raise ValueError('You must have exactly one snapshot to update the CTP')
        snap = rep[0]

        allowed_status = {model.STATUS_OK}
        if self._invalidate:
            allowed_status.add(model.STATUS_NOT_CONVERGED)

        if snap.status not in allowed_status:
            _LOG.info("Not updating CTP, status = %s, allowed status = %s", snap.status, allowed_status)
            return

        self._do_update(rep)

    def _do_update(self, rep: model.BeamSpotRepresentation) -> None:

        snap = rep[0]
        _LOG.info("Updating IS with this beam spot:")
        snap.prettyLog(_LOG)

        with ctp_serialize():
            self.is_translator.write(rep)

            _LOG.info("Telling the CTP to update.")
            try:
                self.ctp_commander.update(_COOL_FOLDER_INDEX)
            except ers.Issue as exc:
                # Do not report some errors from CTP, we want better more specific error
                # messages, but for now just restore the situation when CTP error
                # were completely ignored.
                err = str(exc)
                ignore_msg = (
                    "An error occurred during command: set conditions update. Reason: CommandExecutionFailed"
                )
                if ignore_msg in err:
                    _LOG.info("CTP command failed due to possible duplicate request %s", err)
                else:
                    issue = CTPError(err.strip(), exc)
                    ers.error(issue)
                    raise issue

    @property
    def ctp_commander(self) -> CTPUpdateConditions:
        if self._ctp_commander is None:
            self._ctp_commander = CTPUpdateConditions(self.ctp_partition, self.ctp_controller)
        return self._ctp_commander


class DummyCTPTranslator(CTPTranslator):
    '''Debug-only CTP translator safe for dummies.
        this Translator prints to the screen only.
        Good for testing online or offline
    '''

    def _do_update(self, rep: model.BeamSpotRepresentation) -> None:
        _LOG.info("DummyCTPTranslator: Will NOT update CTP, current snapshot:")
        rep[0].prettyLog(_LOG)


class ConservativeCTPTranslator(CTPTranslator):
    ''' CTP translator writes an invalid (status 5) beam spot to the CTP
        Otherwise like CTPTranslator.
    '''

    def write(self, rep: model.BeamSpotRepresentation) -> None:
        # dosctring is inherited from base class

        # make deep copy to avoid modifying the original
        safeRep = copy.deepcopy(rep)
        safeStatus = 5
        for snap in safeRep:
            snap.status = safeStatus

        _LOG.info("Changing your beamspot status flag to %d", safeStatus)
        CTPTranslator.write(self, safeRep)
