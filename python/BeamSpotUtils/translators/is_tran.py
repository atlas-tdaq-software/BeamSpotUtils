'''Library for write only translators to Information Services'''

from __future__ import annotations

import logging
import os
from typing import Any

from .. import model
from . import base
import ers
from ispy import IS

# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


class ISError(ers.Issue):
    """ERS Issue class for IS errors"""

    def __init__(self, message: str, partition: str, is_object: str, cause: Any = None):
        ers.Issue.__init__(self, message,
                           {'partition': partition, 'is_object': is_object},
                           cause if isinstance(cause, ers.Issue) else None)


monitoringISName = 'LiveMon'                    # pylint: disable=invalid-name
monitoringISNameByBunch = 'ByBunchMon'          # pylint: disable=invalid-name
monitoringISServerName = 'BeamSpot'             # pylint: disable=invalid-name

MAX_BUNCHES = 3564


class ISTranslator(base.Translator):
    """Implementation for write-only translator for data in IS.
    """

    def __init__(self, isObject: str = "HLTParameters", isByBunchObject: str = "ByBunchMon",
                 isServerName: str = "BeamSpot", do_bcid: bool = False,
                 isPartition: str | None = None, **kwargs: Any):
        self.isObject = isObject
        self.isByBunchObject = isByBunchObject
        self.isServerName = isServerName
        self.isPartition = isPartition or os.environ['TDAQ_PARTITION']
        self.do_bcid = do_bcid

        self._IS: Any = IS  # Tests can replace it with a suitable mock.

    def read(self) -> model.BeamSpotRepresentation | None:
        # docstring is inherited from base class
        raise NotImplementedError("%s cannot read, it is write only!" % self.__class__.__name__)

    def write(self, rep: model.BeamSpotRepresentation) -> None:
        # docstring is inherited from base class

        if len(rep) < 1:
            _LOG.info("No snapshots to upload!")
            return

        args = ['status',
                'posX',
                'posY',
                'posZ',
                'sigmaX',
                'sigmaY',
                'sigmaZ',
                'tiltX',
                'tiltY',
                'sigmaXY',
                'posXErr',
                'posYErr',
                'posZErr',
                'sigmaXErr',
                'sigmaYErr',
                'sigmaZErr',
                'tiltXErr',
                'tiltYErr',
                'sigmaXYErr']
        # default values to use for bunches that are missing
        default_vals: list[int | float] = [4]
        default_vals += [-999.] * (len(args) - 1)

        if not self.do_bcid:

            # bcid-average case
            try:
                dstInfo = self.isServerName + '.' + self.isObject
                _LOG.info("Trying to write beam spot parameters to IS object \"%s!%s\"",
                          self.isPartition, dstInfo)
                isobj = self._IS.BeamSpotParameters(self.isPartition, dstInfo)
            except Exception as exc:
                ers.error(ISError('Error instantiating IS object',
                                  partition=self.isPartition,
                                  is_object=dstInfo,
                                  cause=exc))
                return

            try:
                snap = rep[0]
                _LOG.info("BeamSpotParameters: %s", snap)
                # fill all data members
                for arg in args:
                    setattr(isobj, arg, getattr(snap, arg))

                # publish
                isobj.checkin()
            except Exception as exc:
                ers.error(ISError('Error publishing IS object',
                                  partition=self.isPartition,
                                  is_object=dstInfo,
                                  cause=exc))
                return

        else:

            # per-bcid case
            try:
                dstInfo = self.isServerName + '.' + self.isByBunchObject
                _LOG.info("Trying to write per-bunch parameters to IS object \"%s\"", dstInfo)
                isobj = self._IS.BeamSpotParametersByBunch(self.isPartition, dstInfo)
            except Exception as exc:
                ers.error(ISError('Error instantiating IS object',
                                  partition=self.isPartition,
                                  is_object=dstInfo,
                                  cause=exc))
                return

            try:
                # index snapshots by bcid to make filling the gaps easier
                repIndex = dict((snap.bcid, snap) for snap in rep)

                isobj['paramsByBunch'].resize(MAX_BUNCHES)
                for bcid in range(0, MAX_BUNCHES):
                    xsnap = repIndex.get(bcid)
                    if xsnap:  # have info for this bunch
                        data = dict((arg, getattr(xsnap, arg)) for arg in args)
                    else:  # use default values
                        data = dict(zip(args, default_vals))
                    isobj['paramsByBunch'][bcid].set(data)

                # publish
                isobj.checkin()
            except Exception as exc:
                ers.error(ISError('Error publishing IS object',
                                  partition=self.isPartition,
                                  is_object=dstInfo,
                                  cause=exc))
                return


class ISMonTranslator(ISTranslator):
    """Translator which writes to monitoring IS server."""

    def __init__(self, **kwargs: Any):
        kwargs = kwargs.copy()
        ISTranslator.__init__(self,
                              monitoringISName,
                              monitoringISNameByBunch,
                              monitoringISServerName,
                              **kwargs)


class ISCTPTranslator(ISTranslator):
    """Use this one to write HLTParameters.
    """

    def __init__(self, **kwargs: Any):
        kwargs = kwargs.copy()
        kwargs.update(do_bcid=False)
        ISTranslator.__init__(self, **kwargs)


class ISTagTranslator(ISTranslator):
    """Use this one to write IS object with name taken from a tag."""

    def __init__(self, tag: str, **kwargs: Any):
        kwargs = kwargs.copy()
        # tag can be [partition!][server.]object
        part: str | None
        part, sep, obj = tag.partition('!')
        if not obj:
            obj, part = part, None
        srv, sep, obj = obj.partition('.')
        if not obj:
            obj, srv = srv, "BeamSpot"
        kwargs.update(isObject=obj)
        kwargs.update(isServerName=srv)
        kwargs.update(isPartition=part)
        ISTranslator.__init__(self, **kwargs)
