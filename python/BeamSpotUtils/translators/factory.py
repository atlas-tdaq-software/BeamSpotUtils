'''Library: Contains the factory method to produce a translator for a string desc.
This module holds a few static methods related to producing instances
of one of the Translator subclasses.

This is where all the mini-language decoding goes on between for
repInput and repOutput.
'''

from __future__ import annotations

import logging
from collections.abc import Callable
from typing import Any

from . import base
from .cool_tran import (HLTTranslator, BunchTranslator, MonitoringTranslator,
                        OfflineCOOLTranslator, HLTUpdTranslator)
from .ctp_tran import (CTPTranslator, DummyCTPTranslator, ConservativeCTPTranslator)
from .file_tran import (FileTranslator, MassiTranslator, SQLiteTranslator,
                        PickleTranslator, CSVTranslator, StdOutTranslator)
from .is_tran import (ISMonTranslator, ISCTPTranslator, ISTagTranslator)
from .multi_tran import MultiTranslator


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


_USER_INPUT_MAP: dict[str, Callable] = {
    'off': OfflineCOOLTranslator,
    'onhlt': HLTTranslator,
    'onhlt_update': HLTUpdTranslator,
    'onbunch': BunchTranslator,
    'onmon': MonitoringTranslator,
    'ismon': ISMonTranslator,
    'isctp': ISCTPTranslator,
    'is': ISTagTranslator,
    'ctp': CTPTranslator,
    'dummyctp': DummyCTPTranslator,
    'conctp': ConservativeCTPTranslator,
    'stdout': StdOutTranslator,
    'massi': MassiTranslator,
    'sqlite': SQLiteTranslator,
    'file': FileTranslator,
    'csv': CSVTranslator,
    'pickle': PickleTranslator
}

# keys that require a tag
_REQUIRE_TAG = set(["sqlite", "file", "csv", "pickle", "is"])

userIODesc = (
    'A description of a beamspot representation is a short string specifying where to find'
    ' or store beamspot data.  Possible values are a file (csv, sql, pickle), or these strings <%s>.'
    ' Some strings allow a :tag to be appended and overwrite the default tag.'
) % (', '.join(_USER_INPUT_MAP.keys()))

_allowed_keywords = {"do_bcid", "run", "lbn", "tag"}


def make_translator(desc: str, **kwargs: Any) -> base.Translator:
    """Return translator instance for given description

    This method accepts arbitrary number of keyword arguments,
    all of those are passed to translator constructor without
    change, it is the responsibility of class constructor to
    extract necessary keyword arguments or complain if any
    argument is missing. Typical keywords that can be used here
    are 'do_bcid', 'run', 'lbn'.

    Constructor of a Translator subclass should be implemented
    like this:

        class MyTran(Translator):
            def __init__(self, doBCID=None, **kwargs):
                # **kwargs consumes all extra kw arguments not relevant to this class
                self.doBCID = doBCI

    Parameters
    ----------
    desc : str
        Comma-separated list of items, item can be either file name (path),
        or one of the predefined keys, key can include colon and tag.
    kwargs : keyword arguments

    Returns
    -------
    Instance of translator.

    Raises
    ------
    `TypeError` if `desc` is not recognized or empty. Other exceptions can
    potentially be thrown by individual translator constructors.
    """
    _LOG.debug('make_translator: desc=%s kwargs=%s', desc, kwargs)
    if not isinstance(desc, (type(""), type(u""))):
        _LOG.error("Unexpected description type: %s", type(desc))
        raise TypeError("Unexpected description type")

    for kw in kwargs:
        if kw not in _allowed_keywords:
            raise TypeError(f"make_translator(): got an unexpected keyword argument '{kw}'")

    tran_list = []
    for oneTrans in desc.split(','):
        kws = kwargs.copy()
        name, colon, tag = oneTrans.partition(':')
        key = name.strip()

        # assume it to be a file name if it contains dot
        if '.' in key:
            _LOG.debug('%s looks like a file name', key)
            # use file name as tag, tag value is ignored, and avoid collision
            # if someone passes tag as a keyword argument
            kws.pop('tag', None)
            thisTrans = FileTranslator(tag=key, **kws)
            _LOG.debug('Received %s as a FileTranslator', thisTrans)
            tran_list.append(thisTrans)
        else:
            _LOG.debug('%s is not a file, trying all standard keys', key)

            key = key.lower()
            tclass = _USER_INPUT_MAP.get(key)
            if tclass:
                if key in _REQUIRE_TAG and not colon:
                    msg = 'Key "{}" requires a tag (key:tag, tag is typically a file name)'.format(key)
                    _LOG.error(msg)
                    raise TypeError(msg)
                else:
                    if colon != '':
                        # pass tag as a kw argument to constructor, override
                        # explicit tag passes as keyword if any
                        kws.update(tag=tag.strip())
                    thisTrans = tclass(**kws)
                    _LOG.debug('Received %s translator', thisTrans)
                    tran_list.append(thisTrans)
            else:
                msg = 'Unrecognized description: "{}"'.format(desc)
                _LOG.error("%s", msg)
                raise TypeError(msg)

    if len(tran_list) == 0:
        msg = 'Empty description: "{}"'.format(desc)
        _LOG.error("%s", msg)
        raise TypeError(msg)
    elif len(tran_list) == 1:
        return tran_list[0]
    else:
        return MultiTranslator(tran_list)
