'''Definition of Translator interface'''

from __future__ import annotations

from abc import ABCMeta, abstractmethod

from .. import model


class Translator(metaclass=ABCMeta):
    """Abstract base class for all translators.
    """

    @abstractmethod
    def write(self, rep: model.BeamSpotRepresentation) -> None:
        """Translate representation to external format.

        Parameters
        ----------
        rep : BeamSpotRepresentation
            In-memory beam spot representation.
        """
        pass

    @abstractmethod
    def read(self) -> model.BeamSpotRepresentation | None:
        """Translate external format to in-memory representation.

        Returns
        -------
        Instance of BeamSpotRepresentation class, can return None
        on failure.

        Raises
        ------
        Exception can be raised for some critical failure modes.
        """
        pass
