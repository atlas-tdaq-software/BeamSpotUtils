'''Library of fly-weight subclasses for COOL DB translation'''

from __future__ import annotations

from typing import Any

from .db_tran import DBTranslator, ConnParams


HLT_CONN = ConnParams(connStr='COOLONL_INDET/CONDBR2',
                      folder='/Indet/Onl/Beampos',
                      tag='IndetBeamposOnl-HLT-UPD1-001-00')

MON_CONN = ConnParams(connStr='COOLONL_INDET/CONDBR2',
                      folder='/Indet/Onl/Beampos',
                      tag='IndetBeamposOnl-LiveMon-001-00')

BUNCH_CONN = ConnParams(connStr='COOLONL_INDET/CONDBR2',
                        folder='/Indet/Onl/BeamposPerBunch',
                        tag='IndetBeamposOnlPerBunch-LiveMon-001-00')

OFFLINE_CONN = ConnParams(connStr='COOLOFL_INDET/CONDBR2',
                          folder='/Indet/Beampos',
                          tag='IndetBeampos-ES1-UPD2')


def HLTTranslator(**kwargs: Any) -> DBTranslator:
    """Make read-only translator instance for HLT COOL conditions.
    """
    return DBTranslator(HLT_CONN,
                        readOnly=True,
                        create=False,
                        openIOV=True,
                        zeroBCID=-1,
                        **kwargs)


def HLTUpdTranslator(**kwargs: Any) -> DBTranslator:
    """Make update-capable translator instance for HLT COOL conditions.

    Not for regular operations (updates should normally go through CTP).
    """
    return DBTranslator(HLT_CONN,
                        readOnly=False,
                        create=True,
                        openIOV=True,
                        zeroBCID=-1,
                        **kwargs)


def BunchTranslator(**kwargs: Any) -> DBTranslator:
    """Make read-write translator instance for per-bunch COOL conditions.
    """
    return DBTranslator(BUNCH_CONN,
                        readOnly=False,
                        create=False,
                        openIOV=False,
                        allowedBCIDs=set(range(0, 3564)),
                        **kwargs)


def MonitoringTranslator(**kwargs: Any) -> DBTranslator:
    """Make read-write translator instance for monitoring COOL conditions.
    """
    return DBTranslator(MON_CONN,
                        readOnly=False,
                        create=False,
                        openIOV=False,
                        zeroBCID=-1,
                        **kwargs)


def OfflineCOOLTranslator(readOnly: bool = True, create: bool = False, **kwargs: Any) -> DBTranslator:
    """Make read-only translator instance for off-line COOL conditions.
    """
    return DBTranslator(OFFLINE_CONN,
                        readOnly=readOnly,
                        create=create,
                        openIOV=False,
                        zeroBCID=-1,
                        **kwargs)
