'''Library for read/write translators to local files'''

from __future__ import annotations

import csv
import logging
import os
import pickle
from collections.abc import Iterable, Mapping
from typing import Any

from .. import bscool, model
from . import base, cool_tran
from .db_tran import DBTranslator, ConnParams


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


def FileTranslator(tag: str, **kwargs: Any) -> base.Translator:  # pylint: disable=invalid-name
    """Factory method for translators which reads or writes to a file.

    Parameters
    ----------
    tag : str
        File name, if file has extension `.db` or `.sql` it is assumed to be
        sqlite file, if file extension is `.csv` it is CSV file, otherwise
        Python pickle format is used.
    kwargs
        keyword arguments
    """
    ext = tag.rpartition('.')[-1]
    trans: base.Translator
    if ext in ('db', 'sql', 'sqlite', 'sqlite3'):
        trans = SQLiteTranslator(tag=tag, **kwargs)
    elif ext == 'csv':
        trans = CSVTranslator(tag=tag, **kwargs)
    else:
        trans = PickleTranslator(tag=tag, **kwargs)
    return trans


class PickleTranslator(base.Translator):
    """Translator for Pickle format.

    Parameters
    ----------
    tag : str
        File location (path).
    kwargs : keyword arguments
    """
    def __init__(self, tag: str, **kwargs: Any):
        self.loc = tag

    def write(self, rep: model.BeamSpotRepresentation) -> None:
        # docstring is inherited from base class
        with open(self.loc, 'wb') as fpickle:
            pickle.dump(rep, fpickle)

    def read(self) -> model.BeamSpotRepresentation | None:
        # docstring is inherited from base class
        with open(self.loc, 'rb') as fpickle:
            out = pickle.load(fpickle)
        return out


class CSVTranslator(base.Translator):
    """Translator for CSV format.

    by default this translator writes all attributes named in
    `model.requiredFields`, this can be changed by passing a lit of attributes
    to `setFields` method.

    Parameters
    ----------
    tag : str
        File location (path).
    mode : str
        'w' for write/overwrite, 'a' for append.
    """
    def __init__(self, tag: str, mode: str = 'w', **kwargs: Any):
        self.loc = tag
        self.mode = mode
        self.fields: list[str] | None = None

    def setFields(self, li: Iterable[str]) -> None:
        """Define which fields are written to CSV file.

        Parameters
        ----------
        li : iterable of `str`
            Names of attributes of BSSnapshot.
        """
        self.fields = list(li)

    def write(self, rep: model.BeamSpotRepresentation) -> None:
        # docstring is inherited from base class
        _LOG.debug("Saving representation to %s with %d entries",
                   self.loc, len(rep))

        if self.fields is None:
            fields = list(model.requiredFields)
        else:
            fields = self.fields

        records = [snap.as_dict(fields) for snap in rep]
        self._to_file(fields, records)

    def appendRecords(self, records: list[Mapping[str, Any]]) -> None:
        """Appends more data to output file.

        Parameters
        ----------
        records : list of dict
            List of dictionaries, each dictionary has column names as keys and
            their corresponding values.
        """
        _LOG.debug("Appending %d records to %s", len(records), self.loc)

        if self.fields is None:
            fields = list(model.requiredFields)
        else:
            fields = self.fields

        self._to_file(fields, records)

    def _to_file(self, fields: list[str], records: list[Mapping[str, Any]]) -> None:
        """Write representation to output CSV file.

        Parameters
        ----------
        fields : list of str
            Names of columns.
        records : list of dict
            List of dictionaries, each dictionary has column names as keys and
            their corresponding values.
        """
        with open(self.loc, self.mode) as csvFile:
            writer = csv.DictWriter(csvFile, fields, extrasaction='ignore')

            # write header
            if csvFile.tell() == 0:
                writer.writerow(dict(zip(fields, fields)))

            for row in records:
                writer.writerow(row)

    def read(self) -> model.BeamSpotRepresentation | None:
        # docstring is inherited from base class
        with open(self.loc, 'r') as csvFile:
            firstLine = csvFile.readline()
            csvFile.seek(0)

            colNames = [e.strip() for e in firstLine.split(',')]
            _LOG.debug('%s has these column names: %s', self.loc, str(colNames))

            validHeader = set(colNames) >= set(model.requiredFields)
            if not validHeader:
                _LOG.error('I dont understand the csv header.')
                _LOG.error('It was missing these required fields: %s',
                           set(model.requiredFields) - set(colNames))
                return None

            reader = csv.DictReader(csvFile)

            rep = model.BeamSpotRepresentation()
            for row in reader:
                frow = dict((key, float(val)) for key, val in row.items())
                snap = model.BSSnapshot(frow)
                rep.append(snap)

        return rep


class MassiTranslator(base.Translator):
    """Special write only translator for "Massi" CSV format.

    Parameters
    ----------
    fill : int, optional
        LHC fill number
    folder : str, optional
        folder to store the file
    """
    # time stab x dx y dy  z dz xsu dxsu ysu dysu zsu dzsu ax dax ay day
    #      time = as above UTC
    #      stab = as above fraction in stable beams
    #      x, y, z = x-mean, y-mean, z-mean
    #      dx, dy, dz = "one sigma" errors on x-mean, y-mean, z-mean
    #      xsu, ysu, zsu = x-sigma, y-sigma, z-sigma from a raw 1-gaussian
    #          fit, after unfolding the vtx detector resolution
    #      dxsu, dysu, dzsu = "one sigma" errors on xsu, ysu, zsu
    #      ax, ay = angle in x and y when projecting the lumi region
    #                on the xz and yz planes
    #      dax, day = "one sigma" errors on ax, ay
    fields = [
        'utc', 'stab', 'posX', 'posXErr', 'posY', 'posYErr', 'posZ', 'posZErr',
        'sigmaX', 'sigmaXErr', 'sigmaY', 'sigmaYErr', 'sigmaZ', 'sigmaZErr',
        'tiltX', 'tiltXErr', 'tiltY', 'tiltYErr'
    ]

    runTemplate = 'r%010d_lumireg_%d_ATLAS.txt'
    fillTemplate = '%d_lumireg_%d_ATLAS.txt'

    def __init__(self, folder: str | None = None, fill: int | None = None, **kwargs: Any):
        self.folder = folder
        self.fill = fill

    def write(self, rep: model.BeamSpotRepresentation) -> None:
        # docstring is inherited from base class
        bcidSplitReps = rep.groupBy('bcid')
        for _, bcidRep in bcidSplitReps.items():
            doubleSplitGroup = bcidRep.groupBy('run')
            for run, doubleSplitRep in doubleSplitGroup.items():

                machineRep = [self.machineCoordinates(snap) for snap in doubleSplitRep]
                rfb = machineRep[0]['rfBucket']
                if self.fill is None:
                    fName = MassiTranslator.runTemplate % (run, rfb)
                else:
                    fName = MassiTranslator.fillTemplate % (self.fill, rfb)

                if self.folder is not None:
                    fName = os.path.join(self.folder, fName)

                trans = CSVTranslator(fName, mode='a')
                trans.setFields(MassiTranslator.fields)
                trans.appendRecords(machineRep)

    def read(self) -> model.BeamSpotRepresentation | None:
        # docstring is inherited from base class
        _LOG.error("No thanks.  I'd rather not read")
        return None

    def machineCoordinates(self, snapshot: model.BSSnapshot) -> Mapping[str, Any]:
        """Convert snapshot into "LHC coordinates"

        Returns
        -------
        data : dict
            Dictionary with column names and values.
        """
        snap = snapshot.as_dict(self.fields)

        inversions = set(['posX', 'posZ', 'tiltY', 'deltaX', 'deltaZ'])
        for field in inversions & set(snap.keys()):
            snap[field] *= -1  # type:ignore

        # FALL BACK IF actual time fails
        #snap['utc']  = snap['run']*10000+snap['lbn']
        try:
            snap['utc'] = bscool.runLBTimes(snap['run'], snap['lbn'])[0] / 1e9  # type:ignore
        except Exception as exc:
            _LOG.warning("Failed to retrieve LBN timestamps: %s", exc)
            snap['utc'] = -1  # type:ignore

        # print '%(run)d %(lbn)d' % snap
        # print snap['utc']
        snap['stab'] = 1      # type:ignore
        snap['rfBucket'] = (10 * snap['bcid'] + 35631) % 35640  # type:ignore
        if snap['bcid'] == -1:  # type:ignore
            snap['rfBucket'] = -1  # type:ignore

        return snap


class SQLiteTranslator(DBTranslator):
    """Database translator for sqlite files.

    Parameters
    ----------
    tag : str
        File location (path).
    """

    def __init__(self, tag: str, **kwargs: Any):
        dbString = self._makeConnString(tag or "beamspot.db")
        conn = ConnParams(connStr=dbString, folder=cool_tran.HLT_CONN.folder,
                          tag=cool_tran.HLT_CONN.tag)
        DBTranslator.__init__(self,
                              conn,
                              readOnly=False,
                              create=True,
                              openIOV=True,
                              allowedBCIDs=range(-1, 3564),
                              zeroBCID=-1,
                              **kwargs)

    @staticmethod
    def _makeConnString(loc: str | None, rep: model.BeamSpotRepresentation | None = None) -> str:
        """Make connection string for sqlite file
        """
        if loc is None or loc.strip() == '':
            run = -1
            if rep is not None:
                snap = rep.last()
                if snap is not None:
                    run = snap.run
            loc = os.path.join('', 'mysqlitefile_%d.db' % run)
            #loc = os.path.join(outputDir, 'mysqlitefile_%d.db' % run)

        if 'schema' not in loc:
            loc = 'sqlite://;schema=%s;dbname=CONDBR2' % loc

        return loc


class StdOutTranslator(base.Translator):
    """Write-only translator with pretty printing to stdout
    """

    def __init__(self, **kwargs: Any):
        pass

    def write(self, rep: model.BeamSpotRepresentation) -> None:
        # docstring is inherited from base class
        for snapshot in rep:
            print('=' * 40)
            print(snapshot.prettyString())

    def read(self) -> model.BeamSpotRepresentation | None:
        # docstring is inherited from base class
        return None
