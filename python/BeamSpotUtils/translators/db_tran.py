'''Library for translating between internal and DB (file or network)'''

from __future__ import annotations

import logging
from collections import namedtuple
from collections.abc import Iterable
from typing import Any

from coldpie import cool

from .. import bscool, model
from . import base
import ers


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


class DBError(ers.Issue):
    """ERS Issue class for database errors"""

    def __init__(self, message: str, cause: Any = None, **kw: Any):
        ers.Issue.__init__(self, message, kw,
                           cause if isinstance(cause, ers.Issue) else None)


# Connection parameters: connection string, folder name, and tag name
ConnParams = namedtuple("ConnParams", "connStr folder tag")


class DBTranslator(base.Translator):
    """Parent class for database beamspot representation translators.

    This class handles all COOL database-related things.

    Parameters
    ----------
    connParams : ConnParams
        Object encapsulating database/folder/tag.
    readOnly : bool
        If true then database cannot be updated
    create : bool
        If true then database will be created if not exists.
    openIOV : bool
        If true then all stored intervals are open
    allowedBCIDs : iterable
        Set of BCIDs that can be stored.
    zeroBCID : int, optional
        If not zero then its value replaces BCID=0 read from database,
        typically used to when reading from BCID-average folders (in-memory
        representation should have bcid=-1 for those).
    run : int, optional
        Use this run number for read()
    lbn : int, optional
        Use this run number for read()
    kwargs : keyword arguments
        To consume extra keyword arguments that may be passed by factory.
    """

    def __init__(self, connParams: ConnParams, readOnly: bool, create: bool, openIOV: bool,
                 allowedBCIDs: Iterable[int] | None = None, zeroBCID: int = 0,
                 run: int | None = None, lbn: int | None = None, **kwargs: Any):

        self.connParams = connParams
        self.readOnly = readOnly
        self.create = create
        self.openIOV = openIOV
        self.allowedBCIDs = set(allowedBCIDs) if allowedBCIDs is not None else set([-1])
        self.zeroBCID = zeroBCID
        self.run = run
        self.lbn = lbn

        fields = []
        for k in model.coolPayloadFields:
            ftype = cool.StorageType.Float
            if k == 'status':
                ftype = cool.StorageType.Int32
            fields += [cool.FieldSpecification(k, ftype)]
        self.spec = cool.RecordSpecification(fields)

    def read(self) -> model.BeamSpotRepresentation | None:
        # dosctring is inherited from base class
        _LOG.info("Attempting to read from COOL database: %s, folder: %s, tag: %s",
                  self.connParams.connStr, self.connParams.folder, self.connParams.tag)
        try:
            coolDb = bscool.getDB(self.connParams.connStr, self.readOnly, self.create)
        except Exception as exc:
            ers.error(DBError("Failed to open database connection: {}".format(exc),
                              connection=self.connParams.connStr))
            return None

        try:
            assert self.run is not None
            return bscool.readDB(coolDb, self.connParams.folder, self.connParams.tag,
                                 self.run, self.lbn, zeroBCID=self.zeroBCID)
        except Exception as exc:
            ers.error(DBError("Failed to read from COOL folder: {}".format(exc),
                              connection=self.connParams.connStr,
                              folder=self.connParams.folder,
                              tag=self.connParams.tag,
                              run=self.run,
                              LBN=self.lbn))
            return None

    def write(self, rep: model.BeamSpotRepresentation) -> Any:
        # dosctring is inherited from base class
        if self.readOnly:
            _LOG.error('You cannot write through this translator')
            raise RuntimeError('This translator is read-only')

        try:
            coolDb = bscool.getDB(self.connParams.connStr, self.readOnly, self.create)
        except Exception as exc:
            ers.error(DBError("Failed to open database connection: {}".format(exc),
                              connection=self.connParams.connStr))
            raise

        try:
            cFolder = bscool.openOrMakeFolder(coolDb, self.connParams.folder, self.spec)
        except Exception as exc:
            ers.error(DBError("Failed to open/create COOL folder: {}".format(exc),
                              connection=self.connParams.connStr,
                              folder=self.connParams.folder))
            raise

        _LOG.info("Attempting to write to COOL database: %s, folder: %s, tag: %s",
                  self.connParams.connStr, self.connParams.folder, self.connParams.tag)

        rep = model.BeamSpotRepresentation([snap for snap in rep
                                            if snap.bcid in self.allowedBCIDs])
        _LOG.info('After BCID cut there are %d rows left to write', len(rep))
        if len(rep) == 0:
            _LOG.info('Too few rows to write!')
            return

        bscool.writeToFolder(cFolder, self.connParams.tag, rep, self.spec, self.openIOV)
