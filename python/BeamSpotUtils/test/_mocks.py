#!/usr/bin/env tdaq_python

"""Bunch of mock classes and ways to generate mock data.
"""

from __future__ import annotations

import copy
import logging
import random
from collections.abc import Iterable, Iterator
from typing import Any

from BeamSpotUtils import htree
from BeamSpotUtils import hist_def


class tobject(object):
    """Mock for ROOT TObject"""

    def __init__(self, name: str, n_entries: int = 1000):
        self._name = name
        self._n_entries = n_entries

    def GetName(self) -> str:
        return self._name

    def GetCycle(self) -> int:
        return 1

    def GetEntries(self) -> int:
        return self._n_entries

    def Clone(self) -> tobject:
        return copy.copy(self)

    def Delete(self) -> None:
        pass

    def Add(self, other: tobject) -> None:
        self._n_entries += other._n_entries

    def SetName(self, name: str) -> None:
        self._name = name

    def SetDirectory(self, directory: Any) -> None:
        pass


class tkey(tobject):
    """Mock for ROOT TKey"""

    def __init__(self, name: str, is_folder: bool = False):
        tobject.__init__(self, name)
        self._is_folder = is_folder

    def IsFolder(self) -> bool:
        return self._is_folder


class tdirectory(tobject):
    """Mock for TDirectory/TFile.

    Only implements few methods that are needed by BST.
    """

    def __init__(self, name: str, children: Iterable[tobject] | None = None):
        tobject.__init__(self, name)
        self._children = dict((child.GetName(), child) for child in children or [])

    def add(self, tobj: tobject) -> None:
        self._children[tobj.GetName()] = tobj

    def GetListOfKeys(self) -> list[tkey]:
        return [tkey(name, isinstance(child, tdirectory)) for name, child in self._children.items()]

    def Get(self, path: str) -> tobject | None:
        path = path.strip('/')
        tobj: tobject | None = self
        for name in path.split('/'):
            if isinstance(tobj, tdirectory):
                tobj = tobj._children.get(name)
            else:
                return None
        return tobj

    def GetDirectory(self, path: str) -> tdirectory | None:
        tobj = self.Get(path)
        if isinstance(tobj, tdirectory):
            return tobj
        return None

    def mkdir(self, path: str) -> tdirectory:
        path = path.strip('/')
        tdir = self
        for name in path.split('/'):
            child = tdir._children.get(name)
            if child is None:
                tdir1 = tdirectory(name)
                tdir.add(tdir1)
                tdir = tdir1
            elif isinstance(child, tdirectory):
                tdir = child
            else:
                raise LookupError("child exists and it's not a directory")
        return tdir

    def addpaths(self, paths: Iterable[str]) -> None:
        """Add whole bunch of folders and objects to a tree.

        Parameters
        ----------
        paths : `list` [`str`]
            List of paths, path with trailing slash is a directory, otherwise
            it's an object.
        """
        logging.getLogger("htree_test").debug("adding tree:\n  %s", "\n  ".join(sorted(paths)))
        for path in paths:
            if path[-1] == "/":
                self.mkdir(path[:-1])
            else:
                dirname, _, objname = path.rpartition("/")
                tdir = self.mkdir(dirname) if dirname else self
                tdir.add(tobject(objname))


# Histogram names for variant A, from MDA file for run 185277.
# This list is for per-LBN histos
_vtx_names_lbn_A = [
    "BCID_vs_NvtxPass",
    "BCID_vs_TotalTracksPass",
    "BCID_vs_VertexXPass",
    "BCID_vs_VertexYPass",
    "BCID_vs_VertexZPass",
    "EventStatistics",
    "NvtxPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDXPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDYPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDZPass",
    "TrackPhiPass_vs_TrackD0Pass",
    "VertexNTrksPass",
    "VertexNTrksPass_vs_VertexXPass",
    "VertexNTrksPass_vs_VertexYPass",
    "VertexNTrksPass_vs_VertexZPass",
    "VertexXPass",
    "VertexXPass_vs_VertexYPass",
    "VertexXZoomPass",
    "VertexXZoomPass_vs_VertexYZoomPass",
    "VertexYPass",
    "VertexYZoomPass",
    "VertexZPass",
    "VertexZPass_vs_VertexXPass",
    "VertexZPass_vs_VertexYPass",
    "VertexZZoomPass",
    "VertexZZoomPass_vs_VertexXZoomPass",
    "VertexZZoomPass_vs_VertexYZoomPass",
]

# this list is for non-LBN histos
_vtx_names_A = [
    "SplitVertex1NTrksPass_vs_SplitVertex1XPass",
    "SplitVertex1NTrksPass_vs_SplitVertex1YPass",
    "SplitVertex1NTrksPass_vs_SplitVertex1ZPass",
    "SplitVertex2NTrksPass_vs_SplitVertex2XPass",
    "SplitVertex2NTrksPass_vs_SplitVertex2YPass",
    "SplitVertex2NTrksPass_vs_SplitVertex2ZPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDXpullPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDYpullPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDZpullPass",
    "TotalTracks",
    "TotalTracksPass",
    "TrackZ0Pass",
    "VertexNTrksPass_vs_VertexXerrPass",
    "VertexNTrksPass_vs_VertexYerrPass",
    "VertexNTrksPass_vs_VertexZerrPass",
    "VertexZPass_vs_VertexNTrksPass",
    "VertexZPass_vs_VertexXerrPass",
    "VertexZPass_vs_VertexYerrPass",
]

# histogram names for variant B, from MDA file for run 329484
_vtx_names_lbn_B = [
    "BCID_vs_NvtxPassBCID",
    "BCID_vs_TotalTracksPass",
    "BCID_vs_VertexXPassBCID",
    "BCID_vs_VertexXZoomPassBCID",
    "BCID_vs_VertexYPassBCID",
    "BCID_vs_VertexYZoomPassBCID",
    "BCID_vs_VertexZPassBCID",
    "BCID_vs_VertexZZoomPassBCID",
    "EventStatistics",
    "NvtxPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDXPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDYPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDZPass",
    "TrackPhiPass_vs_TrackD0Pass",
    "VertexNTrksPass",
    "VertexNTrksPass_vs_VertexNTrksInVtxPass",
    "VertexNTrksPass_vs_VertexXPass",
    "VertexNTrksPass_vs_VertexYPass",
    "VertexNTrksPass_vs_VertexZPass",
    "VertexXPass",
    "VertexXPass_vs_VertexYPass",
    "VertexXZoomPass",
    "VertexXZoomPass_vs_VertexYZoomPass",
    "VertexYPass",
    "VertexYZoomPass",
    "VertexZPass",
    "VertexZPass_vs_VertexXPass",
    "VertexZPass_vs_VertexYPass",
    "VertexZZoomPass",
    "VertexZZoomPass_vs_VertexXZoomPass",
    "VertexZZoomPass_vs_VertexYZoomPass",
]

# non-LBN histos
_vtx_names_B = [
    "ClusterDeltaVertexZ",
    "ClusterDeltaZ0",
    "ClusterNTracks",
    "ClusterNTracks_vs_ClusterNTracksUnused",
    "ClusterZ",
    "ClusterZ0Pull",
    "NClusters",
    "SplitVertex1NTrksPass_vs_SplitVertex1XPass",
    "SplitVertex1NTrksPass_vs_SplitVertex1YPass",
    "SplitVertex1NTrksPass_vs_SplitVertex1ZPass",
    "SplitVertex2NTrksPass_vs_SplitVertex1NTrksPass",
    "SplitVertex2NTrksPass_vs_SplitVertex2XPass",
    "SplitVertex2NTrksPass_vs_SplitVertex2YPass",
    "SplitVertex2NTrksPass_vs_SplitVertex2ZPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDXpullPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDYpullPass",
    "SplitVertexDNTrksPass_vs_SplitVertexDZpullPass",
    "TotalTracks",
    "TotalTracksPass",
    "TrackZ0Pass",
    "VertexNTrksPass_vs_VertexXerrPass",
    "VertexNTrksPass_vs_VertexYerrPass",
    "VertexNTrksPass_vs_VertexZerrPass",
    "VertexSumPt2Pass",
    "VertexSumPtPass",
    "VertexZPass_vs_VertexNTrksPass",
    "VertexZPass_vs_VertexXerrPass",
    "VertexZPass_vs_VertexYerrPass",
]

# histogram names for variant R3, from offline file on 2021-03-03
_vtx_names_lbn_R3 = [
    "NvtxPassBCID_vs_BCID",
    "NvtxPass",
    "SplitVertexDXPass_vs_SplitVertexDNTrksPass",
    "SplitVertexDYPass_vs_SplitVertexDNTrksPass",
    "SplitVertexDZPass_vs_SplitVertexDNTrksPass",
    "VertexXPassBCID_vs_BCID",
    "VertexXPass",
    "VertexXPass_vs_VertexNTrksPass",
    "VertexXPass_vs_VertexZPass",
    "VertexXZoomPassBCID_vs_BCID",
    "VertexXZoomPass",
    "VertexXZoomPass_vs_VertexZZoomPass",
    "VertexYPassBCID_vs_BCID",
    "VertexYPass",
    "VertexYPass_vs_VertexNTrksPass",
    "VertexYPass_vs_VertexXPass",
    "VertexYPass_vs_VertexZPass",
    "VertexYZoomPassBCID_vs_BCID",
    "VertexYZoomPass",
    "VertexYZoomPass_vs_VertexXZoomPass",
    "VertexYZoomPass_vs_VertexZZoomPass",
    "VertexZPassBCID_vs_BCID",
    "VertexZPass",
    "VertexZPass_vs_VertexNTrksPass",
    "VertexZZoomPassBCID_vs_BCID",
    "VertexZZoomPass",
]

# non-LBN histos
_vtx_names_R3 = [
    "ClusterClusterDeltaZ0",
    "ClusterDeltaVertexZ",
    "ClusterDeltaZ0",
    "ClusterNTracks",
    "ClusterNTracksUnused",
    "ClusterNTracksUnused_vs_ClusterNTracks",
    "ClusterZ",
    "ClusterZ0Pull",
    "NClusters",
    "Nvtx",
    "SelectedHiPTTracksPerROI",
    "SelectedTracksPerROI",
    "SplitVertex1NTrksPass_vs_SplitVertex2NTrksPass",
    "SplitVertexDXpullPass_vs_SplitVertexDNTrksPass",
    "SplitVertexDYpullPass_vs_SplitVertexDNTrksPass",
    "SplitVertexDZpullPass_vs_SplitVertexDNTrksPass",
    "TracksPerROI",
    "VertexNTrksPass_vs_VertexZPass",
    "VertexXerrPass_vs_VertexNTrksPass",
    "VertexXerrPass_vs_VertexXZoomPass",
    "VertexXerrPass_vs_VertexZPass",
    "VertexYerrPass_vs_VertexNTrksPass",
    "VertexYerrPass_vs_VertexYZoomPass",
    "VertexYerrPass_vs_VertexZPass",
    "VertexZerrPass_vs_VertexNTrksPass",
]

# histogram names for track algo for variant R3, variants A and B
# did not have track method.
_trk_names_lbn_R3 = [
    "BeamLSMatricesBCID",
    "BeamLSMatrices",
    "TrackLLPolyCoeff",
]


def _all_vtx_names(variant: htree.HTreeVariant) -> list[str]:
    """Return all names of non-LBN vertex histogram BST expects to find.
    """
    if variant == htree.HTreeVariant.A:
        return _vtx_names_A
    if variant == htree.HTreeVariant.B:
        return _vtx_names_B
    if variant == htree.HTreeVariant.R3:
        return _vtx_names_R3
    return []


def _all_vtx_names_lbn(variant: htree.HTreeVariant) -> list[str]:
    """Return all names of non-LBN vertex histogram BST expects to find.
    """
    if variant == htree.HTreeVariant.A:
        return _vtx_names_lbn_A
    if variant == htree.HTreeVariant.B:
        return _vtx_names_lbn_B
    if variant == htree.HTreeVariant.R3:
        return _vtx_names_lbn_R3
    return []


def _all_trk_names(variant: htree.HTreeVariant) -> list[str]:
    """Return all names of track histogram BST expects to find.
    """
    return []


def _all_trk_names_lbn(variant: htree.HTreeVariant) -> list[str]:
    """Return all names of track histogram BST expects to find.
    """
    return _trk_names_lbn_R3


def make_empty_tree() -> tdirectory:
    """Make empty directory terr (empty ROOT file).
    """
    root = tdirectory("")
    return root


def _make_template_names(templates: Iterable[str], names: Iterable[str], *,
                         lbs: Iterable[int] | None = None, **kwds: Any) -> Iterator[str]:
    """Generate names from templates"""
    if lbs:
        for lbn in lbs:
            for template in templates:
                for name in names:
                    yield template.format(lbn=lbn, name=name, **kwds)
    else:
        for template in templates:
            for name in names:
                yield template.format(name=name, **kwds)


def make_mda_tree(*, server: str, provider: str, detail: str, chain: str, run: int,
                  lbs: Iterable[int] = (10, 123, 987), meta: bool = False,
                  variant: htree.HTreeVariant = htree.HTreeVariant.B) -> tdirectory:
    """Make directory tree which looks like MDA file.

    Parameters
    ----------
    server : `str`
        Name of the OH server.
    provider : `str`
        Provider name.
    detail : `str`
        Detail level, like DEBUG or EXPERT.
    chain : `str`
        HLT chain name (actually algorithm name).
    run : `int`
        Run number.
    lbs : `list` [`int`]
        List of LB numbers.
    meta : `bool`, optional
        If True then generate "/.meta/" folder too.

    Returns
    -------
    tree : tdirectory
        Mock directory/file instance
    """
    root = tdirectory("")

    paths = set()
    if meta:
        paths.add(".meta/data")

    # all names that we need to generate
    templates_lbn = [
        # this is the last version of per-LB histogram that MDA saves
        "{server}/{provider}{lb_folder}/{detail}/{chain}/{name}",
        # regular per-LB histograms
        "run_{run}/lb_{lbn}/{server}/{provider}{lb_folder}/{detail}/{chain}/{name}",
        # some empty folders to see if they confuse things
        "run_{run}/lb_{lbn}/{server}/{provider}{lb_folder}/{detail}/{chain}_empty/",
    ]
    # _runsummary only exist for run1/2
    if variant in (htree.HTreeVariant.A, htree.HTreeVariant.B):
        templates_lbn += [
            # runsummary histograms
            "{server}/{provider}/{detail}/{chain}/{name}_runsummary",
        ]
    templates_nonlbn = [
        # non-lbn histograms
        "{server}/{provider}/{detail}/{chain}/{name}",
    ]
    lb_folder = "" if variant is htree.HTreeVariant.R3 else "/LB"

    names = _all_vtx_names_lbn(variant) + _all_trk_names_lbn(variant)
    paths.update(name for name in _make_template_names(
        templates_lbn,
        names,
        lbs=lbs,
        run=run,
        server=server,
        provider=provider,
        detail=detail,
        chain=chain,
        lb_folder=lb_folder,
    ))

    names = _all_vtx_names(variant) + _all_trk_names(variant)
    paths.update(name for name in _make_template_names(
        templates_nonlbn,
        names,
        run=run,
        server=server,
        provider=provider,
        detail=detail,
        chain=chain,
        lb_folder=lb_folder,
    ))

    root.addpaths(paths)

    return root


def make_ohcp_tree(*, server: str, provider: str, detail: str, chain: str, run: int,
                   lbs: Iterable[int] = (10, 123, 987),
                   variant: htree.HTreeVariant = htree.HTreeVariant.B) -> tdirectory:
    """Make directory tree which looks like OHCP file.

    Parameters
    ----------
    server : `str`
        Name of the OH server.
    provider : `str`
        Provider name.
    detail : `str`
        Detail level, like DEBUG or EXPERT.
    chain : `str`
        HLT chain name (actually algorithm name).
    run : `int`
        Run number.
    lbs : `list` [`int`]
        List of LB numbers.
    """
    root = tdirectory("")

    # all names that we need to generate
    templates_lbn = [
        # regular per-LB histograms
        "run_{run}/lb_{lbn}/{server}/{provider}{lb_folder}/{detail}/{chain}/{name}",
        # some empty folders to see if they confuse things
        "run_{run}/lb_{lbn}/{server}/{provider}{lb_folder}/{detail}/{chain}_empty/",
    ]

    # _runsummary only exist for run1/2
    if variant in (htree.HTreeVariant.A, htree.HTreeVariant.B):
        templates_lbn += [
            # runsummary histograms, stored with LBN=-1
            "run_{run}/lb_-1/{server}/{provider}/{detail}/{chain}/{name}_runsummary",
        ]
    templates_nonlbn = [
        # non-lbn histograms
        "run_{run}/lb_-1/{server}/{provider}/{detail}/{chain}/{name}",
    ]
    lb_folder = "" if variant is htree.HTreeVariant.R3 else "/LB"

    paths: set[str] = set()

    names = _all_vtx_names_lbn(variant) + _all_trk_names_lbn(variant)
    paths.update(name for name in _make_template_names(
        templates_lbn,
        names,
        lbs=lbs,
        run=run,
        server=server,
        provider=provider,
        detail=detail,
        chain=chain,
        lb_folder=lb_folder,
    ))

    names = _all_vtx_names(variant) + _all_trk_names(variant)
    paths.update(name for name in _make_template_names(
        templates_nonlbn,
        names,
        run=run,
        server=server,
        provider=provider,
        detail=detail,
        chain=chain,
        lb_folder=lb_folder,
    ))

    root.addpaths(paths)

    return root


def make_offline_tree(*, chain: str, lbs: Iterable[int] = (95,),
                      variant: htree.HTreeVariant = htree.HTreeVariant.R3) -> tdirectory:
    """Make directory tree which looks like offline file

    Parameters
    ----------
    chain : `str`
        HLT chain name (actually algorithm name).
    variant : htree.HTreeVariant
        Histogram naming variant.

    Notes
    -----
    Differences of R3 files from previous generations:
    - Directory structure looks the same, but we have one more folder in a path
      because histograms are now made by a separate tool.
    - Per-LB histograms have suffix "_LB<n>"
    - there are no runsummary histograms, but names can be "reused" from per-LBN histos
    """

    root = tdirectory("")

    # all names that we need to generate
    if variant == htree.HTreeVariant.R3:
        templates_lbn = [
            # per-LBN histos
            "{chain}/{name}_LB{lbn}",
            # add some other chain
            "OtherChain/",
        ]
    else:
        templates_lbn = [
            # chain name is at the top
            "{chain}/{name}_runsummary",
            # add some other chain
            "OtherChain/",
        ]
    templates_nonlbn = [
        # non-LBN histos
        "{chain}/{name}",
    ]

    paths: set[str] = set()
    names = _all_vtx_names_lbn(variant) + _all_trk_names_lbn(variant)
    paths.update(name for name in _make_template_names(templates_lbn, names, lbs=lbs, chain=chain))

    names = _all_vtx_names(variant) + _all_trk_names(variant)
    paths.update(name for name in _make_template_names(templates_nonlbn, names, chain=chain))

    root.addpaths(paths)

    return root


def make_offline_lbn_tree(*, detail: str, chain: str, lbn: int,
                          variant: htree.HTreeVariant = htree.HTreeVariant.B) -> tdirectory:
    """Make directory tree which looks like offline lbn-monitoring.root file
    from run2.

    Parameters
    ----------
    detail : `str`
        Detail level, like DEBUG or EXPERT.
    chain : `str`
        HLT chain name (actually algorithm name).
    lbn : `int`
        LB number.
    variant : htree.HTreeVariant
        Histogram naming variant.
    """
    root = tdirectory("")

    # in run3 lbn-monitoring is empty, all per-LBN histos go to
    # expert-monitoring
    if variant in (htree.HTreeVariant.A, htree.HTreeVariant.B):
        # all names that we need to generate
        templates = [
            # LBN folder is at the top
            "lb_{lbn}/LB/{detail}/{chain}/{name}",
            # different LBN can appear occasionally
            "lb_{lbn2}/LB/{detail}/TIMERS",
        ]

        lbn2 = lbn - 1
        paths: set[str] = set()

        names = _all_vtx_names_lbn(variant) + _all_trk_names_lbn(variant)
        paths.update(name for name in _make_template_names(
            templates, names, lbn=lbn, lbn2=lbn2, chain=chain, detail=detail
        ))

        root.addpaths(paths)

    return root


class HTreeMock(htree.MemoryHTree[tobject]):
    """Mock for an HTree.

    Parameters
    ----------
    hdefs : list of hist_def.HistoDef
    """
    def __init__(self, hdefs: list[hist_def.HistoDef]):
        super().__init__()
        self.hdefs = hdefs

    def add_histograms(self, lbn: int | None, n_missing: int = 0, all_empty: bool = False) -> None:
        entries = 0 if all_empty else 1000
        for name in self._names(n_missing):
            self.add_histogram(name, tobject(name, n_entries=entries), lbn)

    def add_run_summary(self, n_missing: int = 0, all_empty: bool = False) -> None:
        entries = 0 if all_empty else 30000
        for name in self._names(n_missing, runsummary=True):
            self.add_histogram(name, tobject(name, n_entries=entries), None)

    def _names(self, n_missing: int = 0, runsummary: bool = False) -> Iterator[str]:
        if runsummary:
            hdefs = [hdef for hdef in self.hdefs if hdef.is_rs_res]
        else:
            hdefs = self.hdefs
        all_names = [hdef.name for hdef in hdefs]
        random.shuffle(all_names)
        for name in all_names[n_missing:]:
            yield name
