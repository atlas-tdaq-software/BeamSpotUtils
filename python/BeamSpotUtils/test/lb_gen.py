"""RC.Controllable which increments LB on every publish().

To make a run control application out of this module use `rc_pyrunner`
executable, e.g.:

  rc_pyrunner -M LBGen@BeamSpotUtils.test.lb_gen

or

    <obj class="RunControlApplication" id="LBGenerator">
     <attr name="InterfaceName" type="string">"rc/commander"</attr>
     <attr name="ProbeInterval" type="s32">60</attr>
     <attr name="Parameters" type="string">"-M LBGen@BeamSpotUtils.test.lb_gen"</attr>
     <attr name="RestartParameters" type="string">"-M LBGen@BeamSpotUtils.test.lb_gen"</attr>
     <attr name="InitTimeout" type="u32">30</attr>
     <attr name="ExitTimeout" type="u32">5</attr>
     <rel name="Program">"Binary" "rc_pyrunner"</rel>
     <rel name="Uses" num="1">
      "SW_Repository" "BeamSpot-Repository"
     </rel>
     ...
    </obj>

Make sure that PYTHONPATH is set correctly for this application (se Uses relation
above).
"""

import logging
import os
from typing import Any

from ispy import IS
from pyolc2hlt.LogHandler import initLogging
from rcpy import Controllable

initLogging()

_LOG = logging.getLogger(__name__.partition('.')[2])


class LBGen(Controllable):
    """
    Implementation of controlable interface from RunControl.
    """

    partition = os.environ.get("TDAQ_PARTITION")
    is_object = "RunParams.LumiBlock"
    r4p_object = "RunParams.Ready4Physics"

    #----------------
    #  Constructor --
    #----------------
    def __init__(self) -> None:
        Controllable.__init__(self)

        self._isobj = None
        self._r4pobj = None
        self._initial_lbn = 0
        self._r4p_lbn = -1

    def configure(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in configure: %s", cmd)

    def connect(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def prepareForRun(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in prepareForRun: %s", cmd)

        try:
            _LOG.info("Instantiating IS object \"%s!%s\"",
                      self.partition, self.is_object)
            self._isobj = IS.LumiBlock(self.partition, self.is_object)
            _LOG.info("Instantiating IS object \"%s!%s\"",
                      self.partition, self.r4p_object)
            self._r4pobj = IS.Ready4PhysicsInfo(self.partition, self.r4p_object)
        except Exception:
            _LOG.error('Error instantiating IS object', exc_info=True)
            return

        self._initial_lbn = int(os.environ.get("INITIAL_LBN", "0"))
        self._r4p_lbn = int(os.environ.get("R4P_LBN", "-1"))

    def stopROIB(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in stopROIB: %s", cmd)

        self._isobj = None

    def stopDC(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopHLT(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopRecording(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopGathering(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopArchiving(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def disconnect(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def unconfigure(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in unconfigure: %s", cmd)

    def user(self, cmd: Any) -> None:
        """
        @param cmd: instance of UserCmd type
        """
        _LOG.info("in user, cmd: %s", cmd)

    def onExit(self, state: Any) -> None:
        """
        @param state: instance of FSM_STATE type
        """
        _LOG.info("in onExit: %s", state)

    def publish(self) -> None:
        """
        Publish (incremental) statistics, may be called periodically
        """

        if not self._isobj:
            return

        _LOG.info("in publish")

        try:
            self._isobj.checkout()
            _LOG.info('Current LB number: %s', self._isobj.LumiBlockNumber)
            self._isobj.LumiBlockNumber += 1
            if self._isobj.LumiBlockNumber < self._initial_lbn:
                self._isobj.LumiBlockNumber = self._initial_lbn
            _LOG.info('Updating LB number: %s', self._isobj.LumiBlockNumber)
            self._isobj.checkin()
        except Exception:
            _LOG.error('Error updating IS object', exc_info=True)
            return

        if self._r4p_lbn >= 0:
            assert self._r4pobj is not None
            self._r4pobj.run_number = self._isobj.RunNumber
            self._r4pobj.lumi_block = self._isobj.LumiBlockNumber
            self._r4pobj.ready4physics = self._isobj.LumiBlockNumber >= self._r4p_lbn
            try:
                _LOG.info('Updating R4P flag: %s', self._r4pobj.ready4physics)
                self._r4pobj.checkin()
            except Exception:
                _LOG.error('Error updating IS object', exc_info=True)
                return
