"""RC.Controllable which replays histograms from MDA files.

To make a run control application out of this module use `rc_pyrunner`
executable, e.g.:

  rc_pyrunner -M LBGen@BeamSpotUtils.test.lb_gen

or

    <obj class="RunControlApplication" id="MDAReplay">
     <attr name="InterfaceName" type="string">"rc/commander"</attr>
     <attr name="ProbeInterval" type="s32">60</attr>
     <attr name="Parameters" type="string">"-M MDAReplay@BeamSpotUtils.test.hreplay"</attr>
     <attr name="RestartParameters" type="string">"-M MDAReplay@BeamSpotUtils.test.hreplay"</attr>
     <attr name="InitTimeout" type="u32">30</attr>
     <attr name="ExitTimeout" type="u32">5</attr>
     <rel name="Program">"Binary" "rc_pyrunner"</rel>
     <rel name="Uses" num="1">
      "SW_Repository" "BeamSpot-Repository"
     </rel>
     <rel name="ProcessEnvironment" num="1">
      "Variable" "REPLAY_FILE"
     </rel>
     ...
    </obj>

Make sure that PYTHONPATH is set correctly for this application (se Uses relation
above).
"""

import logging
import os
from typing import Any

from ipc import IPCPartition
from ispy import IS
import ROOT
from oh import OHRootProvider
from pyolc2hlt.LogHandler import initLogging
from rcpy import Controllable

initLogging()

_LOG = logging.getLogger(__name__.partition('.')[2])


class MDAReplay(Controllable):
    """
    Implementation of controllable interface from RunControl.

    When LB number changes this application reads corresponding histograms from
    a MDA ROOT file and publishes them in OH server. The name of the ROOT file
    to use comes from REPLAY_FILE environment variable, it has to be on a local
    disk.

    Note: potentially one can use root://eosatlas// syntax for file name, but
    current TDAQ setup is broken and does not include xrootd directory into
    LD_LIBRARY_PATH, so the only working option now is not to use xrootd.
    """

    partition = os.environ.get("TDAQ_PARTITION")
    is_lb_object = "RunParams.LumiBlock"  # IS object to check for LBN
    mda_server = "Histogramming"  # server name in MDA file
    mda_provider = "TopMIG-OH_HLT"  # provider name in MDA file
    oh_server = "Histogramming"  # OH server name for publishing
    oh_provider = "TopMIG-OH:HLT"  # provider name for publishing

    #----------------
    #  Constructor --
    #----------------
    def __init__(self) -> None:
        Controllable.__init__(self)

        self._is_lb_obj = None
        self._tfile = None
        self._run_dir = None
        self._provider = None
        self._last_lb = 0

    def configure(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in configure: %s", cmd)

    def connect(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def prepareForRun(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in prepareForRun: %s", cmd)

        self._last_lb = 0
        try:
            _LOG.info("Instantiating IS object \"%s!%s\"",
                      self.partition, self.is_lb_object)
            self._is_lb_obj = IS.LumiBlock(self.partition, self.is_lb_object)
        except Exception:
            _LOG.error('Error instantiating IS object', exc_info=True)
            return

        try:
            part = IPCPartition(self.partition)
            self._provider = OHRootProvider(part, self.oh_server, self.oh_provider, None)
        except Exception:
            _LOG.error('Error instantiating OH provider', exc_info=True)
            return

        fname = os.environ["REPLAY_FILE"]
        self._open_file(fname)

    def stopROIB(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in stopROIB: %s", cmd)

        self._is_lb_obj = None
        self._tfile = None
        self._run_dir = None
        self._last_lb = 0

    def stopDC(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopHLT(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopRecording(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopGathering(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopArchiving(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def disconnect(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def unconfigure(self, cmd: Any) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in unconfigure: %s", cmd)

    def user(self, cmd: Any) -> None:
        """
        @param cmd: instance of UserCmd type
        """
        _LOG.info("in user, cmd: %s", cmd)

    def onExit(self, state: Any) -> None:
        """
        @param state: instance of FSM_STATE type
        """
        _LOG.info("in onExit: %s", state)

    def publish(self) -> None:
        """
        Publish (incremental) statistics, may be called periodically
        """
        _LOG.info("in publish")

        if self._is_lb_obj:
            try:
                self._is_lb_obj.checkout()
                lbn = self._is_lb_obj.LumiBlockNumber
                _LOG.debug('Current LB number: %s', lbn)
            except Exception:
                _LOG.error('Error reading IS object', exc_info=True)
                return

            if lbn != self._last_lb:
                _LOG.info('New LB detected: %s, will publish histograms for LB %s',
                          lbn, lbn - 1)
                self._last_lb = lbn
                self._replay(lbn - 1)

    def _open_file(self, fname: str) -> None:
        """open MDA file for given run
        """

        _LOG.info('Opening MDA file %s', fname)
        self._tfile = ROOT.TFile.Open(fname)
        if not self._tfile:
            _LOG.error('Failed to open MDA file %s', fname)
            return

        # find "run_NNN" directory
        top_dirs = [key for key in self._tfile.GetListOfKeys()
                    if str(key.GetName()).startswith("run_")]
        if not top_dirs:
            _LOG.error('Failed to find run_* folder MDA file %s', fname)
            return
        self._run_dir = self._tfile.Get(top_dirs[0].GetName())
        if not self._run_dir:
            _LOG.error('Failed to find folder %s', top_dirs[0].GetName())
            return

    def _replay(self, lbn: int) -> None:
        """Generate histograms for given LBN.
        """
        pdir = "lb_{}/{}/{}".format(lbn,
                                    self.mda_server,
                                    self.mda_provider)
        assert self._run_dir is not None
        top_dir = self._run_dir.Get(pdir)
        if not top_dir:
            _LOG.error('Failed to find folder %s', pdir)
            return

        # iterate over all histograms
        def histos(tdir):
            for key in tdir.GetListOfKeys():
                name = str(key.GetName())
                if key.IsFolder():
                    for histo, hname in histos(tdir.Get(name)):
                        yield histo, name + '/' + hname
                else:
                    yield tdir.Get(name), name

        for histo, hname in histos(top_dir):
            hname = '/' + hname
            _LOG.info('publishing: %s', hname)
            assert self._provider is not None
            self._provider.publish(histo, hname, lbn)
