"""
Module containing methods for generating BeamSpot configuration with PartitionMaker.
"""

import os

import pm
import pm.project
from config.dal import module as dal_module


def make_configs(config):
    """Create all BeamSpot config objects.

    Parameters
    ----------
    config : ConfigParser
        Configuration object

    Returns
    -------
    Two lists, first is the lsit of configuration objects, second is the list of
    includes.
    """
    schema = 'daq/schema/beamspotutils.schema.xml'
    bsdal = dal_module('dal', schema)

    section = "BSCommonOptions"
    common_opts = bsdal.BSControllerCommonOptions("BSCommonOptions",
                                                  Partition=config.get(section, "Partition"),
                                                  LBServer=config.get(section, "LBServer"),
                                                  LBObject=config.get(section, "LBObject"),
                                                  ISServer=config.get(section, "ISServer"),
                                                  ISProvider=config.get(section, "ISProvider"),
                                                  BSTOptions=config.get(section, "BSTOptions"))

    configs = []
    for appname in ['BeamSpotController_HLTParameters',
                    'BeamSpotController_LiveMon',
                    'BeamSpotController_PerBunchLiveMon']:
        configs.append(bsdal.BeamSpotISControllerConfig(appname,
                                                        TriggerChain=config.get(appname, "TriggerChain"),
                                                        CallbackPrescale=config.getint(
                                                            appname, "CallbackPrescale"),
                                                        MinLBN=config.getint(appname, "MinLBN"),
                                                        MaxLBN=config.getint(appname, "MaxLBN"),
                                                        RepInput=config.get(appname, "RepInput"),
                                                        RepOutput=config.get(appname, "RepOutput"),
                                                        DoBCIDs=config.getint(appname, "DoBCIDs"),
                                                        LogLevel=config.getint(appname, "LogLevel"),
                                                        BSTOptions=config.get(appname, "BSTOptions"),
                                                        CommonOptions=common_opts))

    return configs, [schema]


def make_segment(config, scripts, output_file, tag_dal, comp):
    """Create BeamSpot segment.

    Parameters
    ----------
    config : ConfigParser
        Configuration object
    scripts : dict
        Dictionary returned from `get_scripts` method
    output_file : str
        Location of generated XML file
    tag_dal : `Tag`
        Software tag (instance of DAL `Tag` class), only used if `install_area`
        is non-empty.
    comp : `Computer`
        Computer to run all segment applications

    Returns
    -------
    Tuple, first element is `Segment` instance, second is the list of includes.
    """
    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    repo = pm.project.Project("daq/sw/repository.data.xml")
    includes = ['daq/schema/core.schema.xml',
                "daq/sw/repository.data.xml"]

    apps = []
    apps.append(dal.CustomLifetimeApplication("BeamSpotArchiver_PerBunchLiveMon",
                                              Lifetime="UserDefined_Shutdown",
                                              AllowSpontaneousExit=1,
                                              InitTimeout=0,
                                              ExitTimeout=5,
                                              RestartableDuringRun=1,
                                              Program=scripts["BeamSpotArchiver"]))

    params = "-d oksconfig:" + os.path.abspath(output_file)
    for appname in ['BeamSpotController_HLTParameters',
                    'BeamSpotController_LiveMon',
                    'BeamSpotController_PerBunchLiveMon']:
        apps.append(dal.CustomLifetimeApplication(appname,
                                                  Lifetime='Boot_Shutdown',
                                                  Program=scripts["BeamSpotISController"],
                                                  AllowSpontaneousExit=0,
                                                  RestartableDuringRun=1,
                                                  InitTimeout=0,
                                                  ExitTimeout=5,
                                                  Parameters=params,
                                                  RestartParameters=params))

    apps.append(dal.CustomLifetimeApplication('BeamSpotInvalidator_HLTParameters',
                                              Lifetime='UserDefined_Shutdown',
                                              Program=scripts["BeamSpotInvalidator"],
                                              AllowSpontaneousExit=1,
                                              RestartableDuringRun=1,
                                              InitTimeout=0,
                                              ExitTimeout=5))

    # IS server
    isparam = "-s -p ${TDAQ_PARTITION} -n env(TDAQ_APPLICATION_NAME) " \
              "-b ${TDAQ_BACKUP_PATH}/env(TDAQ_APPLICATION_NAME).backup"
    iss = dal.InfrastructureApplication('BeamSpot',
                                        Parameters=isparam,
                                        RestartParameters=isparam,
                                        SegmentProcEnvVarName="BEAMSPOT_IS_SERVER",
                                        SegmentProcEnvVarValue="appId",
                                        Program=repo.getObject("Binary", "is_server"),
                                        RestartableDuringRun=1)

    rcapp = dal.RunControlApplication('BeamSpotRC',
                                      InterfaceName="rc/commander",
                                      InitTimeout=30,
                                      Program=repo.getObject("Binary", "rc_controller"),
                                      RunsOn=comp)

    envvars = _segment_envvars(config, tag_dal)

    hosts = [comp] if comp else []
    segment_name = config.get("BeamSpotSegment", "Name")
    segment = dal.Segment(segment_name,
                          IsControlledBy=rcapp,
                          Applications=apps,
                          Infrastructure=[iss],
                          ProcessEnvironment=envvars,
                          Hosts=hosts)

    return segment, includes


def _segment_envvars(config, tag_dal):
    """Make environment variables needed by apps in Bs segment.

    Parameters
    ----------
    config : ConfigParser
        Configuration object
    tag_dal : `Tag`
        Software tag (instance of DAL `Tag` class), only used if `install_area`
        is non-empty.

    Returns
    -------
    List of Variables.
    """
    dal = dal_module('dal', 'daq/schema/core.schema.xml')

    tag = tag_dal.HW_Tag + "-" + tag_dal.SW_Tag

    coral_includes = "${CORAL_HOME}/" + tag + "/include"
    cool_includes = "${COOL_HOME}/" + tag + "/include"
    root_include = dal.Variable("BeamSpotUtils-ROOT_INCLUDE_PATH",
                                Name="ROOT_INCLUDE_PATH",
                                Value=coral_includes + ":" + cool_includes,
                                Description="ROOT_INCLUDE_PATH with COOL/CORAL headers")

    coral_auth = config.get("DEFAULT", "CORAL_AUTH_PATH")
    coral_auth_var = dal.Variable("BeamSpotUtils-CORAL_AUTH_PATH",
                                  Name="CORAL_AUTH_PATH",
                                  Value=coral_auth)
    coral_dblookup = dal.Variable("BeamSpotUtils-CORAL_DBLOOKUP_PATH",
                                  Name="CORAL_DBLOOKUP_PATH",
                                  Value=coral_auth)

    variables = [coral_auth_var, coral_dblookup, root_include]
    envs = dal.VariableSet("BeamSpotUtils-Environment", Contains=variables)
    return [envs]


def get_scripts(config, tag_dal):
    """Return dict with all BS scripts.

    Parameters
    ----------
    config : ConfigParser
        Configuration object
    tag_dal : `Tag`
        Software tag (instance of DAL `Tag` class), only used if `install_area`
        is non-empty.

    Returns
    -------
    Dictionary with the keys being applciation name and values are `Program`
    instances. Additionally 'include' key has the list of all includes need for
    repository.
    """
    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    repo = pm.project.Project("daq/sw/repository.data.xml")
    exrepo = pm.project.Project("daq/sw/tdaq-common-external-repository.data.xml")
    includes = ['daq/schema/core.schema.xml',
                "daq/sw/repository.data.xml",
                "daq/sw/tdaq-common-external-repository.data.xml"]

    # ================== setup repository ============================

    install_area = config.get("SW_Repository", "InstallationPath")
    if install_area:
        # make local repo
        tag = tag_dal.HW_Tag + "-" + tag_dal.SW_Tag

        pp1 = dal.SW_PackageVariable('PYTHONPATH-BS', Name='PYTHONPATH', Suffix='share/lib/python')
        pp2 = dal.SW_PackageVariable('PYTHONPATH-BS-SOLIB', Name='PYTHONPATH', Suffix=tag + '/lib')

        try:
            pyhome = exrepo.getObject("Variable", "TDAQ_PYTHON_HOME")
        except RuntimeError:
            pyhome = dal.Variable("BeamSpotUtils-TDAQ_PYTHON_HOME",
                                  Name="TDAQ_PYTHON_HOME",
                                  Value=os.environ['TDAQ_PYTHON_HOME'])

        if config.getboolean("SW_Repository", "PythonPath"):
            ppath = ["${TDAQ_INST_PATH}/" + tag + "/lib",
                     "${TDAQ_INST_PATH}/share/lib/python",
                     "${TDAQC_INST_PATH}/" + tag + "/lib",
                     "${TDAQC_INST_PATH}/share/lib/python",
                     "${COOL_HOME}/" + tag + "/python",
                     "${CORAL_HOME}/" + tag + "/lib",
                     "${CORAL_HOME}/" + tag + "/python",
                     "${ROOT_HOME}/" + tag + "/lib"]
            ppath = dal.Variable("BeamSpotUtils-PYTHONPATH",
                                 Name="PYTHONPATH",
                                 Value=":".join(ppath),
                                 Description="PYTHONPATH with tdaq and tdaq-common releases")
        else:
            commonenv = pm.project.Project("daq/segments/common-environment.data.xml")
            includes += ["daq/segments/common-environment.data.xml"]
            ppath = commonenv.getObject("Variable", "PYTHONPATH_NO_TAGS")

        variables = [pyhome, ppath]
        if config.getboolean("SW_Repository", "UsrPath"):
            variables.append(dal.Variable("BS_USR_PATH", Name="PATH", Value="/usr/bin:/bin:"))

        bs_controller = dal.Script('BeamSpotISController', BinaryName='beamSpotISController.py')
        bs_invalidator = dal.Script('BeamSpotInvalidator', BinaryName='beamSpotInvalidator.py')
        bs_archiver = dal.Script('BeamSpotArchiver', BinaryName='beamSpotArchiver.py')

        is_infos = config.get("SW_Repository", "ISInfoDescriptionFiles").split()

        localrepo = dal.SW_Repository('BeamSpot-Repository',
                                      Name="BeamSpot Patch Area",
                                      InstallationPath=install_area,
                                      SW_Objects=[bs_controller, bs_invalidator, bs_archiver],
                                      Tags=[tag_dal],
                                      Uses=[repo.getObject("SW_Repository", "Online")],
                                      ProcessEnvironment=variables,
                                      AddProcessEnvironment=[pp1, pp2],
                                      ISInfoDescriptionFiles=is_infos)

        bs_controller.BelongsTo = localrepo
        bs_invalidator.BelongsTo = localrepo
        bs_archiver.BelongsTo = localrepo

        res = dict(BeamSpotISController=bs_controller,
                   BeamSpotInvalidator=bs_invalidator,
                   BeamSpotArchiver=bs_archiver)
        res['includes'] = includes
        res['sw_repo'] = localrepo

    else:

        # daq/sw/BeamSpot-Repository.data.xml exists at P1 only
        repo_inc = "daq/sw/BeamSpot-Repository.data.xml"
        bsrepo = pm.project.Project(repo_inc)
        res = dict((x, bsrepo.getObject("Script", x)) for x in ['BeamSpotISController',
                                                                'BeamSpotInvalidator',
                                                                'BeamSpotArchiver'])
        res['includes'] = includes + [repo_inc]
        res['sw_repo'] = bsrepo.getObject("SW_Repository", "BeamSpot-Repository")

    return res


def make_histo_segment(config, comp, bsrepo):
    """Make segment containing histogram-producing apps.

    Parameters
    ----------
    config : ConfigParser
        Configuration object
    comp : `Computer`
        Computer to run all segment applications
    bsrepo : `SW_Repository`
        Repository containing Python modules

    Returns
    -------
    Tuple, first element is `Segment` instance, second is the list of includes.
    """

    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    repo = pm.project.Project("daq/sw/repository.data.xml")
    includes = ['daq/schema/core.schema.xml', "daq/sw/repository.data.xml"]

    # this needs PYTHONPATH that includes SW_Repo containing BeamSpotUtils,
    # I expect that repo sets correct PYTHONPATH so I just Use it
    ini_lbn = dal.Variable("LBGEN_INITIAL_LBN",
                           Name="INITIAL_LBN",
                           Value=config.get("HistoSegment", "INITIAL_LBN"))
    r4p_lbn = dal.Variable("LBGEN_R4P_LBN",
                           Name="R4P_LBN",
                           Value=config.get("HistoSegment", "R4P_LBN"))
    params = "-M LBGen@BeamSpotUtils.test.lb_gen"
    lb_gen = dal.RunControlApplication('LBGenerator',
                                       InterfaceName="rc/commander",
                                       InitTimeout=30,
                                       ProbeInterval=30,
                                       Parameters=params,
                                       RestartParameters=params,
                                       Program=repo.getObject("Binary", "rc_pyrunner"),
                                       ProcessEnvironment=[ini_lbn, r4p_lbn],
                                       Uses=[bsrepo])

    replay_run = dal.Variable("MDA_REPLAY_FILE",
                              Name="REPLAY_FILE",
                              Value=config.get("HistoSegment", "REPLAY_FILE"))
    params = "-M MDAReplay@BeamSpotUtils.test.hreplay"
    mda_replay = dal.RunControlApplication('MDAReplay',
                                           InterfaceName="rc/commander",
                                           InitTimeout=30,
                                           ProbeInterval=10,
                                           Parameters=params,
                                           RestartParameters=params,
                                           Program=repo.getObject("Binary", "rc_pyrunner"),
                                           ProcessEnvironment=[replay_run],
                                           Uses=[bsrepo])

    rcapp = dal.RunControlApplication('HistoSegRC',
                                      InterfaceName="rc/commander",
                                      InitTimeout=30,
                                      Program=repo.getObject("Binary", "rc_controller"))

    hosts = [comp] if comp else []
    segment_name = config.get("HistoSegment", "Name")
    segment = dal.Segment(segment_name,
                          IsControlledBy=rcapp,
                          Applications=[lb_gen, mda_replay],
                          Infrastructure=[],
                          Hosts=hosts)

    return segment, includes


def make_partition(segments, config, tag_dal, comp):
    """Create partition which includes BeamSpot segments.

    Parameters
    ----------
    segments : list of Segment
        Segments to include into partition
    config : ConfigParser
        Configuration object
    tag_dal : `Tag`
        Software tag (instance of DAL `Tag` class), only used if `install_area`
        is non-empty.
    comp : `Computer`
        Computer to run all partition applications

    Returns
    -------
    Tuple, first element is `Partition` instance, second is the list of includes.
    """

    dal = dal_module('dal', 'daq/schema/core.schema.xml')
    common_env = pm.project.Project("daq/segments/common-environment.data.xml")
    setup = pm.project.Project("daq/segments/setup.data.xml")
    includes = ['daq/schema/core.schema.xml',
                "daq/segments/common-environment.data.xml",
                "daq/segments/setup.data.xml"]

    # make partition object
    part = dal.Partition(config.get("Partition", "Name"),
                         OnlineInfrastructure=setup.getObject("OnlineSegment", "setup"),
                         Segments=segments,
                         DefaultHost=comp,
                         DefaultTags=[tag_dal],
                         LogRoot=config.get("Partition", "LogRoot"),
                         WorkingDirectory=config.get("Partition", "WorkingDirectory"),
                         ProcessEnvironment=[common_env.getObject('VariableSet', 'CommonEnvironment')],
                         Parameters=[common_env.getObject('VariableSet', 'CommonParameters')])

    return part, includes
