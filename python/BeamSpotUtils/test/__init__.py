"""Package with modules that are useful for testing.

Actual (executable) test scripts should go into top-level test/ directory or
test/unit/.
"""
