'''Library: Class implementation and supporting static functions for represenations'''

from __future__ import annotations

import copy
import dataclasses
import logging
from collections.abc import Hashable, Iterable, Iterator, Mapping
from typing import Any

# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])

# Meaning of the bits in the status word, based on
# https://twiki.cern.ch/twiki/bin/view/Atlas/CoolBeamSpotParameters#Definition_of_the_Status_Word
# bits 0-1
STATUS_BITS_FIT_NOT_CONVERGED = 0x0
STATUS_BITS_FIT_PROBLEMATIC = 0x1
STATUS_BITS_FIT_CONVERGED = 0x3
# bit 2
STATUS_BITS_ONLINE = 0x4
# bit 3
STATUS_BITS_WIDTH_FOUND = 0x8
# combined values
STATUS_NOT_CONVERGED = STATUS_BITS_ONLINE | STATUS_BITS_FIT_NOT_CONVERGED  # 4
STATUS_PROBLEMATIC = STATUS_BITS_ONLINE | STATUS_BITS_FIT_PROBLEMATIC  # 5
STATUS_OK = STATUS_BITS_ONLINE | STATUS_BITS_FIT_CONVERGED  # 7


# NOTE: list of fields has to match attribute definition in BSSnapshot class

# Fields that are associted with COOL intervals or folder
coolMetaFields = ("run", "lbn", "bcid")

# Fields that are regular COOL data
coolPayloadFields = (
    "status",
    "posX", "posY", "posZ", "posXErr", "posYErr", "posZErr",
    "sigmaX", "sigmaY", "sigmaZ", "sigmaXErr", "sigmaYErr", "sigmaZErr",
    "tiltX", "tiltY", "tiltXErr", "tiltYErr",
    "sigmaXY", "sigmaXYErr",
)

# Fields that cannot be empty when storing COOL data
requiredFields = coolMetaFields + coolPayloadFields

# The rest of the fields/attributes that are used internally
possibleFields = set(requiredFields)
possibleFields |= set([
    "unCorrX", "unCorrY", "unCorrZ",
    "unCorrXErr", "unCorrYErr", "unCorrZErr",
    "resX", "resY", "resZ",
    "resXErr", "resYErr", "resZErr",
    "resBCIDX", "resBCIDY", "resBCIDZ",
    "resBCIDXErr", "resBCIDYErr", "resBCIDZErr",
    "deltaX", "deltaY", "deltaZ",
    "deltaXErr", "deltaYErr", "deltaZErr",
    "firstLBN", "lastLBN",
    "bcids",
])


@dataclasses.dataclass(init=False, repr=True, eq=True)
class BSSnapshot:
    """Representation of a single beamspot measurement.

    BSSnapshot can be constructed in one of several ways:

        # create default snapshot, 'status' set to STATUS_NOT_CONVERGED,
        # 'bcid' to -1, other standard keys to 0.
        snap = BSSnapshot()

        # create default snapshot, then update it from a dictionary provided
        # as positional argument, overriding defaults.
        data = dict(key1=value1, key2=value2)
        snap = BSSnapshot(data)

        # create default snapshot, then update it from keyword arguments,
        # overriding defaults.
        snap = BSSnapshot(key1=value1, key2=value2)

        # Combination of above two, first snapshot is updated from positional
        # argument, then from keyword arguments.
        data = dict(key1=value1, key2=value2)
        snap = BSSnapshot(data, key3=value3, key4=value4)

    Parameters
    ----------
    *args :
        Constructor can accept zero or one positional parameter, type of the
        parameter can be either dict or an instance of BSSnapshot.
    **kwargs :
        Any number of keyword argument which are used to update attributes
        (after initializing instance from *args).
    """

    # Run number, 0 if no associated run
    run: int = 0

    # LB number, 0 if no associated LBN
    lbn: int = 0

    # BCID number, -1 for BCID-average measurement
    bcid: int = -1

    # Measurement/fit status, one of the bitmask values defined above.
    status: int = STATUS_NOT_CONVERGED

    # fields saved in COOL
    posX: float = 0.0
    posY: float = 0.0
    posZ: float = 0.0
    posXErr: float = 0.0
    posYErr: float = 0.0
    posZErr: float = 0.0
    sigmaX: float = 0.0
    sigmaY: float = 0.0
    sigmaZ: float = 0.0
    sigmaXErr: float = 0.0
    sigmaYErr: float = 0.0
    sigmaZErr: float = 0.0
    tiltX: float = 0.0
    tiltY: float = 0.0
    tiltXErr: float = 0.0
    tiltYErr: float = 0.0
    sigmaXY: float = 0.0
    sigmaXYErr: float = 0.0

    # these are used internally and are not stored anywhere
    unCorrX: float | None = None
    unCorrY: float | None = None
    unCorrZ: float | None = None
    unCorrXErr: float | None = None
    unCorrYErr: float | None = None
    unCorrZErr: float | None = None
    resX: float | None = None
    resY: float | None = None
    resZ: float | None = None
    resXErr: float | None = None
    resYErr: float | None = None
    resZErr: float | None = None
    resBCIDX: float | None = None
    resBCIDY: float | None = None
    resBCIDZ: float | None = None
    resBCIDXErr: float | None = None
    resBCIDYErr: float | None = None
    resBCIDZErr: float | None = None
    deltaX: float | None = None
    deltaY: float | None = None
    deltaZ: float | None = None
    deltaXErr: float | None = None
    deltaYErr: float | None = None
    deltaZErr: float | None = None

    # first/last LBN for this measurement
    firstLBN: int | None = None
    lastLBN: int | None = None

    # List of per-BCID measurements, BSSnapshot instances
    bcids: list[BSSnapshot] | None = None

    def __new__(cls, *args: dict | BSSnapshot, **kwargs: Any) -> BSSnapshot:
        # check positional arguments
        self = None
        dict1 = None
        if len(args) == 1:
            if isinstance(args[0], BSSnapshot):
                # make a copy, but make sure we have correct type
                assert isinstance(args[0], cls), f"Positional argument is not an instance of {cls.__name__}"
                self = copy.deepcopy(args[0])
            elif isinstance(args[0], dict):
                dict1 = args[0]
            else:
                raise TypeError("positional argument must be dict or BSSnapshot")
        elif len(args) > 1:
            raise ValueError("only one positional argument is acceptable")

        if self is None:
            self = super().__new__(cls)

        # apply arguments
        if dict1:
            self.update(dict1)
        self.update(kwargs)

        return self

    def __setattr__(self, key: str, value: Any) -> None:
        if key not in possibleFields:
            raise AttributeError(f"Attribute {key} does not exist in BSSnapshot class")
        object.__setattr__(self, key, value)

    def update(self, kv: Mapping[str, Any]) -> None:
        for key, val in kv.items():
            if not hasattr(self, key):
                raise KeyError(f"Attribute {key} does not exist in BSSnapshot class")
            setattr(self, key, val)

    def slice(self, axis: str) -> SliceView:  # noqa: A003
        """Return slice view of this snapshot.

        See `SliceView` class for details

        Parameters
        ----------
        axis : str
            Axis name, uppercase, e.g. 'X'.

        Returns
        -------
        slice : SliceView
            Object representing a sliced view of this instance.
        """
        return SliceView(self, axis)

    def emptyPhysicsField(self, field: str) -> bool:
        """Return True if field is empty.

        Empty field means that field and and its "Err" field are either None
        or equal to 0.

        Parameters
        """
        return getattr(self, field) in (None, 0.0) and getattr(self, field + 'Err') in (None, 0.0)

    def metric(self) -> float:
        """Returns metric (number) used for ordering.
        """
        return self.run + self.lbn / (1000 * 1000.0)

    def as_dict(self, attributes: Iterable[str] | None = None) -> Mapping[str, Any]:
        """Convert instance contents to a dict.

        Note: contents of 'bcids' list is not converted.

        Parameters
        ----------
        attributes : iterable of str, optional
            List of attributes to include, by default all attributes are
            included.
        """
        if not attributes:
            attributes = possibleFields
        attributes = [attr for attr in attributes if attr != "bcids"]
        return dict((key, getattr(self, key)) for key in attributes)

    def prettyLog(self, logger: logging.Logger, level: int = logging.INFO) -> None:
        """Send pretty representation to a logger.

        Parameters
        ----------
        logger : logging.Logger
        level : int, optional
            Logging level, default is logging.INFO
        """
        for line in self.prettyString().split('\n'):
            logger.log(level, "%s", line)

    def prettyString(self) -> str:
        """Pretty-printing for snapshot, returns a string"""

        prty = f'Beam spot snapshot beginning at run {self.run}, lbn {self.lbn}\n'
        if self.firstLBN:
            prty += f'It was calculated with lbns in [{self.firstLBN}, {self.lastLBN}]\n'

        if self.bcid >= 0:
            prty += f'BCID is {self.bcid}\n'
        else:
            prty += ' averaged over all BCIDs\n'

        prty += self.__prettyLineI('status')

        for dtype in ('pos', 'sigma'):
            for dir in 'XYZ':
                field = dtype + dir
                fieldErr = field + 'Err'
                prty += self.__prettyLine(field, fieldErr)
        for dtype in ('tilt',):
            for dir in 'XY':
                field = dtype + dir
                fieldErr = field + 'Err'
                prty += self.__prettyLine(field, fieldErr)

        # these fields are only printed if any of them is non-zero
        for dtype in ('unCorr', 'delta', 'res'):
            nonzero = False
            for dir in 'XYZ':
                field = dtype + dir
                if getattr(self, field) not in (None, 0.0):
                    nonzero = True
                    break
            if nonzero:
                for dir in 'XYZ':
                    field = dtype + dir
                    fieldErr = field + 'Err'
                    prty += self.__prettyLine(field, fieldErr)

        prty += self.__prettyLine('sigmaXY', 'sigmaXYErr')

        return prty

    def __prettyLine(self, field: str, fieldErr: str) -> str:
        if getattr(self, field) is not None:
            prty = '%-10s = % 10.6f' % (field, getattr(self, field))
        else:
            prty = '%-10s = None' % field

        if getattr(self, fieldErr) is not None:
            prty += ' +- %8.6f' % getattr(self, fieldErr)
        else:
            prty += ' +- None'
        prty += '\n'
        return prty

    def __prettyLineI(self, field: str) -> str:
        val = getattr(self, field)
        if val is not None:
            prty = '%-10s = %d' % (field, val)
        else:
            prty = '%-10s = None' % field
        prty += '\n'
        return prty


class SliceView:
    """Class representing a slice of BSSnapshot.

    This class simplifies operations on a subset of keys belonging to one
    axis (e.g. posX, poXErr, deltaX, etc.) It allows accessing those keys
    without using explicit axis name on every attribute. Typical use:

        snap = BSSnapshot(...)
        snapx = snap.slice('X')
        snapx.pos = pos      # equivalent to snap.posX = pos
        snapx.posErr = err      # equivalent to snap.posXErr = err

    Parameters
    ----------
    snap : dict
        Snapshot dictionary (or BSSnapshot)
    axis : str
        Axis name, e.g. 'X'. There are no restriction on the name, any name
        that is useful to client code can be used here.
    """
    def __init__(self, snap: BSSnapshot, axis: str):
        self.__dict__["_snap"] = snap
        self.__dict__["_axis"] = axis

    def _key(self, key: str) -> str:
        """Convert slice key to a parent snapshot attribute name"""
        if key.endswith('Err'):
            return key[:-3] + self._axis + key[-3:]
        else:
            return key + self._axis

    def __getattr__(self, key: str) -> Any:
        return getattr(self._snap, self._key(key))

    def __setattr__(self, key: str, value: Any) -> None:
        setattr(self._snap, self._key(key), value)


class BeamSpotRepresentation:
    """A list of beam spot "snapshots" each corresponding to a measurement.
    An instance of this class is an orderable list of beam spot snapshots,
    each snapshot contains a set of key-value pairs for the different parameters
    that describe a beam spot (posX, tiltY, sigmaZErr, etc).

    This is the only implementation in BeamSpotUtils of a "representation",
    and is also called the internal representation.  When we publish a snapshot
    to COOL, there it has some other representation (although it corresponds
    to the same measurements).  Publish to COOL is an example of translation
    when one representation is translate, or converted, to a different
    representation.

    Parameters
    ----------
    snaps : sequencq of BSSnapshot, optional

    """

    def __init__(self, snaps: Iterable[BSSnapshot] | None = None):
        self._history: list[BSSnapshot] = []
        for snap in snaps or []:
            self.append(snap)

    def __eq__(self, other: object) -> bool:
        # other = BeamSpotRepresentation(other)
        # other.history = sorted(other.history, key=BSSnapshot.metric)
        # self._history = sorted(self._history, key=BSSnapshot.metric)
        # try:
        #     for i, my_snapshot in enumerate(self):
        #         for key, val in my_snapshot.items():
        #             if val != other.history[i][key]:
        #                 return False
        # except KeyError:
        #     return False
        # except IndexError:
        #     return False

        # return True

        # I don't think it makes sense comparing them without defining how to
        # compare snapshots which have bunch of floating point numbers.
        raise NotImplementedError()

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    def __iter__(self) -> Iterator[BSSnapshot]:
        return self._history.__iter__()

    def __str__(self) -> str:
        # s = 'A BeamSpot Representation with %d snapshots' % len(self)
        # return s
        return '\n'.join([str(di) for di in self])

    def __len__(self) -> int:
        return self._history.__len__()

    def __getitem__(self, i: int) -> BSSnapshot:
        return self._history[i]

    def groupBy(self, attr: str) -> Mapping[Hashable, BeamSpotRepresentation]:
        """Group snapshots in this representation according to specified
        attribute.

        Parameters
        ----------
        attr : str
            NAme of the attribute to use for gouping.

        Returns
        -------
        groups : dict
            Dictionary where keys are the attribute distinct values and values
            are BeamSpotRepresentation instances containing snapshots having
            corresponding values for an attribute.

        Example
        -------

        If this rep is:

            Rep[{'status': 5, 'posX':1.1}
                {'status': 5, 'posX':1.3}
                {'status': 7, 'posX':1.7}]

        And it was group by "status" then the following dict is returned:

            {
                5: Rep[{'status': 5, 'posX':1.1}
                       {'status': 5, 'posX':1.3}],
                7: Rep[{'status':7 'posx':1.7]
            }

        Its useful for making the Massi format
        """
        allKeys = set([getattr(snap, attr) for snap in self])

        groups = {}
        for k in allKeys:
            groups[k] = BeamSpotRepresentation([snap for snap in self if getattr(snap, attr) == k])

        return groups

    def append(self, snap: BSSnapshot) -> None:
        """Add one more snapshot to a list.

        If snapshot contains 'bcids' key it must be a list of per-BCID
        snapshot, this list will flattened - it will be removed from a
        snapshot and added to a representation instead.

        Parameters
        ----------
        snap : BSSnapshot
            Snapshot to be added

        Raises
        ------
        `TypeError` if snapshot type is not recognized.
        """
        if not isinstance(snap, BSSnapshot):
            if isinstance(snap, dict):
                _LOG.warning("append(dict) is deprecated, use BSSnapshot instead")
                snap = BSSnapshot(snap)
            else:
                _LOG.error('Unexpected type of snapshot appended: %s -- %s', type(snap), snap)
                raise TypeError('Unexpected snapshot type: ' + str(type(snap)))

        # copy just in case it's shared with something else
        snap = copy.copy(snap)
        snaps = self._flatten(snap)
        self._history += snaps

    @staticmethod
    def _flatten(snap: BSSnapshot) -> list[BSSnapshot]:
        # remove from original snapshot
        bcids = snap.bcids or []
        snap.bcids = None
        if bcids:
            # make copy to modify it
            bcids = [copy.copy(bcid) for bcid in bcids]
            for bcid in bcids:
                bcid.run = snap.run
                bcid.lbn = snap.lbn
                bcid.firstLBN = snap.firstLBN
                bcid.lastLBN = snap.lastLBN
        return [snap] + bcids

    def last(self) -> BSSnapshot | None:
        """Return latest snapshot.

        Ordering of snapshots is according to run and LBN.

        Returns
        -------
        snap : BSSnapshot
            BSSnapshot or None if current list is empty.
        """
        if not self._history:
            return None
        return sorted(self._history, key=BSSnapshot.metric)[-1]

    def needsDeltaToPosition(self) -> bool:
        """Return True if any snapshot needs delta-to-position correction.

        Returns
        -------
        need_conversion : bool
        """
        return any(snap.emptyPhysicsField('pos' + d) and not snap.emptyPhysicsField('delta' + d)
                   for snap in self for d in 'XYZ')
