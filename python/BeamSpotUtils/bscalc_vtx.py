"""Implementation of beamspot calculation using our standard vertex-based
method with HLT-produced monitoring histograms.
"""

# Current state of BSCalcVertex:
# currently just uses basic [0] + [1]/sqrt(x) function fit for resolution
# resolution correction calculation uses fit function value
# also plots the fit function only on the fit range
# produces two RGB plots
#
# VertexNTrksPass_vs_VertexXPass_sigma.redgreenblack: RGB plot that uses resolution
# fit for resolution correction. Includes observed uncorrected width, resolution,
# resolution fit, corrected width based on values from resolution fit, and pol0 fit
# of this corrected width.
# this is produced by _plottCorrVTrks
#
# VertexNTrksPass_vs_VertexXPass_sigma.redgreenblack_orig: RGB plot that uses histogram
# values to correct for resolution and shows pol0 fit on corrected width
# this is produced by _plotCorrVTrks_orig
#
# Display updates:
# Observed, uncorrected width histogram color changed from black to blue
# pol0 fit of corrected width changed from red to green
# Y-axis of RGB plot is hard coded to have a maximum of 0.1 mm
# Now shows statistics boxes for resolution fit and pol0 fit so
# likelihood and parameter values are displayed
#
# fit range is from 15 to 55
# minFitRes = 20 Sets lower range of resolution fit, can be set in command line
# minFitWidth = 30 Sets lower range of pol0 corrected width fit, can be set in command line
# minTrk = 6
# maxTrk = 100 Sets upper range on all fits

from __future__ import annotations

import math
import array
import logging
import os
from collections.abc import Callable, Mapping
from typing import Any, NamedTuple, TYPE_CHECKING

from .freaking_ROOT import ROOT
from .hist_def import make_hdefs_vtx
from .model import STATUS_OK, STATUS_NOT_CONVERGED, STATUS_PROBLEMATIC
from .rootil import fillGauss, projectNTrkRange
from . import bscalc, hist_plot, htree, model, translators
import ers

if TYPE_CHECKING:
    from . import lbn_merge, oh_publish


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])

_SQRT2 = math.sqrt(2)


class HistoPublishError(ers.Issue):
    """ERS Issue class for errors in histogram copying"""

    def __init__(self, cause: Any = None):
        ers.Issue.__init__(self, 'Failed to publish intermediate histograms (not fatal)',
                           {},
                           cause if isinstance(cause, ers.Issue) else None)


def _bcidCorr(currentBCID: float, total: float, averageBCID: float) -> float:
    """BCID correction function"""
    return currentBCID * total / averageBCID


def _bcidCorr2(currentBCID: float, total: float, averageBCID: float) -> float:
    """BCID correction function"""
    return math.sqrt(currentBCID ** 2 + 2 * (total ** 2 - averageBCID ** 2))


class BSCalcVertexConfig(NamedTuple):
    """Configuration for the corrected width calculation, with default values.

    Some of this values are overridden by `Config` class based on command line
    parameters.
    """
    outBase: str = "OnlineBeamSpotResults"
    """prefix for output"""

    outExt: str = ".pdf"
    """Extension for output plots"""

    minNVtx: int = 100
    """threshold for merging LBNs"""

    bcidMinTrk: int = 10
    """The lowest track multiplicity bin to use for BCID resolution,
    it has to be the same as VertexBCIDMinNTrk param in HLT algo"""

    minTrk: int = 6
    """The lowest track multiplicity bin to use"""

    maxTrk: int = 100
    """The highest track multiplicity bin to use"""

    nRMS: float = 3.0
    """The number of RMS to fit the width over"""

    nRMSBCID: float = 2.5
    """The number of RMS to fit the width over for BCID bin and resBCID fits"""

    nRMSTilt: float = 1.8
    """The number of RMS over which to fit the tilt"""

    projectAxis: int = 0
    """Which axis to project along (0 - Y, 1 - X)"""

    minSliceOccupancy: int = 40
    """The smallest number of events required to consider a slice for fitting"""

    nBunches: int = 3563
    """The number of bins in the 2D BCID plots"""

    maxPlotsPerPage: int = 25

    drawOpts: str = "goff"

    rebinningUncertainty: float = .18
    """Avg. fractional uncertainty in the fit window needed to stop rebinning"""

    minCoreBins: int = 6
    """The mininum # of bins in the fit window.  IF exceeded rebin fails"""

    minFitNdf: int = 5
    """Minimum NDF for acceptable resolution fit"""

    beQuiet: bool = True
    """If True then produce more compact output"""

    savePlots: bool = True
    """If True then save plots to file"""

    doBCIDs: bool = False
    """Produce BCID histograms"""

    doBCIDCorrection: bool = True
    """If True then correct BCID widths using the global width"""

    resFit: bool = True
    """If true then fit resolution as 1/sqrt(N_tracks)"""

    minFitRes: int = 20
    """Minimum bin (N_tracks) for resolution fit"""

    minFitWidth: int = 30
    """Minimum bin (N_tracks) for width fit"""

    mon_output: str = ""
    """Name of the output translator to publish results"""


class BSCalcVertex(bscalc.BSCalc):
    """All of the steps necessary for calculating a corrected width.

    Parameters
    ----------
    config : `BSCalcVertexConfig`
        Configuration parameters.
    output_dir : `str`
        Path to folder for output files.
    lbn_merge : `lbn_merge.LBNMerge`
        LBNMerge instance.
    oh_publisher : `OHPublisher`, optional
        OHPublisher instance
    """

    def __init__(self, config: BSCalcVertexConfig, output_dir: str, lbn_merge: lbn_merge.LBNMerge,
                 oh_publisher: oh_publish.OHPublisher | None = None):

        self.config = config
        self.hdefs = make_hdefs_vtx(self.config.doBCIDs)

        histoNames = [hdef.name for hdef in self.hdefs]
        _LOG.info('These are histogram names: %s', ' '.join(sorted(histoNames)))

        self.output_dir = output_dir
        self.oh_publisher = oh_publisher
        self.lbn_merge = lbn_merge

        self.hist_plot = None
        if self.config.savePlots:
            self.hist_plot = hist_plot.HistPlot(self.output_dir, self.config)

        ROOT.gStyle.SetOptFit(1011)

        # # from ROOT doc
        #        = "Q"  Quiet mode (minimum printing)
        #        = "R"  Use the Range specified in the function range
        #        = "L"  Use log likelihood fit (does not seem to work reliably)
        #        = "0"  Do not plot the result of the fit. By default the fitted function
        #               is drawn unless the option "N" above is specified.
        self.fitOpts = "RQ"
        # # log likelihood doesn't work for 177682 resolution calculation
        if self.config.beQuiet:
            self.fitOpts += "0"

        # defined here to avoid the reparsing the text inside the each time fit slices is called
        self.singleGauss = ROOT.TF1("singleGauss", "gaus", -1, 1)
        self.sqrtN = ROOT.TF1("sqrtN", "sqrt([0]/(x*x) +[1]*[1])", self.config.minFitRes, self.config.maxTrk)

    def finish(self) -> None:
        # docstring inherited from base class
        if self.hist_plot:
            self.hist_plot.finish()

    @staticmethod
    def _getHistSummary(histo: ROOT.TH1) -> tuple[float, float, float]:
        """Returns (mean, rms, value_at_max) triplet for a histogram
        """
        return histo.GetMean(), histo.GetRMS(), histo.GetMaximum()

    def _titleAwareFit(self, histo: ROOT.TH1, code: str) -> str:
        ''' the titles first word is either vertex, split, resolution or bcid
            decide what kind of fit to enact
        '''
        #             title  : [type of fit      , nrms]
        fitChoices: Mapping[str, tuple[Callable, float]] = {
            'split_ntrks': (self._fitSingleGauss, self.config.nRMS),
            'ntrks': (self._fitSingleGauss, self.config.nRMS),
            'bcid': (self._fitSingleGauss, self.config.nRMSBCID)}

        fit, nRMS = fitChoices[code]
        if not self._tryRebinning(histo, nRMS):
            # too many rebins
            return ''
        return fit(histo, nRMS)

    def _fitSingleGauss(self, histo: ROOT.TH1, nRMS: float) -> str:
        """Fit histogram with gaussian.

        Parameters
        ----------
        histo
            Histogram (1-dim).
        nRMS : float
            Number of sigmas from mean to fit.

        Returns
        -------
        The name of the fit function.
        """
        thisFit = self.singleGauss

        mean, rms, hMax = self._getHistSummary(histo)
        thisFit.SetParLimits(0, hMax / 2, hMax * 1.5)
        thisFit.SetParLimits(1, mean - rms / 2, mean + rms / 2)
        thisFit.SetParLimits(2, rms / 5, rms * 1.5)
        thisFit.SetParameter(0, hMax)
        thisFit.SetParameter(1, mean)
        thisFit.SetParameter(2, rms)

        histo.Fit(thisFit.GetName(), self.fitOpts + 'B', "", mean - (nRMS * rms), mean + (nRMS * rms))
        return thisFit.GetName()

    def _tryRebinning(self, histo: ROOT.TH1, nRMS: float) -> bool:
        ''' if the average fractional uncertainty is too
            low around the mean, rebin
        '''

        width = histo.GetRMS() * nRMS
        center = histo.GetMean()

        while True:
            low = max(0, histo.FindBin(center - width))
            high = min(histo.GetNbinsX(), histo.FindBin(center + width))

            if high - low < self.config.minCoreBins:
                _LOG.debug('%s +- %.1f RMS less than %d bins. Too many rebins.',
                           histo.GetName(), nRMS, self.config.minCoreBins)
                return False

#            contents    = np.array  ([histo.GetBinContent(i) for i in range(low, high)])
#            errs        = np.minimum(np.power(contents, -.5), 1)
#            uncertainty = np.average(errs)
            uncertainty = 0
# FIXME!
            if uncertainty < self.config.rebinningUncertainty:
                # looks healthy
                return True

            histo.Rebin(2)

    @staticmethod
    def _getSliceTitle(code: str, axis: str, sliceValue: float) -> str:
        """Builds histogram title out of histogram name.

        Takes the histogram code name like "split_ntrks" and generates nice
        title which includes axis name and slice value.

        Parameters
        ----------
        code : str
            Histogram code name, e.g. "ntrks", "split_ntrks", etc.
        axis : str
            Axis name, "X", "Y", or "Z"
        sliceValue : float
            Slice value, usually this is an integer number, but can be float
            too, e.g. Nvtx/2.

        Returns
        -------
        title : str
            Title string.
        """
        titles = {
            "ntrks": "Vertex {axis} for nTrks = {sliceValue}",
            "split_ntrks": "Split Vertex {axis} for nTrks = {sliceValue}",
            "bcid": "BCID Vertex {axis} = {sliceValue}",
        }
        return titles[code].format(axis=axis, sliceValue=sliceValue)

    @staticmethod
    def _fitFailed(fit: Any, histo: ROOT.TH1) -> bool:
        """Return true if fit failed

        Parameters
        ----------
        fit : object
            Result of the fit
        histo
            histogram that was fitted
        """
        # to be grown as we get smarter
        wayToFail = [('None', lambda fit: fit is None),
                     ('False', lambda fit: not fit),
                     ('ROOT Null', lambda fit: '0x(nil)' in str(fit))]

        # histo.SetStats(1)
        # ROOT.gPad.Update()
        # st = fit.FindObject("stats")
        st = None
        for title, func in wayToFail:
            if func(fit):
                if st:
                    st.SetLabel(title)
                return True

        if st:
            st.SetLabel('Chi2Prob %f' % fit.GetProb())

        return False

    def _fitSlices(self, h2: ROOT.TH2, *, code: str, axis: str, projAxis: int = 0,
                   firstBin: int | None = None, lastBin: int | None = None,
                   sparse: bool = False, xtitle: str = 'NTrks'
                   ) -> tuple[ROOT.TH1 | None, ROOT.TH1 | None]:
        """Fit each bin-slice of 2-D histogram with gaussian.

        TH2::FitSlices isn't quite right for our purposes.
        This function allows the range of the fit to be specified.
        Histogram slices and fits are plotted (if plotting option is used).

        Parameters
        ----------
        h2 : TH2
            2-D histogram, plot of something over number of tracks or BCID
        code : str
            Histogram code name, e.g. "ntrks", "split_ntrks", etc.
        axis : str
            Axis name, "X", "Y", or "Z"
        projAxis : int
            projAxis == 0 means projection along X so a slice of y=y_0 (use for
            cases when X axis is Ntrks).
        firstBin : int, optional
            Minimal number of tracks, if missing then minTrk option value
            is used
        lastBin : int, optional
            Max number of tracks, if missing then maxTrk option value
            is used
        sparse : bool
            If True then do not plot histogram slices without fit.
        xtitle : str
            Axis title for X axis of the resulting histograms.

        Returns
        -------
        mean_hist : TH1
            Histogram representing mean value of fit of each slice
        width_hist : TH1
            Histogram representing width of fit in each slice
        """
        if not h2:
            return None, None

        # define range
        _LOG.debug("Fit Slices for %s", h2.GetName())
        _LOG.debug("minSliceOccupancy = %s", self.config.minSliceOccupancy)
        if firstBin is None:
            firstBin = self.config.minTrk
        if lastBin is None:
            lastBin = self.config.maxTrk

        # Define two 1-D histos from 2-D
        if projAxis == 0:
            axObj = h2.GetXaxis()
        elif projAxis == 1:
            axObj = h2.GetYaxis()
        else:
            _LOG.critical("unknown axis %s", projAxis)
            return None, None

        gr_width = ROOT.TH1D(h2.GetName() + '_sigma',
                             f"Sigma vs {xtitle};{xtitle};Sigma [mm]",
                             axObj.GetNbins(), axObj.GetXmin(), axObj.GetXmax())
        gr_mean = ROOT.TH1D(h2.GetName() + '_mean',
                            f"Mean vs {xtitle};{xtitle};Mean [mm]",
                            axObj.GetNbins(), axObj.GetXmin(), axObj.GetXmax())

        # # we are inclusive on maxBin
        minBin = max(0, firstBin)
        maxBin = min(axObj.GetNbins(), lastBin)

        histList = []

        for hbin in range(minBin, maxBin + 1):
            title = self._getSliceTitle(code, axis, axObj.GetBinLowEdge(hbin))
            name_buffer = f"{h2.GetName()}_px_{hbin}"
            # projections = (h2.ProjectionY, h2.ProjectionX)
            # histo = projections[projAxis](name_buffer, hbin, hbin)
            if projAxis == 0:
                histo = h2.ProjectionY(name_buffer, hbin, hbin)
            elif projAxis == 1:
                histo = h2.ProjectionX(name_buffer, hbin, hbin)

            mean, rms, _ = self._getHistSummary(histo)

            if histo.GetEntries() == 0:
                _LOG.debug('%s has no entries', title)
                continue

            if histo.GetEntries() > self.config.minSliceOccupancy:
                fitName = self._titleAwareFit(histo, code)
                fit = histo.GetFunction(fitName)
            else:
                fit = None
                _LOG.debug('%s has too few entries: %d', title, histo.GetEntries())
                # too few entries

            histo.SetTitle(title)
            histo.GetXaxis().SetRangeUser(mean - 4 * rms, mean + 4 * rms)

            subList = [histo]

            if self._fitFailed(fit, histo):
                histo.SetLineColor(ROOT.kRed)
                _LOG.debug('%s fit failed', title)
            else:
                _LOG.debug('%s fit succeeded: %s', title,
                           [(fit.GetParName(par), fit.GetParameter(par)) for par in range(fit.GetNpar())])
                subList.append(fit)
                self._makeFitPretty(subList)

                # store mean/sigma together with errors in output histograms
                gr_mean.SetBinContent(hbin, fit.GetParameter(1))
                gr_mean.SetBinError(hbin, fit.GetParError(1))
                gr_width.SetBinContent(hbin, fit.GetParameter(2))
                gr_width.SetBinError(hbin, fit.GetParError(2))

            histList.append(subList)

        if self.hist_plot:
            # plot all 1-d histograms
            if sparse:
                histList = [x for x in histList if len(x) > 1]
            self.hist_plot.saveFitSlicePlots(histList)

        for subList in histList:
            subList[0].Delete()

        return gr_mean, gr_width

    @staticmethod
    def _makeFitPretty(subList: list) -> None:
        """Change plot attributes.
        """
        histo = subList[0]
        fit = subList[1]

        fit.SetLineWidth(1)
        fit.SetLineColor(ROOT.kBlue)

        if fit.GetNpar() != 5:
            return

        # if fit is a function with 5 parameters plot extra gaussian on top?
        amp = fit.GetParameter(0) * fit.GetParameter(3)
        mean = fit.GetParameter(1)
        width = fit.GetParameter(4)
        second = ROOT.TF1(histo.GetName() + '_second',
                          '%f * TMath::Gaus(x, %f, %f)' % (amp, mean, width), -1, 1)

        second.SetLineWidth(1)
        second.SetLineColor(ROOT.kGreen + 1)
        subList.append(second)

    def _getFitPar(self, histo: ROOT.TH1, fname: str, par: int) -> tuple[float, float]:
        """Return the fit parameter value together with error

        Parameters
        ----------
        histo : TH1
            Histogram with fit
        fname : str
            Fit function name
        par : int
            Parameter index
        """
        # Avoid trying to call functions on a NULL fit
        if not histo:
            _LOG.warning("Cannot get function parameter for NULL histogram")
            return 0., 0.

        func = histo.GetFunction(fname)
        if not func:
            _LOG.warning("%s has no function named %s", histo.GetName(), fname)
            return 0., 0.

        return func.GetParameter(par), func.GetParError(par)

    @staticmethod
    def _calcCorr(p: float, dp: float, s: float, ds: float, factor: float = 0) -> list[float]:
        """
        Quadratic substraction and error propagation, with an optional systematic factor
        corr is the "true" sigma after the resolution has been subtraced
        dcorr is the error on our the "true" sigma
        scorr is a good question
        """
        if s > p:
            return [0, 0]

        if s < 0 or p < 0:
            return [0, 0]

        corr = math.sqrt(math.pow(p, 2) - math.pow(s, 2))
        a = math.pow(p * dp, 2)
        b = math.pow(s * ds, 2)
        c = math.pow(s * factor, 2)
        dcorr = math.sqrt((a + b)) / corr
        scorr = math.sqrt(c) / corr

        return [corr, dcorr, scorr]

    def _plotCorrVTrks(self, p_2: ROOT.TH1, s_2: ROOT.TH1, doResFit: bool) -> ROOT.TH1 | None:
        """
        Calculate the corrected width, per track multiplicity bin
        Expects histograms with the vtx sigma (p) and the single-vertex resolution (s)
        ## This relies on  no one else fitting to these histograms before cvt
        """
        if None in (p_2, s_2):
            return None

        output: ROOT.TH1 = p_2.Clone()
        output.Reset()

        # Copy of split vertices histogram, so that the resolution fit
        # is only displayed in this specific RGB plot
        s_2_clone: ROOT.TH1 = s_2.Clone()

        nbins = p_2.GetNbinsX()

        title = "Resolution"
        if doResFit:

            # have to reset parameters otherwise fit may not converge
            fit = self.sqrtN
            for par in (0, 1):
                fit.ReleaseParameter(par)
                fit.SetParameter(par, 0.)
                fit.SetParError(par, 0.)

            resFitResult = s_2_clone.Fit("sqrtN", self.fitOpts + 'BS', "", self.config.minFitRes,
                                         self.config.maxTrk)
            resFit = s_2_clone.GetFunction("sqrtN")
            ndf = resFitResult.Ndf() if resFitResult.Get() else 0
            if self._fitFailed(resFit, s_2_clone) or ndf < self.config.minFitNdf:
                _LOG.info('%s fit failed, retry with minTrk=10', title)
                resFitResult = s_2_clone.Fit("sqrtN", self.fitOpts + 'BS', "", 10, self.config.maxTrk)
                resFit = s_2_clone.GetFunction("sqrtN")
                ndf = resFitResult.Ndf() if resFitResult.Get() else 0
            if self._fitFailed(resFit, s_2_clone) or ndf < self.config.minFitNdf:
                _LOG.info('%s fit failed again', title)
                _LOG.info('Falling back to bin-by-bin resolution correction')
                doResFit = False
            else:
                _LOG.info('Resolution Fit: p0 = %f, p1 = %f',
                          resFit.GetParameter(0), resFit.GetParameter(1))
                _LOG.info('Fit data points=%d, chi2=%s, ndf=%d, npar=%d',
                          resFitResult.FittedBinData().NPoints(),
                          resFitResult.Chi2(),
                          resFitResult.Ndf(),
                          resFitResult.NFreeParameters())

                # Calculate confidence interval for each bin
                edges = array.array('d', [p_2.GetBinLowEdge(hbin) for hbin in range(1, nbins + 1)])
                resArray = [resFit.Eval(edge) for edge in edges]
                # _LOG.debug('  edges: %s', edges)
                resEarray = array.array('d', [0.] * nbins)
                resFitResult.GetConfidenceIntervals(nbins, 1, 1, edges, resEarray, 0.683, False)
                # _LOG.debug('  resE: %s', resEarray)

        # Calculate the corrected width for each multiplicity bin
        for hbin in range(1, nbins + 1):
            _LOG.debug('Calculate corrected width for bin %d', hbin)
            raw = p_2.GetBinContent(hbin)
            rawE = p_2.GetBinError(hbin)
            _LOG.debug('  raw width %f +- %f', raw, rawE)
            res: float | None = None
            resE: float | None = None
            if doResFit:
                res = resArray[hbin - 1]
                resE = resEarray[hbin - 1]
                _LOG.debug('  resolution from fit: %f +- %f', res, resE)

            # if no fit or fit produced unusable something try to use bin value
            if res is None or math.isnan(res) or res > raw or res == 0:
                resBin = s_2.FindBin(p_2.GetBinLowEdge(hbin))
                res = s_2.GetBinContent(resBin)
                resE = s_2.GetBinError(resBin)
                _LOG.debug('  resolution from bin %d: %f +- %f', resBin, res, resE)

            assert res is not None
            if math.isnan(res) or res > raw or res == 0:
                continue

            assert resE is not None
            corr = self._calcCorr(raw, rawE, res, resE)
            _LOG.debug('  corrected width: %f +- %f', corr[0], corr[1])

            output.SetBinContent(hbin, corr[0])
            output.SetBinError(hbin, corr[1])

        title = "Corrected width pol0"
        output.Fit("pol0", self.fitOpts + 'BS', "",
                   self.config.minFitWidth, self.config.maxTrk)
        corrwidthFit = output.GetFunction("pol0")
        if self._fitFailed(corrwidthFit, output):
            _LOG.debug('%s fit failed', title)
            output.Fit("pol0", self.fitOpts + 'BS', "",
                       self.config.minTrk, self.config.maxTrk)
            corrwidthFit = output.GetFunction("pol0")
        if self._fitFailed(corrwidthFit, output):
            _LOG.debug('%s fit failed again', title)
        else:
            corrwidthFit.SetLineColor(ROOT.kGreen)
            _LOG.debug('Corrected Width Fit: pol0 = %f', corrwidthFit.GetParameter(0))

        if not self.config.beQuiet:

            #         histo, color, showStat
            graphs = [(p_2, 4, False),
                      (s_2_clone, 2, True),
                      (output, 3, True)]
            if self.hist_plot:
                self.hist_plot.plotRedGreenBlack(graphs, suffix='_redgreenblack',
                                                 options=self.config.drawOpts)

            for gr in graphs:
                gr[0].Write()

        return output

    def _plotCorrVTrks_orig(self, p_2: ROOT.TH1, s_2: ROOT.TH1) -> ROOT.TH1:
        """
        Calculate the corrected width, per track multiplicity bin
        Expects histograms with the vtx sigma (p) and the single-vertex resolution (s)
        ## This relies on  no one else fitting to these histograms before cvt
        """
        if None in (p_2, s_2):
            return None

        p_2 = p_2.Clone()
        p_2.SetTitle(p_2.GetTitle() + ' (original/obsolete)')
        output: ROOT.TH1 = p_2.Clone()
        output.Reset()

        # Calculate the corrected width for each multiplicity bin
        for hbin in range(2, p_2.GetNbinsX()):
            raw = p_2.GetBinContent(p_2.FindBin(hbin))
            rawE = p_2.GetBinError(p_2.FindBin(hbin))
            res = s_2.GetBinContent(s_2.FindBin(hbin))
            resE = s_2.GetBinError(s_2.FindBin(hbin))

            if res > raw or res == 0:
                continue

            corr = self._calcCorr(raw, rawE, res, resE)

#            print hbin, raw, rawE, res, resE, corr[0], corr[1]
            output.SetBinContent(output.FindBin(hbin), corr[0])
            output.SetBinError(output.FindBin(hbin), corr[1])

        title = "Corrected width pol0"
        output.Fit("pol0", self.fitOpts + 'BS', "", self.config.minFitWidth, self.config.maxTrk)
        corrwidthFit = output.GetFunction("pol0")
        if self._fitFailed(corrwidthFit, output):
            _LOG.debug('_plotCorrVTrks_orig: %s fit failed', title)
            output.Fit("pol0", self.fitOpts + 'BS', "",
                       self.config.minTrk, self.config.maxTrk)
            corrwidthFit = output.GetFunction("pol0")
        if self._fitFailed(corrwidthFit, output):
            _LOG.debug('_plotCorrVTrks_orig: %s fit failed again', title)
        else:
            corrwidthFit.SetLineColor(ROOT.kGreen)
            _LOG.debug('_plotCorrVTrks_orig: Corrected Width Fit: pol0 = %f', corrwidthFit.GetParameter(0))

        if not self.config.beQuiet:

            #         histo, color, showStat
            graphs = [(p_2, 4, False),
                      (s_2, 2, False),
                      (output, 3, True)]
            if self.hist_plot:
                self.hist_plot.plotRedGreenBlack(graphs, suffix='_redgreenblack_orig',
                                                 options=self.config.drawOpts)

            for gr in graphs:
                gr[0].Write()

        return output

    def getBCIDValues(self, allHistSlices: dict[str, dict[str, ROOT.TH1]],
                      output: model.BSSnapshot) -> list[model.BSSnapshot]:
        """Calculate per-BCID snapshots.

        Parameters
        ----------
        allHistSlices : dict
            Dictionary where key is the axis name (e.g. 'X') and value
            is another dictionary, the key for that is "binding" string
        output : dict
            Snapshot dictionary, it is modified by this method.

        Returns
        -------
        List of snapshots
        """
        _LOG.info("Calculating per-BCID snapshots")

        # fill with defaults, we allow bunch 0 here too
        bcids = []
        for bcid in range(self.config.nBunches + 1):
            snap = model.BSSnapshot(status=STATUS_OK, bcid=bcid)
            bcids.append(snap)

        _LOG.debug("Made %d snapshots", len(bcids))

        for direction in 'XYZ':

            outputSlice = output.slice(direction)

            # set the same resolution for all BCIDs
            res = outputSlice.resBCID
            resErr = outputSlice.resBCIDErr
            for snap in bcids:
                snapSlice = snap.slice(direction)
                snapSlice.res = res
                snapSlice.resErr = resErr

            # get individual fields from histograms
            graphs = {'delta': allHistSlices[direction]['bcidDelta'],
                      'sigma': allHistSlices[direction]['bcidSigma'],
                      'unCorr': allHistSlices[direction]['bcidUnCorr']}
            for key, histo in graphs.items():
                # scan all bins, copy value and error
                for ibin in range(1, histo.GetNbinsX() + 1):
                    # get BCID from the histogram axis definition
                    bcid = int(histo.GetXaxis().GetBinCenter(ibin))
                    if bcid < 0 or bcid >= len(bcids):
                        _LOG.warning("Unexpected BCID value %d for bin %d in histogram %s",
                                     bcid, ibin, histo.GetName())
                    else:
                        setattr(bcids[bcid], key + direction, histo.GetBinContent(ibin))
                        setattr(bcids[bcid], key + direction + 'Err', histo.GetBinError(ibin))

        # Do corrections if needed
        if self.config.doBCIDCorrection:
            if not self.correctBCIDs(output, bcids):
                output.status = STATUS_NOT_CONVERGED

        # update status and filter out missing stuff
        _LOG.debug("Filter BCID snapshots")
        filtered = []
        for snap in bcids:
            filled = [getattr(snap, f + d + e) not in (None, 0.)
                      for f in ('delta', 'unCorr', 'sigma')
                      for d in 'XYZ'
                      for e in ('', 'Err')]
            if not all(filled):
                snap.status = STATUS_NOT_CONVERGED
            if any(filled):
                filtered.append(snap)
        bcids = filtered

        _LOG.debug("Done calculating per-BCID snapshots")

        return bcids

    def oneDimCalcs(self, histSlice: dict[str, ROOT.TH1], direction: str,
                    output: model.SliceView) -> None:
        """Calculate values for single dimension.

        This method updates both `allHists` and `output` dictionaries.

        Parameters
        ----------
        histSlice : dict
            Dictionary where key is the histogram "binding" name and value
            is a histogram object
        direction : str
            Dimension axis name (e.g. 'X')
        output : model.SliceView
            Snapshot slice for this dimension
        """

        # ######## Global Width Correction ##############
        histSlice['unCorr'] = self._fitSlices(histSlice['ntrks'],
                                              code="ntrks",
                                              axis=direction,
                                              projAxis=self.config.projectAxis)[1]

        histSlice['res'] = self._fitSlices(histSlice['split_ntrks'],
                                           code="split_ntrks",
                                           axis=direction,
                                           projAxis=self.config.projectAxis)[1]
        if histSlice['res']:
            # scale it for single-vertex resolution
            histSlice['res'].Scale(1 / _SQRT2)

        # # This relies on  no one else fitting to these histograms before cvt
        histSlice['sigma'] = self._plotCorrVTrks(histSlice['unCorr'],
                                                 histSlice['res'],
                                                 self.config.resFit)
        histSlice['sigma_orig'] = self._plotCorrVTrks_orig(histSlice['unCorr'],
                                                           histSlice['res'])

        # ######## Per BCID Width Correction ##############
        if self.config.doBCIDs:
            meanGraph, widthGraph = self._fitSlices(histSlice['bcid'],
                                                    code="bcid",
                                                    axis=direction,
                                                    projAxis=0,
                                                    firstBin=0,
                                                    lastBin=self.config.nBunches,
                                                    sparse=False,
                                                    xtitle='BCID')
            histSlice['bcidDelta'] = meanGraph
            histSlice['bcidUnCorr'] = widthGraph
#             if self.hist_plot:
#                 self.hist_plot.plotHisto(meanGraph, True)
#                 self.hist_plot.plotHisto(widthGraph, True)

        if histSlice['res'] and histSlice['split_ntrks'] and histSlice['ntrks']:
            resName = histSlice['res'].GetName()
            maxBin = histSlice['split_ntrks'].GetXaxis()
            maxBin = maxBin.FindBin(self.config.bcidMinTrk)
            histSlice['resBCID'] = projectNTrkRange(histSlice['split_ntrks'],
                                                    'X', self.config.bcidMinTrk, -1,
                                                    f'{resName}_BCID',
                                                    histSlice['ntrks'])
#             if self.hist_plot:
#                 self.hist_plot.plotHisto(histSlice['unCorr'], True)
#                 self.hist_plot.plotHisto(histSlice['res'], True)
#                 self.hist_plot.plotHisto(histSlice['resBCID'], True)

        mins: dict[str, float | None] = {}
        maxs: dict[str, float | None] = {}
        ranges = [('pos', self.config.nRMS),
                  ('delta', self.config.nRMS),
                  ('resBCID', self.config.nRMSBCID)]
        for var, nRMS in ranges:
            if histSlice.get(var):
                mean, rms, _ = self._getHistSummary(histSlice[var])
                mins[var] = mean - nRMS * rms
                maxs[var] = mean + nRMS * rms
            else:
                mins[var] = None
                maxs[var] = None

        fits = (('pos', 'gaus', 1, mins['pos'], maxs['pos']),
                ('delta', 'gaus', 1, mins['delta'], maxs['delta']),
                ('unCorr', 'pol0', 0, self.config.minTrk, self.config.maxTrk),
                ('sigma', 'pol0', 0, self.config.minFitWidth, self.config.maxTrk),
                ('res', 'pol0', 0, self.config.minTrk, self.config.maxTrk),
                ('resBCID', 'gaus', 2, mins['resBCID'], maxs['resBCID']))

        for outputKey, fitType, fitParam, fitMin, fitMax in fits:
            if histSlice.get(outputKey):
                histSlice[outputKey].Fit(fitType, self.fitOpts, "", fitMin, fitMax)
                histSlice[outputKey].SetTitle('%s %s' % (outputKey, direction))
                value, valueErr = self._getFitPar(histSlice[outputKey], fitType, fitParam)
                _LOG.debug("outputKey=%s value=%s valueErr=%s", outputKey, value, valueErr)
                setattr(output, outputKey, value)
                setattr(output, outputKey + 'Err', valueErr)
#                 if self.hist_plot:
#                     self.hist_plot.plotHisto(histSlice[outputKey], True)

        # In case sigmaZ fit failed (e.g. due to poor statistics) we can try to
        # guess it from full distribution, because corrections are small.
        if direction == "Z" and output.sigma == 0.0:
            histo = histSlice["pos"]
            if histo.GetEntries() > 3:
                output.sigma = histo.GetRMS()
                output.sigmaErr = histo.GetRMSError()
                _LOG.debug("Updating sigmaZ from posZ: %s +- %s", output.sigma, output.sigmaErr)

        if output.resBCID:
            # make it a single-vertex resolution
            output.resBCID /= _SQRT2
            output.resBCIDErr /= _SQRT2

        if self.config.doBCIDs:
            if output.resBCID:
                resBCID = ROOT.TH1D('h_resBCID', "root....", self.config.nBunches + 1,
                                    0, self.config.nBunches + 1)

                resBCIDVal = output.resBCID
                resBCIDErr = output.resBCIDErr
                for i in range(self.config.nBunches + 1):
                    resBCID.SetBinContent(i, resBCIDVal)
                    resBCID.SetBinError(i, resBCIDErr)

                histSlice['bcidSigma'] = self._plotCorrVTrks(histSlice['bcidUnCorr'],
                                                             resBCID,
                                                             False)

    def calcOnHists(self, hists: dict[str, ROOT.TH1], lbn: int | None) -> model.BSSnapshot:
        """Calculate snapshot from set of histograms.

        Parameters
        ----------
        hists : dict
            Dictionary where key is the short name of the histogram (e.g.
            "posX") and the value is the histogram itself.
        lbn : int
            Approximate LBN, used for publishing of histograms. Use -1 for EoR.

        Returns
        -------
        snap : `model.BSSnapshot`
            Measurement snapshot.
        """
        allHistSlices: dict[str, dict[str, ROOT.TH1]] = {'X': dict(), 'Y': dict(), 'Z': dict()}

        # map hdef names to our internal names in allHistSlices
        slice_names = {}
        for axis in "XYZ":
            slice_names[f"pos{axis}"] = ("pos", axis)
            slice_names[f"pos{axis}_vs_ntrks"] = ("ntrks", axis)
            slice_names[f"split{axis}_vs_ntrks"] = ("split_ntrks", axis)
            slice_names[f"delta{axis}_bcid"] = ("bcid", axis)
            slice_names[f"tilt{axis}"] = ("tilt", axis)

        for hdef in self.hdefs:
            if hdef.name in slice_names:
                histo = hists.get(hdef.name)
                if histo and hdef.title:
                    histo.SetTitle(hdef.title)
                code, axis = slice_names[hdef.name]
                _LOG.debug("adding code=%r axis=%r -> %r", code, axis, hdef.name)
                allHistSlices[axis][code] = histo

        h_XvsZ_profile = allHistSlices["X"]["tilt"]
        h_YvsZ_profile = allHistSlices["Y"]["tilt"]

        if h_XvsZ_profile:
            h_XvsZ_profile.SetXTitle("Vertex z [mm]")
            h_XvsZ_profile.SetYTitle("Vertex x [mm]")
        if h_YvsZ_profile:
            h_YvsZ_profile.SetXTitle("Vertex z [mm]")
            h_YvsZ_profile.SetYTitle("Vertex y [mm]")

        output = model.BSSnapshot()

        directionsToDo = 'XYZ'  # useful for fast debugging
        for axis in directionsToDo:
            self.oneDimCalcs(allHistSlices[axis], axis, output.slice(axis))

        if self.config.doBCIDs:
            output.bcids = self.getBCIDValues(allHistSlices, output)

        # h_X = allHistSlices['X']['pos']
        h_Z = allHistSlices['Z']['pos']

        if h_XvsZ_profile:
            h_XvsZ_profile.Fit("pol1", self.fitOpts, "same",
                               h_Z.GetMean() - self.config.nRMSTilt * h_Z.GetRMS(),
                               h_Z.GetMean() + self.config.nRMSTilt * h_Z.GetRMS())
        if h_YvsZ_profile:
            h_YvsZ_profile.Fit("pol1", self.fitOpts, "same",
                               h_Z.GetMean() - self.config.nRMSTilt * h_Z.GetRMS(),
                               h_Z.GetMean() + self.config.nRMSTilt * h_Z.GetRMS())

#         h_NTrkvsX = allHistSlices['X']['ntrks']
#         h_NTrkvsDX = allHistSlices['X']['split_ntrks']
#         nPVtx = h_NTrkvsX.Integral(self.config.minTrk, self.config.maxTrk + 1, 0,
#                                    h_NTrkvsX.GetYaxis().GetNbins())
#         nSVtx = h_NTrkvsDX.Integral((2 * self.config.minTrk) + 1,
#                                     (2 * self.config.maxTrk) + 3, 0,
#                                     h_NTrkvsDX.GetYaxis().GetNbins())

        if not self.config.beQuiet and self.hist_plot:

            for axis in directionsToDo:
                self.hist_plot.oneDimDraw(allHistSlices[axis], axis, self.config.doBCIDs)

            if h_XvsZ_profile:
                h_XvsZ_profile.SetStats(True)
                saveAs = ["Online_XvsZ_tilt" + "_" + self.config.outExt]
                self.hist_plot.plotHisto(h_XvsZ_profile, saveAs=saveAs)

            if h_YvsZ_profile:
                h_YvsZ_profile.SetStats(True)
                saveAs = ["Online_YvsZ_tilt" + "_" + self.config.outExt]
                self.hist_plot.plotHisto(h_YvsZ_profile, saveAs=saveAs)

        # if h_X:
        #     output.nvtx = h_X.GetEntries()
        if h_XvsZ_profile:
            output.tiltX, output.tiltXErr = self._getFitPar(h_XvsZ_profile, "pol1", 1)
        if h_YvsZ_profile:
            output.tiltY, output.tiltYErr = self._getFitPar(h_YvsZ_profile, "pol1", 1)

        output.status = STATUS_OK
        failed = set()
        # Some sanity tests
        for field, axes in (('pos', 'XYZ'), ('sigma', 'XYZ'), ('tilt', 'XY')):
            for axis in axes:
                col = field + axis
                badColumn = output.emptyPhysicsField(col)
                if badColumn:
                    failed.add(col)
                    output.status = STATUS_PROBLEMATIC

        if output.status == STATUS_PROBLEMATIC:
            _LOG.info("Setting status to 5 (STATUS_PROBLEMATIC)!  These fields failed {%s}",
                      ', '.join(s for s in sorted(failed)))

        if self.oh_publisher:
            try:
                for axis, hdict in allHistSlices.items():
                    for hname, hobj in hdict.items():
                        name = hname + '_' + axis.lower()
                        if lbn != -1:
                            hobj.SetTitle('{} {} (LBN={})'.format(hname, axis, lbn))
                        self.oh_publisher.publish(hobj, name, lbn)
                if h_XvsZ_profile:
                    self.oh_publisher.publish(h_XvsZ_profile, "x_vs_z", lbn)
                if h_YvsZ_profile:
                    self.oh_publisher.publish(h_YvsZ_profile, "y_vs_z", lbn)
            except Exception as exc:
                # this is not critical
                ers.warning(HistoPublishError(exc))
            self.oh_publisher.finish()

        return output

    @staticmethod
    def correctBCIDs(results: model.BSSnapshot, bcids: list[model.BSSnapshot],
                     func: Callable[[float, float, float], float] = _bcidCorr) -> bool:
        """
        Correct the mean value of the perBCID results such that they are
        compatible with the result on the global distributions.
        results is the dictionary

        Parameters
        ----------
        results : `model.BSSnapshot`
            Snapshot for global beamspot produced by `calcResults`.
        bcids : list of `model.BSSnapshot`
            Per-BCID snapshots, modified in place.
        func : callable, optional
            func returns new values for sigma and sigmaErr given the per-BCID
            value, the average per-BCID value, and the value from the global
            fit.
        """

        _LOG.debug("Do BCID corrections")

        # Hardcode these for now
        nBins = 500
        minX = -2
        maxX = 2
        amplitude = 1

        bcidAverageVtx = {}
        globalVtx = {}

        bcidWidth = {}
        globalWidth = {}

        for axis in ("X", "Y", "Z"):
            factor = 1 if axis != "Z" else 100
            bcidAverageVtx[axis] = ROOT.TH1F("bcidAverageVtx%s" % axis,
                                             "bcidAverageVtx%s" % axis,
                                             nBins, factor * minX, factor * maxX)

            globalVtx[axis] = ROOT.TH1F("globalVtx%s" % axis,
                                        "globalVtx%s" % axis,
                                        nBins, factor * minX, factor * maxX)

        _LOG.debug("Fill bcidAverageVtx")
        for params in bcids:
            for axis in bcidAverageVtx:
                nbins = int(bcidAverageVtx[axis].GetNbinsX())
                pslice = params.slice(axis)
                mean = pslice.delta
                width = pslice.sigma
                if width == 0:
                    continue

                tmpHisto = ROOT.TH1F("tmp", "tmp", nbins,
                                     bcidAverageVtx[axis].GetBinLowEdge(1),
                                     bcidAverageVtx[axis].GetBinLowEdge(nbins + 1))

                fillGauss(tmpHisto, mean, width, amplitude)
                bcidAverageVtx[axis].Add(tmpHisto)

        _LOG.debug("Fit bcidAverageVtx and globalVtx")
        for axis in globalVtx:
            rslice = results.slice(axis)
            fillGauss(globalVtx[axis], float(rslice.pos), float(rslice.sigma), 1)

            bcidAverageVtx[axis].Fit("gaus", "Q")
            globalVtx[axis].Fit("gaus", "Q")

            bcidFit = bcidAverageVtx[axis].GetFunction("gaus")
            globalFit = globalVtx[axis].GetFunction("gaus")

            if not bcidFit or not globalFit:
                return False

            bcidWidth[axis] = bcidFit.GetParameter(2) / math.sqrt(2.)
            globalWidth[axis] = globalFit.GetParameter(2) / math.sqrt(2.)

        _LOG.debug("Update per-BCID snapshots")
        for params in bcids:
            for axis in ("X", "Y", "Z"):
                pslice = params.slice(axis)

                pslice.sigma = func(pslice.sigma, globalWidth[axis], bcidWidth[axis])
                pslice.sigmaErr = func(pslice.sigmaErr, globalWidth[axis], bcidWidth[axis])

        _LOG.debug("Finished BCID corrections")

        return True

    def calc(self, hist_trees: Mapping[int, htree.HTree],
             run: int | None = None,
             lbn: int | None = None) -> model.BSSnapshot | None:
        # docstring inherited from base class

        if not hist_trees:
            return None

        # merge all LBNs
        if len(hist_trees) > 1:
            hist_tree, merged_lbns = self.lbn_merge.merge(hist_trees, self.hdefs)
            fetch_lbn = None
        else:
            fetch_lbn, hist_tree = next(iter(hist_trees.items()))
            if fetch_lbn < 0:
                fetch_lbn = None

        # Check statistics
        histo_vtxx = hist_tree.get_histogram('posX', fetch_lbn)
        if not histo_vtxx:
            _LOG.warning("Could not find critical histogram for LBN %s", lbn)
            return None
        nVtx = histo_vtxx.Integral()
        if nVtx < self.config.minNVtx:
            _LOG.warning("Not enough stats for LBNs %d (%d < %d)", lbn, nVtx, self.config.minNVtx)
            return None
        else:
            _LOG.info("Total vertex count: %d", nVtx)

        if not self.config.beQuiet:
            outfile = ROOT.TFile(os.path.join(self.output_dir, f"{self.config.outBase}.root"), "RECREATE")
            outfile.cd()

        hists = dict()

        # Get them from the input tfile
        for hdef in self.hdefs:
            hists[hdef.name] = hist_tree.get_histogram(hdef.name, fetch_lbn)

        if None in hists.values():
            # some histograms are missing
            _LOG.info("Could not find necessary histograms")
            return None

        snap = self.calcOnHists(hists, lbn)
        if run is not None:
            snap.run = run
        if lbn is not None:
            snap.lbn = lbn

        _LOG.info("Fit result")
        snap.prettyLog(_LOG)

        self._publish(snap)

#         if self.config.doBCIDCorrection and self.config.doBCIDs:
#             if not self.correctBCIDs(output):
#                 snap.status = STATUS_NOT_CONVERGED

        return snap

    def _publish(self, snap: model.BSSnapshot) -> None:
        """Publish snapshot(s).

        Parameters
        ----------
        snap : `model.BSSnapshot`
            Measurement snapshot.
        """
        mon_output = self.config.mon_output
        if not mon_output:
            return
        trans = translators.make_translator(mon_output, do_bcid=self.config.doBCIDs)

        bsRepr = model.BeamSpotRepresentation([snap])
        _LOG.info("Writing out a representation to %r", mon_output)
        try:
            trans.write(bsRepr)
        except Exception as exc:
            # do not make error message to avoid shifter confusion
            _LOG.info("Publication to %r failed: %s", mon_output, exc, exc_info=True)
