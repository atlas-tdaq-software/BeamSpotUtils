"""Module contains methods for finding histograms in files.

This module encapsulates knowledge about folder structures in various types
of ROOT files coming from different sources (e.g. ohcp or MDA). Currently we
support MDA-produced structure, oh_cp files made in "offline" mode, "offline"
files produced by offline monitoring code, and "BST" files produced by
beamSpotTool.

BST normally uses per-LB histograms produced by HLT, but can also use
"run summary" histograms for some of them. Run summary histograms are the same
histograms but containing statistics for the whole run instead of a single LB.
For run2 all per-LB histograms had their corresponding run summary histograms
filled and saved automatically. Run3 situation was more complicated:

  - in 2022 HLT produced only per-LB histograms, but did not make any run
    summary histograms.

  - for 2023 we manually configured split vertex histograms (e.g.
    splitX_vs_ntrks) to fill run summary histograms in addition to per-LB
    histograms.

MDA histograms
--------------

For MDA files histograms are stored following these rules:

  - regular histograms and run summary histograms (with IS tag=-1) are stored
    using their full name in the directory /$ServerName/$ProviderName/, if any
    part of the path contains colons they are replaced with underscores.
    Example of the run summary histogram path in MDA file:
    /Histogramming/TopMIG-OH_HLT/EXPERT/T2VertexBeamSpot_FTF/VertexXPass_runsummary

  - per-LB histograms are saved by prefixing full histogram name with directory
    /run_$run/lb_$tag/$ServerName/$ProviderName/ ("run_" and "lb_" strings are
    literals, $tag is the LB number). Per-LB histogram names start with "LB/".
    Example of per-LB histogram name:
    /run_313259/lb_98/Histogramming/TopMIG-OH_HLT/LB/EXPERT/T2VertexBeamSpot_FTF/VertexXPass

  - latest versions of per-LB histograms (from latest LB, as opposed to run
    summary) are also saved under /$ServerName/$ProviderName/ folder, note that
    per-LB histograms have "LB/" component in their path, which run summary
    histograms do not have. Example of per-LB histograms saved in this way:
    /Histogramming/TopMIG-OH_HLT/LB/EXPERT/T2VertexBeamSpot_FTF/VertexXPass

  - file can contain more than one provider, and possibly more than one server,
    though for HLT monitoring we store one server (Histogramming) and one
    provider.

  - top-level folder can contain other internal folders, e.g. "/.meta".

Patterns for paths that should appear in MDA files (for run2):

  - {server}/{provider}/{detail}/{chain}/{name}_runsummary
  - {server}/{provider}/LB/{detail}/{chain}/{name}
  - run_{run}/lb_{lbn}/{server}/{provider}/LB/{detail}/{chain}/{name}

Patterns for run3:

  - {server}/{provider}/LB/{detail}/{chain}/{name}
  - run_{run}/lb_{lbn}/{server}/{provider}/LB/{detail}/{chain}/{name}


OHCP in offline mode
--------------------

For files produced by oh_cp in "offline" mode (with -O or --use-offline option,
this is what BeamSpotTool uses) the rules are:

  - all histograms are saved under /run_$run/lb_$tag/$ServerName/$ProviderName/

  - if any folder has colon in its name it is replaced with underscore, this
    includes server and provider name and any part of a histogram name which
    corresponds to folder (but does not include part of histogram name after
    last slash).

  - run summary histogram have tag equal to -1, they are saved in folder
    /run_$run/lb_-1/$ServerName/$ProviderName/, example:
    /run_319249/lb_-1/Histogramming/TopMIG-OH_HLT/EXPERT/T2VertexBeamSpot_FTF/VertexXPass_runsummary

  - per-LB histograms are saved similarly to MDA, example:
    /run_319249/lb_711/Histogramming/TopMIG-OH_HLT/LB/EXPERT/T2VertexBeamSpot_FTF/VertexXPass

  - file can contain more than one provider and more than one server.

Patterns for paths that should appear in oh_cp files (run summary can be
missing in run3):

  - run_{run}/lb_-1/{server}/{provider}/{detail}/{chain}/{name}_runsummary
  - run_{run}/lb_{lbn}/{server}/{provider}/LB/{detail}/{chain}/{name}


Offline expert-monitoring
-------------------------

For offline-produced files in expert-monitoring.root files the rules are:

  - for run2 there are no per-LB histograms, all per-LB histograms go to
    lbn-monitoring file, see below

  - top-level folder contains chain folders (no server/provider in offline),
    example of a histogram name:
    /T2VertexBeamSpot_FTF/TotalTracks

  - run summary histograms are saved with "_runsummary" suffix during run1 and
    run2, e.g. /T2VertexBeamSpot_FTF/VertexXPass_runsummary

  - starting with r22/run3 per-LB histograms saved with "_LB<n>" suffix, e.g.
    /T2VertexBeamSpot_FTF/VertexXPass_LB55, range of LBs can be controlled
    when declaring histograms in Python configuration (e.g. kLBNHistoryDepth=3)

  - since run3 run summary histograms are not created for per-LB histograms,
    but we could declare those histograms separately

Patterns for paths that should appear in offline files (for run2):

  - {chain}/{name}_runsummary

Pattern for run3:

  - {chain}/{name}_{lbn}


Offline lbn-monitoring
----------------------

For offline-produced per-LB histograms in lbn-monitoring.root:

  - top-level folder is "lbn_{lbn}", in run2 HLT could only save single LBN
    in output root file (and it had to run on a file from single LB) but it
    could produce folders for LBS with different numbers for TIMERS histograms.

  - folders below lbn_{lbn} are "LB/EXPERT/T2VertexBeamSpot_FTF"

  - in r22/run3 per-LB histograms are stored together with all other histograms
    in expert-monitoring.root, see above

Patterns for paths that should appear in HLT files (for run2):

  - lb_{lbn}/LB/{detail}/{chain}/{name}

No data is saved for run3.


BeamSpotTool histograms
-----------------------

BeamSpotTool-produced files is just a bunch of histograms in top-level folder,
there is no directory structure.

OHCP in online mode
-------------------

For completeness oh_cp in regular mode (without -O or --use-offline option)
uses following rules:

  - all histograms are saved under /$ServerName/$ProviderName/ folder, per-LB
    histograms use cycle numbers equal to IS tag number

  - same colon replacement rule

  - run summarry histograms are stored normally, their cycle should be 1,
    example:
    /Histogramming/TopMIG-OH_HLT/EXPERT/T2VertexBeamSpot_FTF/VertexXPass_runsummary;1

  - per-LB histograms use cycle number for a tag, example:
    /Histogramming/TopMIG-OH_HLT/LB/EXPERT/T2VertexBeamSpot_FTF/VertexXPass;711

  - file can contain more than one provider and more than one server.
"""

from __future__ import annotations

import abc
from collections import defaultdict
import enum
import logging
import re
from collections.abc import Iterator, Mapping
from typing import Any, Generic, Protocol, TypeVar

from . import hist_def, rootil
from .freaking_ROOT import ROOT


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


class HTreeType(enum.Enum):
    """Known file types
    """
    MDA = "mda"
    OHCP_OFF = "ohcp_ofline"
    OFFLINE = "off"
    BST = "bst"
    OFFLINE_LBN = "offlbn"


class HTreeVariant(enum.Enum):
    """Class for enumerating different variants of histogram naming structure.

    During the whole data taking period there were different conventions for
    naming histograms and folders. This class is an attempt to enumerate
    different conventions into just few distingushed cases. These variants
    mostly cover convetion for histogram names, folder naming also depends on
    where and when ROOT file is produced.
    """
    A = "A"
    B = "B"
    R3 = "R3"

    @classmethod
    def from_run(cls, run: int) -> HTreeVariant:
        """Guess histogram naming periods from run number.

        Currently known run ranges:
          - A: up to run 188800 (Histogram names changed at
            TrigT2BeamSpot-00-08-00, this went online at run 188801)
          - B: to the end of run2 (ended with run 373xxx)
          - R3: run3

        There was aperiod in 2020 when we had runs from different releases but
        no beamspot data were produced. It is safe to assume that period R3
        started arounf run 375000.
        """
        if run < 188801:
            return cls.A
        elif run < 375000:
            return cls.B
        else:
            return cls.R3


def _names_A() -> dict[str, str]:
    """Return mapping of the names to ROOT names for naming variant "A".
    """
    names = {}
    for axis in "XYZ":
        names[f"pos{axis}"] = f"Vertex{axis}Pass"
        names[f"delta{axis}"] = f"Vertex{axis}ZoomPass"
        names[f"pos{axis}_vs_ntrks"] = f"VertexNTrksPass_vs_Vertex{axis}Pass"
        names[f"split{axis}_vs_ntrks"] = f"SplitVertexDNTrksPass_vs_SplitVertexD{axis}Pass"
        names[f"delta{axis}_bcid"] = f"BCID_vs_Vertex{axis}ZoomPass"
        names[f"pos{axis}_bcid"] = f"BCID_vs_Vertex{axis}Pass"
    for axis in "XY":
        names[f"tilt{axis}"] = f"VertexZPass_vs_Vertex{axis}Pass"
    # these did not exist during run1/2, but adding them anyways for uniformity
    names["beam_ls"] = "BeamLSMatrices"
    names["beam_ls_bcid"] = "BeamLSMatricesBCID"
    names["llpoly"] = "TrackLLPolyCoeff"
    return names


def _names_B() -> dict[str, str]:
    """Return mapping of the names to ROOT names for naming variant "B".
    """
    names = _names_A()
    for axis in "XYZ":
        names[f"delta{axis}_bcid"] = f"BCID_vs_Vertex{axis}ZoomPassBCID"
        names[f"pos{axis}_bcid"] = f"BCID_vs_Vertex{axis}PassBCID"
    return names


def _names_R3() -> dict[str, str]:
    """Return mapping of the names to ROOT names for naming variant "R3".
    """
    names = {}
    for axis in "XYZ":
        names[f"pos{axis}"] = f"Vertex{axis}Pass"
        names[f"delta{axis}"] = f"Vertex{axis}ZoomPass"
        names[f"pos{axis}_vs_ntrks"] = f"Vertex{axis}Pass_vs_VertexNTrksPass"
        names[f"split{axis}_vs_ntrks"] = f"SplitVertexD{axis}Pass_vs_SplitVertexDNTrksPass"
        names[f"delta{axis}_bcid"] = f"Vertex{axis}ZoomPassBCID_vs_BCID"
        names[f"pos{axis}_bcid"] = f"Vertex{axis}PassBCID_vs_BCID"
    for axis in "XY":
        names[f"tilt{axis}"] = f"Vertex{axis}Pass_vs_VertexZPass"
    # these did not exist during run1/2, but adding them anyways for uniformity
    names["beam_ls"] = "BeamLSMatrices"
    names["beam_ls_bcid"] = "BeamLSMatricesBCID"
    names["llpoly"] = "TrackLLPolyCoeff"
    return names


_stored_names = {
    HTreeVariant.A: _names_A(),
    HTreeVariant.B: _names_B(),
    HTreeVariant.R3: _names_R3(),
}


def _guess_htree_type(tfile: ROOT.TFile, server: str, chain: str) -> HTreeType | None:
    """Try to determine file structure type.

    Parameters
    ----------
    tfile : TFile
        ROOT TFile instance.
    server : str
        OH server name.
    chain : str
        Trigger chain name, or rather algorithm and tool name that produced
        histograms. This could be a series of names separates by slashes.

    Returns
    -------
    type : `HTreeType`
        Enum with the file type.
    variant : `HTreeVariant` or `None`
        Histogram naming variant, or None if it cannot be guessed.
    """
    _LOG.debug("_guess_htree_type: looking at file %s", tfile.GetName())
    _LOG.debug("_guess_htree_type: server = %s", server)
    _LOG.debug("_guess_htree_type: chain = %s", chain)

    chains = [name for name in chain.split('/') if name]
    _LOG.debug("_guess_htree_type: chain %s", chains)

    # get list of top-level folders
    folders = [str(key.GetName()) for key in tfile.GetListOfKeys() if key.IsFolder()]
    objects = [str(key.GetName()) for key in tfile.GetListOfKeys() if not key.IsFolder()]
    dot_folders = set([folder for folder in folders if folder[0] == '.'])
    run_folders = set([folder for folder in folders if folder.startswith('run_')])
    lbn_folders = set([folder for folder in folders if folder.startswith('lb_')])
    reg_folders = set(folders) - dot_folders - run_folders - lbn_folders
    _LOG.debug("_guess_htree_type: found folders %s", folders)
    _LOG.debug("_guess_htree_type: found objects %s", objects)

    # ohcp should have one folder with run_NNN and no objects at top level
    if len(folders) == 1 and len(run_folders) == 1 and not objects:
        _LOG.info("This is oh_cp-produced file")
        return HTreeType.OHCP_OFF

    # HLT lbn-monitoring.root should have one or more lbn_NNN folders at top level
    if len(lbn_folders) > 0 and len(folders) == len(lbn_folders) and not objects:
        _LOG.info("This is HLT lbn-monitoring file")
        return HTreeType.OFFLINE_LBN

    # BST writes temporary files where all histograms are in top-level folder
    if not folders and objects:
        _LOG.info("This is BST-produced file")
        return HTreeType.BST

    # MDA should have one server directory, run_ is optional
    if reg_folders == set([server]) and not objects:
        _LOG.info("This is MDA-produced file")
        return HTreeType.MDA

    # expert-monitoring file should be one or more chain directories but no
    # run_ directories
    if chains and chains[0] in reg_folders and not run_folders and not objects:
        _LOG.info("This is HLT expert-monitoring file")
        return HTreeType.OFFLINE

    _LOG.debug("Failed to determine file type, folders: %s", folders)
    return None


class HistoProtocol(Protocol):
    """Special protocol class that reflects part of TH1 interface"""
    def Delete(self) -> None:
        pass

    def SetDirectory(self, owner: Any) -> None:
        pass

    def Clone(self) -> HistoProtocol:
        pass

    def GetName(self) -> str:
        pass

    def GetEntries(self) -> int:
        pass

    def SetName(self, name: str) -> None:
        pass


T = TypeVar("T", bound=HistoProtocol)


class HTree(Generic[T]):
    """Abstract class representing histogram trees in a ROOT file.
    """

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def name(self) -> str:
        """Return file name or some other identifying name for the tree.

        Returns
        -------
        name : str
            Name of the file or some other identifier.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def get_run(self) -> int | None:
        """Return run number for this file if it can be determined.

        Returns
        -------
        run : int
            Run number or None if run cannot be determined
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def get_lbns(self) -> list[int]:
        """Return the list of LB numbers in this file.

        Returns
        -------
        lbns : list of int
            List of LB numbers.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def get_histogram(self, name: str, lbn: int | None = None) -> T | None:
        """Returns the histogram given its name and LB number.

        Parameters
        ----------
        name : str
            Histogram internal short name, e.g. "posX".
        lbn : int, optional
            If LBN is missing or None then per-run (or run-summary) histogram
            is retrieved, otherwise per-LB histogram for given LB is returned.

        Returns
        -------
        hist : TH1
            Histogram instance.
        """
        raise NotImplementedError()


class BaseHTree(HTree[T]):
    """Common implementation of HTree interface used for both MDA and ohcp.

    Parameters
    ----------
    tfile : TFile
        ROOT TFile instance.
    server : str, optional
        OH server name, if None then tries to guess it from file.
    provider : str, optional
        OH provider name, if None then tries to guess it from file.
    detail : str, optional
        Histogram detail level, e,g, "EXPERT".
    chain_vtx : str, optional
        Name of the algorithm/chain producing histograms for vertex method.
    chain_trk : str, optional
        Name of the algorithm/chain producing histograms for track method.
    """

    def __init__(self, tfile: ROOT.TFile, server: str | None = None, provider: str | None = None,
                 detail: str | None = None, chain_vtx: str | None = None,
                 chain_trk: str | None = None):
        self._tfile = tfile
        self._server = server
        self._provider = provider
        self._detail = detail
        self._chain_vtx = chain_vtx
        self._chain_trk = chain_trk

        # possible naming variants for this tree, this is updated as we look
        # at the histogram names
        self._variants = set(HTreeVariant)

        if (self._server and not self._provider) or (not self._server and self._provider):
            raise ValueError("only one of server or provider are given: "
                             f"server={self._server} provider={self._provider}")

    def _candidate_names(self, name: str) -> Mapping[str, set[HTreeVariant]]:
        """Make a set of histogram names.

        Parameters
        ----------
        name : str
            Histogram internal short name, e.g. "posX".

        Returns
        -------
        names : dict
            Dictionary where key is the histogram name as it appears in ROOT
            file and value is a set of variants for that name.
        """
        names = defaultdict(set)
        for variant in self._variants:
            root_name = _stored_names[variant].get(name)
            if root_name:
                names[root_name].add(variant)
        return names

    def _chain(self, name: str) -> str | None:
        """Return chain/algorithm name for a histogram.

        Parameters
        ----------
        name : str
            Histogram internal short name, e.g. "posX".

        Returns
        -------
        chain : str
            Returns on of self._chain_vtx or self._chain_trk depending on the
            name of a histogram.
        """
        if name in hist_def.names_vtx():
            return self._chain_vtx
        elif name in hist_def.names_trk():
            return self._chain_trk
        else:
            raise ValueError(f"Unexpected histogram name: {name}")

    def _algoFolder(self, name: str) -> str:
        """Return folder name for detail/chain.

        Parameters
        ----------
        name : str
            Histogram internal short name, e.g. "posX".
        """
        chain = self._chain(name)
        path = []
        if self._detail:
            path.append(self._detail)
        if chain:
            path.append(chain)
        return "/".join(path)

    def name(self) -> str:
        return self._tfile.GetName()

    def get_run(self) -> int | None:
        # docstring inherited from base class
        folders = [key.GetName() for key in self._tfile.GetListOfKeys() if key.IsFolder()]
        folders = [folder for folder in folders if folder.startswith('run_')]
        if len(folders) == 1:
            runstr = folders[0][4:]
            if runstr.isdigit():
                return int(runstr)
        return None

    def get_lbns(self) -> list[int]:
        # docstring inherited from base class
        folders = [key.GetName() for key in self._tfile.GetListOfKeys() if key.IsFolder()]
        folders = [folder for folder in folders if folder.startswith('run_')]
        if not folders:
            # Missing run folder
            return []
        if len(folders) > 1:
            # More than one run folder
            return []
        run_dir = self._tfile.GetDirectory(folders[0])

        folders = [key.GetName() for key in run_dir.GetListOfKeys()]
        folders = [folder for folder in folders if folder.startswith('lb_')]
        lbs = [int(lbn[3:]) for lbn in folders]

        # -1 should not exist in MDA but can appear in the output for ohcp,
        # for lbn=0 it is not clear if it was ever used as per-run folder in oh_cp
        # but we skip it here anyways.
        lbs = [lbn for lbn in lbs if lbn > 0]
        return lbs

    def get_histogram(self, name: str, lbn: int | None = None) -> T | None:
        # docstring inherited from base class
        _LOG.debug("retrieving histogram %s (lbn=%s)", name, lbn)

        # the list may contain duplicates, we don't care for now
        hist_names_map = self._candidate_names(name)
        if not hist_names_map:
            # unknown name
            return None

        # possibly need to order them
        hist_names = list(hist_names_map.items())

        if lbn is None:
            # Try to look for run summary names first
            hist_names = [(aname + '_runsummary', variants) for aname, variants in hist_names] + hist_names
        _LOG.debug("Will check names %s", hist_names)

        for tdir in self._folders(name, lbn):
            for hist_name, variants in hist_names:
                _LOG.debug("try histogram name %r", hist_name)
                hist = tdir.Get(hist_name)
                if hist:
                    _LOG.debug("found histogram %r", hist_name)
                    # limit set of variants
                    self._variants &= variants
                    _LOG.debug("remaining variants %s", self._variants)
                    return hist
                else:
                    _LOG.debug("did not find histogram %r", hist_name)

        return None

    def _folder_candidates(self, name: str) -> list[str]:
        """"Generate possible candidates for folder names (relative to the top
        folder) where histogram can be possibly located.

        Parameters
        ----------
        name : `str`
            Short histogram name,  e.g. "posX".
        """
        folder = self._algoFolder(name)
        if folder:
            names = [folder]
            if ':' in folder:
                names += [folder.replace(':', '_')]
        else:
            names = [""]

        # add LB/ folder for per-LB histos for non-R3 variants
        # TODO: this may not be needed for offline in run3
        variant_names = names[:]
        if self._variants - {HTreeVariant.R3}:
            variant_names += ["LB/" + aname for aname in names]
        return variant_names

    def _folders(self, name: str, lbn: int | None = None) -> Iterator[ROOT.TDirectory]:
        """Generate a sequence of folders where one can look for a histogram.

        Parameters
        ----------
        name : `str`
            Short histogram name,  e.g. "posX".
        lnb : `int` or `None`
            Optional lumi block number.

        Iterator returns instances of TDirectory which are guaranted to exist.
        """
        # top-level folder, has to exist
        top_folder = self._get_top_folder(lbn)
        if not top_folder:
            return

        names = self._folder_candidates(name)
        _LOG.debug("variant folder names: %s", names)

        tdir, server, provider = self._server_provider_folder(top_folder)
        if tdir:
            _LOG.debug("server, provider: %r, %r", server, provider)
            for name in names:
                _LOG.debug("checking folder: %r", name)
                if not name:
                    yield tdir
                else:
                    subdir = tdir.GetDirectory(name)
                    if subdir:
                        _LOG.debug("found folder %s (server=%r provider=%r)", name, server, provider)
                        yield subdir

    def _server_provider_folder(self, top_folder: ROOT.TDirectory
                                ) -> tuple[ROOT.TDirectory | None, str | None, str | None]:
        """Iterate over server/provider folders.

        If server or provider are defined then use only those folders, otherwise
        return top_folder.

        Parameters
        ----------
        top_folder : TDirectory
            Root folder of the folder tree, we expect to find "$server/$provider"
            folder directly under this top folder.

        Returns
        -------
        Triplet (TDirectory, server_name, provider_name) all items can be `None`
        """
        # if server/provider is missing then top folder is used
        if not self._server and not self._provider:
            return (top_folder, "", "")
        assert self._server is not None
        assert self._provider is not None

        folders = set(key.GetName() for key in top_folder.GetListOfKeys() if key.IsFolder())
        # oh_cp and MDA save folder names replacing colons with underscores
        # but not always, try to match both
        candidates = {self._server, self._server.replace(':', '_')}
        folders = folders & candidates
        if not folders:
            _LOG.debug(f"Could not find server folder {self._server}")
            return (None, None, None)
        assert len(folders) == 1, "Only one server folder is expected"

        folder = top_folder.GetDirectory(folders.pop())

        providers = {self._provider, self._provider.replace(':', '_')}
        matches = []
        for key in folder.GetListOfKeys():
            key_name = key.GetName()
            if key.IsFolder() and key_name in providers:
                tdir = folder.GetDirectory(key_name)
                if tdir:
                    matches.append((tdir, folder.GetName(), tdir.GetName()))

        if not matches:
            _LOG.debug(f"Could not find provider folder {self._provider}")
            return (None, None, None)
        assert len(matches) == 1, "Only one provider folder is expected"
        return matches[0]

    @abc.abstractmethod
    def _get_top_folder(self, lbn: int | None) -> ROOT.TDirectory:
        """Return top directory with histograms.

        Parameters
        ----------
        lbn : int, optional
            If lbn is None then find directory with per run-histograms,
            otherwise find per-lbn histograms for given lbn.

        Returns
        -------
        folder : TDirectory
        """
        raise NotImplementedError()


class MDAHTree(BaseHTree[T]):
    """Implements HTree interface for MDA files.

    See base class for constructor parameters.
    """
    def __init__(self, *args: Any, **kwds: Any):
        BaseHTree.__init__(self, *args, **kwds)

        # try to guess variant from contents
        has_runsummary = False
        for hname in ["posX", "llpoly"]:
            for folder in self._folders(hname):
                hist_name = _stored_names[HTreeVariant.A][hname]
                _LOG.debug("try histogram name %r", hist_name)
                if folder.Get(hist_name + "_runsummary"):
                    has_runsummary = True
        if has_runsummary:
            # run2 names
            _LOG.debug("found runsummary, must be run2 variant")
            self._variants = {HTreeVariant.A, HTreeVariant.B}
        else:
            # run2 names
            _LOG.debug("no runsummary, must be run3 variant")
            self._variants = {HTreeVariant.R3}

    def _get_top_folder(self, lbn: int | None) -> ROOT.TDirectory:
        # docstring inherited from base class

        # in MDA per-run histograms are at the top level
        if lbn is None:
            return self._tfile

        folders = [key.GetName() for key in self._tfile.GetListOfKeys() if key.IsFolder()]
        folders = [folder for folder in folders if folder.startswith("run_")]
        if len(folders) == 1:
            run_dir = self._tfile.GetDirectory(folders[0] + "/lb_" + str(lbn))
            if run_dir:
                return run_dir
        return None


class OHCPHTree(MDAHTree[T]):
    """Implements HTree interface for oh_cp-produced files files.

    See base class for constructor parameters.
    """

    def _get_top_folder(self, lbn: int | None) -> ROOT.TDirectory:
        # docstring inherited from base class

        # in oh_cp per-run histograms are in the /run_NNN/lb_-1 folder
        folders = [key.GetName() for key in self._tfile.GetListOfKeys() if key.IsFolder()]

        # ohcp should have one folder with run_NNN
        lbn = -1 if lbn is None else lbn
        if len(folders) == 1 and folders[0].startswith('run_'):
            run_dir = self._tfile.GetDirectory(folders[0] + "/lb_" + str(lbn))
            if run_dir:
                return run_dir
        return None


class OfflineLbnHTree(BaseHTree[T]):
    """Implements HTree interface for offline lbn-monitoring files.

    See base class for constructor parameters.
    """

    def get_lbns(self) -> list[int]:
        # docstring inherited from base class

        # there could be few lbn_NNN folders at top level but only one of
        # then can have folder with our histograms
        folders = [key.GetName() for key in self._tfile.GetListOfKeys() if key.IsFolder()]
        folders = [folder for folder in folders if folder.startswith("lb_")]
        for chain in [self._chain_vtx, self._chain_trk]:
            if not chain:
                continue
            for folder in folders:
                path = "{}/LB/{}/{}".format(folder, self._detail, chain)
                tdir = self._tfile.GetDirectory(path)
                if tdir:
                    lbn = int(folder[3:])
                    return [lbn]
        return []

    def _get_top_folder(self, lbn: int | None) -> ROOT.TDirectory:
        """Return top directory with histograms.

        If lbn is None then find directory with per run-histograms,
        otherwise find per-lbn histograms for given lbn.
        """
        if lbn is None:
            return None

        lbn_dir = self._tfile.GetDirectory("/lb_" + str(lbn))
        if lbn_dir:
            return lbn_dir
        return None


class BSTHTree(BaseHTree[T]):
    """Implements HTree interface for BST-produced files where all histograms
    are stored in top-level directory.

    See base class for constructor parameters.
    """

    def __init__(self, tfile: ROOT.TFile, lbn: int | None = None):
        BaseHTree.__init__(self, tfile)
        self.lbns = []
        if lbn is not None:
            self.lbns.append(lbn)

    def get_lbns(self) -> list[int]:
        # docstring inherited from base class
        return self.lbns

    def _get_top_folder(self, lbn: int | None) -> ROOT.TDirectory:
        """Return top directory with histograms.

        If lbn is None then find directory with per run-histograms,
        otherwise find per-lbn histograms for given lbn.
        """
        return self._tfile


class OfflineHTree(BaseHTree[T]):
    """Implements HTree interface for offline-produced files
    (expert-monitoring.root) where top-level directory contains chain
    directories.

    See base class for constructor parameters.
    """

    lbnRe = re.compile(r"^.*_LB(\d+)$")

    def __init__(self, tfile: ROOT.TFile, chain_vtx: str, chain_trk: str):
        BaseHTree.__init__(self, tfile, chain_vtx=chain_vtx, chain_trk=chain_trk)

        # For run3 (period="R3") LBN is encoded as a part of histogram name for
        # per-LB histogrms. We scan all histogram names to find those suffixes
        # and decode them. We don't differentiate between periods, for other
        # periods this will just return empty list.
        lbns = set()
        for hname in ["posX", "llpoly"]:
            chain_dir = self._algoFolder(hname)
            if not chain_dir:
                continue
            chain_dir = self._tfile.Get(chain_dir)
            if not chain_dir:
                continue
            for name in rootil.objectNames(chain_dir):
                match = self.lbnRe.match(name)
                if match is not None:
                    _LOG.debug("Found per-LB histo: %s", name)
                    lbn = int(match.group(1))
                    lbns.add(lbn)
        if lbns:
            # drop variants that we know are too old
            self._variants.discard(HTreeVariant.A)
            self._variants.discard(HTreeVariant.B)
        self._lbns = lbns

    def get_lbns(self) -> list[int]:
        # docstring inherited from base class
        return sorted(self._lbns)

    def get_histogram(self, name: str, lbn: int | None = None) -> T | None:
        # docstring inherited from base class
        _LOG.debug("get_histogram %s, lbn=%s", name, lbn)

        # the list may contain duplicates, we don't care for now
        hist_names_map = self._candidate_names(name)
        if not hist_names_map:
            # unknown name
            return None

        # possibly need to order them
        hist_names = list(hist_names_map.items())

        if lbn is None:
            hist_names = [(aname + '_runsummary', variants) for aname, variants in hist_names] + hist_names
        elif lbn in self._lbns:
            # add _LBn suffix
            hist_names = [(aname + f"_LB{lbn}", variants) for aname, variants in hist_names] + hist_names
            lbn = None
        _LOG.debug("Will check names %s", hist_names)

        for tdir in self._folders(name, lbn):
            for hist_name, variants in hist_names:
                _LOG.debug("try histogram name %r", hist_name)
                hist = tdir.Get(hist_name)
                if hist:
                    _LOG.debug("found histogram %r", hist_name)
                    # limit set of variants
                    self._variants &= variants
                    _LOG.debug("remaining variants %s", self._variants)
                    return hist

        return None

    def _get_top_folder(self, lbn: int | None) -> ROOT.TDirectory:
        # docstring inherited from base class
        return self._tfile

    def _folder_candidates(self, name: str) -> list[str]:
        # docstring inherited from base class
        return [self._algoFolder(name)]


class MemoryHTree(HTree[T]):
    """Implementation of an HTree which keeps histograms in memory.

    Initially the tree is empty, new histograms can be added with
    `add_histogram` method.

    Parameters
    ----------
    no_lbns : `bool`
        If True then ignore LBNs when adding/getting histograms.
    """
    def __init__(self, no_lbns: bool = False):
        # histograms indexed by LBN and name
        self.lbs: dict[int | None, dict[str, T]] = {}
        self.no_lbns = no_lbns

    def __del__(self) -> None:
        # need to cleanup after ROOT
        for hmap in self.lbs.values():
            for histo in hmap.values():
                histo.Delete()

    def name(self) -> str:
        # docstring inherited from base class
        return "MemoryHTree"

    def get_run(self) -> int | None:
        # docstring inherited from base class
        return None

    def get_lbns(self) -> list[int]:
        # docstring inherited from base class
        return list(key for key in self.lbs.keys() if key is not None)

    def get_histogram(self, name: str, lbn: int | None = None) -> T | None:
        # docstring inherited from base class
        if self.no_lbns:
            lbn = None
        return self.lbs.get(lbn, {}).get(name)

    def add_histogram(self, name: str, histo: T, lbn: int | None = None) -> None:
        """Add one more histogram to a tree.

        Parameters
        ----------
        name: `str`
            Histogram short name, e.g. "posX".
        histo : ROOT.TH1
            Histogram object.
        lbn : `int`
            LBN for this histogram, if `None` then run summary histogram is
            added.
        """
        if self.no_lbns:
            lbn = None
        # make sure the histogram is not owned by anything else.
        histo.SetDirectory(0)
        self.lbs.setdefault(lbn, {})[name] = histo


def make_htree(tfile: ROOT.TFile, server: str, provider: str,
               detail: str, chain_vtx: str,
               chain_trk: str = "") -> HTree[ROOT.TH1] | None:
    """Return instance of the HTree  for given file

    This method tries to guess file origin (ohcp, mda, etc.) and instantiate
    correct subclass of the HTree. For some type of files some of the
    parameters can be ignored.

    Parameters
    ----------
    tfile : TFile
        ROOT TFile instance.
    server : str
        OH server name.
    provider : str
        OH provider name
    detail : str
        Detail folder name, something like "EXPERT", "SHIFT", etc.
    chain_vtx : str
        Name of the algorithm/chain producing histograms for vertex method.
    chain_trk : str
        Name of the algorithm/chain producing histograms for track method.

    Returns
    -------
    Instance of HTree if can determine type of the file or None otherwise.
    """

    htype = _guess_htree_type(tfile, server, chain_vtx)
    if htype is None and chain_trk:
        # try different chain
        htype = _guess_htree_type(tfile, server, chain_trk)

    if htype == HTreeType.MDA:
        return MDAHTree[ROOT.TH1](tfile, server, provider, detail, chain_vtx, chain_trk)
    elif htype == HTreeType.OHCP_OFF:
        return OHCPHTree[ROOT.TH1](tfile, server, provider, detail, chain_vtx, chain_trk)
    elif htype == HTreeType.OFFLINE_LBN:
        return OfflineLbnHTree[ROOT.TH1](tfile, detail=detail, chain_vtx=chain_vtx, chain_trk=chain_trk)
    elif htype == HTreeType.OFFLINE:
        return OfflineHTree[ROOT.TH1](tfile, chain_vtx, chain_trk)
    elif htype == HTreeType.BST:
        return BSTHTree[ROOT.TH1](tfile)
    else:
        return None
