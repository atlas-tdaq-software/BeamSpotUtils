''' Library: Converts one beam spot representation to another.

Its called a pipe because stuff goes in one end and comes out the other, but
perhaps transfigured.  If you like functional programing the class also supports
the common map and reduce symantics.  For more information on representations
themselves see model.py.  To use this from the command line see
beamSpotConverterApp.py.  The module implements a single class also called BeamSpotPipe.
What, not original?  I was originally a java programmer...
'''

from __future__ import annotations

import logging
from collections.abc import Callable, Mapping

from . import model
from .translators import make_translator


# use module name as logger name (remove top-level package name)
_LOG = logging.getLogger(__name__.partition('.')[2])


class BeamSpotPipe:
    ''' This class converts between two representations.

    A pipe object has a single method and it is powerful. The push()
    method!  Calling this method triggers the pipe to begin the
    conversion which depending on the I/O could be a long a slow
    operation.

    Parameters
    ----------
    repInput : str
        Tells the pipe where to go look for its representations.
    repOutput :str
        Tells the pipe where to go write its representations.
    lastN : int
        If lastN is a positive integer the pipe will only write out
        that many representations. If there are less than N available
        it pushes out that many then.
    '''

    def __init__(self, repInput: str, repOutput: str, lastN: int):

        self.lastN = int(lastN)

        self.whereToLook = repInput
        self.whereToWrite = repOutput
        if self.whereToLook is None:
            self.whereToLook = 'onhlt'

        if self.whereToWrite is None:
            self.whereToWrite = 'stdout'

        _LOG.info('I will attempt to dump %s snapshots to %s from %s',
                  'all' if self.lastN <= 0 else str(self.lastN),
                  self.whereToWrite, self.whereToLook)

    def push(self, snapMap: Callable[[model.BSSnapshot], model.BSSnapshot | None] | None = None,
             readArgs: Mapping | None = None, writeArgs: Mapping | None = None,
             skipEmpty: bool = False) -> int:
        '''Perform the conversion between input and output reps.

        Parameters
        ----------
        snapMap : callable, optional
            Function which when passed a dictionary (of beam spot
            parameters) returns a possibly modified dictionary.
            This is the map part.  If the function returns None,
            then that snap shot (a single beam spot at a given
            time) is filtered out.
        readArgs: dict, optional
            map of keyword args (aka **kwargs) passed to the input
            translator during its read() call
        writeArgs: dict, optional
            map of keyword args (aka **kwargs) passed to the output
            trans during its write() call
        skipEmpty : bool, optional
            If True then do not write zero-length representation

        Returns
        -------
        Number of snapshots pushed to the writing end of pipe.
        '''
        # ask input translator for data
        input_tran = make_translator(self.whereToLook, **(readArgs or {}))
        readrep = input_tran.read()

        if readrep is None:
            _LOG.warning('Received no valid beamspot snapshots')
            return 0

        _LOG.info('Received %d snapshots from the input', len(readrep))
        if snapMap is not None:
            snapshots = [snapMap(snap) for snap in readrep]
        else:
            snapshots = [snap for snap in readrep]

        snaps = [snap for snap in snapshots if snap is not None]
        _LOG.info('There are %d snapshots after the filter', len(snaps))
        if skipEmpty and len(snaps) == 0:
            _LOG.info("Skipping empty snapshots list")
            return 0

        if self.lastN > 0 and self.lastN < len(snaps):
            snaps = snaps[-1 * self.lastN:]

        _LOG.info('After number cut, pushing %d snapshots to the output',
                  len(snaps))

        bsrOut = model.BeamSpotRepresentation(snaps)

        # send to output translator
        output = make_translator(self.whereToWrite, **(writeArgs or {}))
        output.write(bsrOut)

        return len(bsrOut)
