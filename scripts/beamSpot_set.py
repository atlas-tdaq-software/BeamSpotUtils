#!/usr/bin/env tdaq_python

"""Write beamspot snapshot into offline COOL folder /Indet/Beampos
and tag name IndetBeampos-ES1-UPD2.

In UPD2 mode we can only create records that do not exceed current run,
this is why this tool takes sigle run number as argument.
"""

from __future__ import annotations

import argparse
import sys

from BeamSpotUtils import model
from BeamSpotUtils.translators import cool_tran


def main() -> int:

    # get command line args
    parser = argparse.ArgumentParser(description='Write beamspot snapshot into offline database.')
    parser.add_argument('-r', '--run', type=int, required=True,
                        help='Run number, required.')
    parser.add_argument('--ls', type=int, required=True, metavar='LBN',
                        help='Lumi Block "since", required.')
    parser.add_argument('--lu', type=int, metavar='LBN', default=(1 << 32) - 2,
                        help='Lumi Block "until". Default: %(default)s')
    parser.add_argument('status', type=int, help='Value for status, integer.')
    parser.add_argument('posx', type=float, help='Value for X, float.')
    parser.add_argument('posy', type=float, help='Value for Y, float.')
    parser.add_argument('posz', type=float, help='Value for Z, float.')
    parser.add_argument('sigmax', type=float, nargs='?',
                        help='Value for sigma X, float. Default: %(default)s')
    parser.add_argument('sigmay', type=float, nargs='?',
                        help='Value for sigma Y, float. Default: %(default)s')
    parser.add_argument('sigmaz', type=float, nargs='?',
                        help='Value for sigma Z, float. Default: %(default)s')
    parser.add_argument('tiltx', type=float, nargs='?',
                        help='Value for tilt X, float. Default: %(default)s')
    parser.add_argument('tilty', type=float, nargs='?',
                        help='Value for tilt Y, float. Default: %(default)s')
    parser.add_argument('sigmaxy', type=float, nargs='?',
                        help='Value for sigma XY, float. Default: %(default)s')
    parser.set_defaults(sigmax=0.015,
                        sigmay=0.015,
                        sigmaz=53.,
                        tiltx=0.,
                        tilty=0.,
                        sigmaxy=0.)
    args = parser.parse_args()

    # make snapshot
    snap = model.BSSnapshot(run=args.run,
                            firstLBN=args.ls,
                            lastLBN=args.lu,
                            status=args.status,
                            posX=args.posx,
                            posY=args.posy,
                            posZ=args.posz,
                            sigmaX=args.sigmax,
                            sigmaY=args.sigmay,
                            sigmaZ=args.sigmaz,
                            tiltX=args.tiltx,
                            tiltY=args.tilty,
                            sigmaXY=args.sigmaxy)
    repr = model.BeamSpotRepresentation([snap])

    # make translator
    tran = cool_tran.OfflineCOOLTranslator(readOnly=False, create=True)
    tran.write(repr)

    return 0


if __name__ == "__main__":
    sys.exit(main())
