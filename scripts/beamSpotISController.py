#!/usr/bin/env tdaq_python

'''CLI Tool: Wakes us BST at P1 with different configurations'''

from __future__ import annotations

import argparse
import fcntl
import logging
import signal
import subprocess
import sys
import os
import threading
from typing import Any

import config
import ers
from ispy import IPCPartition, ISCriteria, ISInfoDynAny, ISInfoReceiver
from BeamSpotUtils import bslog

_LOG = logging.getLogger("beamSpotISController")


class ConfigError(ers.Issue):
    """ERS Issue class for configuration-related errors"""

    def __init__(self, cause: Any = None):
        ers.Issue.__init__(self, 'Failure during configuration', {},
                           cause if isinstance(cause, ers.Issue) else None)


class PartitionError(ers.Issue):
    """ERS Issue class for partition-related errors"""

    def __init__(self, partition: str, cause: Any = None):
        ers.Issue.__init__(self, 'Partition is not valid: {!r}'.format(partition),
                           {'partition': partition},
                           cause if isinstance(cause, ers.Issue) else None)


class SubscribeError(ers.Issue):
    """ERS Issue class for subscription-related errors"""

    def __init__(self, server: str, is_object: str, cause: Any = None):
        ers.Issue.__init__(self, 'Failure in IS subscription',
                           {'server': server, 'is_object': is_object},
                           cause if isinstance(cause, ers.Issue) else None)


class Config(object):
    """
    Class which reads configuration from OKS and initiaizes
    appication from that configuration.
    """

    # OKS class of the configuration object
    oks_class = 'BeamSpotISControllerConfig'

    def __init__(self, oks_db: str, config_id: str):
        """
        Make an instance of Config class.

        @param oks_db:  OKS configuration, e.g. oksconfig:/path/file.data.xml
        @param config_id:  ID of the application config object in OKS
        """

        self.oks_db = oks_db
        self.config_id = config_id

    def makeApp(self) -> BeamSpotISController:
        """
        Make an instance of application class from configuration parameters.
        """

        # default-initialize logging
        rootLogger = bslog.initLogger(4)

        # find app instance in a database
        try:
            _LOG.info("Load OKS configuration from %r", self.oks_db)
            if "TDAQ_DB_VERSION" in os.environ:
                # have to unset TDAQ_DB_VERSION to read from git HEAD
                del os.environ["TDAQ_DB_VERSION"]
            db = config.Configuration(self.oks_db)
            cfg = db.get_obj(self.oks_class, self.config_id)
        except Exception as exc:
            ers.error(ConfigError(exc))
            raise

        # setup logging level
        if cfg['LogLevel'] > 4:
            rootLogger.setLevel(logging.DEBUG)

        # start parsing general parameters
        _LOG.info("Reading OKS configuration %s@%s", self.config_id, self.oks_class)

        currentPart = os.environ.get('TDAQ_PARTITION', "")

        # get all params from OKS
        commonCfg = cfg['CommonOptions']
        partName = commonCfg['Partition'] or currentPart
        lbServerName = commonCfg['LBServer']
        lbObjectName = commonCfg['LBObject']
        isServerName = commonCfg['ISServer']
        isProviderName = commonCfg['ISProvider']
        chain = cfg['TriggerChain']
        callbackPrescale = cfg['CallbackPrescale']
        minLBN = cfg['MinLBN']
        maxLBN = cfg['MaxLBN']
        repInput = cfg['RepInput']
        repOutput = cfg['RepOutput']
        doBCIDs = cfg['DoBCIDs']
        bstOptions = commonCfg['BSTOptions'].split() + cfg['BSTOptions'].split()

        # dump configuration options
        for obj in (commonCfg, cfg):
            _LOG.info("%r:", obj)
            for attr in sorted(obj.__schema__['attribute'].keys()):
                _LOG.info("    %s = %r", attr, obj[attr])

        # instantiate app
        app = BeamSpotISController(partName, lbServerName, lbObjectName, isServerName, isProviderName,
                                   chain, callbackPrescale, minLBN, maxLBN, repInput, repOutput, doBCIDs,
                                   bstOptions)
        return app


class BeamSpotISController(object):
    """
    Class defining application logic.
    """

    def __init__(self, partName: str, lbServerName: str, lbObjectName: str,
                 isServerName: str, isProviderName: str, chain: str,
                 callbackPrescale: int, minLBN: int, maxLBN: int,
                 repInput: str, repOutput: str, doBCIDs: bool, bstOptions: list[str]):
        """
        Create application instance.
        """
        self.partName = partName
        self.lbServerName = lbServerName
        self.lbObjectName = lbObjectName
        self.callbackPrescale = callbackPrescale

        # build complete command line
        self.bstArgs = ['beamSpotTool.py']
        self.bstArgs += bstOptions

        self.bstArgs += ['--server=' + isServerName]
        self.bstArgs += ['--provider=' + isProviderName]
        self.bstArgs += ['--chain=' + chain]
        if minLBN:
            self.bstArgs += ['--minLBN=' + str(minLBN)]
        if maxLBN:
            self.bstArgs += ['--maxLBN=' + str(maxLBN)]
        if repInput:
            self.bstArgs += ['--repInput=' + repInput]
        if repOutput:
            self.bstArgs += ['--repOutput=' + repOutput]
        if doBCIDs:
            self.bstArgs += ['--doBCIDs']

        self.lock = threading.Lock()
        self.callbackCount = 0
        self.signalled = False
        self.children: set[subprocess.Popen] = set()

        # this pipe is used to notify main thread about signals,
        # when (C-level) signal handler is called one byte is
        # sent to this pipe (see set_wakeup_fd below)
        self.sigpipe = os.pipe()
        # Sending end must be in non-blocking mode.
        fcntl.fcntl(self.sigpipe[1], fcntl.F_SETFL, os.O_NONBLOCK)

    def run(self) -> int:
        """
        This is main method of the whole application, it runs the whole shebang.
        Returns 0 on success (when stopped normally) or non-zero on any error.
        """

        part = IPCPartition(self.partName)
        if not part.isValid():
            ers.error(PartitionError(self.partName))
            return 1

        # subscribe to IS updates
        lbCriteria = ISCriteria(self.lbObjectName)
        _LOG.info('Subscribing to IS server: %s.%s', self.lbServerName, self.lbObjectName)
        _LOG.info('Registering callback: %s', ' '.join(self.bstArgs))
        receiver = ISInfoReceiver(part)
        try:
            receiver.subscribe_info(self.lbServerName, self.is_handler, lbCriteria)
        except Exception as exc:
            ers.error(SubscribeError(server=self.lbServerName,
                                     is_object=self.lbObjectName,
                                     cause=exc))
            return 1

        # wait until signalled
        _LOG.info('Waiting for updates...')
        signal.signal(signal.SIGTERM, self.signal_handler)
        signal.signal(signal.SIGCHLD, self.signal_handler)
        signal.set_wakeup_fd(self.sigpipe[1])
        while not self.signalled:

            # this will hang until any signal handler is called
            try:
                os.read(self.sigpipe[0], 1)
            except OSError:
                # exception usually happens when TERM signal is received
                # due to interrupted system call
                pass

            # take care of the finished BSTs
            self.killZombies()

        # unsubscribe
        _LOG.info('Trying to unsubscribe')
        receiver.unsubscribe(self.lbServerName, lbCriteria, True)
        _LOG.info('Success')
        return 0

    def killZombies(self) -> None:
        """
        Rip dead BST processes.
        """

        def _hasFinished(proc: subprocess.Popen) -> bool:
            ' method to check whether process has finished '
            rc = proc.poll()
            finished = rc is not None
            if finished:
                _LOG.info('Process %s completed with return code %s', proc.pid, rc)
            return finished

        # rip the dead BST processes (those which return non-None from poll()),
        # the list needs to be protected from concurrent modification by IS handler
        with self.lock:
            # make a copy of the list to avoid holding lock for too long
            children = self.children.copy()

        # check for processes which have finished already
        finished = set(proc for proc in children if _hasFinished(proc))

        with self.lock:
            # removed finished ones from list
            self.children -= finished

    def is_handler(self, cb: Any) -> None:
        """
        Method which handles IS callbacks. This may be called from
        multiple IS reciever threads so it needs to be protected,
        but our subscriptions should not happen frequently so there
        is no chance that race condition actually happens here.

        In general IS handler is expected to return fast, so starting
        new process from this method is not the best idea, but again
        there is no chance that it can cause actual problem with the
        callback rates that we expect (once per minute or so).
        """

        self.callbackCount += 1
        _LOG.info('Called handler at count: %s', self.callbackCount)

        _LOG.info('Callback info: name=%s time=%s type=%s reason=%s',
                  cb.name(), cb.time(), cb.type().name(), cb.reason())
        value = ISInfoDynAny()
        cb.value(value)
        _LOG.info('IS value: %s', str(value).replace('\n', '; '))

        if self.callbackPrescale <= 1 or self.callbackCount % self.callbackPrescale == 1:

            # start process and save it for later
            _LOG.info('Calling: %s', ' '.join(self.bstArgs))
            proc = subprocess.Popen(self.bstArgs)
            _LOG.info('Child PID: %s', proc.pid)

            with self.lock:
                self.children.add(proc)

    def signal_handler(self, signum: int, dummy_frame: Any) -> None:
        """ set termination flag for TERM signal """
        if signum == signal.SIGTERM:
            self.signalled = True
        _LOG.info('Caught signal %s', signum)


def main() -> int:
    """
    Main application function.
    """

    parser = argparse.ArgumentParser(description='beamSpotTool launcher application.')
    parser.add_argument('-d', '--database', dest='oks_db', default='', metavar='TDAQ_DB',
                        help='Configure application from specified OKS database, '
                        'e.g. oksconfig:/path/file.data.xml. Default is to configure from $TDAQ_DB.')
    parser.add_argument('-n', '--name', dest='config_id', default='', metavar='STRING',
                        help='ID of BeamSpotISControllerConfig application in OKS database. '
                        'Default is to use $TDAQ_APPLICATION_NAME.')
    args = parser.parse_args()

    if not args.oks_db:
        args.oks_db = os.environ.get('TDAQ_DB')
        if not args.oks_db:
            print('TDAQ_DB env.var. is missing, use -d option or define env.var.', file=sys.stderr)
            return 1

    if not args.config_id:
        args.config_id = os.environ.get('TDAQ_APPLICATION_NAME')
        if not args.config_id:
            print('TDAQ_APPLICATION_NAME env.var. is missing, use -n option or define env.var.',
                  file=sys.stderr)
            return 1

    print('Environment:')
    for key, val in sorted(os.environ.items()):
        print("%s=%s" % (key, val))

    # read configuration
    cfg = Config(args.oks_db, args.config_id)

    # instantiate app and run it until it stops
    app = cfg.makeApp()
    return app.run()


if __name__ == "__main__":
    sys.exit(main())
