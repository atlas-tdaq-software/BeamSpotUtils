#!/usr/bin/env tdaq_python

from __future__ import annotations

import argparse
import sys
from datetime import datetime, timezone
from typing import Any, TYPE_CHECKING

from coldpie import cool

if TYPE_CHECKING:
    import prettytable


def _fmt(x: Any) -> str:
    if isinstance(x, float):
        return "{:.6g}".format(x)
    elif isinstance(x, cool.Time):
        ts = x.total_nanoseconds() / 1e9
        dt = datetime.fromtimestamp(ts, timezone.utc)
        return dt.isoformat()
    return str(x)


class CSVPrinter:
    def header(self, header: list[str]) -> None:
        print(",".join(header))

    def row(self, values: list[str]) -> None:
        print(",".join(values))

    def close(self) -> None:
        pass


class TablePrinter:
    def __init__(self) -> None:
        self._table: prettytable.PrettyTable | None = None

    @property
    def table(self) -> prettytable.PrettyTable:
        if self._table is None:
            import prettytable

            self._table = prettytable.PrettyTable()
        return self._table

    def header(self, header: list[str]) -> None:
        for col in header:
            self.table.add_column(col, [], align="r")

    def row(self, values: list[str]) -> None:
        self.table.add_row(values)

    def close(self) -> None:
        print(self.table)
        self._table = None


def main() -> int:
    parser = argparse.ArgumentParser(description="Dump BeamSpot conditions from COOL")
    parser.add_argument("conn_str", help="COOL connection string, e.g. COOLOFL_INDET/CONDBR2")
    parser.add_argument("folder", help="COOL folder name, e.g. /Indet/Onl/Beampos")
    parser.add_argument("tag", help="COOL tag name, e.g. IndetBeamposOnl-HLT-UPD1-001-00")
    parser.add_argument(
        "-c",
        "--channel",
        type=int,
        default=0,
        metavar="NUMBER",
        help="COOL channel number, def: $(default)s, use -1 to dump all channels.",
    )
    parser.add_argument(
        "-s",
        "--start",
        type=int,
        default=0,
        metavar="NUMBER",
        help="Starting run number, default is all runs.",
    )
    parser.add_argument(
        "-e",
        "--end",
        type=int,
        default=1000000,
        metavar="NUMBER",
        help="Ending run number, default is all runs.",
    )
    parser.add_argument(
        "-a",
        "--auto-fields",
        action="store_true",
        default=False,
        help="Dump all fields instead of pre-defined ones, can be used on any folder.",
    )
    parser.add_argument("--time", action="store_true", default=False, help="Add insertion_time column.")
    parser.add_argument(
        "-t", "--table", action="store_true", default=False, help="Dump in table format, default is CSV."
    )
    args = parser.parse_args()

    printer: TablePrinter | CSVPrinter = TablePrinter() if args.table else CSVPrinter()

    db_svc = cool.DatabaseSvcFactory.databaseService()
    db = db_svc.openDatabase(args.conn_str, True)
    folder = db.getFolder(args.folder)
    folder.setPrefetchAll(False)

    showChannel = args.channel < 0

    if args.auto_fields:
        fields = [field.name() for field in folder.payloadSpecification()]
    else:
        fields = ["posX", "posY", "posZ", "sigmaX", "sigmaY", "sigmaZ", "tiltX", "tiltY"]
        fields = ["status"] + fields + [f + "Err" for f in fields]

    header = ["run", "LB", "toRun", "toLB"]
    if args.time:
        header += ["insertion_time"]
    header += fields
    if showChannel:
        header.insert(0, "channel")
    printer.header(header)

    since = args.start << 32
    until = args.end << 32
    if args.channel >= 0:
        channels = cool.ChannelSelection(args.channel)
    else:
        channels = cool.ChannelSelection.all()
    itr = folder.browseObjects(since, until, channels, args.tag)
    for obj in itr:
        since = divmod(obj.since(), 1 << 32)
        until = divmod(obj.until(), 1 << 32)
        values = [since[0], since[1], until[0], until[1]]
        if args.time:
            values += [obj.insertionTime()]
        payload = obj.payload()
        values += [payload[f] for f in fields]
        if showChannel:
            values.insert(0, obj.channelId())
        printer.row([_fmt(v) for v in values])

    printer.close()

    return 0


if __name__ == "__main__":
    sys.exit(main())
