#!/usr/bin/env tdaq_python

"""Write beamspot snapshot into online COOL folder /Indet/Onl/Beampos
and tag name IndetBeamposOnl-HLT-UPD1-001-00.

In UPD1 mode we can only create records that extend to +Infinity, this is why
this script takes single run and LB number.
"""

from __future__ import annotations

import argparse
import sys

from BeamSpotUtils import model
from BeamSpotUtils.translators import factory


def main() -> int:

    # get command line args
    parser = argparse.ArgumentParser(description='Write beamspot snapshot into offline database.')
    parser.add_argument('-r', '--run', type=int, required=True,
                        help='Run number, required.')
    parser.add_argument('--lbn', type=int, required=True, metavar='LBN',
                        help='Lumi Block "since", required.')
    parser.add_argument('--output', metavar='STRING', default="onhlt_update",
                        help='Specify where to store the snapshot. ' + factory.userIODesc +
                             ' Default: %(default)s')
    parser.add_argument('status', type=int, help='Value for status, integer.')
    parser.add_argument('posx', type=float, help='Value for X, float.')
    parser.add_argument('posy', type=float, help='Value for Y, float.')
    parser.add_argument('posz', type=float, help='Value for Z, float.')
    parser.add_argument('sigmax', type=float, nargs='?',
                        help='Value for sigma X, float. Default: %(default)s')
    parser.add_argument('sigmay', type=float, nargs='?',
                        help='Value for sigma Y, float. Default: %(default)s')
    parser.add_argument('sigmaz', type=float, nargs='?',
                        help='Value for sigma Z, float. Default: %(default)s')
    parser.add_argument('tiltx', type=float, nargs='?',
                        help='Value for tilt X, float. Default: %(default)s')
    parser.add_argument('tilty', type=float, nargs='?',
                        help='Value for tilt Y, float. Default: %(default)s')
    parser.add_argument('sigmaxy', type=float, nargs='?',
                        help='Value for sigma XY, float. Default: %(default)s')
    parser.set_defaults(sigmax=0.015,
                        sigmay=0.015,
                        sigmaz=53.,
                        tiltx=0.,
                        tilty=0.,
                        sigmaxy=0.)
    args = parser.parse_args()

    # make snapshot
    snap = model.BSSnapshot(run=args.run,
                            lbn=args.lbn,
                            status=args.status,
                            posX=args.posx,
                            posY=args.posy,
                            posZ=args.posz,
                            sigmaX=args.sigmax,
                            sigmaY=args.sigmay,
                            sigmaZ=args.sigmaz,
                            tiltX=args.tiltx,
                            tiltY=args.tilty,
                            sigmaXY=args.sigmaxy)
    repr = model.BeamSpotRepresentation([snap])

    # make translator
    tran = factory.make_translator(args.output)
    tran.write(repr)

    return 0


if __name__ == "__main__":
    sys.exit(main())
