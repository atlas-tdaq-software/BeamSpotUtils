#!/usr/bin/env tdaq_python

'''CLI tool: COOL -> Massi -> CoCa for per bunch measurements, see -h'''

from __future__ import annotations

import argparse
import errno
import logging
import re
import os
import stat
import sys
from typing import Any, cast

from coldpie import cool

import ispy
import ipc

from BeamSpotUtils import bscool, bslog, model, os_utils, pipe


_LOG = logging.getLogger("beamSpotArchiver")


std_whereToLook = 'onbunch'
debug_whereToLook = 'test/bsa.input.csv'

whereToWrite = 'massi'
tempDir = '/tmp/beamspot'
folderTemplate = '%d_lumireg_ATLAS'
dataset = 'HLT-BeamSpot'
version = '001'  # this should match the three digit field in the COOL tag
cocaPrefix = 'beamspot-%s' % version
cocaRegex = 'RelPath: %s-r([0-9]{10})_l([-]?[0-9]+).*' % cocaPrefix
cocaTemplate = cocaPrefix + '-r%010d_l%04d-' + folderTemplate + '.tgz'


def getArchiverCLI() -> argparse.Namespace:
    """Parse command line
    """

    header = '''\
Use this application to convert all of "this" run's per bunch
measurements (from COOL) into the "Massi" format.  This format
uses one file per bunch with LHC coordinates used inside.  Next
the set of (newly made) per bunch files are tarballed and registered
with coca.
'''

    parser = argparse.ArgumentParser(description=header)

    parser.add_argument('-r', '--runNumber', type=int, metavar='NUMBER',
                        help='The integer run number corresponding to'
                        ' the run you wish to convert. If not specified,'
                        ' the tool will attempt to fetch it from IS.')

    parser.add_argument('-d', '--debug', action='store_true', default=False,
                        help='Instead of reading from onbunch '
                        ' the tool uses %s.  Also doesnt register the tarball'
                        ' with CoCa.  Note you must be in'
                        ' the top level BeamSpotUtils directory!!'
                        % (debug_whereToLook))

    parser.add_argument('-v', '--verbosity', nargs='?', default=4, const=5,
                        type=int, metavar='LEVEL',
                        help="Logging level, without this option script will "
                        "log messages at INFO level, if -v is given (without "
                        "LEVEL) logging will happen at DEBUG level. For backward "
                        "compatibility we also allow `-v LEVEL' format, if "
                        "LEVEL<5 then  INFO level is used, otherwise DEBUG.")

    return parser.parse_args()


def coolFill(run: int) -> int:
    """Return fill number from COOL database.

    Parameters
    ----------
    run : int
        Run number

    Returns
    -------
    Integer number, negative if fill number is not known.
    """
    spec = cool.RecordSpecification([])

    connStr = 'oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_DCS;dbname=CONDBR2'
    db = bscool.getDB(connStr, True, False)
    coolFolder = bscool.openOrMakeFolder(db, '/LHC/DCS/FILLSTATE', spec)

    try:
        begIOV, endIOV = bscool.runLBTimes(run, None)
    except Exception as exc:
        _LOG.error('Failed to retrieve LBN timestamps.  This was exception: %s', exc)
        return -1

    try:
        objs = coolFolder.browseObjects(begIOV, endIOV,
                                        cool.ChannelSelection.all(), '')
    except Exception as exc:
        _LOG.error('Object browsing failed.  This was exception: %s', exc)
        return -1

    fills: dict[int, float] = {}
    for obj in objs:
        payload = obj.payload()
        if payload['StableBeams'] != 1:
            continue

        fill = cast(int, payload['FillNumber'])
        duration = (obj.until() - obj.since()) / 1e9
        try:
            fills[fill] += duration
        except KeyError:
            fills[fill] = duration

    if len(fills.keys()) != 1:
        _LOG.error('I received %d fills for this run, expected just 1',
                   len(fills))
        return -1

    return list(fills.keys())[0]


def lhcFill(run: int) -> int:
    """Return fill number for given run number
    """
#    fill = fetchISAccess(
#                          partition = 'initial'
#                        , server    = 'LHC'
#                        , obj       = 'RunConfig-FillNum'
#                        , field     = ''
#                        )
    fill = None
    if fill is None:
        fill = coolFill(run)

    return fill


def fetchISAccess(partition: str = 'ATLAS', server: str = 'RunParams',
                  obj: str = 'SOR_RunParams', field: str = 'run_number') -> Any:
    """Retrieve some info from IS.

    Parameters
    ---------
    partition : str, optional
        Partition name
    server : str, optional
        IS server name
    obj : str, optional
        Name of the IS object
    field : str, optional
        name of the attribute of IS object

    Returns
    -------
    Value of the attribute or None
    """
    try:
        part = ipc.IPCPartition(partition)  # pylint: disable=no-member
        di = ispy.ISInfoDictionary(part)  # pylint: disable=no-member
        val = ispy.ISInfoDynAny()  # pylint: disable=no-member
        di.getValue('%s.%s' % (server, obj), val)
        return val[field]
    except UserWarning:
        return None


def fetchRunNumber() -> int:
    """Return run number from IS
    """
    value = fetchISAccess()
    assert isinstance(value, int)
    return value


def fetchThisLBN() -> int:
    """Return lumi block number from IS
    """
    value = fetchISAccess(obj='LumiBlock', field='LumiBlockNumber')
    assert isinstance(value, int)
    return value


def fetchCoCaLBN(thisRun: int) -> int:
    """Return last known (to coca) LB number or -1.
    """
    lbn = -1
    cmd = ['coca_get_info', '-d', dataset, '-l', 'files']
    out, _ = os_utils.quickOSCall(cmd, mustReturnZero=True)
    pathRE = re.compile(cocaRegex)

    for line in out.split('\n'):
        match = pathRE.search(line)
        if match is not None:
            if thisRun == int(match.group(1)):
                lbn = max(int(match.group(2)), lbn)

    lbn = -1
    return lbn


def quickOSCall(cmd: list[str]) -> tuple[str, str]:
    """Call os_utils.quickOSCall with few default parameters
    """
    return os_utils.quickOSCall(cmd, userEnv=None, mustReturnZero=True)


def main() -> int:

    opts = getArchiverCLI()

    if opts.verbosity > 3:
        print('Environment:')
        for key, val in os.environ.items():
            print("%s=%s" % (key, val))

    bslog.initLogger(opts.verbosity)

    # ## Systems current settings
    if opts.runNumber is None:
        thisRun = fetchRunNumber()
        thisLBN = fetchThisLBN()
    else:
        thisRun = opts.runNumber
        thisLBN = 0

    if thisRun is None:
        _LOG.error('I have no run number')
        return 1

    thisFill = lhcFill(thisRun)
    _LOG.info('We work on run %d, %d, with LHC Fill %d',
              thisRun, thisLBN, thisFill)
    if thisFill < 0:
        # could not fetch fill number, stop right here, return OK
        # status so that we don't cause error in partition
        _LOG.warning('Failed to retrieve fill number')
        return 0

    specificFolder = folderTemplate % thisFill
    dropLocation = os.path.join(tempDir, specificFolder)

    # ## Toss our temp directory if it exists, then remake it
    _LOG.info('Clearing temp directory: %s', dropLocation)
    os_utils.rmdir(dropLocation)
    try:
        os.makedirs(dropLocation)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise

    # ## Look into our coca cache and check for other files with this run
    beginLBN = fetchCoCaLBN(thisRun)
    _LOG.info('CoCa "high water mark" is LBN %d.  Zero means all.', beginLBN)
    if beginLBN == 0:
        _LOG.info('This run is already registered')
        return 0
    elif beginLBN == -1:
        beginLBN = 0

    if opts.debug:
        whereToLook = debug_whereToLook
    else:
        whereToLook = std_whereToLook

    # ## Manufacture the individual massi files
    def extraIOVFilter(snap: model.BSSnapshot) -> model.BSSnapshot | None:
        return None if snap.lbn < beginLBN else snap

    bspipe = pipe.BeamSpotPipe(whereToLook, whereToWrite, 0)
    nReps = bspipe.push(snapMap=extraIOVFilter,
                        readArgs={'run': thisRun, 'lbn': None},
                        writeArgs={'folder': dropLocation, 'fill': thisFill})

    _LOG.info('%d snapshots outputted', nReps)
    if nReps == 0:
        return 0

    # ## tarball that file, then register it with CoCa.
    previousCWD = os.getcwd()
    os.chdir(tempDir)

    tarball = os.path.join(tempDir, cocaTemplate % (thisRun, thisLBN, thisFill))
    _LOG.info("Tarballing %s into %s", specificFolder, tarball)
    quickOSCall(['tar', 'czf', tarball, specificFolder])

    os.chmod(tarball, (stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH |
                       stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH))
    try:
        # TODO: quick hack to allow coca to remove tarball
        os.chmod(tempDir, 0o777)
    except OSError:
        pass

    if opts.debug:
        _LOG.info("You said no CoCa write, so I no CoCa write.")
    else:
        _LOG.info("Registering file %s with CoCa dataset %s", tarball, dataset)
        quickOSCall(['coca_register', '-d', dataset, '-f', tarball])
        os.chdir(previousCWD)

    _LOG.info("Goodbye")

    return 0


if __name__ == '__main__':
    sys.exit(main())
