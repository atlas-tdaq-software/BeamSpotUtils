#!/usr/bin/env tdaq_python

'''CLI Tool to read from one representation and write to the other'''

from __future__ import annotations

import logging
import argparse

from BeamSpotUtils import bslog, pipe
from BeamSpotUtils.translators import factory


_LOG = logging.getLogger("beamSpotConverterApp")


def getConverterCLI() -> argparse.Namespace:
    """Parse command line.
    """
    header = '''\t%(prog)s -r 187815 [options]
... or ...
%(prog)s -r 187815  --repInput=onhlt --repOutput=hlt.beamspots.187815.csv

Use this application to convert one beamspot representation to another.  For
instance, you might want to dump (ie convert) all per bunch measurements from
COOL (one representation) to a local CSV file (the other representation).
See the repInput and repOutput --help entries for allowed values.
'''
    epilog = '''N.B. onbunch and onhlt are only accessible through the online
system only.  From lxplus only only onmon is accessible (and from similar machines).
Currently the file translators require the file to *already exist* before they
write to it, so try $> touch myFile.csv
'''

    p = argparse.ArgumentParser(usage=header, epilog=epilog)

    p.add_argument('-r', '--runNumber', type=int, metavar='NUMBER',
                   help='The integer run number corresponding to'
                   ' the run you wish to convert. If not'
                   ' specified, it will attempt to dump ')

    p.add_argument('-n', dest='count', type=int, default=0,
                   help='The number of representations to convert.'
                   '  They are always the last (chronologically).'
                   '  If there are too few representations the tool'
                   ' will simply convert all of them.  If n <= 0,'
                   ' the tool converts *all* representations it'
                   ' receives.')

    p.add_argument('--repInput', default='onhlt',
                   help=('Specify where the representation to'
                         ' convert comes from.  If repInput'
                         ' is not specified it defaults to'
                         ' %(default)s. See repOutput for more info'))

    p.add_argument('--repOutput', default='stdout',
                   help=('Specify where the representation '
                         ' will be stored.  If repOutput'
                         ' is not specified it defaults to'
                         ' %(default)s. ' + factory.userIODesc))

    p.add_argument('-v', '--verbosity', nargs='?', default=4, const=5,
                   type=int, metavar='LEVEL',
                   help="Logging level, without this option script will "
                   "log messages at INFO level, if -v is given (without "
                   "LEVEL) logging will happen at DEBUG level. For backward "
                   "compatibility we also allow `-v LEVEL' format, if "
                   "LEVEL<5 then  INFO level is used, otherwise DEBUG.")

    return p.parse_args()


def main() -> None:

    opts = getConverterCLI()

    bslog.initLogger(opts.verbosity)

    if opts.runNumber is None:
        pipeReadArgs = None
        _LOG.warning("Without a run number reading from COOL is slow!")
    else:
        pipeReadArgs = {'run': opts.runNumber}

    converter = pipe.BeamSpotPipe(opts.repInput, opts.repOutput,
                                  opts.count)

    converter.push(readArgs=pipeReadArgs)
    _LOG.info('Goodbye.')


if __name__ == '__main__':
    main()
