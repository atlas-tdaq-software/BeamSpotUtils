#!/usr/bin/env tdaq_python

'''Store this runs last LB of histograms for next run's width measurement'''

from __future__ import annotations

import argparse
import logging
import os
import sys

from BeamSpotUtils.freaking_ROOT import ROOT
from BeamSpotUtils import bslog, file_getter


_LOG = logging.getLogger("beamSpotStoreRes")


def handleCLI() -> argparse.Namespace:
    """Parse command line.
    """
    header = '''\
Use this application to convert all of "this" run's per bunch
measurements (from COOL) into the "Massi" format.  This format
uses one file per bunch with LHC coordinates used inside.  Next
the set of (newly made) per bunch files are tarballed and registered
with coca
'''

    p = argparse.ArgumentParser(description=header)

    p.add_argument('--partition', default='ATLAS',
                   help='Which partition\'s to use for ohcp (and maybe oneday IS). '
                   'Not expressed in the root file. [default: %(default)s]')

    p.add_argument('--server', default='Histogramming',
                   help='Which server\'s results to use.  Also the first folder '
                   'inside the root file [default: %(default)s]')

    p.add_argument('--provider', default='TopMIG-OH_HLT',
                   help='Which provider\'s results to use.  Also the second folder '
                   'inside the root file [default: %(default)s]')

    p.add_argument('--detail', default='EXPERT',
                   help='Histogram level.  Also the third folder '
                   'inside the root file [default: %(default)s]')

    p.add_argument('--chain', default='T2VertexBeamSpot_FTF',
                   help='Which chain\'s results to use.  Also the fourth folder '
                   'inside the root file [default: %(default)s]')

    p.add_argument('--chain-trk', default='T2VertexBeamSpot_FTF',
                   help='Which chain\'s results to use.  Also the fourth folder '
                   'inside the root file [default: %(default)s]')

    p.add_argument('--dirPrefix', default=None,
                   help='This steers the inspector to the correct folder'
                   ' to find the lb_[0-9]+ folders.  It is complimentary'
                   ' with dirSuffix which specifies all the folders'
                   ' afterwards.  If either is specified it overrides'
                   ' the tools best guess, which works for the offline'
                   ' convention for both per LB and end-of-run histograms.')

    p.add_argument('--dirSuffix', default=None,
                   help='See --dirPrefix.')

    p.add_argument('-v', '--verbosity', nargs='?', default=4, const=5,
                   type=int, metavar='LEVEL',
                   help="Logging level, without this option script will "
                   "log messages at INFO level, if -v is given (without "
                   "LEVEL) logging will happen at DEBUG level. For backward "
                   "compatibility we also allow `-v LEVEL' format, if "
                   "LEVEL<5 then  INFO level is used, otherwise DEBUG.")

    return p.parse_args()


def getDirectories(server: str, provider: str, detail: str, chain: str, dirPrefix: str,
                   dirSuffix: str, runNumber: int) -> tuple[str, str]:
    ''' Returns the directories that BSCalcVertex will use to find the histograms.
        There is a prefix (directories up to the lb_[0-9]+ folder) and suffix
        for those after.  Right now these are constructed using the offline convention.
        However they are overwritable by --dirPrefix --dirSuffix
    '''
    noEOR = True
    dirServerProvider = os.path.join(server, provider)
    dirDetailChain = os.path.join(detail, chain)

    if dirPrefix is not None:
        prefix = dirPrefix
    else:
        if noEOR:
            prefix = 'run_%d' % runNumber
        else:
            prefix = ''

    if dirSuffix is not None:
        suffix = dirSuffix
    else:
        if noEOR:
            suffix = os.path.join(dirServerProvider, 'LB', dirDetailChain) + '/'
        else:
            suffix = os.path.join('run_%d' % runNumber, 'lb_0',
                                  dirServerProvider, dirDetailChain) + '/'

    return prefix, suffix


def inspectHistograms(loc: str, prefix: str, suffix: str) -> None:
    """Look at the histograms.

    Never finished...
    """
    fh = ROOT.TFile(loc)  # pylint: disable=no-member
    fh.cd(prefix)
    fh.cd('lb_0')
    fh.cd(suffix)


def main() -> int:

    opts = handleCLI()

    if opts.verbosity > 3:
        print('Environment:')
        for item in os.environ.items():
            print("%s=%s" % item)

    bslog.initLogger(opts.verbosity)

    # ## GetFile
    oh_src = (opts.partition, opts.server, opts.provider)
    getter = file_getter.OHCPGetter(oh_src, detail=opts.detail, algorithm_vtx=opts.chain,
                                    algorithm_trk=opts.chain_trk, minLBN=2)
    _LOG.debug("Created the OHCPGetter")
    finfo = getter.goFetch()
    if finfo:
        _LOG.info("Using the file %s", finfo.path)
    else:
        _LOG.critical("The %s failed to get the file!", getter.__class__.__name__)
        return 1
    assert finfo.path is not None

    run = finfo.htree.get_run()
    if run is None:
        _LOG.critical("Run number is unknown")
        return 1

    getDirectories(opts.server, opts.provider, opts.detail, opts.chain,
                   opts.dirPrefix, opts.dirSuffix, run)
    _LOG.info('Attempting to use this prefix and suffix directory pair (%s, %s)',
              opts.dirPrefix, opts.dirSuffix)

    inspectHistograms(finfo.path, opts.dirPrefix, opts.dirSuffix)

    _LOG.info("Goodbye")

    return 0


if __name__ == '__main__':
    sys.exit(main())
