#!/usr/bin/env tdaq_python

'''CLI Tool: Handles primary calculation work in p1'''

from __future__ import annotations

import sys

from BeamSpotUtils import bst

if __name__ == '__main__':
    sys.exit(bst.main())
