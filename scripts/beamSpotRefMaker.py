#!/usr/bin/env tdaq_python

'''CLI Tool: Writes most recent onmon rep back to onmon with invalid status'''

from __future__ import annotations

import argparse
import logging
import os
import shutil
import sys

from BeamSpotUtils.freaking_ROOT import ROOT
from BeamSpotUtils import bslog, file_getter

_LOG = logging.getLogger("beamSpotRefMaker")

MIN_ENTRIES = 1000 * 1000


def getRefMakerCLI() -> argparse.Namespace:
    """Parse command line.
    """
    header = '''\t%(prog)s [options]
... or ...
%(prog)s -f fileNameToUse.root -v5

Use this application to a) determine if this run's
EoR histograms are good enough to become the new reference
and b) leave them on the local filesystem if they are.
'''

    parser = argparse.ArgumentParser(usage=header)

    parser.add_argument('-f', '--filename', default='reference.root',
                        help='The filename, the application will use for the'
                        ' reference histograms if they pass the criteria.')

    parser.add_argument('--partition', default='ATLAS',
                        help='Which partition\'s to use for ohcp (and maybe oneday is). '
                        'Not expressed in the root file. [default: %(default)s]')

    parser.add_argument('--server', default='Histogramming',
                        help='Which server\'s results to use.  Also the first folder '
                        'inside the root file [default: %(default)s]')

    parser.add_argument('--provider', default='TopMIG-OH_HLT',
                        help='Which provider\'s results to use.  Also the second folder '
                        'inside the root file [default: %(default)s]')

    parser.add_argument('--detail', default='EXPERT',
                        help='Which level of detail to use.  Also the third folder '
                        'inside the root file [default: %(default)s]')

    parser.add_argument('--chain', default='T2VertexBeamSpot_FTF',
                        help='Which chain\'s results to use.  Also the fourth folder '
                        'inside the root file [default: %(default)s]')

    parser.add_argument('--chain-trk', default='T2VertexBeamSpot_FTF',
                        help='Which chain\'s results to use.  Also the fourth folder '
                        'inside the root file [default: %(default)s]')

    parser.add_argument('-v', '--verbosity', nargs='?', default=4, const=5,
                        type=int, metavar='LEVEL',
                        help="Logging level, without this option script will "
                        "log messages at INFO level, if -v is given (without "
                        "LEVEL) logging will happen at DEBUG level. For backward "
                        "compatibility we also allow `-v LEVEL' format, if "
                        "LEVEL<5 then  INFO level is used, otherwise DEBUG.")

    return parser.parse_args()


def isGoodReference(origName: str, runNumber: int, cliOpts: argparse.Namespace) -> bool:
    """Check data quality of the histograms.
    """

    fh = ROOT.TFile(origName, 'READ')  # pylint: disable=no-member

    order = ('server', 'provider', 'detail', 'chain')
    folder = 'run_%d/lb_0/%s/' % \
        (runNumber, '/'.join(getattr(cliOpts, f) for f in order))

    isGood = True
    for dd in 'XYZ':
        histname = 'SplitVertexDNTrksPass_vs_SplitVertexD%sPass_runsummary' % dd
        hh = fh.Get('%s%s' % (folder, histname))
        _LOG.info("Found %s here: %s" % (histname, str(hh)))
        try:
            nn = hh.GetEntries()
        except AttributeError:
            # for, whatever reason, unable to read the histogram
            isGood = False
            _LOG.info("%s is bad" % (histname))

        _LOG.info("%s had %d entries" % (histname, nn))
        if nn < MIN_ENTRIES:
            isGood = False
            _LOG.info("%s is bad" % (histname))

    return isGood


def main() -> int:
    """Main entry point
    """
    opts = getRefMakerCLI()
    bslog.initLogger(opts.verbosity)

    oh_src = (opts.partition, opts.server, opts.provider)
    getter = file_getter.OHCPGetter(oh_src, detail=opts.detail, algorithm_vtx=opts.chain,
                                    algorithm_trk=opts.chain_trk, minLBN=-1)
    finfo = getter.goFetch()
    if finfo is None:
        # #I dont' think the code can get here
        # # I believe OHCPGetter will kill the proc if it can't get the file
        _LOG.critical('Unable to fetch the ROOT file from oh_cp')
        _LOG.critical('OHCPGetter had these parameters: oh_src=%s chain=%s',
                      oh_src, opts.chain)
        return 1

    runNumber = finfo.htree.get_run()
    if runNumber is None:
        _LOG.critical("Run number is unknown")
        return 1

    origName = finfo.path
    assert origName is not None
    if isGoodReference(origName, runNumber, opts):
        _LOG.info("%s is a good reference.  I am squirreling it away.", origName)
        shutil.move(origName, opts.filename)
        _LOG.info("New reference is here: %s", opts.filename)
    else:
        _LOG.warning("%s is a bad reference.  I am deleting it", origName)
        os.remove(origName)

    _LOG.info('Goodbye.')

    return 0


if __name__ == '__main__':
    sys.exit(main())
