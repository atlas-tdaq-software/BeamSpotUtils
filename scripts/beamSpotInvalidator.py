#!/usr/bin/env tdaq_python

'''CLI Tool: Writes most recent onmon rep back to onmon with invalid status'''

from __future__ import annotations

import argparse
import os

from BeamSpotUtils import bslog, model, pipe


std_whereToLook = 'onhlt'
std_whereToWrite = 'ctp:invalidate'

debug_whereToLook = 'test/bsi.input.csv'
debug_whereToWrite = 'test/bsi.output.csv'

statusToSet = model.STATUS_NOT_CONVERGED


def invalidate(snap: model.BSSnapshot) -> model.BSSnapshot | None:
    """Return snapshot corresponding to "invalid" record.
    """
    # if status is already set, return None, this will get
    # filtered out by pipe
    if snap.status == statusToSet:
        return None
    snap.status = statusToSet
    return snap


def getConverterCLI() -> argparse.Namespace:
    """Parse command line.
    """
    header = '''\t%(prog)s [options]

Use this application to invalidate the current beamspot on the HLT.
Specifically it will read the last beamspot it could on from <onhlt>
change its status to 4 and then write it back onto onhlt.  Use the
--debug option to change these to local files to test.
'''

    p = argparse.ArgumentParser(usage=header)

    p.add_argument('-d', '--debug', action='store_true', default=False,
                   help='Instead of reading and writing to onhlt and ctp'
                   ' the tool uses %s and %s.  Note you must be in'
                   ' the top level BeamSpotUtils directory!!'
                   % (debug_whereToLook, debug_whereToWrite))

    p.add_argument('-v', '--verbosity', nargs='?', default=4, const=5,
                   type=int, metavar='LEVEL',
                   help="Logging level, without this option script will "
                   "log messages at INFO level, if -v is given (without "
                   "LEVEL) logging will happen at DEBUG level. For backward "
                   "compatibility we also allow `-v LEVEL' format, if "
                   "LEVEL<5 then  INFO level is used, otherwise DEBUG.")

    return p.parse_args()


def main() -> None:

    opts = getConverterCLI()

    if opts.verbosity > 3:
        print('Environment:')
        for k, v in os.environ.items():
            print("%s=%s" % (k, v))
        print('\n')

    bslog.initLogger(opts.verbosity)

    if opts.debug:
        whereToWrite = debug_whereToWrite
        whereToLook = debug_whereToLook
    else:
        whereToWrite = std_whereToWrite
        whereToLook = std_whereToLook

    print(whereToWrite)
    print(whereToLook)

    p = pipe.BeamSpotPipe(whereToLook, whereToWrite, 1)
    # we need read from last open interval, use very large run number for that
    run = 2000000000
    p.push(invalidate, readArgs=dict(run=run), skipEmpty=True)


if __name__ == '__main__':
    main()
