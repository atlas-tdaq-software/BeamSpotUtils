#!/usr/bin/env tdaq_python

from __future__ import annotations

from ispy import IPCPartition, ISInfoReceiver, ISInfoDynAny
import getopt
import sys
import time
import signal
from typing import Any


def usage() -> None:
    print("Usage:", sys.argv[0], " [-p partition]")


part_name = 'initial'

try:
    opts, args = getopt.getopt(sys.argv[1:], "hp:n:", ["help", "partition=", "name="])
except getopt.GetoptError:
    #    usage()
    sys.exit(2)

for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
        sys.exit(1)
    elif opt in ("-p", "--partition"):
        part_name = arg


p = IPCPartition(part_name)

if not p.isValid():
    print("Partition", part_name, "is not valid")
    sys.exit(1)

recv = ISInfoReceiver(p)

global cb_count
cb_count = 0


def myhandler(cb: Any) -> None:
    global cb_count
    cb_count += 1
    print('Called handler at count: ', cb_count)

    print(cb.name(), cb.time(), cb.type().name(), cb.reason())
    value = ISInfoDynAny()
    cb.value(value)
    print(value)


#recv.subscribe_info( 'RunParams', myhandler, ISCriteria( IS.LumiBlock(p,'').type() ) )
#recv.subscribe_info( 'RunParams', myhandler, ISCriteria( 'RunInfo' ) )
#recv.subscribe_info( 'RunParams', myhandler, ISCriteria( 'LumiBlock' ) )
#recv.subscribe_info( 'RunParams', myhandler, ISCriteria( '.*Info' ) )
recv.subscribe_info('RunParams.LumiBlock', myhandler)

# subscribe to all RCStateInfo changes
# recv.subscribe_info('RunCtrl', myhandler, ISCriteria(IS.RCStateInfo(p,'').type()))


def cleanup(signum: int, frame: Any) -> None:
    print('Caught the term signal (', signum, '), trying to unsubscribe')
#finally:
#    recv.unsubscribe( 'RunParams', ISCriteria( IS.LumiBlock(p,'').type() ), True )
#    recv.unsubscribe( 'RunParams', ISCriteria( 'RunInfo' ), True )
#    recv.unsubscribe( 'RunParams', ISCriteria( 'LumiBlock' ), True )
#    recv.unsubscribe( 'RunParams', ISCriteria( '.*Info' ), True )
    recv.unsubscribe('RunParams.LumiBlock', True)
    print("Done")
    sys.exit(0)

#    recv.unsubscribe('RunCtrl', ISCriteria(IS.RCStateInfo(p,'').type()), True)


signal.signal(signal.SIGTERM, cleanup)
# overwrite default KeyboardInterrupt exception
signal.signal(signal.SIGINT, cleanup)

print("Waiting for updates...")
while True:
    time.sleep(1000)
