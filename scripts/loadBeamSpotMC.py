#!/bin/env python
#
# loadBeamSpotMC.py
#
# 12-May-2010  Rainer Bartoldus <bartoldu@slac.stanford.edu>
#
# Butchered version of loadBeamSpot.py that deals with the Monte Carlo
# conditions (also used by the HLT). Aim to merge the two into one script.
#

"""This scripts loads a set of beam spot parameters into COOL to be used by the HLT.

./loadBeamSpot.py [-h|--help] [-r|--run <run number>]
    -h : this help
    -r : run number or key (e.g. 142193 or BS7T)
"""

from __future__ import annotations

import subprocess
import traceback
import sys
import getopt
from typing import Any

run   = 0
#run   = 152166
#run   = 153159
#run   = 153565

#run   = 'BS900'
#run   = 'BS2T'
#run   = 'BS7T'

#tag = 'IndetBeamposOnl-HLT-UPD1-000-00'
#tag = 'IndetBeamposOnl-HLT-UPD1-001-00'

tag_v: dict[str, Any] = { '0': 0 }
tag_v['BS900'] = 'IndetBeampos-900GeV-Dec2009-disp-002'
tag_v['BS2T' ] = 'IndetBeampos-2TeV-Dec2009-disp-002'
tag_v['BS7T' ] = 'IndetBeampos-7TeV-11m-0urad-disp-003'

#app = './beamSpotOnl_set.py'
app = './beamSpot_set.py'

# Take these from: https://twiki.cern.ch/twiki/bin/view/Atlas/OnlineBeamSpotResults
#

lbn   = 0
#               status  x        y        z      sigmax  sigmay  sigmaz  tiltxz    tiltyz    sigmaxy
parms_v: dict[str, list] = {}
parms_v[    '0'] = [ 1,  0. , 0.,  0., 1000., 1000., 3000., 0., 0., 0 ]
parms_v['BS900'] = [ 3, -0.2 ,    1,       0,     0.190,   0.290,   40,     0.00045,  2e-05,    0 ]
parms_v['BS2T' ] = [ 3, -0.15,    1,     -13,     0.160,   0.160,   27,     0.00046,  3e-05,    0 ]
parms_v['BS7T' ] = [ 3, -0.15,    1,       0,     0.074,   0.074,   60,     0.00046,  3e-05,    0 ]


def usage() -> None:

    print(globals()['__doc__'])


def main() -> None:

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hr:", ["help", "run="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    if len(args) > 1:
        usage()

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit(1)
        elif opt in ("-r", "--run"):
            run = arg

    parms = parms_v[str(run)]
    tag   = tag_v[str(run)]

    dbname = 'OFLP200'
    db  = 'sqlite://;schema=mysqlitefile_' + str(run) + '.db;dbname=' + dbname

    strParms = [str(i) for i in parms]
    coolArgs = [ app, db, tag ] + strParms

    print('coolArgs=',coolArgs)

    subprocess.Popen(coolArgs)


if __name__ == "__main__":

    try:
        main()
    except Exception as e:
        print(str(e))
        traceback.print_exc()
        sys.exit(-1)
