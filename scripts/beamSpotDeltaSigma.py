#!/usr/bin/env tdaq_python

'''CLI Tool: To calculate deltaSigma using onhlt'''

from __future__ import annotations

import sys
import os

from BeamSpotUtils import bslog, model, pos_calc
from BeamSpotUtils.translators import make_translator


def touch(fname: str) -> None:
    fhandle = open(fname, 'a')
    try:
        os.utime(fname, None)
    finally:
        fhandle.close()


def usage() -> None:
    print('\nYou must specify a file I can read a beamspot represenation from!\n')
    sys.exit(1)


def main() -> None:
    if len(sys.argv) < 2:
        usage()

    print('Using %s and %s' % (model.__file__, bslog.__file__))

    bslog.initLogger(3)

    for arg in sys.argv[1:]:
        head, tail = os.path.split(arg)
        outName = "%s/dsigma_%s" % (head, tail)
        touch(outName)

        inRep = make_translator(arg).read()
        assert inRep is not None
        outRep = pos_calc.sigmaToDSigma(inRep)
        assert outRep is not None
        make_translator(outName).write(outRep)


if __name__ == '__main__':
    main()
