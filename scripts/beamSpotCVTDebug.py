#!/usr/bin/env tdaq_python

"""Application which runs BSCalcVertex and dumps its output.
"""

from __future__ import annotations

import sys
import argparse

from BeamSpotUtils.freaking_ROOT import ROOT

from BeamSpotUtils import bslog, bscalc_vtx, htree, lbn_merge


def getConverterCLI() -> argparse.Namespace:
    """Parse command line.
    """
    header = '''%(prog)s -f myPerLBN.root [options] [files ...]

Use this application to avoid all the normal BST heft and just apply
BSCalcVertex to your own perLBN file.  All the needed histograms must
be in one and only one subdirectory in the root file you pass!
'''

    parser = argparse.ArgumentParser(usage=header)

    parser.add_argument('-f', '--file', default=None, metavar='PATH',
                        help='The location of the file containing'
                        ' the histograms for BSCalcVertex. Will be added'
                        ' to positional arguments')

    parser.add_argument('-d', '--dir', default='',
                        help='If the file has any ROOT directory structure'
                        ' this allows you to point the tool to'
                        ' the correct subdirectory.')

    parser.add_argument('--doBCIDs', action='store_true', default=False,
                        help='BSCalcVertex always produces some bcid information'
                        ' but this flag causes a csv file to be written.')

    parser.add_argument('--noBCIDCorrection', action='store_false',
                        dest='doBCIDCorrection', default=True,
                        help='BSCalcVertex will correct the BCID widths using the global'
                        ' width unless this option is specified')

    parser.add_argument('--cvtPlots', action='store_true', default=False,
                        help='CorrVTrks goes verbose and saves its plots.')

    parser.add_argument('-v', '--verbosity', nargs='?', default=4, const=5,
                        type=int, metavar='LEVEL',
                        help="Logging level, without this option script will "
                        "log messages at INFO level, if -v is given (without "
                        "LEVEL) logging will happen at DEBUG level. For backward "
                        "compatibility we also allow `-v LEVEL' format, if "
                        "LEVEL<5 then  INFO level is used, otherwise DEBUG.")

    parser.add_argument('files', nargs='*',
                        help='The location of the file with histograms')

    return parser.parse_args()


def main() -> None:

    opts = getConverterCLI()

    bslog.initLogger(opts.verbosity)

    config = bscalc_vtx.BSCalcVertexConfig(beQuiet=opts.cvtPlots, savePlots=opts.cvtPlots,
                                           doBCIDs=opts.doBCIDs, doBCIDCorrection=opts.doBCIDCorrection)
    lbnmerge = lbn_merge.LBNMerge(1, 1000000, 1000000)
    corr = bscalc_vtx.BSCalcVertex(config, output_dir='./', lbn_merge=lbnmerge)

    names = opts.files[:]
    if opts.file is not None:
        names.append(opts.file)

    if len(names) == 0:
        print('ERR: You must specify a file to use!  See --help')
        sys.exit(1)

    snapshots = {}
    for fname in names:
        tfile = ROOT.TFile(fname, "READ")
        # TODO: arguments need to be fixed
        server, provider, detail, chain_vtx = "", "", "", ""
        tree = htree.make_htree(tfile, server, provider, detail, chain_vtx)
        assert tree is not None
        snap = corr.calc({0: tree})
        assert snap is not None
        snapshots[fname] = snap
        tfile.Close()

    for fname, snap in snapshots.items():
        print("====  %s  ====" % fname)
        for line in snap.prettyString().split('\n'):
            print('    ', line)

        print()


if __name__ == '__main__':
    main()
