#!/usr/bin/env tdaq_python

'''CLI Tool: Grab histograms root files from network locations'''

from __future__ import annotations

import argparse

from BeamSpotUtils import bslog, file_getter, os_utils


def getFileGetterCLI() -> argparse.Namespace:
    """Parse command line
    """
    header = '''\
Use this application to grab the monitoring root file that contains
the beamspot info, wherever it might be.  This tool looks for the
histogram containing root file at the DQM web interface as well as
castor (attempts to use rfcp to retrieve it).  This tool doesn't
work correctly both offline and online do to different access rights.
'''

    parser = argparse.ArgumentParser(description=header)
    parser.add_argument('-v', '--verbosity', nargs='?', default=4, const=5,
                        type=int, metavar='LEVEL',
                        help="Logging level, without this option script will "
                        "log messages at INFO level, if -v is given (without "
                        "LEVEL) logging will happen at DEBUG level. For backward "
                        "compatibility we also allow `-v LEVEL' format, if "
                        "LEVEL<5 then  INFO level is used, otherwise DEBUG.")
    parser.add_argument('run', type=int, nargs='+',
                        help='One or more run numbers')

    opts = parser.parse_args()
    return opts


def main() -> None:
    """Main function
    """
    opts = getFileGetterCLI()
    bslog.initLogger(opts.verbosity)

    tempDir, _ = os_utils.makeTempDir()

    for run in opts.run:
        config = file_getter.SmartFileGetterConfig(
            sources=['coca'], run_number=run, tmpdir=tempDir
        )
        getter = file_getter.SmartFileGetter(config)
        finfo = getter.goFetch()
        assert finfo is not None
        print("\nFile dropped here: ", finfo.path)


if __name__ == '__main__':
    main()
