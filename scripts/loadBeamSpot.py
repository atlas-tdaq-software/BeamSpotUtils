#!/bin/env python
#
# loadBeamSpot.py
#
# 15-Apr-2010  Rainer Bartoldus <bartoldu@slac.stanford.edu>
#

"""This scripts loads a set of beam spot parameters into COOL to be used by the HLT.

./loadBeamSpot.py [-h|--help] [-r|--run <run number>] [-f|--csvfile <file name>]
    -h : this help
    -r : run number or key (e.g. 142193T)
    -f : file name of a csv file with parameters to load
"""

from __future__ import annotations

import subprocess
import traceback
import sys
import getopt
import csv

run   = 0
#run   = 152166
#run   = 153159
#run   = 153565

dbname = 'CONDBR2'

#tag = 'IndetBeamposOnl-HLT-UPD1-000-00'
tag = 'IndetBeamposOnl-HLT-UPD1-001-00'

app = './beamSpotOnl_set.py'

# Take these from: https://twiki.cern.ch/twiki/bin/view/Atlas/OnlineBeamSpotResults
#

lbn   = 0
#           status  x        y        z      sigmax  sigmay  sigmaz  tiltxz    tiltyz    sigmaxy
parms_v: dict[str, list] = {}
parms_v[     '0'] = [ 1,  0. , 0.,  0., 1000., 1000., 3000., 0., 0., 0 ]
parms_v['152166'] = [ 7, -0.370 , 0.628 ,  1.030 , 0.042 , 0.061 , 22.142, 0.000542,  0.000060, 0 ]
parms_v['153159'] = [ 7, -0.3622, 0.6153, -0.77  , 0.0498, 0.0691, 27.69 , 0.000410, -0.000110, 0 ]
parms_v['153565'] = [ 7, -0.4020, 0.6218, -1.34  , 0.0309, 0.0339, 34.01 , 0.000471, -0.000071, 0 ]
parms_v['155074'] = [ 7, -0.3466, 0.6127, -2.247 , 0.0335, 0.0289, 36.79 , 0.000461, -0.000088, 0 ]
parms_v['158045'] = [ 7,  0.03939, 1.10662, -3.97572, 0.04677, 0.05722, 56.77, 0.0004190, -0.0000146, 0 ]
parms_v['158116'] = [ 7,  0.0755558, 1.11116, -2.93133, 0.0560864, 0.0522284, 64.3886, 0.000403943, -3.46603e-06, 0 ]
parms_v['158269'] = [ 7,  0.048773,  1.16643, -3.01947, 0.053231,  0.0515162, 65.0101, 0.000404534, -8.24674e-05, 0 ]
parms_v['158299'] = [ 7,  0.127642,  1.06055, -2.91755, 0.0758088, 0.0529607, 66.0747, 0.000426493, -4.97838e-05, 0]


def usage() -> None:

    print(globals()['__doc__'])


def main() -> None:

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hr:f:", ["help", "run=", "csvfile="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    if len(args) > 1:
        usage()

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit(1)
        elif opt in ("-r", "--run"):
            run = arg
        elif opt in ("-f", "--csvfile"):
            csvfile = arg

    reader = csv.reader(open(csvfile, "r"))

    for row in reader:
        print(row)

    sys.exit(3)

    strRun = str(run)

    if strRun not in parms_v:
        print('unknown run number: ', strRun)
        sys.exit(2)

    parms = parms_v.get(strRun)

    runArg = '--rs=' + strRun
    lbArg  = '--ls=' + str(lbn)

    db  = 'sqlite://;schema=mysqlitefile_' + strRun + '.db;dbname=' + dbname

    strParms = [ str(i) for i in parms ]
    coolArgs = [ app, runArg, lbArg, db, tag ] + strParms

    print('coolArgs=',coolArgs)

    subprocess.Popen(coolArgs)


if __name__ == "__main__":

    try:
        main()
    except Exception as e:
        print(str(e))
        traceback.print_exc()
        sys.exit(-1)
